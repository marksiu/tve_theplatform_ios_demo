//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.

#import "VSRemoteImageView.h"
#import <CommonCrypto/CommonDigest.h>

#import "NetworkActivityIndicatorManager.h"

static UIActivityIndicatorViewStyle style = -1;

@interface VSRemoteImageView () {
    int version;
}
@property (strong) UIImageView *placeholderImageView;
@property (strong) UIActivityIndicatorView *spinner;
@property (strong) UIImageView *realImageView;
+ (NSString *) cacheDirectory;
+ (NSString *) md5:(NSString *)input;
@end

@implementation VSRemoteImageView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self buildUI];
    }
    return self;
}

- (id)init {
    self = [super init];
    if (self) {
        [self buildUI];
    }
    return self;
}

- (void) awakeFromNib {
    [self buildUI];
}

- (void) buildUI {
    
    if (style < 0) {
        style = UIActivityIndicatorViewStyleGray;
    }
    
    self.placeholderImageView = [[[UIImageView alloc] initWithFrame:self.bounds] autorelease];
    self.placeholderImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.placeholderImageView.backgroundColor = [UIColor clearColor];
    
    [self addSubview:self.placeholderImageView];
    
    self.spinner = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:style] autorelease];
    self.spinner.hidesWhenStopped = YES;
    self.spinner.center = self.center;
    
    [self addSubview:self.spinner];
    
    self.realImageView = [[[UIImageView alloc] initWithFrame:self.bounds] autorelease];
    self.realImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.realImageView.backgroundColor = [UIColor clearColor];
    self.realImageView.alpha = 0;
    
    [self addSubview:self.realImageView];
    
    //});
}

- (void) setContentMode:(UIViewContentMode)contentMode {
    [super setContentMode:contentMode];
    
    self.placeholderImageView.contentMode = contentMode;
    self.realImageView.contentMode = contentMode;
}

- (void) setClipsToBounds:(BOOL)clipsToBounds {
    [super setClipsToBounds:clipsToBounds];
    
    self.placeholderImageView.clipsToBounds = clipsToBounds;
    self.realImageView.clipsToBounds = clipsToBounds;
}

- (void) resetImage {
    
    ++version;
    
    if (![NSThread isMainThread]) {
        return;
    }
    
    self.realImageView.image = nil;
}
- (void) resetPlaceholder {
    
    ++version;
    
    if (![NSThread isMainThread]) {
        return;
    }
    
    self.placeholderImageView.image = nil;
}

- (UIImage *)image {
    return self.realImageView.image;
}

- (void) setImageUrl:(NSString *)url {
    [self setImageUrl:url withPlaceholder:nil animated:NO];
}

- (void) setImageUrl:(NSString *)url completion:(void(^)(UIImage*))compBlock {
    [self setImageUrl:url withPlaceholder:nil withSpinner:NO animated:NO completion:compBlock];
}

- (void) setImageUrl:(NSString *)url withPlaceholder:(UIImage *)placeholder animated:(BOOL)animated {
    [self setImageUrl:url withPlaceholder:placeholder withSpinner:YES animated:YES completion:nil];
}

- (void) setLocalImage:(NSString *)url withChannel:(BOOL)isChan {
    if (![NSThread isMainThread]) {
        return;
    }
    
    self.realImageView.image = nil;
    self.realImageView.alpha = 0;
    
    if (url && [url length] > 0) {
        
        NSRange lastIndex = [url rangeOfString:@"/" options:NSBackwardsSearch];
        NSString *imgName = [url substringFromIndex:lastIndex.location];
        if ([UIImage imageNamed: imgName])
            self.realImageView.image = [UIImage imageNamed: imgName];
        self.realImageView.alpha = 1;
    }
}

- (void) setImageUrl:(NSString *)url withPlaceholder:(UIImage *)placeholder withSpinner:(BOOL)spinner animated:(BOOL)animated {
    [self setImageUrl:url withPlaceholder:placeholder withSpinner:spinner animated:NO completion:nil];
}

- (void) setImageUrl:(NSString *)url withPlaceholder:(UIImage *)placeholder withSpinner:(BOOL)spinner animated:(BOOL)animated completion:(void(^)(UIImage*))compBlock {
    
    int _version = ++version;
    
    if (![NSThread isMainThread]) {
        return;
    }
    
    self.placeholderImageView.image = placeholder;
    self.realImageView.image = nil;
    self.realImageView.alpha = 0;
    
    url = [url stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (!url || [url length] <= 0) {
        [self.spinner stopAnimating];
        return;
    }
    
    NSString *md5 = [VSRemoteImageView md5:url];
    NSString *cacheFilePath = [[VSRemoteImageView cacheDirectory] stringByAppendingPathComponent:md5];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        BOOL hadToDownload = YES;
        
        UIImage *_image = [UIImage imageWithContentsOfFile:cacheFilePath];
        if (_image) {
            hadToDownload = NO;
        } else {
            dispatch_sync(dispatch_get_main_queue(), ^{
                if (_version == version && spinner) {
                    [self.spinner startAnimating];
                }
            });
            
            // image not in cache... load it from url
            NSURL *URL = [NSURL URLWithString:url];
            NSError *error = nil;

            [URL setResourceValue:[NSNumber numberWithBool:YES] forKey:NSURLIsExcludedFromBackupKey error:&error];
            
            [NetworkActivityIndicatorManager increaseCounter];
            
            //NSData *remoteData = [NSData dataWithContentsOfURL:URL];

            NSMutableURLRequest *urlr = [NSMutableURLRequest requestWithURL:URL
                                                                cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                            timeoutInterval:3];
            NSData *remoteData = [NSURLConnection sendSynchronousRequest:urlr
                                                       returningResponse:nil
                                                                   error:nil];


            [NetworkActivityIndicatorManager decreaseCounter];
            
            if (remoteData) {
                _image = [UIImage imageWithData:remoteData];
                if (_image) {
                    //write image to disk cache...
                    [remoteData writeToFile:cacheFilePath atomically:YES];
                }
            }
        }
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (_version == version) {
                if (_image) {
                    self.realImageView.image = _image;
                    if (compBlock) {
                        compBlock(_image);
                    }
                    if (hadToDownload && animated) {
                        [UIView animateWithDuration:.3
                                         animations:^{
                                             self.realImageView.alpha = 1;
                                         }
                                         completion:^(BOOL finished) {
                                             [self.spinner stopAnimating];
                                         }];
                    } else {
                        self.realImageView.alpha = 1;
                        [self.spinner stopAnimating];
                    }
                    
                }
            }
        });
        
    });
}

+ (void) setActivityIndicatorStyle:(UIActivityIndicatorViewStyle)_style {
    style = _style;
}

+ (NSString *) cacheDirectory {
    
    static NSString *_cacheDirectory;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _cacheDirectory = [[[NSSearchPathForDirectoriesInDomains (NSCachesDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:@"VSRemoteImageView"] retain];
        
        BOOL directory;
        BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:_cacheDirectory isDirectory:&directory];
        
        if (exists && directory) {
            // we are cool
        } else {
            [[NSFileManager defaultManager] createDirectoryAtPath: _cacheDirectory
                                      withIntermediateDirectories: YES
                                                       attributes: nil
                                                            error: nil];
        }
    });
    
    return _cacheDirectory;
}

+ (NSString *) md5:(NSString *)input {
	const char *cString = [input UTF8String];
	unsigned char result[16];
	CC_MD5(cString, strlen(cString), result);
    
	return [NSString stringWithFormat:
			@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			result[0], result[1], result[2], result[3],
			result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11],
			result[12], result[13], result[14], result[15]
			];
}

@end
