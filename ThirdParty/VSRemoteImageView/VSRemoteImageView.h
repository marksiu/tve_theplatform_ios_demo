//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.

#import <UIKit/UIKit.h>

@interface VSRemoteImageView : UIView

- (void) setImageUrl:(NSString *)url;
- (void) setImageUrl:(NSString *)url completion:(void(^)(UIImage*)) compBlock;

- (void) setImageUrl:(NSString *)url withPlaceholder:(UIImage *)placeholder animated:(BOOL)animated;
- (void) setLocalImage:(NSString *)url withChannel:(BOOL)isChan;
- (void) setImageUrl:(NSString *)url withPlaceholder:(UIImage *)placeholder withSpinner:(BOOL)spinner animated:(BOOL)animated;
- (void) setImageUrl:(NSString *)url withPlaceholder:(UIImage *)placeholder withSpinner:(BOOL)spinner animated:(BOOL)animated completion:(void(^)(UIImage*))compBlock ;

- (void) resetImage;
- (void) resetPlaceholder;

- (UIImage *)image;

+ (void) setActivityIndicatorStyle:(UIActivityIndicatorViewStyle)style;

@end
