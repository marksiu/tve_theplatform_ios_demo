//
//  ConnectSDK.h
//  ConnectSDK
//
//  Created by Alexej Kubarev on 9/17/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ConnectChannel.h"
#import "ConnectChannelDelegate.h"

/**
 Connect SDK.
 */
@interface ConnectSDK : NSObject

//------------------------------------------------------------------

/**
 Get current SDK version
 @return SDK Version string
 */
+ (NSString*) getVersion;

/**
 Get device UUID.
 @return UUID String of the device.
 */
+ (NSString*) getUUID;

//------------------------------------------------------------------


/**
 Initializes Accedo Connect SDK.
 @param pubKey Publish Key
 @param pubKey Subscribe Key
 @return a newly initialized object
*/
- (id) initWithPubKey:(NSString*) pubKey subKey:(NSString*) subKey;

/**
 Join a given channel.
 @param channelName Name of the channel
 @param delegate Delegate object implementing ConnectChannelDelegate protocol
 @return ConnectChannel
 */
- (ConnectChannel*) joinChannel: (NSString*) channelName delegate:(id<ConnectChannelDelegate>) delegate;
/**
 Connect to a channel for pairing.
 Returned ConnectChannel can be queried for the connection code to be used by other devices for pairing.
 @param delegate Delegate object implementing ConnectChannelDelegate protocol
 @return ConnectChannel
 */
- (ConnectChannel*) pairWithDelegate:(id<ConnectChannelDelegate>) delegate;
/**
 Connect to a channel with a pairing code
 @param pairingCode Pairing code to use for connection
 @param delegate Delegate object implementing ConnectChannelDelegate protocol
 @return ConnectChannel
 */
- (ConnectChannel*) pairWithCode: (NSString*) pairingCode delegate:(id<ConnectChannelDelegate>) delegate;



@end