//
//  ConnectChannelDelegate.h
//  ConnectSDK
//
//  Created by Alexej Kubarev on 9/18/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ConnectChannelDelegate <NSObject>

/**
 Handle message object received through channel
 @param message Object of any JSON datatype.
 */
- (void) didRecieveMessage:(id) message;

/**
 Channel was unable to publish message due to error
 @param error NSError object containing details
 */
- (void) didFailSendingMessageWithError: (NSError*) error;

//NOT IMPLEMENTED
- (void) didRecieveNotification;

/**
 Channel connection established
 */
- (void) didConnect;

/**
 Channel connection restored
 */
- (void) didReconnect;

/**
 Channel disconnected
 */
- (void) didDisconnect;

/**
 Channel connection failed with error
 @param error NSError object containing details
 */
- (void) didFailConnectingWithError: (NSError*) error;

/**
 Channel presence changed
 @param presence NSDictionary containing changes
 */
- (void) presenceChanged: (NSDictionary*) presence;
@end
