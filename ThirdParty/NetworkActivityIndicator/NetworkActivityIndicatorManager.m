//
//  NetworkActivityIndicatorManager.m
//  MapView
//
//  Created by Paál Rita on 9/29/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "NetworkActivityIndicatorManager.h"

@interface NetworkActivityIndicatorManager () {
    int counter;
}

- (void) increaseCounter;
- (void) decreaseCounter;

@end

@implementation NetworkActivityIndicatorManager

-(id)init {
	if (self = [super init]) {
		counter = 0;
	}
	return self;
}

- (void) increaseCounter {
	@synchronized(self) {
		if (1 == ++counter) {
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		}
	}
}

- (void) decreaseCounter {
	@synchronized(self) {
		if (0 == counter) {
			return;
		}
		if (0 == --counter) {
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		}
	}
}

+ (void) increaseCounter {
    [[NetworkActivityIndicatorManager sharedManager] increaseCounter];
}
+ (void) decreaseCounter {
    [[NetworkActivityIndicatorManager sharedManager] decreaseCounter];
}

#pragma mark - Singleton Methods

+ (NetworkActivityIndicatorManager *)sharedManager {
    
    static NetworkActivityIndicatorManager *_sharedManager;
    
    if (!_sharedManager) {
        static dispatch_once_t oncePredicate;
        dispatch_once(&oncePredicate, ^{
            _sharedManager = [[super allocWithZone:nil] init];
        });
    }
    
    return _sharedManager;
}

+ (id)allocWithZone:(NSZone *)zone {
    return [self sharedManager];
}


- (id)copyWithZone:(NSZone *)zone {
    return self;
}

@end
