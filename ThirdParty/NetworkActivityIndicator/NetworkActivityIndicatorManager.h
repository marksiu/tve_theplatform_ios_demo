//
//  NetworkActivityIndicatorManager.h
//  MapView
//
//  Created by Paál Rita on 9/29/09.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkActivityIndicatorManager : NSObject
+ (void) increaseCounter;
+ (void) decreaseCounter;
@end

