//
//  VSEPGGridViewController.m
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//

#import "VSEPGGridViewController.h"

@implementation VSEPGGridViewController

@synthesize gridView=_gridView;

- (void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - View lifecycle

- (void) dealloc {
    [_gridView reset];
    [_gridView release];
    [super dealloc];
}

- (BOOL)gridView:(VSEPGGridView *)gridView shouldHighlightTitleCell:(VSEPGGridViewTitleCell *)cell forRow:(NSUInteger)row{
    return NO;
}
- (BOOL)gridView:(VSEPGGridView *)gridView shouldHighlightContentCell:(VSEPGGridViewContentCell *)cell byScheduleInstance:(VSEPGScheduleInstance *)scheduleInstance{
    return NO;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _gridView = [[VSEPGGridView alloc] initWithFrame:self.view.bounds];
    self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.gridView.gridViewDataSource = self;
    self.gridView.gridViewDelegate = self;
    self.gridView.showsHorizontalScrollIndicator = NO;
    self.gridView.showsVerticalScrollIndicator = NO;
    
    [self.view addSubview:self.gridView];
}

- (void)reload {
    [self.gridView reload];
}

#pragma mark - VSEPGGridViewDataSource

- (NSUInteger) numberOfRowsInGridView:(VSEPGGridView *)gridView {
    return 0;
}
- (CGFloat) heightOfRowsInGridView:(VSEPGGridView *)gridView {
    return 0;
}
- (CGFloat) widthOfTitleInGridView:(VSEPGGridView *)gridView {
    return 0;
}
- (CGFloat) heightOfRulerInGridView:(VSEPGGridView *)gridView {
    return 0;
}
- (void) prepareForTiles:(NSArray *)tileKeys forGridView:(VSEPGGridView *)gridView {
    // nop
}
- (BOOL) gridView:(VSEPGGridView *)gridView shouldWaitForTileKey:(NSNumber *)titleKey {
    return YES;
}
- (NSArray *) gridView:(VSEPGGridView *)gridView dataForChunkHoursSince1970:(NSTimeInterval)hoursSince1970 row:(NSUInteger)row {
    return nil;
}
- (VSEPGGridViewTitleCell *) titleCellForGridView:(VSEPGGridView *)gridView {
    return nil;
}
- (void) gridView:(VSEPGGridView *)gridView configureTitleCell:(VSEPGGridViewTitleCell *)cell forRow:(NSUInteger)row {
    // nop
}
- (VSEPGGridViewContentCell *) contentCellForGridView:(VSEPGGridView *)gridView {
    return nil;
}

#pragma mark - VSEPGGridViewDelegate

- (void) gridView:(VSEPGGridView *)gridView titleCellSelected:(VSEPGGridViewTitleCell *)cell row:(NSUInteger)row previouslySelectedCell:(VSEPGGridViewTitleCell *)previouslySelectedCell {
    // nop
}
- (void) gridView:(VSEPGGridView *)gridView contentCellSelected:(VSEPGGridViewContentCell *)cell {
    // nop
}

- (void) gridView:(VSEPGGridView *)gridView dayChanged:(NSDate *)newDate {
    // nop
}

@end
