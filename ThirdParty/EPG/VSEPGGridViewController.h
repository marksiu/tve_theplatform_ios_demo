//
//  VSEPGGridViewController.h
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSEPGGridView.h"

@interface VSEPGGridViewController : UIViewController <VSEPGGridViewDataSource, VSEPGGridViewDelegate>

@property (assign, readonly) VSEPGGridView *gridView;

- (void)reload;

@end

