//
//  DaySelectController.m
//  CMore
//
//  Created by Gabor Bottyan on 8/20/12.
//  Copyright (c) 2012 Meganet-Soft Bt. All rights reserved.
//

#import "DaySelectController.h"
#import "AppDelegate.h"
#import "NSDate+Extras.h"
#import "LocalizationSystem.h"

@implementation DaySelectController

@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
	return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    // Return the number of rows in the section.
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"DayCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell){
        cell = [[[UITableViewCell alloc] init] autorelease];
    }
    int selected = -1;
    if (delegate){
        selected = [delegate getSelectedDay];
    }
    if (selected==indexPath.row){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    NSString * lockey = [self labelForIndex:indexPath.row];
    NSString * text = [[LocalizationSystem sharedLocalSystem] locStringForKey:lockey];
    cell.textLabel.text  = text;
    return cell;
}

-(NSString *) labelForIndex: (int) index {
    AppDelegate * del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSTimeInterval localTimeIntervalSinceLocalDay = [[NSDate localDate]timeIntervalSince1970] - del.boxTimeDelta;
    NSDate * d = [NSDate dateWithTimeIntervalSince1970:localTimeIntervalSinceLocalDay];

    NSCalendar *gregorian = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:d];
    int weekday = [comps weekday];
    
    NSString * lockey = Nil;
    
    if (index == 0){
        lockey = @"lblToday";
    } else if (index == 1) {
        lockey = @"lblTomorrow";
    } else {
        int w = ((weekday-1 + index)%7);
        lockey = [NSString stringWithFormat:@"lblWeekday%d", w];
    }
    
    return lockey;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (delegate){
        NSString * lockey = [self labelForIndex:indexPath.row];
        NSString * text = [[LocalizationSystem sharedLocalSystem] locStringForKey:lockey];
        [delegate daySelected: indexPath.row withLabel:text];
    }
}

@end
