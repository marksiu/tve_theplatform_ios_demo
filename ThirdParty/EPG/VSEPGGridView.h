//
//  VSEPGGridView.h
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VSEPGGridViewContentCell.h"
#import "VSEPGGridViewTitleCell.h"
#import "EPGConstants.h"
#import "VSEPGScheduleInstance.h"

@protocol VSEPGGridViewDelegate;
@protocol VSEPGGridViewDataSource;

/*
 * The grid data is organized into tiles. A tile is a rectangular area which' size is controlled by kVSEPGChunkInterval and
 * kVSEPGTileRows. Chunk interval is a specific number of hours determining the "width" of a tile and tile rows determine the
 * number of chunks in a tile or in other words the height of the tile.
 *
 * A chunk is a time slice for one grid row. The chunk is identified by it's start time since EPOCH in hours. While the
 * application is running, the chunk size must be constant ensuring a chunk can be cahced and reused.
 *
 * If the set of rows can be modified runtime, the tiles should not be reused after reconfiguring the rows.
 *
 * While the tiles are not designed to be reusable when the set of rows are mutable, a chunk is typically well cacheable.
 * It is the caller's responsibility to elaborate a good caching solution, for example based on the row's 3rd party id (channel id)
 * and the chunk start time.
 *
 * The term tile key refers to an NSNumber object that can be decomposed into a chunk start time and the chunk's topmost row number.
 * Since it contains row number and not a 3rd party id, it is not usable after reconfiguring the rows.
 *
 */
@interface VSEPGGridView : UIScrollView

@property (nonatomic, assign) id<VSEPGGridViewDelegate> gridViewDelegate;
@property (nonatomic, assign) id<VSEPGGridViewDataSource> gridViewDataSource;

@property (nonatomic, retain) UIColor *rulerBackgroundColor;
@property (nonatomic, retain) UIColor *titleBackgroundColor;
@property (nonatomic, retain) UIColor *contentBackgroundColor;
@property (nonatomic, retain) UIColor *hairlineBackgroundColor;

// Set to YES/TRUE if you want to force redrawing the content.
// Doesn't do anything in itself, you must call setNeedsLayout manually after setting this.
@property (assign) BOOL forceNewLayout;

// Set to YES/TRUE while animating.
@property (assign) BOOL animating;

// Releases all allocated memory and calls setNeedsLayout. Will clean the screen and ask
// datasource for data again. It is the caller's responsibility to elaborate a data caching solution.
- (void)reload;

// Must be called before release.
// It resets the view: initialises everything to 0x0 and releases the dispatch queue and invalidates the hairline timer.
- (void)reset;

- (BOOL)tileVisible:(NSNumber *)tileKey;

- (void)scrollCellToVisible:(VSEPGGridViewContentCell *)cell animated:(BOOL)animated;

// The tile key is the most important part of the concept of this grid implementation.
// Read more about it in the class documentation.
+ (void) resolveTileKey:(NSNumber *)tileKey intoChunkHoursSince1970:(int *)chunkHoursSince1970 andRow:(int *)row;

- (void) titleButtonSelected:(id)button;

@end

@protocol VSEPGGridViewDelegate <NSObject>
@required
- (void) gridView:(VSEPGGridView *)gridView titleCellSelected:(VSEPGGridViewTitleCell *)cell row:(NSUInteger)row previouslySelectedCell:(VSEPGGridViewTitleCell *)previouslySelectedCell;
- (void) gridView:(VSEPGGridView *)gridView contentCellSelected:(VSEPGGridViewContentCell *)cell;
- (void) gridView:(VSEPGGridView *)gridView dayChanged:(NSDate *)newDate;
- (BOOL) shouldShowHairlineInGridView:(VSEPGGridView *)gridView;
- (BOOL) shouldConsiderHairlineInGridView:(VSEPGGridView *)gridView;
@end

@protocol VSEPGGridViewDataSource <NSObject>

@required

- (NSUInteger) numberOfRowsInGridView:(VSEPGGridView *)gridView; // Called on first render and on reload
- (CGFloat) heightOfRowsInGridView:(VSEPGGridView *)gridView; // Called on first render and on reload
- (CGFloat) widthOfTitleInGridView:(VSEPGGridView *)gridView; // Called on first render and on reload
- (CGFloat) heightOfRulerInGridView:(VSEPGGridView *)gridView; // Called on first render and on reload

- (void) prepareForTiles:(NSArray *)tileKeys forGridView:(VSEPGGridView *)gridView;

- (BOOL) gridView:(VSEPGGridView *)gridView shouldWaitForTileKey:(NSNumber *)tileKey; // Called each time a not-yet-seen tile enters into the viewport. Return YES, if the data is not yet available for the given tile.
- (NSArray *) gridView:(VSEPGGridView *)gridView dataForChunkHoursSince1970:(NSTimeInterval)hoursSince1970 row:(NSUInteger)row; // Called in a loop for each chunks of tile if the tile enters into the viewport and gridView:shouldWaitForTileKey: returns TRUE.

- (VSEPGGridViewTitleCell *) titleCellForGridView:(VSEPGGridView *)gridView; // Called on each render in loop for rows in the viewport.
- (void) gridView:(VSEPGGridView *)gridView configureTitleCell:(VSEPGGridViewTitleCell *)cell forRow:(NSUInteger)row; // Called for title cells entering into the viewport.

- (VSEPGGridViewContentCell *) contentCellForGridView:(VSEPGGridView *)gridView;

- (BOOL)gridView:(VSEPGGridView *)gridView shouldHighlightContentCell:(VSEPGGridViewContentCell *)cell byScheduleInstance:(VSEPGScheduleInstance *)scheduleInstance;

- (BOOL)gridView:(VSEPGGridView *)gridView shouldHighlightTitleCell:(VSEPGGridViewTitleCell *)cell forRow:(NSUInteger)row;

@end
