//
//  EPGConstants.h
//  CMore
//
//  Created by Gabor Bottyan on 7/30/12.
//  Copyright (c) 2012 Meganet-Soft Bt. All rights reserved.
//

#ifndef CMore_EPGConstants_h
#define CMore_EPGConstants_h
#define kVSEPGTileRowsD 24.0 // 12.0
#define kVSEPGPixelPerMinuteD 8.0 // 8.0
#define kVSEPGChunkIntervalD 24.0 // 2.0
#define kVSRulerLabelWidth 100.0
#define kVSRulerLabelHalfWidth 50.0
#define kVSEPGChunkInterval 24 // 2
#define kVSEPGTileRows 24 // 12
#define kVSEPGPixelPerMinute 8 // 8

#define NUMBOOL(x) [NSNumber numberWithBool:x]

#define NUMLONG(x) [NSNumber numberWithLong:x]

#define NUMLONGLONG(x) [NSNumber numberWithLongLong:x]

#define NUMINT(x) [NSNumber numberWithInt:x]

#define NUMUINT(x) [NSNumber numberWithUnsignedInteger:x]

#define NUMDOUBLE(x) [NSNumber numberWithDouble:x]

#define NUMFLOAT(x) [NSNumber numberWithFloat:x]

#define STRINT(x) [NSString stringWithFormat:@"%d", x]

#define APPLog(fmt, ...) NSLog((@"=== APP === " fmt), ##__VA_ARGS__);
#define NDSLog(fmt, ...) NSLog((@"=== NDS === " fmt), ##__VA_ARGS__);
#define EPGLog(fmt, ...) NSLog((@"=== EPG === " fmt), ##__VA_ARGS__);
#define WRNLog(fmt, ...) NSLog((@"=== WRN === " fmt), ##__VA_ARGS__);
#define ERRLog(fmt, ...) NSLog((@"=== ERR === " fmt), ##__VA_ARGS__);


#define kVSSettingsVisualizeEPGTiles 0
#define kVSNotificationShouldSeekEPG @"n.sse"
#define kVSKeySeekTo @"k.st"
#define kVSNotificationStopping @"n.s"
#define kVSAllTilesProcessed @"n.atp"
#define kVSTilesRendered @"n.tr"
#define kVSEPGGridViewTileKeyChunkMultiplier 10000
#define kVSEPGProductionBackwardScrollingLimitMinutes 0; // minutes
#define kVSEPGProductionForwardScrollingLimitMinutes 0;
#define kVSEPGProductionBackwardScrollingLimitMinutesD 0.0;
#define kVSEPGProductionForwardScrollingLimitMinutesD 0.0;

static int kVSEPGBackwardScrollingLimitMinutes = kVSEPGProductionBackwardScrollingLimitMinutes;
static int kVSEPGForwardScrollingLimitMinutes = kVSEPGProductionForwardScrollingLimitMinutes;

static double kVSEPGBackwardScrollingLimitMinutesD = kVSEPGProductionBackwardScrollingLimitMinutesD;
static double kVSEPGForwardScrollingLimitMinutesD = kVSEPGProductionForwardScrollingLimitMinutesD;



#endif
