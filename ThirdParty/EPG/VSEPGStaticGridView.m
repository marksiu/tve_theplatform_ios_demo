//
//  VSEPGStaticGridView.m
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "VSEPGStaticGridView.h"
#import "VSShadowView.h"
#import "VSParametricLayoutView.h"

typedef struct {
    NSUInteger nrOfCells;
    CGFloat *cellOffsets;
    CGFloat *cellWidths;
} VSEPGGridCellInfo;

#define kVSRulerLabelWidth 100

#define __rowDelta 100000

@interface VSEPGStaticGridView () {
    VSEPGGridCellInfo *cellInfo;
    CGPoint lastContentOffset;
    
    UIColor *_rulerBackgroundColor;
    UIColor *_titleBackgroundColor;
    UIColor *_contentBackgroundColor;
    UIColor *_hairlineBackgroundColor;
    
    // TODO: remove
    //NSUInteger fps;
    // TODO: remove
    //NSDate *fpsDate;
}

@property (assign) CGFloat rowHeight;
@property (assign) CGFloat numberOfRows;
@property (assign) CGFloat titleWidth;
@property (assign) CGFloat rulerHeight;

// To keep the onscreen cells in a structure
@property (nonatomic, retain) NSMutableDictionary *onScreenTitleCellsCache;
@property (nonatomic, retain) NSMutableDictionary *onScreenContentCellsCache;

// To eliminate the need of cell creation
@property (nonatomic, retain) NSMutableSet *offScreenTitleCellsCache;
@property (nonatomic, retain) NSMutableSet *offScreenContentCellsCache;

// TODO: remove
//@property (nonatomic, retain) NSTimer *timer;
// TODO: remove
//operty (retain) NSDate *fpsDate;

@property (nonatomic, retain) UIView *rulerTitleSeparator;
@property (nonatomic, retain) VSShadowView *titleContentSeparator;
@property (nonatomic, retain) VSParametricLayoutView *contentBackgroundView;
@property (nonatomic, retain) UIView *hairlineView;

@property (nonatomic, retain) NSMutableArray *timeLabels;

@property (assign) VSSDPMinutes leftMostMinuteSinceReference;

@property (retain) UIView *touchedView;

@property (retain) NSTimer *hairlineTimer;

- (void) freeCellInfo;
- (void) doLayoutSubViews;
- (void) removeSeparatorViews;

+ (NSNumber *) positionFromRow:(NSUInteger)rowPos cell:(NSUInteger)cellPos;

@end

@implementation VSEPGStaticGridView

@synthesize gridViewDelegate;
@synthesize gridViewDataSource;

@dynamic rulerBackgroundColor;
@dynamic titleBackgroundColor;
@dynamic contentBackgroundColor;
@dynamic hairlineBackgroundColor;

@synthesize rowHeight;
@synthesize numberOfRows;
@synthesize titleWidth;
@synthesize rulerHeight;
@synthesize onScreenTitleCellsCache;
@synthesize onScreenContentCellsCache;
@synthesize offScreenTitleCellsCache;
@synthesize offScreenContentCellsCache;

//@synthesize timer;
//@synthesize fpsDate;

@synthesize rulerTitleSeparator;
@synthesize titleContentSeparator;
@synthesize contentBackgroundView;
@synthesize hairlineView;

@synthesize timeLabels;

@synthesize leftMostMinuteSinceReference;

@synthesize touchedView;

@synthesize hairlineTimer;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.multipleTouchEnabled = NO;
        self.userInteractionEnabled = YES;
        self.canCancelContentTouches = YES;
        
        self.rulerBackgroundColor = [UIColor redColor];
        self.titleBackgroundColor = [UIColor greenColor];
        self.contentBackgroundColor = [UIColor blueColor];
        self.hairlineBackgroundColor = [UIColor yellowColor];
        
    }
    return self;
}

- (void) dealloc {
    
    self.rulerBackgroundColor = nil;
    self.titleBackgroundColor = nil;
    self.contentBackgroundColor = nil;
    self.hairlineBackgroundColor = nil;
    
    self.onScreenTitleCellsCache = nil;
    self.onScreenContentCellsCache = nil;
    self.offScreenTitleCellsCache = nil;
    self.offScreenContentCellsCache = nil;
    self.rulerTitleSeparator = nil;
    self.titleContentSeparator = nil;
    self.contentBackgroundView = nil;
    self.hairlineView = nil;
    
    self.timeLabels = nil;
    self.touchedView = nil;
    
    [self.hairlineTimer invalidate];
    self.hairlineTimer = nil;
    
    [self freeCellInfo];
    [super dealloc];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (!cellInfo) {
        
        if (!self.onScreenContentCellsCache) {
            self.onScreenContentCellsCache = [NSMutableDictionary dictionary];
        }
        if (!self.onScreenTitleCellsCache) {
            self.onScreenTitleCellsCache = [NSMutableDictionary dictionary];
        }
        if (!self.offScreenContentCellsCache) {
            self.offScreenContentCellsCache = [NSMutableSet set];
        }
        if (!self.offScreenTitleCellsCache) {
            self.offScreenTitleCellsCache = [NSMutableSet set];
        }
        
        // TODO: remove the next block
        /*
         if (!self.timer) {
         self.timer = [NSTimer scheduledTimerWithTimeInterval:1
         target:self
         selector:@selector(printFPS)
         userInfo:nil
         repeats:YES];
         }
         */
        
        self.numberOfRows = [self.gridViewDataSource numberOfRowsInGridView:self];
        self.rowHeight = [self.gridViewDataSource heightOfRowsInGridView:self];
        self.titleWidth = [self.gridViewDataSource widthOfTitleInGridView:self];
        self.rulerHeight = [self.gridViewDataSource heightOfRulerInGridView:self];
        
        if (!self.contentBackgroundView) {
            self.contentBackgroundView = [[[VSParametricLayoutView alloc] initWithFrame:CGRectMake(0, self.rulerHeight, self.bounds.size.width, self.bounds.size.height - self.rulerHeight)
                                                                                  leftExtension:0
                                                                                   topExtension:0
                                                                                 widthExtension:0
                                                                                heightExtension:self.rowHeight] autorelease];
            self.contentBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            self.contentBackgroundView.contentView.backgroundColor = self.contentBackgroundColor;
            [self addSubview:self.contentBackgroundView];
        }
        
        if (!self.titleContentSeparator) {
            self.titleContentSeparator = [[[VSShadowView alloc] initWithFrame:CGRectZero
                                                                  shadowColor:[UIColor blackColor]
                                                                 shadowOffset:CGSizeMake(0, 1)
                                                                 shadowRadius:5
                                                                shadowOpacity:.8] autorelease];
            self.titleContentSeparator.backgroundColor = self.titleBackgroundColor;
            [self addSubview:self.titleContentSeparator];
        }
        
        if (!self.rulerTitleSeparator) {
            self.rulerTitleSeparator = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.rulerHeight)] autorelease];
            self.rulerTitleSeparator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
            self.rulerTitleSeparator.backgroundColor = self.rulerBackgroundColor;
            [self addSubview:self.rulerTitleSeparator];
        }
        
        cellInfo = malloc(sizeof(VSEPGGridCellInfo) * self.numberOfRows);
        
        self.leftMostMinuteSinceReference = [self.gridViewDataSource leftMostMinuteInGridView:self];
        
        CGFloat maxWidth = 0;
        
        malloc(self.numberOfRows * sizeof(CGFloat));
        
        for (int rowPos = 0; rowPos < self.numberOfRows; rowPos++) {
            VSEPGGridCellInfo ci;
            ci.nrOfCells = [self.gridViewDataSource gridView:self numberOfCellsInRow:rowPos];
            ci.cellOffsets = malloc(ci.nrOfCells * sizeof(CGFloat));
            ci.cellWidths = malloc(ci.nrOfCells * sizeof(CGFloat));
            [self.gridViewDataSource gridView:self
                                      offsets:ci.cellOffsets
                                 ofCellsInRow:rowPos];
            [self.gridViewDataSource gridView:self
                                       widths:ci.cellWidths
                                 ofCellsInRow:rowPos];
            cellInfo[rowPos] = ci;
            CGFloat rowWidth = ci.cellOffsets[ci.nrOfCells - 1] + ci.cellWidths[ci.nrOfCells - 1];
            if (maxWidth < rowWidth) {
                maxWidth = rowWidth;
            }
        }
        
        if (!self.timeLabels) {
            NSUInteger initialCapacity = maxWidth / (kVSEPGPixelPerMinute * 30 /* halfHourPixels*/);
            self.timeLabels = [NSMutableArray arrayWithCapacity:initialCapacity];
        }
        
        if (!self.hairlineView) {
            NSDate *now = [NSDate localDate];
            CGFloat nowSinceReference = [now timeIntervalSinceReferenceDate] / 60.0;
            if (nowSinceReference < self.leftMostMinuteSinceReference || self.leftMostMinuteSinceReference + (maxWidth / kVSEPGPixelPerMinute) < nowSinceReference) {
                // ignore hairline
            } else {
                // show hairline
                CGFloat nowSinceLeftMost = nowSinceReference - self.leftMostMinuteSinceReference;
                CGFloat hairlineX = self.titleWidth + (nowSinceLeftMost * (CGFloat)kVSEPGPixelPerMinute);
                NSLog(@"--- initializing hairline X: %f", hairlineX);
                self.hairlineView = [[[UIView alloc] initWithFrame:CGRectMake(hairlineX,
                                                                             0,
                                                                             3,
                                                                             self.bounds.size.height)] autorelease];
                self.hairlineView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
                self.hairlineView.backgroundColor= self.hairlineBackgroundColor;
                [self insertSubview:self.hairlineView belowSubview:self.titleContentSeparator];
                self.hairlineTimer = [NSTimer scheduledTimerWithTimeInterval:60.0 / (double)kVSEPGPixelPerMinute
                                                                      target:self
                                                                    selector:@selector(updateHairlinePosition)
                                                                    userInfo:nil
                                                                     repeats:YES];
            }
        }
        self.contentSize = CGSizeMake(maxWidth + self.titleWidth, ((CGFloat)self.numberOfRows * self.rowHeight) + self.rulerHeight);
    }
    [self doLayoutSubViews];
}

- (void) freeCellInfo {
    for (int i = 0; i < self.numberOfRows; i++) {
        VSEPGGridCellInfo ci = cellInfo[i];
        free(ci.cellOffsets);
        free(ci.cellWidths);
    }
    free(cellInfo);
    cellInfo = NULL;
}

- (void)removeSeparatorViews {
    [self.titleContentSeparator removeFromSuperview];
    self.titleContentSeparator = nil;
    
    [self.rulerTitleSeparator removeFromSuperview];
    self.rulerTitleSeparator = nil;
    
    [self.contentBackgroundView removeFromSuperview];
    self.contentBackgroundView = nil;
    
    [self.hairlineView removeFromSuperview];
    self.hairlineView = nil;
}

- (void)reload {
    [self freeCellInfo];
    for (UIView *view in [self.onScreenTitleCellsCache allValues]) {
        [view removeFromSuperview];
    }
    for (UIView *view in [self.onScreenContentCellsCache allValues]) {
        [view removeFromSuperview];
    }
    
    self.onScreenTitleCellsCache = nil;
    self.onScreenContentCellsCache = nil;
    self.offScreenTitleCellsCache = nil;
    self.offScreenContentCellsCache = nil;
    
    [self removeSeparatorViews];
    
    for (UIView *view in self.timeLabels) {
        [view removeFromSuperview];
    }
    self.timeLabels = nil;
    
    self.leftMostMinuteSinceReference = 0;
    
    self.rowHeight = 0;
    self.numberOfRows = 0;
    
    self.touchedView = nil;
    
    [self.hairlineTimer invalidate];
    self.hairlineTimer = nil;
    
    [self setNeedsLayout];
}

- (void) doLayoutSubViews {
    //fps++;
    CGFloat viewPortTopY = self.contentOffset.y;
    CGFloat viewPortBottomY = viewPortTopY + self.bounds.size.height;
    CGFloat viewPortLeftX = self.contentOffset.x;
    CGFloat viewPortContentLeftX = viewPortLeftX + self.titleWidth;
    CGFloat viewPortRightX = viewPortLeftX + self.bounds.size.width;
    
    CGFloat topRowPos = viewPortTopY / self.rowHeight;
    CGFloat _viewPortBottomY = (viewPortBottomY - self.rulerHeight);
    CGFloat bottomRowPos = (_viewPortBottomY / self.rowHeight) + (0 == (int)_viewPortBottomY % (int)self.rowHeight ? 0 : 1);
    
    if (topRowPos < 0) topRowPos = 0;
    if ((CGFloat)self.numberOfRows < bottomRowPos) bottomRowPos = (CGFloat)self.numberOfRows;
    
    // These two are not yet used
    /*
     BOOL scrolledLeft = NO, scrolledRight = NO;
     if (lastContentOffset.x < self.contentOffset.x) {
     scrolledLeft = YES;
     }
     if (self.contentOffset.x < lastContentOffset.x) {
     scrolledRight = YES;
     }
     */
    
#ifdef VS_DEBUGGING
    NSLog(@"Top row: %d, bottom row: %d", topRowPos, bottomRowPos);
#endif
    
    NSMutableDictionary *__contentCellsCache = [NSMutableDictionary dictionaryWithCapacity:[self.onScreenContentCellsCache count]];
    NSMutableDictionary *__titleCellsCache = [NSMutableDictionary dictionaryWithCapacity:[self.onScreenTitleCellsCache count]];
    
    NSUInteger __maxNrOfCellsOnScreen = 0;
    
    CGRect rulerTitleSeparatorFrame = self.rulerTitleSeparator.frame;
    rulerTitleSeparatorFrame.origin.x = viewPortLeftX;
    rulerTitleSeparatorFrame.origin.y = viewPortTopY;
    self.rulerTitleSeparator.frame = rulerTitleSeparatorFrame;
    
    // Configure hairline
    
    CGRect hf = self.hairlineView.frame;
    hf.origin.y = viewPortTopY;
    self.hairlineView.frame = hf;
    
    // Configure ruler
    if (self.numberOfRows) {
        
        double pixelPerMinuteInEPG = (double)kVSEPGPixelPerMinute;
        double halfHourPixels = 30.0 * pixelPerMinuteInEPG;
        double viewPortContentLeftXMinutesSinceReference = (double)self.leftMostMinuteSinceReference + ((double)viewPortLeftX / pixelPerMinuteInEPG);
        double leftHiddenRemaining = viewPortContentLeftXMinutesSinceReference / halfHourPixels;
        int _leftHiddenRemaining = leftHiddenRemaining;
        leftHiddenRemaining = leftHiddenRemaining - (double)_leftHiddenRemaining;
        leftHiddenRemaining = leftHiddenRemaining * halfHourPixels;
        
        double leftVisibleFirst = (double)viewPortContentLeftX;
        if (0 != leftHiddenRemaining) {
            leftVisibleFirst += halfHourPixels - (leftHiddenRemaining * pixelPerMinuteInEPG);
        }
        double halfLabelWidth = (double)kVSRulerLabelWidth / 2.0;
        UILabel *label;
        int i = 0;
        double x = leftVisibleFirst - halfHourPixels, rightVisibleLast = (double)viewPortRightX + halfHourPixels;
        for (; x < rightVisibleLast; x += halfHourPixels, i++) {
            if ([self.timeLabels count] < (i + 1)) {
                label = [[[UILabel alloc] initWithFrame:CGRectMake(x - halfLabelWidth, viewPortTopY, kVSRulerLabelWidth, self.rulerHeight)] autorelease];
                label.backgroundColor = [UIColor clearColor];
                label.textAlignment = UITextAlignmentCenter;
                label.font = [UIFont boldSystemFontOfSize:16];
                label.textColor = [UIColor whiteColor];
                [self.timeLabels addObject:label];
            } else {
                label = [self.timeLabels objectAtIndex:i];
                CGRect lf = label.frame;
                lf.origin.x = x - halfLabelWidth;
                lf.origin.y = viewPortTopY;
                label.frame = lf;
            }
            [self insertSubview:label aboveSubview:self.rulerTitleSeparator];
            
            int labelMinutes = self.leftMostMinuteSinceReference + ((x - self.titleWidth) / kVSEPGPixelPerMinute);
            int dayMinutes = labelMinutes % 1440;
            
            label.text = [NSString stringWithFormat:@"%02d:%02d", dayMinutes / 60, dayMinutes % 60];
        }
        int timeLabelsCount = [self.timeLabels count];
        for (; i < timeLabelsCount; i++) {
            label = [self.timeLabels objectAtIndex:i];
            [label removeFromSuperview];
        }
    }
    
    // Configure contentBackgroundView
    
    CGRect cbf = self.contentBackgroundView.frame;
    cbf.origin.x = viewPortLeftX;
    
    int delta = ((int)viewPortTopY % (int)self.rowHeight);
    
    cbf.origin.y = (viewPortTopY + self.rulerHeight) - (delta < 0 ? self.rowHeight - abs(delta) : delta);
    //NSLog(@"ZZZ: %f @ %f, %f, %d", cbf.origin.y, viewPortTopY, self.rulerHeight, delta);
    self.contentBackgroundView.frame = cbf;
    
    // Configure cells
    for (int rowPos = topRowPos; rowPos < bottomRowPos; rowPos++) {
        // Add/position content cells
        NSUInteger nrOfCells = cellInfo[rowPos].nrOfCells;
        CGFloat left;
        CGFloat right;
        CGFloat top = (CGFloat)rowPos * self.rowHeight + self.rulerHeight;
        BOOL visible = NO;
        for (NSUInteger cellPos = 0; cellPos < nrOfCells; cellPos++) {
            CGFloat width = cellInfo[rowPos].cellWidths[cellPos];
            CGFloat offset = cellInfo[rowPos].cellOffsets[cellPos];
            left = self.titleWidth +  + offset;
            right = left + width;
            if (left <= viewPortRightX && viewPortContentLeftX <= right) {
                visible = YES;
                NSNumber *position = [VSEPGStaticGridView positionFromRow:rowPos cell:cellPos];
                VSEPGGridViewCell *cell = [self.onScreenContentCellsCache objectForKey:position];
                if (!cell) {
                    cell = [self.offScreenContentCellsCache anyObject];
                    if (cell) {
                        [[cell retain] autorelease];
                        [self.offScreenContentCellsCache removeObject:cell];
                        
                        [cell resetPosition:position];
                        cell.frame = CGRectMake(left, top, width, self.rowHeight);
                        [self.gridViewDataSource gridView:self
                                     configureContentCell:cell
                                                   forRow:rowPos
                                                  andCell:cellPos];
                        [self insertSubview:cell belowSubview:self.hairlineView ? self.hairlineView : self.titleContentSeparator];
                    }
                }
                if (!cell) {
                    cell = [self.gridViewDataSource gridView:self contentCellForRow:rowPos andCell:cellPos];
                    cell.autoresizingMask = UIViewAutoresizingNone;
                    cell.frame = CGRectMake(left, top, width, self.rowHeight);
                    [cell resetPosition:position];
                    [self.gridViewDataSource gridView:self
                                 configureContentCell:cell
                                               forRow:rowPos
                                              andCell:cellPos];
                    [self insertSubview:cell belowSubview:self.hairlineView ? self.hairlineView : self.titleContentSeparator];
                } else {
                    [self.onScreenContentCellsCache removeObjectForKey:position];
                    cell.frame = CGRectMake(left, top, width, self.rowHeight);
                }
                [__contentCellsCache setObject:cell forKey:position];
                __maxNrOfCellsOnScreen++;
            } else if (visible) {
                // the visible region is over for now
                break;
            }
            left = right;
        }
        
        // Make sure shadow is right between layers
        self.titleContentSeparator.frame = CGRectMake(viewPortLeftX,
                                                      viewPortTopY,
                                                      self.titleWidth,
                                                      self.bounds.size.height);
        
        // Configure channel titles
        NSNumber *position = [VSEPGStaticGridView positionFromRow:rowPos cell:0];
        VSEPGGridViewCell *cell = [self.onScreenTitleCellsCache objectForKey:position];
        if (!cell) {
            cell = [self.offScreenTitleCellsCache anyObject];
            if (cell) {
                [[cell retain] autorelease];
                [self.offScreenTitleCellsCache removeObject:cell];
                
                [cell resetPosition:position];
                cell.frame = CGRectMake(viewPortLeftX, top, self.titleWidth, self.rowHeight);
                [self.gridViewDataSource gridView:self
                               configureTitleCell:cell
                                           forRow:rowPos];
                [self insertSubview:cell aboveSubview:self.titleContentSeparator];
            }
        }
        if (!cell) {
            cell = [self.gridViewDataSource gridView:self titleCellForRow:rowPos];
            cell.autoresizingMask = UIViewAutoresizingNone;
            cell.frame = CGRectMake(viewPortLeftX, top, self.titleWidth, self.rowHeight);
            [cell resetPosition:position];
            [self.gridViewDataSource gridView:self
                           configureTitleCell:cell
                                       forRow:rowPos];
            [self insertSubview:cell aboveSubview:self.titleContentSeparator];
        } else {
            [self.onScreenTitleCellsCache removeObjectForKey:position];
            cell.frame = CGRectMake(viewPortLeftX, top, self.titleWidth, self.rowHeight);
        }
        [__titleCellsCache setObject:cell forKey:position];
    }
    
    NSArray *toBeRemovedCells;
    
    toBeRemovedCells = [self.onScreenContentCellsCache allValues];
    for (UIView *view in toBeRemovedCells) {
        [self.offScreenContentCellsCache addObject:view];
        [view removeFromSuperview];
    }
    toBeRemovedCells = [self.onScreenTitleCellsCache allValues];
    for (UIView *view in toBeRemovedCells) {
        [self.offScreenTitleCellsCache addObject:view];
        [view removeFromSuperview];
    }
    
    self.onScreenTitleCellsCache = __titleCellsCache;
    self.onScreenContentCellsCache = __contentCellsCache;
    
#ifdef VS_DEBUGGING
    NSLog(@"Cache situation -> ONST: %d, ONSC: %d, OFFST: %d, OFFSC: %d",
          [self.titleCellsCache count],
          [self.contentCellsCache count],
          [self.offScreenTitleCellsCache count],
          [self.offScreenContentCellsCache count]);
#endif
    
    lastContentOffset = self.contentOffset;
}

// TODO: remove this method
/*
 - (void) printFPS {
 double __fps = fabs((double)fps / [self.fpsDate timeIntervalSinceNow]);
 if (0 < __fps) {
 NSLog(@"FPS: is: %.2f", __fps);
 }
 fps = 0;
 self.fpsDate = [NSDate date];
 }
 */

#pragma mark - Touch handling

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.touchedView) {
        UIView *v = self.touchedView;
        while (v && ![v isKindOfClass:[VSEPGGridViewCell class]]) {
            v = [v superview];
        }
        if (v) {
            VSEPGGridViewCell *cell = (VSEPGGridViewCell *)v;
            int n = [cell.position intValue];
            int r = n / __rowDelta;
            
            switch (cell.title) {
                case YES: {
                    [self.gridViewDelegate gridView:self titleCellSelectedForRow:r];
                    break;
                }
                default: {
                    int c = n % __rowDelta;
                    [self.gridViewDelegate gridView:self contentCellSelectedForRow:r andCell:c];
                    break;
                }
            }
            
        }
    }
    self.touchedView = nil;
    
    [super touchesEnded:touches withEvent:event];
}
- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    self.touchedView = nil;
    
    [super touchesCancelled:touches withEvent:event];
}


- (BOOL)touchesShouldCancelInContentView:(UIView *)view {
    self.touchedView = nil;
    return YES;
}

- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view {
    self.touchedView = view;
    return YES;
}

#pragma mark - Actions

- (void) updateHairlinePosition {
    
    NSDate *now = [NSDate localDate];
    CGFloat nowSinceReference = [now timeIntervalSinceReferenceDate] / 60.0;
    CGFloat nowSinceLeftMost = nowSinceReference - (CGFloat)self.leftMostMinuteSinceReference;
    CGFloat hairlineX = (CGFloat)self.titleWidth + (nowSinceLeftMost * (CGFloat)kVSEPGPixelPerMinute);
    NSLog(@"--- updating hairline X: %f", hairlineX);
    CGRect hf = self.hairlineView.frame;
    hf.origin.x = hairlineX;
    self.hairlineView.frame = hf;
}

#pragma mark - Visuals

- (void) setRulerBackgroundColor:(UIColor *)colorOrPattern {
    UIColor *__oldColor = _rulerBackgroundColor;
    _rulerBackgroundColor = [colorOrPattern retain];
    [__oldColor release];
    [self reload];
}
- (void) setTitleBackgroundColor:(UIColor *)colorOrPattern {
    UIColor *__oldColor = _titleBackgroundColor;
    _titleBackgroundColor = [colorOrPattern retain];
    [__oldColor release];
    [self reload];
}
- (void) setContentBackgroundColor:(UIColor *)colorOrPattern {
    UIColor *__oldColor = _contentBackgroundColor;
    _contentBackgroundColor = [colorOrPattern retain];
    [__oldColor release];
    [self reload];
}
- (void) setHairlineBackgroundColor:(UIColor *)colorOrPattern {
    UIColor *__oldColor = _hairlineBackgroundColor;
    _hairlineBackgroundColor = [colorOrPattern retain];
    [__oldColor release];
    [self reload];
}

- (UIColor *) rulerBackgroundColor {
    return [[_rulerBackgroundColor retain] autorelease];
}
- (UIColor *) titleBackgroundColor {
    return [[_titleBackgroundColor retain] autorelease];
}
- (UIColor *) contentBackgroundColor {
    return [[_contentBackgroundColor retain] autorelease];
}
- (UIColor *) hairlineBackgroundColor {
    return [[_hairlineBackgroundColor retain] autorelease];
}

+ (NSNumber *) positionFromRow:(NSUInteger)rowPos cell:(NSUInteger)cellPos {
    return NUMINT((__rowDelta * rowPos) + cellPos);
}

@end
