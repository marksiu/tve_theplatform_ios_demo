//
//  VSEPGGridViewContentCell.h
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSEPGScheduleInstance.h"

@interface VSEPGGridViewContentCell : UIView

@property (retain) VSEPGScheduleInstance *scheduleInstance;

- (void) olRetain;
- (int) olRelease;

- (void)highlight:(BOOL)highlight;

@end
