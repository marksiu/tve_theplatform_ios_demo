//
//  VSEPGGridViewTitleCell.h
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSRemoteImageView.h"

@interface VSEPGGridViewTitleCell : UIView

@property (retain) NSDictionary *channel;

-(void) showLock;

- (void)highlight:(BOOL)highlight;

@end
