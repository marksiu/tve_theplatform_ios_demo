//
//  VSTapThroughView.m
//  Viasat
//
//  Created by Zoltan Gobolos on 11/30/11.
//  Copyright (c) 2011 zgobolos@yaffaworks.com. All rights reserved.
//

#import "VSTapThroughView.h"

@implementation VSTapThroughView

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    for (UIView *subView in [[self.subviews copy] autorelease] ) {
        CGPoint _point = [subView convertPoint:point fromView:self];
        if ([subView pointInside:_point withEvent:event]) {
            return YES;
        }
    }
    return NO;
}

@end
