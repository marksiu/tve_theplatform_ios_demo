//
//  NSDate.m
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//

#import "NSDate+Extras.h"
#import "AppDelegate.h"
//#import "VSUtils.h"
#import "Constants.h"
@implementation NSDate (VSExtras)

+ (NSDate *) unadjustedLocalDate {
    NSDate *current = [NSDate dateWithTimeInterval:[[NSTimeZone localTimeZone] secondsFromGMT] sinceDate:[NSDate date]];
    return current;
}

+ (NSDate *) localDate {
    NSDate *current = [self unadjustedLocalDate];
    
    
    NSDate *adjusted = [current dateByAddingTimeInterval:boxTimeDelta];
    return adjusted;
}

+ (NSDate *) localDay {
    NSDate *localNow = [NSDate localDate];
    int daysSince1970 = [localNow timeIntervalSince1970] / 86400;
    return [NSDate dateWithTimeIntervalSince1970:daysSince1970 * 86400];
}

+ (NSTimeInterval) localTimeIntervalSinceLocalDay {
    NSDate *localNow = [NSDate localDate];
    int daysSince1970 = [localNow timeIntervalSince1970] / 86400;
    NSDate *localDay = [NSDate dateWithTimeIntervalSince1970:daysSince1970 * 86400];
    return [localNow timeIntervalSinceDate:localDay];
}
@end
