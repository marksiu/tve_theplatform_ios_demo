
//
//  VSEPGProgram.m
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//

#import "VSEPGScheduleInstance.h"

@interface VSEPGScheduleInstance () {
    NSUInteger hash;
}
@end

@implementation VSEPGScheduleInstance

#if VS_DEVELOPMENT
@synthesize original;
#endif

@synthesize instanceId;

@synthesize channelId;
@synthesize channelName;
@synthesize channelImageUrl;

@synthesize contentId;
@synthesize contentName;
@synthesize contentImageUrl;
@synthesize contentType;
@synthesize contentSeasonNr;
@synthesize contentEpisodeNr;

@synthesize serieId;
@synthesize serieName;

@synthesize broadcastMinutesSince1970;
@synthesize durationMinutes;
@synthesize durationUnits;

@synthesize broadcastStartTime;
@synthesize broadcastEndTime;
@synthesize broadcastStartToEndTimes;
@synthesize urlDateFormatter;
@synthesize timeDateFormatter;


- (NSTimeInterval) minutesSince1970FromString:(NSString *)utcDateTimeString {
    @synchronized (self.urlDateFormatter) {
        NSDate *date = [self.urlDateFormatter dateFromString:utcDateTimeString];
        return (long)(((double)[date timeIntervalSince1970]) / 60.0);
    }
}

- (NSString *) timeStringFromMinutesSince1970:(NSTimeInterval)minutesSince1970 {
    @synchronized (self.timeDateFormatter) {
        return [self.timeDateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:(minutesSince1970 * 60.0)]];
    }
}


- (id) initWithSDPDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {

        
        self.instanceId = [[dict valueForKey:@"channel"] stringByAppendingFormat:@"%@", [dict valueForKey:@"startTimeSeconds"]];
        hash = [self.instanceId hash];
        
        self.channelId = [dict valueForKeyPath:@"channel"];
        self.contentName = [dict valueForKeyPath:@"title"];
        
        self.broadcastMinutesSince1970 = [[dict valueForKeyPath:@"startTimeSeconds"] doubleValue] / 60.0f;
        self.durationMinutes = [[dict valueForKeyPath:@"durationSeconds"] doubleValue] / 60.0f;

        NSTimeInterval minutesFromGMT = ((NSTimeInterval)[[NSTimeZone localTimeZone] secondsFromGMT] / 60.0);
        self.broadcastStartTime = [self timeStringFromMinutesSince1970:self.broadcastMinutesSince1970 + minutesFromGMT];
        self.broadcastEndTime = [self timeStringFromMinutesSince1970:self.broadcastMinutesSince1970 + minutesFromGMT + self.durationMinutes];
        self.broadcastStartToEndTimes = [NSString stringWithFormat:@"%@ - %@", self.broadcastStartTime, self.broadcastEndTime];
        
        
#if VS_DEVELOPMENT
        self.original = dict;
#endif

    }
    return self;
}

- (void) dealloc {
#if VS_DEVELOPMENT
    self.original = nil;
#endif
    
    self.instanceId = nil;
    
    self.channelId = nil;
    self.channelName = nil;
    self.channelImageUrl = nil;
    
    self.contentId = nil;
    self.contentName = nil;
    self.contentImageUrl = nil;
    self.contentType = nil;
    
    self.serieId = nil;
    self.serieName = nil;
    
    self.durationUnits = nil;
    
    self.broadcastStartTime = nil;
    self.broadcastEndTime = nil;
    self.broadcastStartToEndTimes = nil;
    
    [super dealloc];
}

- (NSString *) description {
#if VS_DEBUG_OVERLAPPING_SCHEDULE
    return self.instanceId;
//#elif VS_DEVELOPMENT
//    return self.original.description;
#else
    return [NSString stringWithFormat:@"SIID:%@, CN:%@", self.instanceId, self.contentName];
#endif
}

- (NSUInteger) hash {
    return hash;
}

- (BOOL) isEqual:(id)object {
    return [object isKindOfClass:[self class]] && [object hash] == hash;
}

@end
