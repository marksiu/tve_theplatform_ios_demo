//
//  NSDate.h
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (VSExtras)

+ (NSDate *) unadjustedLocalDate;
+ (NSDate *) localDate;

+ (NSDate *) localDay;

+ (NSTimeInterval) localTimeIntervalSinceLocalDay;

@end
