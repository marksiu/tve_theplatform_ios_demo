//
//  VSEPGProgram.h
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VSEPGScheduleInstance : NSObject

#if VS_DEVELOPMENT
@property(retain) NSDictionary *original;
#endif

@property(retain) NSString *instanceId;
@property(retain) NSDateFormatter *urlDateFormatter;
@property(retain) NSDateFormatter *timeDateFormatter;

@property(retain) NSString *channelId;
@property(retain) NSString *channelName;
@property(retain) NSString *channelImageUrl;
@property(retain) NSString *contentType;

@property(retain) NSString *contentId;
@property(retain) NSString *contentName;
@property(retain) NSString *contentImageUrl;
@property(assign) int contentSeasonNr;
@property(assign) int contentEpisodeNr;

@property(retain) NSString *serieId;
@property(retain) NSString *serieName;

@property(assign) NSTimeInterval broadcastMinutesSince1970;
@property(assign) NSTimeInterval durationMinutes;
@property(retain) NSString *durationUnits;

@property(retain) NSString *broadcastStartTime;
@property(retain) NSString *broadcastEndTime;
@property(retain) NSString *broadcastStartToEndTimes;
- (NSTimeInterval) minutesSince1970FromString:(NSString *)utcDateTimeString;
- (NSString *) timeStringFromMinutesSince1970:(NSTimeInterval)minutesSince1970;

- (id) initWithSDPDictionary:(NSDictionary *)dict;

@end
