//
//  DaySelectController.h
//  CMore
//
//  Created by Gabor Bottyan on 8/20/12.
//  Copyright (c) 2012 Meganet-Soft Bt. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DaySelectControllerDelegate 

-(int) getSelectedDay;
-(void) daySelected: (int) selected  withLabel:(NSString *) text;


@end


@interface DaySelectController : UITableViewController
@property (assign) id <DaySelectControllerDelegate> delegate;
@end
