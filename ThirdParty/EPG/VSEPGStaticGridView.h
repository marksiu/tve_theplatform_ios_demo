//
//  VSEPGStaticGridView.h
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VSEPGGridViewCell.h"

@protocol VSEPGStaticGridViewDelegate;
@protocol VSEPGStaticGridViewDataSource;

@interface VSEPGStaticGridView : UIScrollView

@property (nonatomic, assign) id<VSEPGStaticGridViewDelegate> gridViewDelegate;
@property (nonatomic, assign) id<VSEPGStaticGridViewDataSource> gridViewDataSource;

@property (nonatomic, retain) UIColor *rulerBackgroundColor;
@property (nonatomic, retain) UIColor *titleBackgroundColor;
@property (nonatomic, retain) UIColor *contentBackgroundColor;
@property (nonatomic, retain) UIColor *hairlineBackgroundColor;

- (void)reload;

@end

@protocol VSEPGStaticGridViewDelegate <NSObject>
- (void) gridView:(VSEPGStaticGridView *)gridView titleCellSelectedForRow:(NSUInteger)rowPos;
- (void) gridView:(VSEPGStaticGridView *)gridView contentCellSelectedForRow:(NSUInteger)rowPos andCell:(NSUInteger)cellPos;
@end

@protocol VSEPGStaticGridViewDataSource <NSObject>

@required

- (NSUInteger) numberOfRowsInGridView:(VSEPGStaticGridView *)gridView; // Called on first render and reload
- (CGFloat) heightOfRowsInGridView:(VSEPGStaticGridView *)gridView; // Called on first render and reload
- (CGFloat) widthOfTitleInGridView:(VSEPGStaticGridView *)gridView; // Called on first render and reload
- (CGFloat) heightOfRulerInGridView:(VSEPGStaticGridView *)gridView; // Called on first render and reload

- (VSSDPMinutes) leftMostMinuteInGridView:(VSEPGStaticGridView *)gridView; // Called on first render and reload

- (NSUInteger)gridView:(VSEPGStaticGridView *)gridView numberOfCellsInRow:(NSUInteger)rowPos; // Called on first render and reload
- (void) gridView:(VSEPGStaticGridView *)gridView offsets:(CGFloat *)offsets ofCellsInRow:(NSUInteger)rowPos; // Called on first render and reload
- (void) gridView:(VSEPGStaticGridView *)gridView widths:(CGFloat *)widths ofCellsInRow:(NSUInteger)rowPos; // Called on first render and reload

- (VSEPGGridViewCell *) gridView:(VSEPGStaticGridView *)gridView titleCellForRow:(NSUInteger)rowPos; // Called on each render for rows in sight.
- (void) gridView:(VSEPGStaticGridView *)gridView configureTitleCell:(VSEPGGridViewCell *)cell forRow:(NSUInteger)rowPos; // Called on each render for rows in sight.

- (VSEPGGridViewCell *) gridView:(VSEPGStaticGridView *)gridView contentCellForRow:(NSUInteger)rowPos andCell:(NSUInteger)cellPos; // Called on each render for rows in sight.
- (void) gridView:(VSEPGStaticGridView *)gridView configureContentCell:(VSEPGGridViewCell *)cell forRow:(NSUInteger)rowPos andCell:(NSUInteger)cellPos; // Called on each render for rows in sight.

@end

