//
//  VSEPGGridView.m
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "NSDate+Extras.h"
#import "VSEPGGridView.h"
#import "EPGConstants.h"
#import "VSTapThroughView.h"
#import "AppDelegate.h"
#import "Constants.h"
#pragma mark - Grid view

@interface VSEPGGridView () {
    
    CGPoint lastContentOffset;
    
    UIColor *_rulerBackgroundColor;
    UIColor *_titleBackgroundColor;
    UIColor *_contentBackgroundColor;
    UIColor *_hairlineBackgroundColor;
    
    BOOL considerHairline;
    BOOL showHairline;
    
    
    dispatch_queue_t epgContentRendererQueue;
}

@property (retain) NSMutableSet *lastTileKeys;

// The height of a row in the grid
@property (assign) CGFloat rowHeight;
// The number of rows in the grid. Typically this is the number of channels.
@property (assign) CGFloat numberOfRows;
// The width of the title bar on the right, where the channel indicator resides.
@property (assign) CGFloat titleWidth;
// The height of the ruler on the top. The ruler is for displayiong time data.
@property (assign) CGFloat rulerHeight;

// This is the structure in which we store the onscreen title cells. On each render loop
// (layoutSubViews), this will be overwritten with a new dictionary.
@property (nonatomic, retain) NSMutableDictionary *onScreenTitleCellsCache;
// The next two fields are for storing offscreen cells to eliminate cell creation. The actual
// number of elements in the onscreen and offscreen caches together are the same as the highest
// number of cells ever displayed onscreen in thisn view's lifecycle.
@property (nonatomic, retain) NSMutableSet *offScreenTitleCellsCache;


// This is the structure in which we store the onscreen content cells. On each render loop
// we check whether a new tile entered into the sight. If yes, rendering happens, and the
// end of the rendering / lyouting process, this structure will be replaced with a current one.
@property (nonatomic, retain) NSMutableDictionary *onScreenContentCellsCache;
@property (nonatomic, retain) NSMutableSet *offScreenContentCellsCache;


@property (nonatomic, retain) UIView *tileOverlaysSeparator;

// The next 4 views are called separators. As the ruler, the title bar, the content and the background
// are all managed inside this class, these have a special purpose to help managing the content and
// title cells' Z-positioning.

// The ruler background view. Always positioned to the top, flexible width matching the current
// bounds width.
@property (nonatomic, retain) UIView *rulerSeparator;
// Background view behid the title cells. Flexible height matching the bounds height.
@property (nonatomic, retain) UIView *titleSeparator;
// The background helper view. Its purpose is to fix the background row pattern position
// by positioning it below the ruler. Its height is bounds' height - rulerHeight. Always positioned
// origin.x = contentOffset.x + titleWidth
@property (nonatomic, retain) UIView *contentBackground;
// This thin view represents the current time. Its height is flexible, origin.x is set by a timer fired
// 60 / pixelPerMinutes times a minute.
@property (nonatomic, retain) UIView *hairline;

// A cache containing labels for the ruler.
@property (nonatomic, retain) NSMutableArray *timeLabels;

@property (nonatomic, retain) NSMutableArray *tileOverlays;

// This ivar is for storing the selected view temporarily.
@property (retain) UIView *touchedView;

// For storing the timer that updates the hairline view.
@property (retain) NSTimer *hairlineTimer;

// Stores the date representation of the center of the viewport. On change the delegate gets notified.
@property (retain) NSDate *lastContentViewportCenterDay;

@property (assign) int selectedTitleRow;
@property (retain) VSEPGGridViewTitleCell *selectedTitleCell;


// Does the heavy lifting. Called at the end of each layoutSubviews.
- (void) doLayoutSubViews:(BOOL)initial;

// A small helper method that drops all the separator views.
- (void) removeSeparatorViews;

@end

@implementation VSEPGGridView

@synthesize gridViewDelegate;
@synthesize gridViewDataSource;

@dynamic rulerBackgroundColor;
@dynamic titleBackgroundColor;
@dynamic contentBackgroundColor;
@dynamic hairlineBackgroundColor;

@synthesize lastTileKeys;

@synthesize rowHeight;
@synthesize numberOfRows;
@synthesize titleWidth;
@synthesize rulerHeight;

@synthesize onScreenTitleCellsCache;
@synthesize offScreenTitleCellsCache;
@synthesize onScreenContentCellsCache;
@synthesize offScreenContentCellsCache;

@synthesize tileOverlaysSeparator;
@synthesize rulerSeparator;
@synthesize titleSeparator;
@synthesize contentBackground;
@synthesize hairline;

@synthesize timeLabels;

@synthesize tileOverlays;

@synthesize touchedView;

@synthesize hairlineTimer;

@synthesize forceNewLayout;

@synthesize animating;

@synthesize lastContentViewportCenterDay;

@synthesize selectedTitleRow;
@synthesize selectedTitleCell;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.multipleTouchEnabled = NO;
        self.userInteractionEnabled = YES;
        self.canCancelContentTouches = YES;
        self.directionalLockEnabled = YES;
        
        self.rulerBackgroundColor = [UIColor redColor];
        self.titleBackgroundColor = [UIColor greenColor];
        self.contentBackgroundColor = [UIColor blueColor];
        self.hairlineBackgroundColor = [UIColor yellowColor];
        
        self.clipsToBounds = YES;
        self.delaysContentTouches = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleShouldSeekEPGNotification:)
                                                     name:kVSNotificationShouldSeekEPG
                                                   object:nil];
        epgContentRendererQueue = dispatch_queue_create("tv.accedo.epgContentRenderer", NULL);
    }
    return self;
}

- (void) dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    dispatch_release(epgContentRendererQueue);
    
    self.lastTileKeys = nil;
    
    self.rulerBackgroundColor = nil;
    self.titleBackgroundColor = nil;
    self.contentBackgroundColor = nil;
    self.hairlineBackgroundColor = nil;
    
    self.onScreenTitleCellsCache = nil;
    self.offScreenTitleCellsCache = nil;
    self.onScreenContentCellsCache = nil;
    self.offScreenContentCellsCache = nil;
    
    self.tileOverlaysSeparator = nil;
    self.rulerSeparator = nil;
    self.titleSeparator = nil;
    self.contentBackground = nil;
    self.hairline = nil;
    considerHairline = NO;
    showHairline = NO;
    
    self.timeLabels = nil;
    
    self.tileOverlays = nil;
    
    self.touchedView = nil;
    
    [self.hairlineTimer invalidate];
    self.hairlineTimer = nil;
    
    self.lastContentViewportCenterDay = nil;
    
    self.selectedTitleCell = nil;
    
    [super dealloc];
}

- (void)layoutSubviews {

    [super layoutSubviews];
    
    if (!self.gridViewDataSource) {
        return;
    }
    
    /*
     * The policy is to free all allocated memory on each reload call. We check whether
     * we need to recreate structures this way, then recreate if evaluates to true.
     */
    if (!self.lastTileKeys) {
        
        self.decelerationRate = UIScrollViewDecelerationRateNormal;
        
        self.onScreenTitleCellsCache = [NSMutableDictionary dictionary];
        self.offScreenTitleCellsCache = [NSMutableSet set];
        self.onScreenContentCellsCache = [NSMutableDictionary dictionary];
        self.offScreenContentCellsCache = [NSMutableSet set];
        
        self.numberOfRows = [self.gridViewDataSource numberOfRowsInGridView:self];
        self.rowHeight = [self.gridViewDataSource heightOfRowsInGridView:self];
        self.titleWidth = [self.gridViewDataSource widthOfTitleInGridView:self];
        self.rulerHeight = [self.gridViewDataSource heightOfRulerInGridView:self];
        
        //NSTimeInterval secondsFromGMT = (NSTimeInterval)[[NSTimeZone localTimeZone] secondsFromGMT];
        //NSDate *ld = [[NSDate localDay] dateByAddingTimeInterval:+secondsFromGMT];
        NSDate *ld = [NSDate localDay];
        
        NSTimeInterval currentDayHoursSince1970 = [ld timeIntervalSince1970] / 3600.0;
        NSTimeInterval leftMostDayHoursSince1970 = currentDayHoursSince1970 - (kVSEPGBackwardScrollingLimitMinutesD / 60.0);
        NSTimeInterval durationHours = 24.0 /* today */ + (kVSEPGBackwardScrollingLimitMinutesD / 60.0) + (kVSEPGForwardScrollingLimitMinutesD / 60.0);
        long long maxHours = leftMostDayHoursSince1970 + durationHours;
        int nrOFTilesVertically = (self.numberOfRows / kVSEPGTileRows) + (((int)(self.numberOfRows) % kVSEPGTileRows) ? 1 : 0);
        
        NSMutableArray *tileKeys = [NSMutableArray array];
        for (long long c = leftMostDayHoursSince1970; c < maxHours; c += kVSEPGChunkInterval) {
            for (long long r = 0; r < nrOFTilesVertically; r++) {
                NSNumber *tileKey = [NSNumber numberWithLongLong:((c * kVSEPGGridViewTileKeyChunkMultiplier) + (r * kVSEPGTileRows))];
                [tileKeys addObject:tileKey];
            }
        }
        
#if VS_DEVELOPMENT
        if ([tileKeys count]) {
            int hrs;
            int row;
            NSNumber *tileKey = nil;
            
            tileKey = [tileKeys objectAtIndex:0];
            [VSEPGGridView resolveTileKey:tileKey intoChunkHoursSince1970:&hrs andRow:&row];
            NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:hrs * 3600];
            int rowFrom = row;
            
            tileKey = [tileKeys lastObject];
            [VSEPGGridView resolveTileKey:tileKey intoChunkHoursSince1970:&hrs andRow:&row];
            NSDate *dateTo = [NSDate dateWithTimeIntervalSince1970:hrs * 3600];
            int rowTo = row;
            
            EPGLog(@"Will prepare for tiles between %@/%d AND %@/%d", dateFrom, rowFrom, dateTo, rowTo);
            
        }
#endif
        
        [self.gridViewDataSource prepareForTiles:tileKeys forGridView:self];
        
        
        self.contentBackground = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width + 8, self.bounds.size.height + self.rowHeight)] autorelease];
        self.contentBackground.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.contentBackground.backgroundColor = self.contentBackgroundColor;
        [self addSubview:self.contentBackground];
        
        
        self.titleSeparator = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
        self.titleSeparator.backgroundColor = self.titleBackgroundColor;
        [self addSubview:self.titleSeparator];
        
        self.rulerSeparator = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.rulerHeight)] autorelease];
        self.rulerSeparator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.rulerSeparator.backgroundColor = self.rulerBackgroundColor;
        [self addSubview:self.rulerSeparator];
        
        
#if kVSSettingsVisualizeEPGTiles
            self.tileOverlaysSeparator = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)] autorelease];
            self.tileOverlaysSeparator.autoresizingMask = UIViewAutoresizingNone;
            self.tileOverlaysSeparator.backgroundColor = [UIColor clearColor];
            [self addSubview:self.tileOverlaysSeparator];
#endif
        
        CGFloat hairlineX = 0.0;
        
        self.contentSize = CGSizeMake(((1440 + kVSEPGBackwardScrollingLimitMinutes + kVSEPGForwardScrollingLimitMinutes) * kVSEPGPixelPerMinute) + self.titleWidth, self.numberOfRows * self.rowHeight + self.rulerHeight);
        
#warning - Refactor from here
        //AppDelegate * del = (AppDelegate *)[UIApplication sharedApplication].delegate;
        
        showHairline = [self.gridViewDelegate shouldShowHairlineInGridView:self];
        considerHairline = [self.gridViewDelegate shouldConsiderHairlineInGridView:self];
        
        NSTimeInterval localTimeIntervalSinceLocalDay = [NSDate localTimeIntervalSinceLocalDay]- boxTimeDelta;
        NSTimeInterval minutes = localTimeIntervalSinceLocalDay / 60.0;
        
        hairlineX = ((kVSEPGBackwardScrollingLimitMinutes + minutes) * kVSEPGPixelPerMinute) + self.titleWidth;
        
        if (boxTimeDelta == 0 && considerHairline){
            self.contentOffset = CGPointMake(hairlineX - (self.bounds.size.width / 2.0), 0.0);
        } else {
            float mins = 480;
            float nohairlineX = ((kVSEPGBackwardScrollingLimitMinutes + mins) * kVSEPGPixelPerMinute) + self.titleWidth;
            self.contentOffset = CGPointMake(nohairlineX - (self.bounds.size.width / 2.0), 0.0);
        }
        
        self.hairline = [[[UIView alloc] initWithFrame:CGRectMake(hairlineX,
                                                                  0,
                                                                  3,
                                                                  self.bounds.size.height)] autorelease];
        self.hairline.hidden = !showHairline;
#warning - Refactor to here. Better to position to NOW instead of the hairline
        
        self.hairline.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.hairline.backgroundColor= self.hairlineBackgroundColor;
        [self insertSubview:self.hairline belowSubview:self.titleSeparator];
        
        self.hairlineTimer = [NSTimer scheduledTimerWithTimeInterval:60.0 / (double)kVSEPGPixelPerMinute
                                                              target:self
                                                            selector:@selector(updateHairlineX)
                                                            userInfo:nil
                                                             repeats:YES];
        
        if (!self.timeLabels) {
            self.timeLabels = [NSMutableArray array];
        }
        
#if kVSSettingsVisualizeEPGTiles
            if (!self.tileOverlays) {
                self.tileOverlays = [NSMutableArray array];
            }
#endif
        
        self.selectedTitleRow = -1;
        self.selectedTitleCell = nil;
        
        [self doLayoutSubViews:YES];
        
    } else {
        //NSDate *startDate = [NSDate date];
        [self doLayoutSubViews:NO];
        //NSLog(@"doLayout in: %.3fs", [startDate timeIntervalSinceNow]);
    }
}

- (void)removeSeparatorViews {
    
    [self.tileOverlaysSeparator removeFromSuperview];
    self.tileOverlaysSeparator = nil;
    
    [self.titleSeparator removeFromSuperview];
    self.titleSeparator = nil;
    
    [self.rulerSeparator removeFromSuperview];
    self.rulerSeparator = nil;
    
    [self.contentBackground removeFromSuperview];
    self.contentBackground = nil;
    
    [self.hairline removeFromSuperview];
    self.hairline = nil;
}

- (void)reset {
    
    self.lastTileKeys = nil;
    
    for (UIView *view in [self.onScreenTitleCellsCache allValues]) {
        [view removeFromSuperview];
    }
    for (UIView *view in [self.onScreenContentCellsCache allValues]) {
        [view removeFromSuperview];
    }
    for (UIView *view in [self.offScreenContentCellsCache allObjects]) {
        [view removeFromSuperview];
    }
    
    self.onScreenTitleCellsCache = nil;
    self.offScreenTitleCellsCache = nil;
    self.onScreenContentCellsCache = nil;
    self.offScreenContentCellsCache = nil;
    
    [self removeSeparatorViews];
    
    for (UIView *view in self.timeLabels) {
        [view removeFromSuperview];
    }
    self.timeLabels = nil;
    
    for (UIView *view in self.tileOverlays) {
        [view removeFromSuperview];
    }
    self.tileOverlays = nil;
    
    self.rowHeight = 0;
    self.numberOfRows = 0;
    
    self.touchedView = nil;
    
    [self.hairlineTimer invalidate];
    self.hairlineTimer = nil;
    
    self.contentSize = CGSizeZero;
    
    lastContentOffset = CGPointZero;
    
    self.lastContentViewportCenterDay = nil;
    
    self.selectedTitleRow = -1;
    self.selectedTitleCell = nil;
    
    considerHairline = NO;
    showHairline = NO;
    
    [self.hairlineTimer invalidate];
    self.hairlineTimer = nil;
}

- (void) reload {
    [self reset];
    [self setNeedsLayout];
}

- (BOOL)tileVisible:(NSNumber *)tileKey {
    int chunkHoursSince1970;
    int row;
    [VSEPGGridView resolveTileKey:tileKey intoChunkHoursSince1970:&chunkHoursSince1970 andRow:&row];
    
    CGFloat tileTop = row * self.rowHeight;
    CGFloat tileBottom = tileTop + (self.rowHeight * kVSEPGTileRowsD);
    int chunkMinutesSince1970 = chunkHoursSince1970 * 60;
    
    if (self.contentOffset.y < tileBottom && tileTop < self.contentOffset.y + self.bounds.size.height - self.rulerHeight) {
        NSTimeInterval currentDayMinutesSince1970 = [[NSDate localDay] timeIntervalSince1970] / 60.0;
        NSTimeInterval leftMostDayMinutesSince1970 = currentDayMinutesSince1970 - kVSEPGBackwardScrollingLimitMinutesD;
        NSTimeInterval contentViewportWidthMinutes = ceil((NSTimeInterval)(self.bounds.size.width - self.titleWidth) / kVSEPGPixelPerMinuteD);
        NSTimeInterval contentViewportLeftMostMinutesSince1970 = leftMostDayMinutesSince1970 + ((NSTimeInterval)(self.contentOffset.x) / kVSEPGPixelPerMinuteD);
        NSTimeInterval contentViewportRightMostMinutesSince1970 = contentViewportLeftMostMinutesSince1970 + contentViewportWidthMinutes;
        if (contentViewportLeftMostMinutesSince1970 < chunkMinutesSince1970 + (kVSEPGChunkInterval * 60) && chunkMinutesSince1970 < contentViewportRightMostMinutesSince1970) {
            return YES;
        }
    }
    return NO;
}
- (void)scrollCellToVisible:(VSEPGGridViewContentCell *)cell animated:(BOOL)animated {
    return;
    CGPoint co = self.contentOffset;
    CGFloat bh = self.bounds.size.height;
    //CGFloat csh = self.contentSize.height;
    CGFloat cey = cell.frame.origin.y;
    CGFloat ceh = cell.frame.size.height;
    co.y = cey + (ceh / 2.0) - (bh / 2.0);
    self.contentOffset = co;
    /*
    [UIView animateWithDuration:.35
                          delay:0
                        options:UIViewAnimationOptionAllowAnimatedContent | UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionTransitionNone
                     animations:^(void){
                         self.contentOffset = co;
                     }
                     completion:^(BOOL finished) {
                     }];
     */
}

- (void) doLayoutSubViews:(BOOL)initial {
    
    
    
    /*
     * Save and reset forceNewLayout and animating.
     */
    BOOL __forceNewLayout = self.forceNewLayout;
    BOOL __animating = self.animating;
    self.forceNewLayout = NO;
    
    /*
     * Calculate time boundaries, viewport dimension based on today's time and contentOffset
     */
    NSTimeInterval currentDayMinutesSince1970 = [[NSDate localDay] timeIntervalSince1970] / 60.0;
    
    //NSTimeInterval currentDayMinutesSince1970 = [[NSDate localDay] timeIntervalSince1970] / 60.0;
    NSTimeInterval leftMostDayMinutesSince1970 = currentDayMinutesSince1970 - kVSEPGBackwardScrollingLimitMinutesD;
    //NSTimeInterval rightMostDayMinutesSince1970 = currentDayMinutesSince1970 + kVSEPGForwardScrollingLimitMinutesD;
    
    CGFloat technicalViewportX = self.contentOffset.x;
    CGFloat technicalViewportY = self.contentOffset.y;
    CGFloat technicalViewportWidth = self.bounds.size.width;
    CGFloat technicalViewportHeight = self.bounds.size.height;
    //NSTimeInterval technicalViewportWidthMinutes = ceil((NSTimeInterval)technicalViewportWidth / kVSEPGPixelPerMinuteD);
    
    CGFloat contentViewportX = technicalViewportX + self.titleWidth;
    //CGFloat contentViewportY = technicalViewportY + self.rulerHeight;
    CGFloat contentViewportWidth = technicalViewportWidth - self.titleWidth;
    CGFloat contentViewportHeight = technicalViewportHeight - self.rulerHeight;
    NSTimeInterval contentViewportWidthMinutes = ceil((NSTimeInterval)contentViewportWidth / kVSEPGPixelPerMinuteD);
    
    NSTimeInterval contentViewportLeftMostMinutesSince1970 = leftMostDayMinutesSince1970 + ((NSTimeInterval)technicalViewportX / kVSEPGPixelPerMinuteD);
    NSTimeInterval contentViewportRightMostMinutesSince1970 = contentViewportLeftMostMinutesSince1970 + contentViewportWidthMinutes;
    
#warning optimize block from here
    /*
     * Determine visible rows (vertical condition)
     */
    int _tileHeight = kVSEPGTileRows * (int)self.rowHeight;
    int tileTopRow = 0;
    if (0 < _tileHeight) {
        tileTopRow = ((int)technicalViewportY / _tileHeight) * kVSEPGTileRows;
    }
    
    
    CGFloat tileY = (CGFloat)tileTopRow * self.rowHeight;
    CGFloat tileHeight = kVSEPGTileRowsD * self.rowHeight;
    int numberOfTilesVertically = 1;
    CGFloat bottomY = technicalViewportY + contentViewportHeight;
    for (; tileY + (tileHeight * (CGFloat)numberOfTilesVertically) < bottomY; numberOfTilesVertically++) {
        // nop
    }
    
    /*
     * Determine viewport chunks (horizontal condition)
     */
    int contentViewportLeftMostHoursSince1970 = contentViewportLeftMostMinutesSince1970 / 60.0;
    int chunkHoursSince1970 = (contentViewportLeftMostHoursSince1970 / kVSEPGChunkInterval) * kVSEPGChunkInterval;
    //NSLog(@"CVLMH: %d, %d", contentViewportLeftMostHoursSince1970, chunkHoursSince1970);
    int numberOfTilesHorizontally = 1;
    for (; (double)(chunkHoursSince1970 + (numberOfTilesHorizontally * kVSEPGChunkInterval)) * 60.0 < contentViewportRightMostMinutesSince1970; numberOfTilesHorizontally++) {
        // nop
    }
    
    /*
     * At this point we know the tiles to be displayed. Let's compute the tileKeys.
     *
     */
    NSMutableSet *allTileKeys = [NSMutableSet setWithCapacity:4];
    for (NSUInteger h = 0; h < numberOfTilesHorizontally; h++) {
        long long tileKeyHPart = (chunkHoursSince1970 + (h * kVSEPGChunkInterval)) * kVSEPGGridViewTileKeyChunkMultiplier;
        for (long long v = 0; v < numberOfTilesVertically; v++) {
            long long tileKeyVPart = (tileTopRow + (v * kVSEPGTileRows));
            [allTileKeys addObject:[NSNumber numberWithLongLong:(tileKeyHPart + tileKeyVPart)]];
        }
    }
#warning optimize block to here
    
    /*
     * Time to redraw content cells only if forced or a new tile entered into the viewport
     */
    BOOL onScreenTilesSetUnchanged = [self.lastTileKeys isEqualToSet:allTileKeys];
    if (__forceNewLayout || __animating || !onScreenTilesSetUnchanged) {
        
        /*
#if kVSSettingsVisualizeEPGTiles
            NSLog(@"Rendering with forceLayout: %d, animating: %d, onScreenTilesSetChanged: %d", __forceNewLayout, __animating, !onScreenTilesSetUnchanged);
#endif
         */
        
        dispatch_async(epgContentRendererQueue, ^(void) {
            // Find out what's still on screen
            NSMutableSet *survivorTileKeys = [NSMutableSet setWithSet:allTileKeys];
            [survivorTileKeys intersectSet:self.lastTileKeys];
            
            // Find out what's new
            NSMutableSet *newTileKeys = [NSMutableSet setWithSet:allTileKeys];
            [newTileKeys minusSet:self.lastTileKeys];
            
            // Find out what's left
            NSMutableSet *deadTileKeys = [NSMutableSet setWithSet:self.lastTileKeys];
            [deadTileKeys minusSet:allTileKeys];
            
            self.lastTileKeys = [NSMutableSet setWithSet:allTileKeys];
            
            /*
             * Move dead tile cells off screen
             */
            for (NSNumber *tileKey in deadTileKeys) {
                
                // Explode tile key.
                int chunkHoursSince1970;
                int row;
                [VSEPGGridView resolveTileKey:tileKey intoChunkHoursSince1970:&chunkHoursSince1970 andRow:&row];
                
                // Iterate through rows.
                int lastRow = row + kVSEPGTileRows;
                
                for (; row < lastRow; row++) {
                    NSArray *chunkData = [self.gridViewDataSource gridView:self
                                                dataForChunkHoursSince1970:chunkHoursSince1970
                                                                       row:(NSUInteger)row];
                    if (!chunkData) {
                        continue;
                    }
                    
                    for (VSEPGScheduleInstance *scheduleInstance in chunkData) {
                        VSEPGGridViewContentCell *cell = [self.onScreenContentCellsCache objectForKey:scheduleInstance.instanceId];
                        if (0 == [cell olRelease]) {
                            dispatch_sync(dispatch_get_main_queue(), ^(void){
                                cell.hidden = YES;
                            });
                            // CRASH
                            [self.offScreenContentCellsCache addObject:cell];
                            [self.onScreenContentCellsCache removeObjectForKey:scheduleInstance.instanceId];
                        } else {
                            // otherwise leave
                        }
                    }
                }
            }
            
            /*
             * Build a cache from survivor cells' scheduleInstan  s
             */
            NSMutableSet *scheduleInstanceIdStore = [NSMutableSet set];
            for (NSNumber *tileKey in survivorTileKeys) {
                
                // Explode tile key.
                int chunkHoursSince1970;
                int row;
                [VSEPGGridView resolveTileKey:tileKey intoChunkHoursSince1970:&chunkHoursSince1970 andRow:&row];
                
                // Iterate through rows.
                int lastRow = row + kVSEPGTileRows;
                
                for (; row < lastRow; row++) {
                    NSArray *chunkData = [self.gridViewDataSource gridView:self
                                                dataForChunkHoursSince1970:chunkHoursSince1970
                                                                       row:(NSUInteger)row];
                    if (!chunkData) {
                        continue;
                    }
                    
                    for (VSEPGScheduleInstance *scheduleInstance in chunkData) {
                        if (!scheduleInstance.instanceId) {
                            NSLog(@"CRASH");
                        }
                        [scheduleInstanceIdStore addObject:scheduleInstance.instanceId];
                    }
                }
            }
            
            /*
             * Now let's go ahead and process the new tiles
             */
            for (NSNumber *tileKey in newTileKeys) {
                
                /*
                 * Ask datasource whether it should prepare tiles.
                 * Please note we don't do this with survivors, as those were already processed.
                 */
                if ([self.gridViewDataSource gridView:self shouldWaitForTileKey:tileKey]) {
                    // Skip tile this time.
                    // Removing the tile, because if we get an update for this tile, we will consider that as new.
                    [self.lastTileKeys removeObject:tileKey];
                    continue;
                }
                
                // Tile data is available or preparation is in progress. We'll see later.
                
                // Explode tile key.
                int chunkHoursSince1970;
                int row;
                [VSEPGGridView resolveTileKey:tileKey intoChunkHoursSince1970:&chunkHoursSince1970 andRow:&row];
                
                // Iterate through rows.
                int lastRow = row + kVSEPGTileRows;
                
                for (; row < lastRow; row++) {
                    
                    // Get data for the given chunk
                    NSArray *chunkData = [self.gridViewDataSource gridView:self
                                                dataForChunkHoursSince1970:chunkHoursSince1970
                                                                       row:(NSUInteger)row];
                    if (!chunkData) {
                        // Chunk data is not yet available. Still downloading maybe ...
                        continue;
                    }
                    
                    // This is the current row's y coordinate
                    CGFloat top = ((CGFloat)row * self.rowHeight) + self.rulerHeight;
                    
                    // Process chunk data for the given row. Chunk data is an array of scheduleInstances
                    // ordered by broadcastDateTime. We got it this way from NDS.
                    
                    for (VSEPGScheduleInstance *scheduleInstance in chunkData) {
                        
                        if ([scheduleInstanceIdStore containsObject:scheduleInstance.instanceId]) {
                            // Whoops, this schedule instance overlaps the same scheduleInstance from another
                            // chunk. From a tile right or the left. Newermind, a cell with the same scheduleInstance id
                            // must be in the onScreen cache. Let's find it and increase its olRefs.
                            
                            VSEPGGridViewContentCell *cell = [self.onScreenContentCellsCache objectForKey:scheduleInstance.instanceId];
                            [cell olRetain];
                            
                            // This step prevents killing the cell.
                            
                            continue;
                        }
                        
                        
                        // Let's store the scheduleInstance id. This way we build a registry of scheduleInstances on scren.
                        if (!scheduleInstance.instanceId) {
                            NSLog(@"CRASH");
                        }
                        [scheduleInstanceIdStore addObject:scheduleInstance.instanceId];
                        
                        // Let's calculate the cell's frame. The trickiest part of it is calculating the x coordinate.
                        NSTimeInterval minutesDelta = scheduleInstance.broadcastMinutesSince1970 - contentViewportLeftMostMinutesSince1970;
                        CGRect cellRect;
                        cellRect.origin.x = contentViewportX + (minutesDelta * kVSEPGPixelPerMinuteD);
                        cellRect.origin.y = top;
                        cellRect.size.width = scheduleInstance.durationMinutes * kVSEPGPixelPerMinuteD;
                        cellRect.size.height = self.rowHeight;
                        
                        __block VSEPGGridViewContentCell *cell = [self.offScreenContentCellsCache anyObject];
                        if (cell) {
                            [cell olRetain];
                            // Protect cell form being autoreleased once removed from the offscreen cache.
                            [self.onScreenContentCellsCache setObject:cell forKey:scheduleInstance.instanceId];
                            [self.offScreenContentCellsCache removeObject:cell];
                            
                            // Peepare cell for reuse.
                            dispatch_sync(dispatch_get_main_queue(), ^(void) {
                                cell.scheduleInstance = scheduleInstance;
                                [cell highlight:[self.gridViewDataSource gridView:self
                                                       shouldHighlightContentCell:cell
                                                               byScheduleInstance:scheduleInstance]];
                                cell.frame = cellRect;
                                
                                // Add it to the screen.
                                cell.hidden = NO;
#if VS_EPG_CELLS_APPEAR_ANIMATED
                                cell.alpha = 0;
                                [UIView animateWithDuration:.25
                                                      delay:0
                                                    options:UIViewAnimationOptionAllowAnimatedContent | UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionTransitionNone
                                                 animations:^(void){
                                                     cell.alpha = 1;
                                                 }
                                                 completion:NULL];
#endif
                            });
                        }
                        if (!cell) {
                            // No reusable cell found. We must ask the datasource to create one.
                            // You probably noticed that we haven't picked up a cell from the current onscreen cache.
                            // This is because we're hoping we'll find matches in this loop later.
                            dispatch_sync(dispatch_get_main_queue(), ^(void){
                                cell = [self.gridViewDataSource contentCellForGridView:self];
                                [self.onScreenContentCellsCache setObject:cell forKey:scheduleInstance.instanceId];
                                
                                // PRepare cell.
                                cell.autoresizingMask = UIViewAutoresizingNone;
                                cell.frame = cellRect;
                                cell.scheduleInstance = scheduleInstance;
                                [cell highlight:[self.gridViewDataSource gridView:self
                                                       shouldHighlightContentCell:cell
                                                               byScheduleInstance:scheduleInstance]];
                                // Add it to the screen.
                                [self insertSubview:cell belowSubview:self.hairline];
#if VS_EPG_CELLS_APPEAR_ANIMATED
                                cell.alpha = 0;
                                [UIView animateWithDuration:.25
                                                      delay:0
                                                    options:UIViewAnimationOptionAllowAnimatedContent | UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionTransitionNone|
                                                    
                                                 animations:^(void){
                                                     cell.alpha = 1;
                                                 }
                                                 completion:NULL];
#endif
                            });
                        }
                    }
                }
            }
            if ([self.onScreenContentCellsCache count]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:kVSTilesRendered
                                                                        object:self];
                });
            }
        });
    }
    
    int topRow = floorf(technicalViewportY / self.rowHeight);
    int rowsCount = ceilf(contentViewportHeight / self.rowHeight) + 1;
    if (self.numberOfRows < rowsCount) {
        rowsCount = self.numberOfRows;
    }
    if (topRow < 0) {
        rowsCount += topRow;
        topRow = 0;
    }
    if (self.numberOfRows < topRow + rowsCount) {
        rowsCount = self.numberOfRows - topRow;
    }
    
    NSMutableDictionary *__titleCellsCache = [NSMutableDictionary dictionaryWithCapacity:[self.onScreenTitleCellsCache count]];
    int lastRow = topRow + rowsCount;
    for (int row = topRow; row < lastRow; row++) {
        
        CGFloat top = (CGFloat)row * self.rowHeight + self.rulerHeight;
        
        NSNumber *rowKey = [NSNumber numberWithInt:row];
        VSEPGGridViewTitleCell *cell = nil;
        if (row == self.selectedTitleRow) {
            cell = self.selectedTitleCell;
            [self insertSubview:cell aboveSubview:self.titleSeparator];
        } else {
            cell = [self.onScreenTitleCellsCache objectForKey:rowKey];
        }
        if (cell) {
            [self.onScreenTitleCellsCache removeObjectForKey:rowKey];
            // Title cells must always align to the left of the viewport, so we should set frame each time.
            cell.frame = CGRectMake(technicalViewportX, top, self.titleWidth, self.rowHeight);
        } else {
            cell = [self.offScreenTitleCellsCache anyObject];
            if (cell) {
                [[cell retain] autorelease];
                [self.offScreenTitleCellsCache removeObject:cell];
                
                cell.frame = CGRectMake(technicalViewportX, top, self.titleWidth, self.rowHeight);
                [self.gridViewDataSource gridView:self
                               configureTitleCell:cell
                                           forRow:row];
                [cell highlight:[self.gridViewDataSource gridView:self shouldHighlightTitleCell:cell forRow:row]];
                [self insertSubview:cell aboveSubview:self.titleSeparator];
            }
        }
        if (!cell) {
            cell = [self.gridViewDataSource titleCellForGridView:self];
            cell.autoresizingMask = UIViewAutoresizingNone;
            cell.frame = CGRectMake(technicalViewportX, top, self.titleWidth, self.rowHeight);
            [self.gridViewDataSource gridView:self
                           configureTitleCell:cell
                                       forRow:row];
            [cell highlight:[self.gridViewDataSource gridView:self shouldHighlightTitleCell:cell forRow:row]];
            [self insertSubview:cell aboveSubview:self.titleSeparator];
        }
        [__titleCellsCache setObject:cell forKey:rowKey];
    }
    
    /*
     * Rearrange title cells cache
     */
    NSArray *toBeRemovedCells = [self.onScreenTitleCellsCache allValues];
    for (UIView *view in toBeRemovedCells) {
        if ([view isEqual:self.selectedTitleCell]) {
            // prevent selected cell's offline reuse
        } else {
            if (!view) {
                NSLog(@"CRASH");
            }
            [self.offScreenTitleCellsCache addObject:view];
        }
        // we are free to move selected cell offscreen
        [view removeFromSuperview];
    }
    self.onScreenTitleCellsCache = __titleCellsCache;
    
    /*
     * Configure content background
     */
    CGRect cbf = self.contentBackground.frame;
    cbf.origin.x = technicalViewportX - ((int)technicalViewportX % 8);
    int delta = ((int)technicalViewportY % (int)self.rowHeight);
    cbf.origin.y = (technicalViewportY + self.rulerHeight) - (delta < 0 ? self.rowHeight - abs(delta) : delta);
    cbf.size.height = self.bounds.size.height + self.rowHeight;
    self.contentBackground.frame = cbf;
    
    /*
     * Configure title bar background view
     */
    self.titleSeparator.frame = CGRectMake(technicalViewportX,
                                           technicalViewportY,
                                           self.titleWidth,
                                           self.bounds.size.height);
    
    /*
     * Configure ruler, regardless of data. This seems overcomplicated, maybe there's a much easier way.
     */
    CGRect rf = self.rulerSeparator.frame;
    rf.origin.x = technicalViewportX;
    rf.origin.y = technicalViewportY;
    self.rulerSeparator.frame = rf;
    
    double halfHourPixels = 30.0 * kVSEPGPixelPerMinuteD;
    double leftHiddenRemaining = contentViewportLeftMostMinutesSince1970 / halfHourPixels;
    int _leftHiddenRemaining = leftHiddenRemaining;
    leftHiddenRemaining = leftHiddenRemaining - (double)_leftHiddenRemaining;
    leftHiddenRemaining = leftHiddenRemaining * halfHourPixels;
    
    double leftVisibleFirst = (double)contentViewportX;
    if (0 != leftHiddenRemaining) {
        leftVisibleFirst += halfHourPixels - (leftHiddenRemaining * kVSEPGPixelPerMinuteD);
    }
    UILabel *label;
    int i = 0;
    double x = leftVisibleFirst - halfHourPixels, rightVisibleLast = contentViewportX + contentViewportWidth + halfHourPixels;
    for (; x < rightVisibleLast; x += halfHourPixels, i++) {
        if ([self.timeLabels count] < (i + 1)) {
            label = [[[UILabel alloc] initWithFrame:CGRectMake(x - kVSRulerLabelHalfWidth, technicalViewportY, kVSRulerLabelWidth, self.rulerHeight)] autorelease];
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.font = [UIFont boldSystemFontOfSize:16];
            label.textColor = [UIColor whiteColor];
            [self.timeLabels addObject:label];
        } else {
            label = [self.timeLabels objectAtIndex:i];
            CGRect lf = label.frame;
            lf.origin.x = x - kVSRulerLabelHalfWidth;
            lf.origin.y = technicalViewportY;
            label.frame = lf;
        }
        [self insertSubview:label aboveSubview:self.rulerSeparator];
        
        int labelMinutes = leftMostDayMinutesSince1970 + ((x - self.titleWidth) / kVSEPGPixelPerMinute);
//        int labelMinutes = leftMostDayMinutesSince1970 + ((x - self.titleWidth) / kVSEPGPixelPerMinute);
        int dayMinutes = labelMinutes % 1440;
        
        label.text = [NSString stringWithFormat:@"%02d:%02d", dayMinutes / 60, dayMinutes % 60];
    }
    int timeLabelsCount = [self.timeLabels count];
    for (; i < timeLabelsCount; i++) {
        label = [self.timeLabels objectAtIndex:i];
        [label removeFromSuperview];
    }
    
#if kVSSettingsVisualizeEPGTiles
        /*
         * Display tile overlays if enabled
         */
        for (UIView *tv in self.tileOverlays) {
            [tv removeFromSuperview];
        }
        [self.tileOverlays removeAllObjects];
        for (NSNumber *tileKey in allTileKeys) {
            int chunkHoursSince1970;
            int row;
            [VSEPGGridView resolveTileKey:tileKey intoChunkHoursSince1970:&chunkHoursSince1970 andRow:&row];
            //VSTapThroughView *overlay = [[VSTapThroughView alloc] initWithFrame:CGRectZero];
            UIView *overlay = [[UIView alloc] initWithFrame:CGRectZero];
            overlay.userInteractionEnabled = NO;
            overlay.backgroundColor = [UIColor purpleColor];
            overlay.layer.borderWidth = 1;
            overlay.layer.borderColor = [UIColor redColor].CGColor;
            overlay.alpha = 0.4;
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 300, 60)];
            label.backgroundColor = [UIColor redColor];
            label.textColor = [UIColor whiteColor];
            label.numberOfLines = 2;
            label.text = [[tileKey description] stringByAppendingFormat:@"\n%@", [NSDate dateWithTimeIntervalSince1970:chunkHoursSince1970 * 3600]];
            [overlay addSubview:label];
            [label release];
            
            [self insertSubview:overlay aboveSubview:self.tileOverlaysSeparator];
            [self.tileOverlays addObject:overlay];
            [overlay release];
            
            NSTimeInterval minutesDelta = chunkHoursSince1970 * 60 - contentViewportLeftMostMinutesSince1970;
            CGFloat tx = contentViewportX + (minutesDelta * kVSEPGPixelPerMinuteD);
            overlay.frame = CGRectMake(tx, ((CGFloat)row * self.rowHeight) + self.rulerHeight, kVSEPGChunkIntervalD * 60.0 * kVSEPGPixelPerMinuteD, kVSEPGTileRowsD * self.rowHeight);
        }
#endif
    
    /*
     * Set hairline origin Y, regardless of data
     */
    CGRect hf = self.hairline.frame;
    hf.origin.y = technicalViewportY;
    self.hairline.frame = hf;
    
    lastContentOffset = self.contentOffset;
    
    NSTimeInterval contentViewportCenterMinutesSince1970 = (((contentViewportWidth / 2.0)) / kVSEPGPixelPerMinuteD) + contentViewportLeftMostMinutesSince1970;
    NSTimeInterval contentViewportCenterSecondsSince1970 = contentViewportCenterMinutesSince1970 * 60.0;
    NSTimeInterval contentViewportCenterDaySecondsSince1970 = ((int)(contentViewportCenterSecondsSince1970 / 86400.0)) * 86400;
    NSDate *contentViewportCenterDay = [NSDate dateWithTimeIntervalSince1970:contentViewportCenterDaySecondsSince1970];
    if (![contentViewportCenterDay isEqualToDate:self.lastContentViewportCenterDay]) {
        [self.gridViewDelegate gridView:self dayChanged:contentViewportCenterDay];
    }
    self.lastContentViewportCenterDay = contentViewportCenterDay;
}

#pragma mark - Touch handling

- (void) titleButtonSelected:(id)button  {
   int row = ((UIView*)button).superview.frame.origin.y / self.rowHeight;
   [self.gridViewDelegate gridView:self titleCellSelected:(VSEPGGridViewTitleCell *)(((UIView*)button).superview)
                               row:row previouslySelectedCell:self.selectedTitleCell];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.touchedView) {
        UIView *v = self.touchedView;
        while (v) {
            if ([v isKindOfClass:[VSEPGGridViewTitleCell class]]) {
                int row = v.frame.origin.y / self.rowHeight;
                [self.gridViewDelegate gridView:self titleCellSelected:(VSEPGGridViewTitleCell *)v row:row previouslySelectedCell:self.selectedTitleCell];
                self.selectedTitleCell = (VSEPGGridViewTitleCell *)v;
                self.selectedTitleRow = row;
                break;
            } else if ([v isKindOfClass:[VSEPGGridViewContentCell class]]) {
                [self.gridViewDelegate gridView:self contentCellSelected:(VSEPGGridViewContentCell *)v];
                break;
            } else {
                v = [v superview];
            }
        }
    }
    self.touchedView = nil;
    [super touchesEnded:touches withEvent:event];
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    self.touchedView = nil;
    
    [super touchesCancelled:touches withEvent:event];
}


- (BOOL)touchesShouldCancelInContentView:(UIView *)view {
    self.touchedView = nil;
    return YES;
}

- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view {
    /*
     if (1 == [touches count]) {
     UITouch *touch = [touches anyObject];
     if ([touch locationInView:self].x < self.contentOffset.x + self.titleWidth) {
     } else {
     }
     }
     */
    self.touchedView = view;
    return YES;
}

#pragma mark - Actions

- (void) updateHairlineX {
    //AppDelegate * del = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSTimeInterval localTimeIntervalSinceLocalDay = [NSDate localTimeIntervalSinceLocalDay]- boxTimeDelta;
    NSTimeInterval minutes = localTimeIntervalSinceLocalDay / 60.0;
    int hairlineX = ((kVSEPGBackwardScrollingLimitMinutesD + minutes) * kVSEPGPixelPerMinuteD) + self.titleWidth;
    CGRect hf = self.hairline.frame;
    hf.origin.x = hairlineX;
    hf.origin.y = self.contentOffset.y;
    self.hairline.frame = hf;
}

#warning - check for midnight
- (void) handleShouldSeekEPGNotification:(NSNotification *)notification {
    NSNumber *seekTo = [notification.userInfo valueForKey:kVSKeySeekTo];
    NSTimeInterval seekToSecondsSince1970 = [seekTo doubleValue];
    if (!seekTo) {
        seekToSecondsSince1970 = [[NSDate localDate] timeIntervalSince1970];
    }
    NSTimeInterval seekToMinutesSince1970 = seekToSecondsSince1970 / 60.0;
    
    NSTimeInterval currentDayMinutesSince1970 = [[NSDate localDay] timeIntervalSince1970] / 60.0;
    NSTimeInterval leftMostDayMinutesSince1970 = currentDayMinutesSince1970 - kVSEPGBackwardScrollingLimitMinutesD;
    
    NSTimeInterval minutes = seekToMinutesSince1970 - leftMostDayMinutesSince1970;
    
    int x = (minutes * kVSEPGPixelPerMinuteD) - (self.bounds.size.width / 2) + self.titleWidth;
    
    CGPoint co = self.contentOffset;
    
    co.x = x;
    
    [self setContentOffset:co animated:YES];
    
}

#pragma mark - Visuals

- (void) setRulerBackgroundColor:(UIColor *)colorOrPattern {
    UIColor *__oldColor = _rulerBackgroundColor;
    _rulerBackgroundColor = [colorOrPattern retain];
    [__oldColor release];
}
- (void) setTitleBackgroundColor:(UIColor *)colorOrPattern {
    UIColor *__oldColor = _titleBackgroundColor;
    _titleBackgroundColor = [colorOrPattern retain];
    [__oldColor release];
}
- (void) setContentBackgroundColor:(UIColor *)colorOrPattern {
    UIColor *__oldColor = _contentBackgroundColor;
    _contentBackgroundColor = [colorOrPattern retain];
    [__oldColor release];
}
- (void) setHairlineBackgroundColor:(UIColor *)colorOrPattern {
    UIColor *__oldColor = _hairlineBackgroundColor;
    _hairlineBackgroundColor = [colorOrPattern retain];
    [__oldColor release];
}

- (UIColor *) rulerBackgroundColor {
    return [[_rulerBackgroundColor retain] autorelease];
}
- (UIColor *) titleBackgroundColor {
    return [[_titleBackgroundColor retain] autorelease];
}
- (UIColor *) contentBackgroundColor {
    return [[_contentBackgroundColor retain] autorelease];
}
- (UIColor *) hairlineBackgroundColor {
    return [[_hairlineBackgroundColor retain] autorelease];
}

#pragma mark - Static stuff
+ (void) resolveTileKey:(NSNumber *)tileKey intoChunkHoursSince1970:(int *)chunkHoursSince1970 andRow:(int *)row {
    
    NSUInteger __tileKey = [tileKey unsignedIntegerValue];
    
    int _chunkHoursSince1970 = (__tileKey / kVSEPGGridViewTileKeyChunkMultiplier);
    int _row = (__tileKey % kVSEPGGridViewTileKeyChunkMultiplier);
    
    *chunkHoursSince1970 = _chunkHoursSince1970;
    *row = _row;
}


@end
