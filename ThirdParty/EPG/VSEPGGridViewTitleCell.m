//
//  VSEPGGridViewTitleCell.m
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//

#import "VSEPGGridViewTitleCell.h"
#import "AppView.h"
@interface VSEPGGridViewTitleCell () {
    NSDictionary *_channel;
}

@property (retain) UIButton* titleButton;
@property (retain) VSRemoteImageView *remoteImageView;
@property (retain) UILabel* titleLabel;
@property (retain) UIImageView *lock;
@property (retain) UIView* backgroundView;

@end

@implementation VSEPGGridViewTitleCell

@synthesize remoteImageView;
@synthesize titleLabel;
@synthesize titleButton;

@dynamic channel;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame]; // 151x52
    if (self) {
        
        self.backgroundView = [[[UIView alloc] initWithFrame:CGRectMake(0, 1, self.bounds.size.width - 1, self.bounds.size.height - 2)]autorelease];
        self.backgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:.6];
        [self addSubview:self.backgroundView];
        
        /*
        self.titleButton = [[[UIButton alloc] initWithFrame:CGRectMake(5, 5, self.bounds.size.width - 10, self.bounds.size.height - 10)] autorelease];
        [self.titleButton setBackgroundImage:[UIImage imageNamed:@"channel_logo_bg"] forState:UIControlStateNormal];
        [self addSubview:titleButton];
         */
        
        self.remoteImageView = [[[VSRemoteImageView alloc] initWithFrame:CGRectMake(81, 8, 85, 30)] autorelease];
        self.remoteImageView.backgroundColor = [UIColor clearColor];
        self.remoteImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.remoteImageView.clipsToBounds = YES;
        [self addSubview:self.remoteImageView];
        
        self.titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 81, 25)] autorelease];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.titleLabel];
        
        self.lock = [[[UIImageView alloc] initWithFrame:CGRectMake(30, 25, 12, 15)] autorelease];
        self.lock.backgroundColor = [UIColor clearColor];
        self.lock.clipsToBounds = YES;
        self.lock.image = [UIImage imageNamed:@"lock"];
        self.lock.hidden = YES;
        [self addSubview:self.lock];
    }
    return self;
}

-(void) showLock {
    self.lock.hidden = NO;
}

- (void) setImageUrl:(NSString*)imageUrl {
    //imageUrl = @"http://www.channel4.com/static/global/css/4od/img/4od-logo.png";
    // Use channel mockup images which were saved locally.
    if ([imageUrl rangeOfString:@".png"].location != NSNotFound)
        [self.remoteImageView setLocalImage:imageUrl withChannel:true];
    else
        [self.remoteImageView setImageUrl:imageUrl];
}

- (void) setTitle:(NSString*)title {
    self.titleLabel.text = title;
}

- (void) setChannel:(NSDictionary *)__channel {
    @synchronized(self) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        NSDictionary *__old = _channel;
        
        _channel = [__channel retain];
        [__old release];
        
        self.lock.hidden = YES;
        
        if (_channel) {
            [self setImageUrl:[_channel valueForKey:@"logo"]];
            [self setTitle:[@"CH " stringByAppendingString:[_channel valueForKey:@"channel"]]];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(handleVSEPGChannelSelectionChanged:)
                                                         name:@"kVSEPGChannelSelectionChanged"
                                                       object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(handleSubscribedToChannel:)
                                                         name:@"PAYD_CHANNELS_CHANGED"
                                                       object:nil];
        } else {
            [self.remoteImageView resetImage];
            [self setTitle:@""];
        }
    }
}

- (NSDictionary *) channel {
    NSDictionary *__channel = nil;
    @synchronized(self) {
        __channel = [[_channel retain] autorelease];
    }
    return __channel;
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.remoteImageView = nil;
    self.titleLabel = nil;
    self.titleButton = nil;
    self.channel = nil;
    
    [super dealloc];
}

-(void)handleVSEPGChannelSelectionChanged:(NSNotification *)notification {
    [self highlight:[[notification.userInfo valueForKey:@"cid"] isEqualToString:[_channel valueForKey:@"channel"]]];
}

-(void)handleSubscribedToChannel:(NSNotification *)notification {
    NSNumber * nr = [NSNumber numberWithInt:[[self.channel valueForKey:@"channel"] intValue]];
    NSArray * paydChannels = [[AppView sharedInstance] getPaidChannels];
    NSNumber * payd = [self.channel objectForKey:@"pay"];
    if (payd && [payd boolValue]){
        if ([paydChannels containsObject:nr]){
            //NSLog(@"CHANNEL LOCKED %@", nr);
            self.lock.hidden = YES;
        }else{
            //NSLog(@"CHANNEL UNLOCKED %@", nr);
            self.lock.hidden = NO;
        }
    }else{
        self.lock.hidden = YES;
    }
    
    
    
}

- (void)highlight:(BOOL)highlight {
    if (highlight) {
        self.backgroundView.backgroundColor = [UIColor colorWithRed:6.0/255.0 green:135.0/255.0 blue:1 alpha:.54];
    } else {
        self.backgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:.6];
    }
}

@end
