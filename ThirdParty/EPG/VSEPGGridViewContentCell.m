//
//  VSEPGGridViewContentCell.m
//  Viasat
//
//  Copyright (c) 2011 Accedo Broadband AB. All rights reserved.
//

#import "VSEPGGridViewContentCell.h"

@interface VSEPGGridViewContentCell () {
    VSEPGScheduleInstance *_scheduleInstance;
    int olRefs;
}

@property (retain) UIView *backgroundView;
@property (retain) UILabel *titleLabel;

@end

@implementation VSEPGGridViewContentCell

@dynamic scheduleInstance;

@synthesize titleLabel;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.autoresizingMask = UIViewAutoresizingNone;
        
        self.backgroundView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
        self.backgroundView.autoresizingMask = UIViewAutoresizingNone;
        self.backgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:.6];
        [self addSubview:self.backgroundView];
        
        self.titleLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
        self.titleLabel.autoresizingMask = UIViewAutoresizingNone;
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.textColor = [UIColor whiteColor];
        [self addSubview:self.titleLabel];
        
        olRefs = 1;
    }
    return self;
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.scheduleInstance = nil;
    
    self.titleLabel = nil;
    self.backgroundView = nil;
    
    [super dealloc];
}

- (void)layoutSubviews {
    self.backgroundView.frame = CGRectMake(1,
                                        1,
                                        self.bounds.size.width - 2,
                                        self.bounds.size.height - 2);
    
    self.titleLabel.frame = CGRectMake(10,
                                           1,
                                           self.bounds.size.width - 14,
                                           self.bounds.size.height - 2);
}

- (NSString *) description {
    return [NSString stringWithFormat:@"title = %@, olRefs = %d", self.titleLabel.text, olRefs];
}

- (void) setScheduleInstance:(VSEPGScheduleInstance *)__scheduleInstance {
    @synchronized(self) {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        VSEPGScheduleInstance *__old = _scheduleInstance;
        _scheduleInstance = [__scheduleInstance retain];
        [__old release];

        if (_scheduleInstance) {
            if (_scheduleInstance.serieName) {
                self.titleLabel.text = _scheduleInstance.serieName;
            } else {
                self.titleLabel.text = _scheduleInstance.contentName;
            }
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(handleVSEPGChannelSelectionChanged:)
                                                         name:@"kVSEPGChannelSelectionChanged"
                                                       object:nil];
        } else {
            self.titleLabel.text = @"";
        }
    }
}

- (VSEPGScheduleInstance *) scheduleInstance {
    VSEPGScheduleInstance *__scheduleInstance = nil;
    @synchronized(self) {
        __scheduleInstance = [[_scheduleInstance retain] autorelease];
    }
    return __scheduleInstance;
}

- (void) olRetain {
    olRefs++;
}
- (int) olRelease {
    return --olRefs;
}

-(void)handleVSEPGChannelSelectionChanged:(NSNotification *)notification {
    [self highlight:[[notification.userInfo valueForKey:@"cid"] isEqualToString:self.scheduleInstance.channelId]];
}

- (void)highlight:(BOOL)highlight {
    if (highlight) {
        self.backgroundView.backgroundColor = [UIColor colorWithRed:6.0/255.0 green:135.0/255.0 blue:1 alpha:.54];
    } else {
        self.backgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:.6];
    }
}

@end
