//
//  OnDemandDetailView.h
//  TVE
//
//  Created by Gabor Bottyan on 11/30/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailOpenerDelegate.h"
#import "AssetView.h"
#import "RatingViewController.h"
@interface OnDemandDetailView : UIScrollView <AsetViewDelegate, RatingViewControllerDelegate, NSURLConnectionDelegate>
- (id)initWithFrame:(CGRect) frame config:(NSDictionary *)configuration;
- (void)configureView:(NSDictionary *)configuration;
- (void)setRelatedContent:(NSArray * )relatedContent;
@property (assign) id <DetailOpenerDelegate> detaildelegate;
@end
