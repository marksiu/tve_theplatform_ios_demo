//
//  SeasonDetailView.m
//  TVE
//
//  Created by Gabor Bottyan on 12/3/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "SeasonDetailView.h"
#import "Constants.h"
#import "UIHelper.h"
#import "VSRemoteImageView.h"
#import "AssetView.h"
#import "PopupHelper.h"
#import "AppView.h"
extern const CGSize kButtonSize;

@interface SeasonDetailView ()
@property (retain) VSRemoteImageView *logoImageView;
@property (retain) UILabel *titleLabel;
@property (retain) UIView *ratingView;
@property (retain) UIImageView *ageRatingImageView;
@property (retain) UILabel *subtitleLabel2;
@property (retain) UIView *rentPriceView;
@property (retain) UIView *rentalPeriodView;
@property (retain) UIButton *buttonAddToFavorites;
@property (retain) UIButton *buttonShare;
@property (retain) UIButton *buttonFriendsShared;
@property (retain) UIButton *buttonNextSeason;
@property (retain) UIView *relatedView;
@property (retain) UIView *lineView;
@property (retain) UIView *buttonSeparator;
@property (retain) UIView *episodesView;



@property (retain) UIButton *clipsButton;
@property (retain) UIButton *episodesButton;
@property (assign) BOOL clipsVisible;
@property (retain) NSDictionary *config;

@property (retain) UIView *upperView;

@property (retain) UICollectionView *collectionView;
@property (retain) NSArray *episodeContent;
@property (retain) NSArray *clipsContent;
@end

@implementation SeasonDetailView

- (id)initWithFrame:(CGRect)frame config:(NSDictionary *)configuration {
	self = [super initWithFrame:frame];

	if (self) {
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(updateFavorite)
													 name:@"FAVORITE_SHOWS_CHANGED"
												   object:nil];

		UICollectionViewFlowLayout *flowLayout = [[[UICollectionViewFlowLayout alloc] init] autorelease];
		// [flowLayout setItemSize:CGSizeMake(detailRightSize, 1)];
		[flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
		flowLayout.minimumInteritemSpacing = 0;
		flowLayout.minimumLineSpacing = 0;

		[self.collectionView setCollectionViewLayout:flowLayout];
		self.collectionView = [[[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout] autorelease];

		self.collectionView.delegate = self;
		self.collectionView.dataSource = self;
		[self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"topCell"];
		[self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"bottomCell"];
		self.collectionView.backgroundColor = [UIColor clearColor];

		[self addSubview:self.collectionView];
		self.config = configuration;
		[self createTopview:configuration];
	}

	return self;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.section == 0) {
		return CGSizeMake(detailRightSize, 190);
	}
	else {
		if (!self.clipsVisible) {
			return CGSizeMake(detailRightSize, [AssetView sizeOfStyle:ASSET_TYPE_EPISODE].height);
		}
		else {
			float bigSize = (detailRightSize - [AssetView sizeOfStyle:ASSET_TYPE_RELATED_GRID].width) / 2;
			float posDelta = 20;

			if (indexPath.row % 3 == 1) {
				return [AssetView sizeOfStyle:ASSET_TYPE_RELATED_GRID];
			}
			else if (indexPath.row % 3 == 0) {
				return CGSizeMake(bigSize - posDelta, [AssetView sizeOfStyle:ASSET_TYPE_RELATED_GRID].height);
			}
			else {
				return CGSizeMake(bigSize + posDelta, [AssetView sizeOfStyle:ASSET_TYPE_RELATED_GRID].height);
			}
		}
	}
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	UICollectionViewCell *cell = Nil;

	// NSLog(@"SECTION %d ROW %d", indexPath.section, indexPath.row);

	if (indexPath.section == 0) {
		cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"topCell" forIndexPath:indexPath];

		[cell addSubview:self.upperView];
	}
	else {
		cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"bottomCell" forIndexPath:indexPath];

		AssetView *assetView = Nil;

		if (!self.clipsVisible) {
			NSDictionary *entry = [self.episodeContent objectAtIndex:indexPath.row];
			assetView = [[[AssetView alloc] initWithStyle:ASSET_TYPE_EPISODE data:entry] autorelease];
			assetView.frame = CGRectMake(30, 0, assetView.frame.size.width, assetView.frame.size.height);
		}
		else {
			NSDictionary *entry = [self.clipsContent objectAtIndex:indexPath.row];
			assetView = [[[AssetView alloc] initWithStyle:ASSET_TYPE_RELATED_GRID data:entry] autorelease];

			if (indexPath.row % 3 == 0) {
				assetView.frame = CGRectMake(cell.contentView.frame.size.width - assetView.frame.size.width, 0, assetView.frame.size.width, assetView.frame.size.height);
			}
			if (indexPath.row % 3 == 1) {
				assetView.frame = CGRectMake(0, 0, assetView.frame.size.width, assetView.frame.size.height);
				assetView.center = cell.contentView.center;
			}
			if (indexPath.row % 3 == 1) {
				assetView.frame = CGRectMake(0, 0, assetView.frame.size.width, assetView.frame.size.height);
				assetView.center = cell.contentView.center;
			}
		}

		// cell = [[[UICollectionViewCell alloc] initWithFrame:CGRectMake(, 0, assetView.frame.size.width, assetView.frame.size.height)] autorelease];
		assetView.tag = indexPath.row;
		assetView.delegate = self;

		for (UIView *v in [[[cell.contentView subviews] copy] autorelease]) {
			[v removeFromSuperview];
		}

		[cell.contentView addSubview:assetView];

		// add separator line...
		UIView *sv = [[[UIView alloc] initWithFrame:CGRectInset(CGRectMake(0, 127, 704, 1), 30, 0)] autorelease];
		sv.backgroundColor = [UIColor colorWithRed:109.0f / 255.0f green:118.0f / 255.0f blue:129.0f / 255.0f alpha:0.5f];

		[cell.contentView addSubview:sv];
	}

	// cell.backgroundColor = [UIColor colorWithWhite:indexPath.row%2 ? 0 : 1 alpha:1];

	return cell;
}

- (void)createTopview:(NSDictionary * )config {
	if (!self.upperView) {
		self.upperView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, detailRightSize, 300)] autorelease];

		self.logoImageView = [[[VSRemoteImageView alloc] initWithFrame:CGRectMake(30, 24, 150, 62)] autorelease];

		[self.logoImageView setContentMode:UIViewContentModeScaleAspectFill];
		self.logoImageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
		[self.logoImageView setClipsToBounds:YES];

		self.titleLabel = [[[UILabel alloc] initWithFrame:CGRectMake(self.logoImageView.frame.origin.x + 165, 20, 400, 40)] autorelease];
		self.ageRatingImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y + 40, 46, 17)] autorelease];
		self.subtitleLabel2 = [[[UILabel alloc] initWithFrame:CGRectMake(self.titleLabel.frame.origin.x + 53, self.titleLabel.frame.origin.y + 41, 46, 14)] autorelease];

		self.rentPriceView = [[[UIView alloc] initWithFrame:CGRectMake(self.ageRatingImageView.frame.origin.x, self.subtitleLabel2.frame.origin.y + 40, 281, 35)] autorelease];
		self.rentalPeriodView = [[[UIView alloc] initWithFrame:CGRectMake(self.ageRatingImageView.frame.origin.x, self.rentPriceView.frame.origin.y + 38, 281, 35)] autorelease];

		self.buttonAddToFavorites = [UIButton buttonWithType:UIButtonTypeCustom];
		self.buttonShare = [UIButton buttonWithType:UIButtonTypeCustom];
		self.buttonFriendsShared = [UIButton buttonWithType:UIButtonTypeCustom];
		self.buttonNextSeason = [UIButton buttonWithType:UIButtonTypeCustom];

		[self.buttonShare addTarget:self action:@selector(shareButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
		[self.buttonAddToFavorites addTarget:self action:@selector(addToFavorites:) forControlEvents:UIControlEventTouchUpInside];

		self.episodesButton = [UIButton buttonWithType:UIButtonTypeCustom];
		self.episodesButton.frame = CGRectMake(40, 155, 70, 37);
		self.episodesButton.titleLabel.textAlignment = NSTextAlignmentLeft;

		self.buttonSeparator = [[[UIView alloc] initWithFrame:CGRectMake(115, 165, 1, 15)] autorelease];
		self.buttonSeparator.backgroundColor = [UIColor grayColor];

		self.clipsButton = [UIButton buttonWithType:UIButtonTypeCustom];
		self.clipsButton.frame = CGRectMake(130, 155, 70, 37);
		self.clipsButton.titleLabel.font = [UIFont systemFontOfSize:14];
		self.clipsButton.titleLabel.textAlignment = NSTextAlignmentLeft;

		self.lineView.frame = CGRectInset(CGRectMake(0, 190, 704, 1), 30, 0);
		self.lineView = [[[UIView alloc] initWithFrame:CGRectInset(CGRectMake(0, 190, 704, 1), 30, 0)] autorelease];
		self.lineView.backgroundColor =  [UIColor colorWithRed:109.0f / 255.0f green:118.0f / 255.0f blue:129.0f / 255.0f alpha:0.5f];

		/*
		 * self.relatedView = [[[UIView alloc] initWithFrame:CGRectMake(21, 210, 704, 400)]autorelease];
		 * [self.relatedView setBackgroundColor:[UIColor clearColor]];
		 * self.relatedView.alpha = 0;
		 *
		 * self.episodesView = [[[UIView alloc] initWithFrame:CGRectMake(21, 210, 704, 400)]autorelease];
		 * [self.episodesView setBackgroundColor:[UIColor clearColor]];
		 *
		 *
		 */
		[self configureView:config];

		// [self.upperView addSubview:self.relatedView];
		[self.upperView addSubview:self.logoImageView];
		[self.upperView addSubview:self.titleLabel];
		[self.upperView addSubview:self.ratingView];
		[self.upperView addSubview:self.ageRatingImageView];
		[self.upperView addSubview:self.subtitleLabel2];
		//        [self.upperView addSubview:self.episodesView];
		[self.upperView addSubview:self.rentPriceView];
		[self.upperView addSubview:self.rentalPeriodView];
		[self.upperView addSubview:self.buttonSeparator];

		[self.upperView addSubview:self.buttonAddToFavorites];
		[self.upperView addSubview:self.buttonShare];
		[self.upperView addSubview:self.buttonFriendsShared];
		[self.upperView addSubview:self.buttonNextSeason];

		[self.upperView addSubview:self.clipsButton];
		[self.upperView addSubview:self.episodesButton];
		[self.upperView addSubview:self.lineView];
	}
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
	return 2;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	int res = 0;

	if (section == 0) {
		res = 1;
	}
	else {
		if (!self.clipsVisible) {
			res = self.episodeContent ? [self.episodeContent count] : 0;
		}
		else {
			res =  self.clipsContent ? [self.clipsContent count] : 0;
		}
	}

	// NSLog(@"SEC: %d ROWS: %d", section,res);

	return res;
}

- (void)configureView:(NSDictionary *)configuration {
	self.clipsVisible = NO;

	[self.logoImageView setImageUrl:[UIHelper getTVShowImageUrl:configuration]];

	[self.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:20]];
	NSString *title = [configuration objectForKey:@"pl1$seriesTitle"];
	if (!title) {
		title = [configuration objectForKey:@"title"];
	}
	title = [title stringByAppendingFormat:@", %@", [configuration objectForKey:@"pl1$showTime"]];

	[self.titleLabel setText:title];
	[self.titleLabel setTextColor:[UIColor whiteColor]];
	[self.titleLabel setBackgroundColor:[UIColor clearColor]];

	// NSString * rating = [configuration objectForKey:@"pl1$parentalControlRating"];
	// UIImage* ageRatingImage = [UIImage imageNamed:[NSString stringWithFormat:@"RATED_%@.png", rating]];
	UIImage *ageRatingImage = [UIImage imageNamed:@"PG-13.png"];

	self.ageRatingImageView.image = ageRatingImage;
	self.ageRatingImageView.contentMode = UIViewContentModeScaleAspectFit;

	NSDictionary *genreList = [configuration objectForKey:@"media$categories"];
	NSMutableArray *genres = [NSMutableArray array];

	for (NSDictionary *key in genreList) {
		NSString *genre = [key objectForKey:@"media$name"];
		genre = [genre stringByReplacingOccurrencesOfString:@"TV Series" withString:@""];
		genre = [genre stringByReplacingOccurrencesOfString:@"/" withString:@""];
		genre = [genre stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		if (0 < [genre length]) {
			[genres addObject:genre];
		}
	}

	NSMutableString *str = [NSMutableString string];

	for (int i = 0; i < [genres count]; ++i) {
		[str appendString:genres[i]];
		if (i != [genres count] - 1) {
			[str appendString:@", "];
		}
	}

	NSMutableString *season = [NSMutableString string];

	if ([configuration objectForKey:@"pl1$season"]) {
		[season appendFormat:@"Season %@", [configuration objectForKey:@"pl1$season"]];
	}
	else {
		if ([UIHelper getSeason:configuration]) {
			[season appendFormat:@"Season %@", [UIHelper getSeason:configuration]];
		}
	}

	if ([season length] > 0) {
		[season appendString:@" "];
	}

	NSTimeInterval pubDate = [[[configuration valueForKey:@"pubDate"] description] longLongValue] / 1000;
	NSDateFormatter *f = [[NSDateFormatter alloc] init];
	f.dateFormat = @"yyyy";
	NSString *year = [f stringFromDate:[NSDate dateWithTimeIntervalSince1970:pubDate]];
	[f release];

	[self.subtitleLabel2 setText:[season stringByAppendingFormat:@" | %@ | %@", year, str]];
	[self.subtitleLabel2 setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];
	CGSize size = [[season stringByAppendingFormat:@" | %@ | %@", year, str] sizeWithFont:self.subtitleLabel2.font];
	self.subtitleLabel2.frame = CGRectMake(self.subtitleLabel2.frame.origin.x, self.subtitleLabel2.frame.origin.y, size.width, 15);
	[self.subtitleLabel2 setTextColor:[UIColor whiteColor]];
	[self.subtitleLabel2 setBackgroundColor:[UIColor clearColor]];

	UIImage *buttonBackgroundImage = [UIImage imageNamed:@"btn.png"];
	UIImage *buttonBackgroundImageSelected = [UIImage imageNamed:@"button_selected.png"];
	self.buttonAddToFavorites.frame = CGRectMake(self.logoImageView.frame.origin.x, self.logoImageView.frame.origin.y + 78, kButtonSize.width, kButtonSize.height);
	[self.buttonAddToFavorites.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
	// [self.buttonAddToFavorites setTitle:@"Add to Favorites" forState:UIControlStateNormal];
	[self.buttonAddToFavorites setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.buttonAddToFavorites setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
	[self.buttonAddToFavorites setBackgroundImage:buttonBackgroundImageSelected forState:UIControlStateHighlighted];

	self.buttonShare.frame = CGRectMake(self.buttonAddToFavorites.frame.origin.x + kButtonSize.width + 10, self.buttonAddToFavorites.frame.origin.y, kButtonSize.width, kButtonSize.height);

	[self.buttonShare.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
	[self.buttonShare setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.buttonShare setTitle:@"Share" forState:UIControlStateNormal];
	[self.buttonShare setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
	[self.buttonShare setBackgroundImage:buttonBackgroundImageSelected forState:UIControlStateHighlighted];
	[self.buttonShare setImage:[UIImage imageNamed:@"share.png"] forState:UIControlStateNormal];
	[self.buttonShare setImage:[UIImage imageNamed:@"share.png"] forState:UIControlStateHighlighted];
	[self.buttonShare setImageEdgeInsets:UIEdgeInsetsMake(0, -105, 0, 0)];

	self.buttonFriendsShared.frame = CGRectMake(self.buttonAddToFavorites.frame.origin.x + (kButtonSize.width + 10) * 2, self.buttonAddToFavorites.frame.origin.y, kButtonSize.width, kButtonSize.height);
	[self.buttonFriendsShared addTarget:self action:@selector(friendsButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
	[self.buttonFriendsShared setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
	[self.buttonFriendsShared setBackgroundImage:buttonBackgroundImageSelected forState:UIControlStateHighlighted];
	[self.buttonFriendsShared setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.buttonFriendsShared setTitle:@"3 Friends Shared" forState:UIControlStateNormal];
	[self.buttonFriendsShared.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
	[self.buttonFriendsShared setImage:[UIImage imageNamed:@"friend_btn.png"] forState:UIControlStateNormal];
	[self.buttonFriendsShared setImage:[UIImage imageNamed:@"friend_btn.png"] forState:UIControlStateHighlighted];
	[self.buttonFriendsShared setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];


	self.buttonNextSeason.frame = CGRectMake(self.logoImageView.frame.origin.x + self.logoImageView.frame.size.width + 320, self.logoImageView.frame.origin.y - 20, kButtonSize.width, kButtonSize.height);
	[self.buttonNextSeason addTarget:self action:@selector(showNextSeason:) forControlEvents:UIControlEventTouchUpInside];
	[self.buttonNextSeason.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:16]];
	[self.buttonNextSeason setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.buttonNextSeason setTitle:@"Season 2" forState:UIControlStateNormal];
	[self.buttonNextSeason setImage:[UIImage imageNamed:@"next_season_button"] forState:UIControlStateNormal];
	[self.buttonNextSeason setImageEdgeInsets:UIEdgeInsetsMake(0, 145, 0, 0)];

	// @stef: #43 In case of only 1 season, no season 2 at top right side
	int nSeasons = 1;
	// currently hardcoded, no multiple season handling yet!
	self.buttonNextSeason.hidden = nSeasons <= 1 ? YES : NO;

	[self.episodesButton setTitle:@"Episodes" forState:UIControlStateNormal];
	self.episodesButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
	[self.episodesButton setTitleColor:[UIColor colorWithRed:35.0 / 255.0 green:117.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
	[self.episodesButton addTarget:self action:@selector(showEpisodes:) forControlEvents:UIControlEventTouchUpInside];
	self.episodesButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

	[self.clipsButton setTitle:@"Clips" forState:UIControlStateNormal];
	self.clipsButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
	[self.clipsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.clipsButton addTarget:self action:@selector(showClips:) forControlEvents:UIControlEventTouchUpInside];
	self.clipsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
	self.config = configuration;

	[self updateFavorite];
}

- (void)addToFavorites:(id)sender {
	if ([AppView sharedInstance].fingerprint) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
						   [[AppView sharedInstance] addAssetToFavorites:self.config];
					   });
	}
	else {
		[self.detaildelegate openLogin:Nil];
	}
}

- (void)updateFavorite {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   BOOL favorite = [[AppView sharedInstance] isFavoriteShow:self.config];
					   dispatch_async (dispatch_get_main_queue (), ^{
										   if (favorite) {
											   [self.buttonAddToFavorites setTitle:@"Favorite" forState:UIControlStateNormal];
											   [self.buttonAddToFavorites setTitleColor:[UIColor colorWithRed:35.0 / 255.0 green:117.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
										   }
										   else {
											   [self.buttonAddToFavorites setTitle:@"Add to Favorites" forState:UIControlStateNormal];
											   [self.buttonAddToFavorites setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
										   }
									   });
				   });
}

- (void)setContent:(NSArray * )relatedContent type:(BOOL)episodes {
	// NSLog(@"CONTENT %@", relatedContent);

	if (episodes) {
		// NSLog(@"%@", [relatedContent objectAtIndex:0]);

		self.episodeContent = relatedContent;

		// @stef: #44: Remove number behind "Episodes" and "Clips" (Medium)
		/*
		 * [self.episodesButton setTitle:[NSString stringWithFormat:@"Episodes (%d)", [self.episodeContent count]] forState:UIControlStateNormal];
		 */
	}
	else {
		/*
		 * self.clipsContent = relatedContent;
		 * [self.clipsButton setTitle:[NSString stringWithFormat:@"Clips (%d)", [self.clipsContent count]] forState:UIControlStateNormal];
		 */
	}

	[self.collectionView reloadData];
}

- (void)asetTapped:(id)sender {
	AssetView *asset = (AssetView *)sender;
	int selected = asset.tag;

	if (!self.clipsVisible) {
		BaseViewController *bvc = (BaseViewController *)self.detaildelegate;
		NSArray *entries = self.episodeContent;
		NSDictionary *content = [entries objectAtIndex:selected];
		[bvc playMovie:content isStream:NO isRemote:NO initialPosition:0 containerView:Nil];
	}
	else {
		NSArray *entries = self.clipsContent;
		NSDictionary *content = [entries objectAtIndex:selected];
		[self.detaildelegate openDetail:content withCollection:entries title:@""];
	}
}

- (void)showEpisodes:(id)sender {
	if (self.clipsVisible) {
		self.clipsVisible = NO;

		[self.episodesButton setTitleColor:[UIColor colorWithRed:35.0 / 255.0 green:117.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
		[self.clipsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

		[self.collectionView reloadData];
	}
}

- (void)showClips:(id)sender {
	if (!self.clipsVisible) {
		self.clipsVisible = YES;
		[self.clipsButton setTitleColor:[UIColor colorWithRed:35.0 / 255.0 green:117.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
		[self.episodesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[self.collectionView reloadData];
	}
}

- (void)showNextSeason:(id)sender {
}

- (void)shareButtonTouched:(id)sender {
	NSString *title = [self.config objectForKey:@"pl1$seriesTitle"];

	if (!title) {
		title = [self.config objectForKey:@"title"];
	}
	[PopupHelper showSharePopupInView:self atButton:self.buttonShare orientedToTop:NO asset:title];
}

- (void)friendsButtonTouched:(id)sender {
	[PopupHelper showFriendsPopupInView:self atButton:self.buttonFriendsShared orientedToTop:NO];
}

/*
 * // Only override drawRect: if you perform custom drawing.
 * // An empty implementation adversely affects performance during animation.
 * - (void)drawRect:(CGRect)rect
 * {
 * // Drawing code
 * }
 */

@end
