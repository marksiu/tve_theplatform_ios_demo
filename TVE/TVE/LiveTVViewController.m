//
//  LiveTVViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 11/26/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "LiveTVViewController.h"
#import "LiveView.h"
#import "Constants.h"
#import "DetailsListView.h"
#import "ThePlatform.h"
#import "AppView.h"
#import "UIImage+Extension.h"
#import "PopupHelper.h"
#import "TVEConfig.h"

@interface LiveTVViewController ()
@property (retain) LiveView *liveView;

@property (retain) NSDictionary *feed;
@property (retain) NSDictionary *subscribeItem;
@property (retain) NSString *searchPhrase;
@end

@implementation LiveTVViewController

- (id)initWithFeed:(NSDictionary *)desc {
	self = [super init];
	if (self) {
		self.feed = desc;
		// Custom initialization
	}
	return self;
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	self.listView = Nil;
	self.listView = Nil;
	self.feed = Nil;
	self.subscribeItem = Nil;
	[super dealloc];
}

- (void)fetchData {
	NSArray *entries = [[AppView sharedInstance] searchChannels:self.searchPhrase];

	NSMutableArray *entriesWithAds = [NSMutableArray array];

	int a = 1;

	for (int i = 0; i <  [entries count]; i++) {
		[entriesWithAds addObject:[entries objectAtIndex:i]];
		if ((i + 1) % 4 == 0) {
			// NSLog(@"MATCHES %d", i);
			NSString *addName = [NSString stringWithFormat:@"cad%d.png", a];
			[entriesWithAds addObject:[NSDictionary dictionaryWithObject:addName forKey:@"ad"]];

			a++;

			if (a > 4) {
				a = 1;
			}
		}
	}


	if ([entries count]) {
		[self.listView setData:entriesWithAds index:0];
	}
}

/*
 * - (void)viewDidLoad1
 * {
 *  [super viewDidLoad];
 *  [self customNavigationBar];
 *  [[NSNotificationCenter defaultCenter] addObserver:self
 *                                           selector:@selector(appLogin:)
 *                                               name:@"APP_LOGIN"
 *                                             object:nil];
 *
 *  [[NSNotificationCenter defaultCenter] addObserver:self
 *                                           selector:@selector(appLogin:)
 *                                               name:@"PAYD_CHANNELS_CHANGED"
 *                                             object:nil];
 *
 *  UIImage * backgroundImage = [UIImage externalImageNamed:@"background"];
 *  UIImageView * backgroundImageView = [[[UIImageView alloc] initWithImage: backgroundImage] autorelease];
 *  //[backgroundImage release];
 *  [self.view addSubview:backgroundImageView];
 *
 *  self.liveView = [[[LiveView alloc] initWithFrame:CGRectMake(0, 0, detailRightSize, 768)  data: Nil]autorelease];
 *  self.liveView.delegate = self;
 *
 *
 *
 *  self.listView = [[[DetailsListView alloc] initWithFrame:CGRectMake(0, 0, detailLeftSize, detailHeight)] autorelease];
 *  self.listView.listdelegate = self;
 *  self.liveView.backgroundColor = [UIColor clearColor];
 *  [self.view addSubview:self.listView];
 *  [self.view addSubview:self.liveView];
 *
 *  //[backgroundImageView release];
 *  [self fetchData];
 * }
 */

- (void)viewDidLoad {
	// [self fetchData];

	// self.view.backgroundColor = [UIColor clearColor];

	UIImage *backgroundImage;

	if ([UIImage externalImageNamed:@"background"]) {
		backgroundImage = [UIImage externalImageNamed:@"background"];
	}
	else {
		backgroundImage = [UIImage imageNamed:@"background.png"];
	}
	UIImageView *backgroundImageView = [[[UIImageView alloc] initWithImage:backgroundImage] autorelease];

	[self.view addSubview:backgroundImageView];

	[super viewDidLoad];

	[self customNavigationBar];

	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appLogout:)
												 name:@"APP_LOGOUT"
											   object:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appLogin:)
												 name:@"PAYD_CHANNELS_CHANGED"
											   object:nil];


	UIView *leftConteiner = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, detailLeftSize, detailHeight)] autorelease];
	self.leftViewController = [[[UIViewController alloc] init] autorelease];
	self.leftViewController.view = leftConteiner;
	UINavigationController *leftnav = [[[UINavigationController alloc] initWithRootViewController:self.leftViewController] autorelease];

	UIView *rightConteiner = [[[UIView alloc] initWithFrame:CGRectMake(detailLeftSize, 0, detailRightSize, detailHeight)] autorelease];
	self.rightViewController = [[[UIViewController alloc] init] autorelease];
	self.rightViewController.view = rightConteiner;
	UINavigationController *rightNav = [[[UINavigationController alloc] initWithRootViewController:self.rightViewController] autorelease];



	self.liveView = [[[LiveView alloc] initWithFrame:CGRectMake(0, 0, detailRightSize, 768)  data:Nil] autorelease];
	self.liveView.delegate = self;

	self.listView = [[[DetailsListView alloc] initWithFrame:CGRectMake(0, 0, detailLeftSize, detailHeight)] autorelease];
	self.listView.listdelegate = self;
	self.liveView.backgroundColor = [UIColor clearColor];

	[self fetchData];

	[rightConteiner addSubview:self.liveView];
	[leftConteiner addSubview:self.listView];

	[self addChildViewController:leftnav];
	[self.view addSubview:leftnav.view];


	[self addChildViewController:rightNav];
	[self.view addSubview:rightNav.view];

	// @stef: Missed a separated tab "l" on top title bar between listing part and details content part
	CGRect sr = self.leftViewController.view.frame;
	sr.origin.x = sr.size.width - 2;
	sr.size.width = 2;
	sr.size.height = 21;

	UIView *sv = [[UIView alloc] initWithFrame:sr];
	UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top_bar_separtor.png"]];
	iv.clipsToBounds = YES;
	iv.contentMode = UIViewContentModeScaleAspectFit;

	[sv addSubview:iv];

	[self.leftViewController.navigationController.view addSubview:sv];

	[sv release];
	[iv release];

	// bg image adjust
	CGRect bf = backgroundImageView.frame;
	bf.origin.y = self.leftViewController.navigationController.navigationBar.frame.size.height;
	backgroundImageView.frame = bf;
}

- (void)viewWillAppear:(BOOL)animated {
	// NSLog(@"%@",[self.view.subviews objectAtIndex:0]);

	// only for message show only
	TVEConfig *config = [TVEConfig sharedConfig];

	[config updateShowSubscriptionMessage:YES];


	self.navigationController.navigationBarHidden = YES;

	self.leftViewController.navigationController.view.frame = CGRectMake(0, 0, detailLeftSize, self.leftViewController.navigationController.view.frame.size.height);
	self.rightViewController.navigationController.view.frame = CGRectMake(320, 0, 1024 - 320, self.rightViewController.navigationController.view.frame.size.height);


	UINavigationBar *navBar = [self.rightViewController.navigationController navigationBar];
	[navBar setTintColor:[UIColor blackColor]];



	if ([navBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
		[navBar setBackgroundImage:[UIImage imageNamed:@"top.png"] forBarMetrics:UIBarMetricsDefault];
	}
	else {
		UIImageView *imageView = (UIImageView *)[navBar viewWithTag:12000];
		if (imageView == nil) {
			imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top.png"]];
			[imageView setTag:12000];
			[navBar insertSubview:imageView atIndex:0];
			[imageView release];
		}
	}


	navBar = [self.leftViewController.navigationController navigationBar];
	[navBar setTintColor:[UIColor blackColor]];



	if ([navBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
		[navBar setBackgroundImage:[UIImage imageNamed:@"top.png"] forBarMetrics:UIBarMetricsDefault];
	}
	else {
		UIImageView *imageView = (UIImageView *)[navBar viewWithTag:12000];
		if (imageView == nil) {
			imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top.png"]];
			[imageView setTag:12000];
			[navBar insertSubview:imageView atIndex:0];
			[imageView release];
		}
	}

	if (self.title) {
		self.leftViewController.navigationItem.title = self.title;
	}


	UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[settingsButton addTarget:self action:@selector(openSettings) forControlEvents:UIControlEventTouchUpInside];
	[settingsButton setBackgroundImage:[UIImage imageNamed:@"setting.png"] forState:UIControlStateNormal];
	settingsButton.frame = CGRectMake(0, 0, 23, 23);
	UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[searchButton addTarget:self action:@selector(openSearch) forControlEvents:UIControlEventTouchUpInside];
	[searchButton setBackgroundImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
	searchButton.frame = CGRectMake(53, 2, 21, 21);


	UIView *parentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 23)];
	[parentView addSubview:settingsButton];
	[parentView addSubview:searchButton];
	parentView.backgroundColor = [UIColor clearColor];
	UIBarButtonItem *customBarButtomItem = [[UIBarButtonItem alloc] initWithCustomView:parentView];
	[parentView release];
	[self.rightViewController.navigationItem setRightBarButtonItem:customBarButtomItem animated:YES];
	[customBarButtomItem release];

	self.leftViewController.navigationItem.title = @"Channels";
	self.rightViewController.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage externalImageNamed:@"logo"]];
	/*
	 * self.leftViewController.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Categories" style:UIBarButtonItemStyleDone target:self action:@selector(showCategories:)]autorelease];
	 */

	[self addHomeChannelButton];

	// [[self.view.subviews objectAtIndex:0] setFrame:CGRectMake(0, 0, 1024, 768)];
}

- (void)startVideoIfPossible {
	NSLog(@"START LIVE TV VIDEO");
	if (self.listView) {
		[self.listView reloadSelected];
	}
	else {
		NSLog(@"NO LIST TO SELECT");
	}
}

- (void)showCategories:(id)sender {
}

- (void)homeChannel:(id)sender {
	NSString *homeChannelId =  [[AppView sharedInstance] getHomeChannel];

	NSLog(@"HOME CHANNEL: %@", homeChannelId);
	NSDictionary *homeChannel = [[AppView sharedInstance] channelByChannelId:homeChannelId];

	if (!homeChannelId || !homeChannel) {
		NSLog(@"SETTING FIRST CHANNEL AS HOME CHANNEL");
		homeChannel = [[[AppView sharedInstance] allChannels] objectAtIndex:0];
	}
	if (homeChannel) {
		[self.listView jumpToChannel:[homeChannel objectForKey:@"channel"]];
	}
}

- (void)addHomeChannelButton {
	if ([AppView sharedInstance].fingerprint) {
		self.rightViewController.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Home Channel" style:UIBarButtonItemStylePlain target:self action:@selector(homeChannel:)] autorelease];
	}
	else {
		self.rightViewController.navigationItem.leftBarButtonItem = Nil;
	}
}

- (void)openSearch {
	SearchViewController *content = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];

	content.delegate = self;
	UIPopoverController *controller = [[UIPopoverController alloc] initWithContentViewController:content];
	content.popover = controller;
	controller.popoverContentSize = CGSizeMake(334, 44);
	// controller.contentViewController.contentSizeForViewInPopover = CGSizeMake(337, 290);
	// content.contentSizeForViewInPopover = CGSizeMake(334, 400);

	[controller presentPopoverFromBarButtonItem:self.rightViewController.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
}

- (void)appLogin:(id)sender {
	[self.listView reloadItems];
	[self addHomeChannelButton];
}

- (void)appLogout:(id)sender {
	[self closePlayer];
	[self addHomeChannelButton];
	[self.listView reloadItems];
}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
	NSLog(@"Live TV View Controller viewDidDisappear");

	TVEConfig *config = [TVEConfig sharedConfig];
	[config updateShowSubscriptionMessage:NO];
}

- (void)itemSelected:(NSDictionary *)item {
	NSLog(@"ITEM SELECTED");
	[self closePlayer];
	NSNumber *pay = [item objectForKey:@"pay"];

	if (pay && [pay boolValue]) {
		self.subscribeItem = item;


		if ([AppView sharedInstance].fingerprint) {
			NSArray *paidChannels = [[AppView sharedInstance] getPaidChannels];


			NSNumber *chan = [NSNumber numberWithInt:[[item objectForKey:@"channel"] intValue]];

			// NSLog(@"CHANNEL IS PAYD: %@ %@", chan, paidChannels);

			if (![paidChannels containsObject:chan]) {
				[PopupHelper showSubscribeAlert:self];
			}
			else {
				[self.liveView configureView:item];
				if (self.navigationController == ((UITabBarController *)self.view.window.rootViewController).selectedViewController && self.navigationController.topViewController == self) {
					[self playEmbedded:item inView:self.liveView.movieConteiner];
				}
			}
		}
		else {
			[self openLogin:Nil];
		}
	}
	else {
		[self.liveView configureView:item];
		NSLog(@" %@", ((UITabBarController *)[UIApplication sharedApplication].delegate.window.rootViewController).selectedViewController);
		if (self.navigationController == ((UITabBarController *)[UIApplication sharedApplication].delegate.window.rootViewController).selectedViewController) {
			if (self.navigationController.topViewController == self) {
				[self playEmbedded:item inView:self.liveView.movieConteiner];
			}
			else {
				NSLog(@"NOT ON TOP");
			}
		}
		else {
			NSLog(@"NOT SELECTED");
		}
	}
}

- (void)pinCorrect:(NSDictionary *)item {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   [[AppView sharedInstance] addChannelToPaidChannels:item];


					   dispatch_async (dispatch_get_main_queue (), ^{
	                                       // [[NSNotificationCenter defaultCenter] postNotificationName:@"CHANNEL_SUBSCRIBED" object:self userInfo:item];

	                                       // [self.listView reloadSelected];

										   UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"Subscription Succesful" message:[NSString stringWithFormat:@"Your subscription was confirmed. You can now watch %@", [item objectForKey:@"name"]] delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease ];
										   [alertView show];
									   });
				   });
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 1) {
		[PopupHelper showSubscribePinPopupForItem:self.subscribeItem delegate:self];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

@end
