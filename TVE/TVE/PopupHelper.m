//
//  PopupHelper.m
//  TVE
//
//  Created by Mate Beres on 12/12/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "PopupHelper.h"
#import "ShareViewController.h"
#import "RatingViewController.h"
#import "FilterViewController.h"
#import "FriendsPopupViewController.h"
#import "ProgramInfoPopupViewController.h"
#import "PinViewController.h"
#import "SortViewController.h"
#import <QuartzCore/QuartzCore.h>

#import "Constants.h"
@implementation PopupHelper

+ (void)showSharePopupInView:(UIView *)view atButton:(UIButton *)button orientedToTop:(BOOL)orientation asset:(NSString *)title {
	ShareViewController *vc = [[[ShareViewController alloc] initWithAssetTitle:title] autorelease];
	CGRect f = [button convertRect:button.bounds toView:view];
	UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:vc];

	popover.popoverContentSize = CGSizeMake(140, 45);

	if (orientation) {
		[popover presentPopoverFromRect:f inView:view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
	}
	else {
		[popover presentPopoverFromRect:f inView:view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
	}
}

+ (void)showRatePopupInView:(UIView *)view atButton:(UIButton *)button delegate:(id <RatingViewControllerDelegate> )delegate {
	RatingViewController *vc = [[RatingViewController alloc] init];

	vc.delegate = delegate;

	CGRect f = [button convertRect:button.bounds toView:view];

	UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:vc];

	// popover.popoverContentSize = CGSizeMake(280, 90);
	[popover presentPopoverFromRect:f inView:view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

+ (void)showFriendsPopupInView:(UIView *)view atButton:(UIButton *)button orientedToTop:(BOOL)orientation {
	FriendsPopupViewController *vc = [[[FriendsPopupViewController alloc] init] autorelease];


	CGRect f = [button convertRect:button.bounds toView:view];

	UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:vc];

	// popover.popoverContentSize = CGSizeMake(280, 90);
	if (orientation) {
		[popover presentPopoverFromRect:f inView:view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
	}
	else {
		[popover presentPopoverFromRect:f inView:view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
	}
}

+ (void)showFilterPopupInView:(UIView *)view atButton:(UIButton *)button withData:(NSArray *)data delegate:(id)delegate {
	FilterViewController *vc = [[[FilterViewController alloc] initWithData:data delegate:delegate] autorelease];
	CGRect f = [button convertRect:button.bounds toView:view];
	UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:vc];

	[vc setPopover:popover];
	popover.popoverContentSize = CGSizeMake(300, 400);
	[popover presentPopoverFromRect:f inView:view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

+ (void)showSortPopupInView:(UIView *)view atButton:(UIButton *)button withData:(NSArray *)data delegate:(id)delegate {
	SortViewController *vc = [[[SortViewController alloc] initWithData:data delegate:delegate] autorelease];
	CGRect f = [button convertRect:button.bounds toView:view];
	UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:vc];

	[vc setPopover:popover];
	popover.popoverContentSize = CGSizeMake(300, 400);
	[popover presentPopoverFromRect:f inView:view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

+ (void)showProgramInfo:(NSDictionary *)program viewController:(UIViewController *)vc {
	ProgramInfoPopupViewController *set = [[ProgramInfoPopupViewController alloc] initWithNibName:@"ProgramInfoPopupViewController" bundle:Nil];

	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:set];

	nav.modalPresentationStyle = UIModalPresentationFormSheet;

	nav.view.layer.cornerRadius = 0;
	[vc presentViewController:nav animated:YES completion:^{}];

	nav.view.superview.frame = CGRectMake (0, 100, 700, 271);
	CGPoint p = vc.view.center;
	nav.view.superview.center = CGPointMake (p.x, p.y + 50);

	[nav release];
	[set release];
}

+ (void)showSubscribeAlert:(UIViewController *)vc {
	UIAlertView *alert = [[[UIAlertView alloc] initWithTitle:@"Sorry" message:@"This is a subscription only TV Channel. Select Subscribe below to view now!" delegate:vc cancelButtonTitle:@"Cancel" otherButtonTitles:@"Subscribe", nil] autorelease];

	[alert show];
}

+ (void)showSubscribePinPopupForItem:(NSDictionary * )item
							delegate:(id)delegate {
	PinViewController *set = [[[PinViewController alloc] initWithNibName:@"SubscribePinViewController"
																  bundle:Nil] autorelease];

	set.item = item;
	set.delegate = delegate;
	[self showPinPopupForItem:item
					 withRoot:set
						 size:CGSizeMake (540, 225)
					 delegate:delegate];
}

+ (void)showRentalPinPopupForItem:(NSDictionary * )item
						 delegate:(id)delegate {
	PinViewController *set = [[[PinViewController alloc] initWithNibName:@"RentalPinViewController"
																  bundle:Nil] autorelease];

	set.item = item;
	set.delegate = delegate;
	[self showPinPopupForItem:item
					 withRoot:set
						 size:CGSizeMake (700, 231)
					 delegate:delegate];
}

+ (void)showPinPopupForItem:(NSDictionary * )item
				   withRoot:(UIViewController *)set
					   size:(CGSize)size
				   delegate:(id)delegate {
	UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:set] autorelease];

	nav.modalPresentationStyle = UIModalPresentationFormSheet;
	nav.navigationItem.title = @"Enter Payment PIN";

	UIView *parentView = Nil;

	if ([delegate isKindOfClass:[UIView class]]) {
		parentView = ((UIView *)delegate).window.rootViewController.view;
	}
	if ([delegate isKindOfClass:[UIViewController class]]) {
		parentView = ((UIViewController *)delegate).view;
	}

	[CURRENT_WINDOW.rootViewController presentViewController:nav
													animated:YES
												  completion:nil];

	nav.view.superview.frame = CGRectMake(0, 0, size.width, size.height + nav.navigationBar.bounds.size.height);
	nav.view.superview.center = CGPointMake(512, 374);
}

@end
