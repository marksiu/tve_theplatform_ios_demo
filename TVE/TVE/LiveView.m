//
//  LiveView.m
//  TVE
//
//  Created by Mate Beres on 12/4/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "LiveView.h"
#import "Constants.h"
#import "ShareViewController.h"
#import "PopupHelper.h"
#import "AssetView.h"
#import "AppView.h"
#import "VSRemoteImageView.h"
@interface LiveView ()
@property (retain) UIButton *buttonShare;
@property (retain) UIButton *buttonAddToFavorites;
@property (retain) UIButton *buttonFriendsShared;
@property (retain) UIButton *buttonMakeHomeChannel;
@property (retain) NSDictionary *config;


extern const CGSize kButtonSize;

@end

@implementation LiveView

- (id)initWithFrame:(CGRect)frame data:(NSDictionary * )configuration {
	self = [super initWithFrame:frame];
	if (self) {
		[self configureView:Nil];


		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(updateHomeChannel)
													 name:@"HOME_CHANNEL_CHANGED"
												   object:nil];


		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(updateFavorite)
													 name:@"FAVORITE_CHANNEL_CHANGED"
												   object:nil];


		//     [buttonShare release];
	}
	return self;
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	self.movieConteiner = Nil;
	self.buttonAddToFavorites = Nil;
	self.buttonFriendsShared = Nil;
	self.buttonMakeHomeChannel = Nil;
	self.buttonShare = Nil;
	[super dealloc];
}

- (void)configureView:(NSDictionary *)item {
	float lblFS = 15.0f;
	float titleFS = 19.0f;
	float subtitleFS = 14.5f;

	for (UIView *v in [[[self subviews] copy] autorelease]) {
		[v removeFromSuperview];
	}

	// NSLog(@"%@", item);

	UIView *channelContentView = [[UIView alloc] initWithFrame:CGRectMake(1, 0, 704, 370)];
	channelContentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];

	UIImageView *live = [[UIImageView alloc] initWithFrame:CGRectInset(CGRectMake(0, 0, channelContentView.frame.size.width, channelContentView.frame.size.height), 5, 5)];
	live.image = [UIImage imageNamed:@"live.png"];
	live.clipsToBounds = YES;
	// live.backgroundColor = [UIColor greenColor];
	live.contentMode = UIViewContentModeScaleAspectFill;


	[channelContentView addSubview:live];
	[live release];

	if (!self.movieConteiner) {
		self.movieConteiner = [[[UIView alloc] initWithFrame:CGRectInset(CGRectMake(0, 0, channelContentView.frame.size.width, channelContentView.frame.size.height), 5, 5)] autorelease];
	}

	self.movieConteiner.backgroundColor = [UIColor blackColor];
	[channelContentView addSubview:self.movieConteiner];

	[self addSubview:channelContentView];
	[channelContentView release];

	UIView *channelInfoContentView = [[UIView alloc] initWithFrame:CGRectInset(CGRectMake(30, channelContentView.frame.size.height + 5, 640, channelHeight), 2, 2)];
	channelInfoContentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];

	UILabel *channelNumber = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 100, 20)];
	channelNumber.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:lblFS];
	channelNumber.text = [NSString stringWithFormat:@"CH %@", [item objectForKey:@"channel"]];
	channelNumber.backgroundColor = [UIColor clearColor];
	channelNumber.textColor = [UIColor colorWithRed:141.0f / 255.0f green:141.0f / 255.0f blue:141.0f / 255.0f alpha:1.0f];
	[channelInfoContentView addSubview:channelNumber];
	[channelNumber release];

	UILabel *channelName = [[UILabel alloc] initWithFrame:CGRectMake(10, 25, 200, 20)];
	channelName.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:lblFS];
	channelName.text = [item objectForKey:@"name"];
	channelName.backgroundColor = [UIColor clearColor];
	channelName.textColor = [UIColor colorWithRed:141.0f / 255.0f green:141.0f / 255.0f blue:141.0f / 255.0f alpha:1.0f];
	[channelInfoContentView addSubview:channelName];
	[channelName release];

	NSString *imageUrl = [item objectForKey:@"logo"];

	VSRemoteImageView *imageView = [[VSRemoteImageView alloc] initWithFrame:CGRectMake(100, 10, 165, 40)];
	[imageView setContentMode:UIViewContentModeCenter];
	imageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
	[imageView setImageUrl:imageUrl withPlaceholder:nil animated:YES];


	[channelInfoContentView addSubview:imageView];
	// imageView.center = CGPointMake(channelInfoContentView.center.x, imageView.center.y);
	imageView.center = CGPointMake(channelInfoContentView.center.x, channelInfoContentView.frame.size.height / 2);
	[imageView release];

	/*
	 * UIImageView * channelLogo = [[UIImageView alloc] initWithFrame:CGRectMake(140, 15, 60, 17)];
	 * channelLogo.image = [UIImage imageNamed:@"espn_logo.png"];
	 * [channelInfoContentView addSubview:channelLogo];
	 * [channelLogo release];
	 */
	[self addSubview:channelInfoContentView];
	[channelInfoContentView release];

	UIView *nowContentView = [[UIView alloc] initWithFrame:CGRectInset(CGRectMake(30, channelInfoContentView.frame.origin.y + channelHeight, smallliveTVWidth - 10, nextHeight), 2, 2)];
	nowContentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];

	NSDictionary *currentProgram = [[AppView sharedInstance] currentEPGProgramOfChannel:item programsOffset:0];
	NSDictionary *nextProgram = [[AppView sharedInstance] currentEPGProgramOfChannel:item programsOffset:1];
	NSDictionary *afterNextProgram = [[AppView sharedInstance] currentEPGProgramOfChannel:item programsOffset:2];

	// NSLog(@"%@", currentProgram);

	UILabel *nowText = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, smallliveTVWidth - 24, 20)];
	nowText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:lblFS];
	nowText.text = @"ON NOW";
	nowText.textColor = [UIColor whiteColor];
	// [UIColor colorWithRed:85.0/255.0 green:175.0/255.0 blue:236.0/255.0 alpha:1.0];
	nowText.backgroundColor = [UIColor clearColor];
	[nowContentView addSubview:nowText];
	[nowText release];

	UILabel *nowTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 22, smallliveTVWidth - 24, 30)];
	nowTitle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:titleFS];
	nowTitle.text = [currentProgram objectForKey:@"title"];
	nowTitle.textColor = [UIColor whiteColor];
	nowTitle.backgroundColor = [UIColor clearColor];
	[nowContentView addSubview:nowTitle];
	[nowTitle release];

	UILabel *timeText = [[UILabel alloc] initWithFrame:CGRectMake(10, 49, smallliveTVWidth - 24, 20)];
	timeText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:subtitleFS];
	timeText.text = [[AppView sharedInstance] getEPGPeriod:currentProgram];
	timeText.textColor = [UIColor colorWithRed:141.0f / 255.0f green:141.0f / 255.0f blue:141.0f / 255.0f alpha:1.0f];
	timeText.backgroundColor = [UIColor clearColor];
	[nowContentView addSubview:timeText];
	[timeText release];

	/*
	 *
	 * CGSize lastLabelSize = [timeText.text sizeWithFont:timeText.font];
	 * int watchNumber =[[item objectForKey:@"watching"] intValue];
	 * //    rand() % 10;
	 * if (watchNumber > 0){
	 *
	 *  UIView * watchView = [[[UIView alloc] initWithFrame:CGRectMake(25+ lastLabelSize.width, 51, liveTVWidth, 20)]autorelease];
	 *
	 *
	 *  UILabel * watchText = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, itemRectSize.width-(itemImageRectSize.width+30), friendIconSize.height)]autorelease];
	 *  watchText.text = [NSString stringWithFormat: @"%d", watchNumber];
	 *  watchText.textColor = [UIColor colorWithRed:198.0/255.0 green:198.0/255.0 blue:198.0/255.0 alpha:1.0];
	 *  watchText.backgroundColor = [UIColor clearColor];
	 *  [watchView addSubview:watchText];
	 *
	 *  UIImage * watchImage = [UIImage imageNamed:@"friend_icon.png"];
	 *  UIImageView * friends = [[[UIImageView alloc] initWithFrame:CGRectMake(20, 3, friendIconSize.width, friendIconSize.height)] autorelease];
	 *  friends.image = watchImage;
	 *  [watchView addSubview:friends];
	 *
	 *
	 *  //[watchText release];
	 *
	 *  [nowContentView addSubview:watchView];
	 *  //   [watchView release];
	 * }
	 *
	 */

	[self addSubview:nowContentView];
	[nowContentView release];


	UIView *nextContentView = [[UIView alloc] initWithFrame:CGRectInset(CGRectMake(smallliveTVWidth + 25, channelInfoContentView.frame.origin.y + channelHeight, smallliveTVWidth, nextHeight), 2, 2)];
	nextContentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];

	UILabel *nextText = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, smallliveTVWidth - 24, 20)];
	nextText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:lblFS];
	nextText.text = @"UP NEXT";
	nextText.textColor = [UIColor whiteColor];
	// [UIColor colorWithRed:85.0/255.0 green:175.0/255.0 blue:236.0/255.0 alpha:1.0];
	nextText.backgroundColor = [UIColor clearColor];
	[nextContentView addSubview:nextText];
	[nextText release];

	UILabel *nextTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 22, smallliveTVWidth - 24, 30)];
	nextTitle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:titleFS];
	nextTitle.text = [nextProgram objectForKey:@"title"];
	nextTitle.textColor = [UIColor whiteColor];
	nextTitle.backgroundColor = [UIColor clearColor];
	[nextContentView addSubview:nextTitle];
	[nextTitle release];

	UILabel *nextTimeText = [[UILabel alloc] initWithFrame:CGRectMake(10, 49, smallliveTVWidth - 24, 20)];
	nextTimeText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:subtitleFS];
	nextTimeText.text = [[AppView sharedInstance] getEPGPeriod:nextProgram];
	nextTimeText.textColor = [UIColor colorWithRed:141.0f / 255.0f green:141.0f / 255.0f blue:141.0f / 255.0f alpha:1.0f];
	nextTimeText.backgroundColor = [UIColor clearColor];
	[nextContentView addSubview:nextTimeText];
	[nextTimeText release];

	[self addSubview:nextContentView];
	[nextContentView release];


	UIView *laterContentView = [[UIView alloc] initWithFrame:CGRectInset(CGRectMake(smallliveTVWidth * 2 + 30, channelInfoContentView.frame.origin.y + channelHeight, smallliveTVWidth, nextHeight), 2, 2)];
	laterContentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];

	UILabel *laterText = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, smallliveTVWidth - 24, 20)];
	laterText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:lblFS];
	laterText.text = @"LATER ON";
	laterText.textColor = [UIColor whiteColor];
	laterText.backgroundColor = [UIColor clearColor];
	[laterContentView addSubview:laterText];
	[laterText release];

	UILabel *laterTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 22, smallliveTVWidth - 24, 30)];
	laterTitle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:titleFS];
	laterTitle.text = [afterNextProgram objectForKey:@"title"];
	laterTitle.textColor = [UIColor whiteColor];
	laterTitle.backgroundColor = [UIColor clearColor];
	[laterContentView addSubview:laterTitle];
	[laterTitle release];

	UILabel *laterTimeText = [[UILabel alloc] initWithFrame:CGRectMake(10, 49, smallliveTVWidth - 24, 20)];
	laterTimeText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:subtitleFS];
	laterTimeText.text = [[AppView sharedInstance] getEPGPeriod:afterNextProgram];
	laterTimeText.textColor = [UIColor colorWithRed:141.0f / 255.0f green:141.0f / 255.0f blue:141.0f / 255.0f alpha:1.0f];
	laterTimeText.backgroundColor = [UIColor clearColor];
	[laterContentView addSubview:laterTimeText];
	[laterTimeText release];

	[self addSubview:laterContentView];
	[laterContentView release];

	UIImage *buttonBackgroundImage = [UIImage imageNamed:@"btn_right.png"];
	UIImage *buttonBackgroundImageSelected = [UIImage imageNamed:@"button_selected.png"];

	if (!self.buttonMakeHomeChannel) {
		self.buttonMakeHomeChannel = [UIButton buttonWithType:UIButtonTypeCustom];
		self.buttonMakeHomeChannel.frame = CGRectMake(145, 520, kButtonSize.width, kButtonSize.height);
		[self.buttonMakeHomeChannel.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
		[self.buttonMakeHomeChannel setTitle:@"Set Home Channel" forState:UIControlStateNormal];
		[self.buttonMakeHomeChannel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[self.buttonMakeHomeChannel setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
		[self.buttonMakeHomeChannel setBackgroundImage:buttonBackgroundImageSelected forState:UIControlStateHighlighted];
		[self.buttonMakeHomeChannel addTarget:self action:@selector(makeHomeChannel:) forControlEvents:UIControlEventTouchUpInside];
	}

	[self addSubview:self.buttonMakeHomeChannel];

	if (!self.buttonAddToFavorites) {
		self.buttonAddToFavorites = [UIButton buttonWithType:UIButtonTypeCustom];
		self.buttonAddToFavorites.frame = CGRectMake(self.buttonMakeHomeChannel.frame.origin.x + kButtonSize.width + 10, self.buttonMakeHomeChannel.frame.origin.y, kButtonSize.width, kButtonSize.height);
		[self.buttonAddToFavorites.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16]];
		[self.buttonAddToFavorites setTitle:@"Add To Favorites" forState:UIControlStateNormal];
		[self.buttonAddToFavorites setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[self.buttonAddToFavorites setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
		[self.buttonAddToFavorites addTarget:self action:@selector(addToFavorites:) forControlEvents:UIControlEventTouchUpInside];
	}

	[self addSubview:self.buttonAddToFavorites];

	if (!self.buttonShare) {
		self.buttonShare = [UIButton buttonWithType:UIButtonTypeCustom];
		[self.buttonShare addTarget:self action:@selector(shareButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
		self.buttonShare.frame = CGRectMake(self.buttonMakeHomeChannel.frame.origin.x, self.buttonMakeHomeChannel.frame.origin.y + kButtonSize.height + 5, kButtonSize.width, kButtonSize.height);
		[self.buttonShare.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16]];
		[self.buttonShare setTitle:@"Share" forState:UIControlStateNormal];
		[self.buttonShare setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

		[self.buttonShare setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
		[self.buttonShare setBackgroundImage:buttonBackgroundImageSelected forState:UIControlStateHighlighted];
		[self.buttonShare setImage:[UIImage imageNamed:@"share.png"] forState:UIControlStateNormal];
		[self.buttonShare setImage:[UIImage imageNamed:@"share.png"] forState:UIControlStateHighlighted];
		[self.buttonShare setImageEdgeInsets:UIEdgeInsetsMake(0, -105, 0, 0)];
	}

	[self addSubview:self.buttonShare];
	self.config = item;

	// [self updateFavorite];

	if (!self.buttonFriendsShared) {
		self.buttonFriendsShared = [UIButton buttonWithType:UIButtonTypeCustom];
		self.buttonFriendsShared.frame = CGRectMake(self.buttonShare.frame.origin.x + kButtonSize.width + 10, self.buttonShare.frame.origin.y, kButtonSize.width, kButtonSize.height);
		[self.buttonFriendsShared addTarget:self action:@selector(friendsButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
		[self.buttonFriendsShared setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
		[self.buttonFriendsShared setBackgroundImage:buttonBackgroundImageSelected forState:UIControlStateHighlighted];
		[self.buttonFriendsShared setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[self.buttonFriendsShared setTitle:@"3 Friends Watching" forState:UIControlStateNormal];
		[self.buttonFriendsShared.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
		[self.buttonFriendsShared setImage:[UIImage imageNamed:@"friend_btn.png"] forState:UIControlStateNormal];
		[self.buttonFriendsShared setImage:[UIImage imageNamed:@"friend_btn.png"] forState:UIControlStateHighlighted];
		[self.buttonFriendsShared setImageEdgeInsets:UIEdgeInsetsMake(0, -25, 0, 0)];
	}

	[self addSubview:self.buttonFriendsShared];

	[self updateFavorite];
	[self updateHomeChannel];
}

- (void)addToFavorites:(id)sender {
	if ([AppView sharedInstance].fingerprint) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
						   [[AppView sharedInstance] addChannelToFavorites:self.config];
		                   // [self updateFavorite];
					   });
	}
	else {
		[self.delegate openLogin:Nil];
	}
}

- (void)makeHomeChannel:(id)sender {
	if ([AppView sharedInstance].fingerprint) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
						   [[AppView sharedInstance] setHomeChannel:[self.config objectForKey:@"channel"]];
		                   // [self updateHomeChannel];
					   });
	}
	else {
		[self.delegate openLogin:Nil];
	}
}

- (void)updateHomeChannel {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   NSString *homeChannel = [[AppView sharedInstance] getHomeChannel];

					   dispatch_async (dispatch_get_main_queue (), ^{
	                                       // NSLog(@"%@ %@", homeChannel, self.config);
										   if ([homeChannel isEqualToString:[self.config objectForKey:@"channel"]]) {
											   [self.buttonMakeHomeChannel setTitle:@"Home Channel" forState:UIControlStateNormal];
											   [self.buttonMakeHomeChannel setTitleColor:[UIColor colorWithRed:35.0 / 255.0 green:117.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
										   }
										   else {
											   [self.buttonMakeHomeChannel setTitle:@"Set Home Channel" forState:UIControlStateNormal];
											   [self.buttonMakeHomeChannel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
										   }
									   });
				   });
}

- (void)updateFavorite {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   BOOL favorite = [[AppView sharedInstance] isFavoriteChannel:self.config];
					   dispatch_async (dispatch_get_main_queue (), ^{
										   if (favorite) {
											   [self.buttonAddToFavorites setTitle:@"Favorite" forState:UIControlStateNormal];
											   [self.buttonAddToFavorites setTitleColor:[UIColor colorWithRed:35.0 / 255.0 green:117.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
										   }
										   else {
											   [self.buttonAddToFavorites setTitle:@"Add to Favorites" forState:UIControlStateNormal];
											   [self.buttonAddToFavorites setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
										   }
									   });
				   });
}

- (void)shareButtonTouched:(id)sender {
	[PopupHelper showSharePopupInView:self atButton:self.buttonShare orientedToTop:YES asset:[self.config objectForKey:@"name"]];
}

- (void)friendsButtonTouched:(id)sender {
	[PopupHelper showFriendsPopupInView:self atButton:self.buttonFriendsShared orientedToTop:YES];
}

@end
