//
//  LiveTVViewController.h
//  TVE
//
//  Created by Gabor Bottyan on 11/26/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DetailsListView.h"
@interface LiveTVViewController : BaseViewController <DetailsListViewDelegate>{
}
@property (retain) DetailsListView *listView;
@property (retain) UIViewController *leftViewController;
@property (retain) UIViewController *rightViewController;

- (id)initWithFeed:(NSDictionary *)desc;
@end
