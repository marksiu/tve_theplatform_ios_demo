//
//  TVGuideViewController.h
//  TVE
//
//  Created by Gabor Bottyan on 11/26/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//
#define VS_DEBUG_LOG_EPG 1
#import "VSEPGGridView.h"

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface TVGuideViewController : BaseViewController




@property (assign, readonly) VSEPGGridView *gridView;
@property (retain) NSArray *channels;
@property (retain) IBOutlet UINavigationBar *navBar;

- (void)reload;
- (int)getSelectedDay;
- (void)daySelected:(int)selected withLabel:(NSString *)text;

@end
