//
//  ConnectViewController.h
//  TVE
//
//  Created by Gabor Bottyan on 12/10/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <AccedoConnect/AccedoConnect.h>
#import "UIViewController+TVEMoviePlayer.h"

@interface ConnectViewController : SmartTVViewController

@property (retain, nonatomic) CMOREPlayerViewController *cmorePlayer;
@property (nonatomic, retain) NSTimer *remoteTimeoutTimer;

@end
