//
//  ConnectViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 12/10/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "ConnectViewController.h"
#import "SettingsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage+Extension.h"

#import "Constants.h"

@interface ConnectViewController ()

@end

@implementation ConnectViewController

@synthesize cmorePlayer;
@synthesize remoteTimeoutTimer;

- (void)customNavigationBar {
	UINavigationBar *navBar = [self.navigationController navigationBar];

	[navBar setTintColor:[UIColor blackColor]];

	if ([navBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
		[navBar setBackgroundImage:[UIImage imageNamed:@"top.png"] forBarMetrics:UIBarMetricsDefault];
	}
	else {
		UIImageView *imageView = (UIImageView *)[navBar viewWithTag:12000];

		if (imageView == nil) {
			imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top.png"]];
			[imageView setTag:12000];
			[navBar insertSubview:imageView atIndex:0];
			[imageView release];
		}
	}

	UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[settingsButton addTarget:self action:@selector(openSettings)
			 forControlEvents:UIControlEventTouchUpInside];
	[settingsButton setBackgroundImage:[UIImage imageNamed:@"setting.png"] forState:UIControlStateNormal];
	settingsButton.frame = CGRectMake(-10, 0, 23, 23);

	UIView *parentView = [[UIView alloc] initWithFrame:CGRectMake(-10, 0, 23, 23)];
	[parentView addSubview:settingsButton];
	parentView.backgroundColor = [UIColor clearColor];
	UIBarButtonItem *customBarButtomItem = [[UIBarButtonItem alloc] initWithCustomView:parentView];
	[parentView release];

	[self.navigationItem setRightBarButtonItem:customBarButtomItem animated:YES];
	[customBarButtomItem release];

	self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage externalImageNamed:@"logo"]];
}

- (void)openSettings {
	SettingsViewController *set = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:Nil];

	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:set];

	nav.modalPresentationStyle = UIModalPresentationFormSheet;
	// UIModalPresentationPageSheet;
	// nav.navigationItem.title = @"Settings";

	nav.view.layer.cornerRadius = 0;
	[CURRENT_WINDOW.rootViewController presentViewController:nav animated:YES completion:^{}];

	nav.view.superview.frame = CGRectMake (0, 100, 510, 540);
	CGPoint p = self.view.center;
	nav.view.superview.center = CGPointMake (p.x, p.y + 50);

	[nav release];
	[set release];
}

- (void)switchToLocalPlaybackWithPosUpdate:(BOOL)posUpdate {
	ACStatusRecorder *statusRecorder = [ACManager sharedInstance].statusRecorder;

	[self playMovie:statusRecorder.asset isStream:statusRecorder.isStream
		   isRemote:NO initialPosition:statusRecorder.currentPlaybackTime
	  containerView:nil];

	// [self playAsset:self.remotePlayerVC.statusRecorder.asset isStream:self.remotePlayerVC.statusRecorder.isStream isAC:NO initialPosition:self.remotePlayerVC.statusRecorder.currentPlaybackTime fullScreen:YES containerView:nil];
}

- (void)addPopupView:(NSString *)popupString {
}

- (void)viewDidLoad {
	[super viewDidLoad];

	[self customNavigationBar];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tvBecameOnline:) name:@"AC_PLAYER_BECAME_ONLINE" object:[ACManager sharedInstance]];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tvBecameOnline:) name:@"AC_PLAYER_BECAME_OFFLINE" object:[ACManager sharedInstance]];

	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(handleQRButtonClicked:)
												 name:@"QRBUTTONCLICKED"
											   object:nil];

	if ([UIImage externalImageNamed:@"background"])
		[self.pairingViewController.backgroundImage setImage:[UIImage externalImageNamed:@"background"]];
	else
		[self.pairingViewController.backgroundImage setImage:[UIImage imageNamed:@"background.png"]];
	// Do any additional setup after loading the view.
}

- (void)handleQRButtonClicked:(NSNotification *)n {
	if (![[n.userInfo valueForKey:@"fullscreen"] boolValue])
		return;

	NSLog (@"didReadQRButtonClicked called. Should show camera...");
	// QR Code reader implementation...
	ZBarReaderViewController *reader = [[[ZBarReaderViewController alloc] init] autorelease];
	reader.readerDelegate = self;
	/*[reader.scanner setSymbology: ZBAR_QRCODE
	 * config: ZBAR_CFG_ENABLE
	 * to: 0];*/
	reader.readerView.zoom = 1.0;

	[CURRENT_WINDOW.rootViewController presentViewController:reader animated:YES completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	NSLog (@"imagePickerControllerDidCancel...");

	[CURRENT_WINDOW.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	NSLog (@"imagePickerController didFinish...");

	id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];

	for (ZBarSymbol *zbs in results) {
		if (zbs.data) {
			// NSInteger i = [zbs.data rangeOfString:@"token="].location;
			// NSInteger i = 0;

			// if (i < [zbs.data length]) {
			//  NSString * code = [zbs.data substringFromIndex: (i + [@"token=" length])];
			NSString *code = zbs.data;


			/*
			 * [CURRENT_WINDOW dismissViewControllerAnimated:YES completion:^(void) {
			 * txtInputCode.text = code;
			 * [self didConnectButtonSelected:txtInputCode];
			 * }];
			 */

			[CURRENT_WINDOW.rootViewController dismissViewControllerAnimated:YES completion:^{
				 self.pairingViewController.txtInputCode.text = code;
				 [self.pairingViewController didConnectButtonSelected:nil];
			 }];

			// }
		}
	}
}

- (void)tvBecameOnline:(NSNotification *)n {
	self.pairingViewController.connectedImageHidden = self.pairingViewController.connectedImageHidden;
}

- (void)startVideoIfPossible {
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
