//
//  TVEHTTPRequest.h
//  TVE
//
//  Created by Mark Siu on 1/8/13.
//  Copyright (c) 2013 Accedo Broadband. All rights reserved.
//

@class TVEHTTPRequest;

// @protocol ABHTTPRequestDelegate <NSObject>
// @required
// - (void)httpRequest:(ABHTTPRequest *)httpRequest didReceiveResponse:(NSURLResponse *)response;
// - (void)httpRequest:(ABHTTPRequest *)httpRequest didReceiveData:(NSData *)data;
//
// @optional
//
// @end

@interface TVEHTTPRequest : NSObject <NSURLConnectionDelegate>

// @property (nonatomic, weak) id<ABHTTPRequestDelegate> delegate;
// @property (nonatomic, strong) NSString *uslStr;
// @property (nonatomic, strong) NSMutableData *responseData;
// @property (nonatomic, copy) NSMutableDictionary *httpParams;
// @property (nonatomic, strong) NSTimer *timer;

@end
