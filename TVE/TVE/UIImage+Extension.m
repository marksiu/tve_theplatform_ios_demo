//
//  UIImage+Extension.m
//  TVE
//
//  Created by Zoltan Gobolos on 12/20/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "UIImage+Extension.h"
#import "AppView.h"

@implementation UIImage (Extension)
+ (UIImage *)externalImageNamed:(NSString *)name {
	NSString *originalKey = name;
	NSString *key = originalKey;

	CGFloat scale = 1.0;

	if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
		scale = [[UIScreen mainScreen] scale];
		if (2.0 == scale) {
			// @2x
			key = [NSString stringWithFormat:@"%@@2x", key];
		}
	}
	NSData *asset = nil;
	asset = [[AppView sharedInstance] assetByKey:key];
	if (!asset) {
		asset = [[AppView sharedInstance] assetByKey:originalKey];
	}

	return [UIImage imageWithData:asset scale:scale];
}

@end
