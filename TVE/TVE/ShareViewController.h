//
//  ShareViewController.h
//  TVE
//
//  Created by Mate Beres on 12/11/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ShareViewController : BaseViewController
- (IBAction)twitterShare:(id)sender;
- (IBAction)facebookShare:(id)sender;
- (id)initWithAssetTitle:(NSString *)___title;
@end
