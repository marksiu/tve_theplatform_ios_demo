//
//  AppDelegate.h
//  TVE
//
//  Created by Gabor Bottyan on 11/26/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
// #import <AccedoConnect/AccedoConnect.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate, NSURLConnectionDelegate>

@property (strong, nonatomic) UIWindow *window;
// @property (retain) ACManager* acManager;
// @property (retain) ACStatusRecorder* acStatusRecorder;


@property (assign) UINavigationController *onDemandStack;
@property (assign) UINavigationController *homeStack;
@property (assign) int onDemandIndex;

@property (assign) BOOL playOnAccedoConnect;
@property (retain) NSString *acCode;

- (BaseViewController *)currentViewController;
- (void)launch;

@end
