//
//  RemoteViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 12/11/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "RemoteViewController.h"
#import "AppDelegate.h"

static inline BOOL IsEmpty(id object) {
	return object == nil || (object == [NSNull null]);
}

static double start = 0;

@interface RemoteViewController ()
@property (assign) BOOL scrolling;
@property (assign) BOOL playbackRunning;
@end

@implementation RemoteViewController

- (id)initWithBundle:(NSBundle *)__bundle {
	self = [super initWithBundle:__bundle ];

	if (self) {
		[self customInit];
	}
	return self;
}

- (void)viewDidLoad {
	self.delegate = self;
	[super viewDidLoad];


	[self on:@"pauseClicked:" run: ^(id sender) {
		 NSLog (@"PAUSE");
		 [self tick:Nil];
	 }];

//	start = [NSDate timeIntervalSinceReferenceDate];
//
//    NSTimer *timer;
//
//    timer = [NSTimer scheduledTimerWithTimeInterval: 0.5
//                                             target: self
//                                           selector: @selector(tick:)
//                                           userInfo: nil
//                                            repeats: YES];
//
//    NSRunLoop *runner = [NSRunLoop currentRunLoop];
//    [runner addTimer:timer forMode: NSDefaultRunLoopMode];
}

- (void)tick:(id)sender {
	NSLog(@"--TICK");
	ACStatusRecorder *moviePlayer = [ACManager sharedInstance].statusRecorder;
	moviePlayer.playbackState = ACPlaybackStatePlaying;
	moviePlayer.duration = 160;
	double now = [NSDate timeIntervalSinceReferenceDate];
	moviePlayer.currentPlaybackTime = moviePlayer.currentPlaybackTime + (now - start);
	start = now;
	[self updateTime];
}

- (void)customInit {
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPlaybackStatusUpdate:) name:@"AC_PLAYBACK_STATUS" object:Nil];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didClientDisconnected:) name:@"AC_CLIENT_DISCONNECTED" object:Nil];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stateUpdate:) name:@"AC_UPDATE_OVERLAY" object:Nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(assetUpdated:) name:@"AC_ASSET_UPDATED" object:Nil];
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super dealloc];
}

- (void)pauseClicked:(id)sender {
	[self tick:Nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	UIView *scrubber = (UIView *)[self.controls objectForKey:@"scrubber"];

	if ([[event touchesForView:scrubber] count] > 0) {
		NSLog(@"Touched scrubber");
		self.scrolling = YES;
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	//
	UIView *scrubber = (UIView *)[self.controls objectForKey:@"scrubber"];
	UIView *scrubberButton = (UIView *)[self.controls objectForKey:@"scrubberbutton"];

	if ([[event touchesForView:scrubber] count] > 0) {
		NSLog(@"Touched scrubber");
		CGPoint location = [[touches anyObject] locationInView:scrubber];
		float all = scrubber.frame.size.width;
		float ratio = location.x / all;
		scrubberButton.center = CGPointMake(all * ratio, scrubberButton.center.y);
	}
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	UIView *scrubber = (UIView *)[self.controls objectForKey:@"scrubber"];
	UIView *scrubberButton = (UIView *)[self.controls objectForKey:@"scrubberbutton"];

	if ([[event touchesForView:scrubber] count] > 0) {
		NSLog(@"Touched scrubber");
		CGPoint location = [[touches anyObject] locationInView:scrubber];
		float all = scrubber.frame.size.width;
		float ratio = location.x / all;
		scrubberButton.center = CGPointMake(all * ratio, scrubberButton.center.y);
		[ACManager sharedInstance].statusRecorder.currentPlaybackTime = [ACManager sharedInstance].statusRecorder.duration * ratio;
	}
	self.scrolling = NO;
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
	UIView *scrubber = (UIView *)[self.controls objectForKey:@"scrubber"];
	UIView *scrubberButton = (UIView *)[self.controls objectForKey:@"scrubberbutton"];

	if ([[event touchesForView:scrubber] count] > 0) {
		NSLog(@"Touched scrubber");
		CGPoint location = [[touches anyObject] locationInView:scrubber];
		float all = scrubber.frame.size.width;
		float ratio = location.x / all;
		scrubberButton.center = CGPointMake(all * ratio, scrubberButton.center.y);
		[ACManager sharedInstance].statusRecorder.currentPlaybackTime = [ACManager sharedInstance].statusRecorder.duration * ratio;
	}
	self.scrolling = NO;
}

- (void)updateTime {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	// I do my label update

	UILabel *leftLabel = (UILabel *)[self.controls objectForKey:@"leftlabel"];
	UILabel *rightLabel = (UILabel *)[self.controls objectForKey:@"rightlabel"];
	UIView *scrubberFilled = (UIView *)[self.controls objectForKey:@"scrubberfilled"];
	UIView *scrubberEmpty = (UIView *)[self.controls objectForKey:@"scrubberempty"];
	UIView *scrubberButton = (UIView *)[self.controls objectForKey:@"scrubberbutton"];
	UIView *scrubber = (UIView *)[self.controls objectForKey:@"scrubber"];


	if (leftLabel && rightLabel) {
		// if (finito) return;
		ACStatusRecorder *moviePlayer = [ACManager sharedInstance].statusRecorder;
		// [self updateButtons];
		if (moviePlayer.playbackState == ACPlaybackStatePlaying) {
			double remaining = moviePlayer.duration - moviePlayer.currentPlaybackTime;


			NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:remaining];
			NSDate *playeddate = [NSDate dateWithTimeIntervalSinceReferenceDate:moviePlayer.currentPlaybackTime];

			NSDateFormatter *timeFormatter = [[[NSDateFormatter alloc] init] autorelease];
			[timeFormatter setDateFormat:@"HH:mm:ss"];
			[timeFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];

			NSString *remainingString = [timeFormatter stringFromDate:date];
			NSString *playedString = [timeFormatter stringFromDate:playeddate];
			[leftLabel performSelectorOnMainThread:@ selector(setText:) withObject:remainingString waitUntilDone:YES];
			[rightLabel performSelectorOnMainThread:@ selector(setText:) withObject:playedString waitUntilDone:YES];


			// NSLog(@" PLAYED :%@  REMAINING: %@,", playedString, remainingString);
			if (moviePlayer.duration != 0 && !self.scrolling) {
				float ratio = MIN(1, moviePlayer.currentPlaybackTime / moviePlayer.duration);


				float all = scrubber.frame.size.width;

				NSLog(@"%f %f", ratio, all);

				scrubberFilled.frame = CGRectMake(0, 0, all * ratio, scrubber.frame.size.height);
				scrubberEmpty.frame = CGRectMake(all * ratio, 0, all * (1 - ratio), scrubberEmpty.frame.size.height);
				scrubberButton.center = CGPointMake(all * ratio, scrubberButton.center.y);
			}
		}
		// self.volumeSlider.value = self.moviePlayer.volume / 100.0;
	}
	[pool release];
}

- (void)didClientDisconnected:(NSNotification *)notification {
	NSString *uuid = [notification.userInfo valueForKey:@"uuid"];

	if ([uuid isEqualToString:[ACManager sharedInstance].statusRecorder.player]) {
		[[NSNotificationCenter defaultCenter] postNotificationName:@"REMOTE_PLAYBACK_ENDED" object:Nil];
		self.playbackRunning = NO;
	}
}

- (void)didPlaybackStatusUpdate:(NSNotification *)notification {
	NSDictionary *value = [[notification userInfo] valueForKey:@"payload"];
	NSString *deviceType = [[notification userInfo] valueForKey:@"device_type"];

	if (![deviceType hasPrefix:@"TV"]) {
		return;
	}

	[ACManager sharedInstance].statusRecorder.player = [[notification userInfo] valueForKey:@"sender"];
	if (!self.playbackRunning && [[value valueForKey:@"playback_state"] intValue] != ACPlaybackStateTerminated) {
		AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
		//   [[appDelegate currentCMoreViewController] hideHUD];
		[[appDelegate currentViewController].remoteTimeoutTimer invalidate];
		[appDelegate currentViewController].remoteTimeoutTimer = nil;
		[[NSNotificationCenter defaultCenter] postNotificationName:@"REMOTE_PLAYBACK_STARTED" object:[ACManager sharedInstance]];
		self.playbackRunning = YES;
	}
	if (self.view.hidden) {
		return;
	}
	NSString *assetId = [value valueForKey:@"asset_id"];



	[ACManager sharedInstance].statusRecorder.playbackState = [[value valueForKey:@"playback_state"] intValue];
	[ACManager sharedInstance].statusRecorder.isStream = [[value valueForKey:@"stream"] boolValue];


	// [self.overlayView updateLive:self.statusRecorder.isStream];

	// AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
	switch ([ACManager sharedInstance].statusRecorder.playbackState)
	{
		case ACPlaybackStateTerminated :
			[[NSNotificationCenter defaultCenter] postNotificationName:@"REMOTE_PLAYBACK_ENDED" object:[ACManager sharedInstance]];
			self.playbackRunning = NO;
			break;
		default :
			if (!IsEmpty([value valueForKey:@"current_playback_time"])) {
				[ACManager sharedInstance].statusRecorder.currentPlaybackTime = [[value valueForKey:@"current_playback_time"] doubleValue];
			}
			if (!IsEmpty([value valueForKey:@"duration"])) {
				[ACManager sharedInstance].statusRecorder.duration = [[value valueForKey:@"duration"] doubleValue];
			}
			[[ACManager sharedInstance].statusRecorder updateAsset:assetId];
			break;
	}
	[self updateTime];
}

- (void)updateTitleYearLabel:(NSDictionary *)mdl {
	// NSLog(@"MODEL: %@", mdl);
	// NSString * title = [mdl getSafeStringValueForKeyPath:@"metadata" andField:@"title"];
	// NSString * year = [mdl getSafeStringValueForKeyPath:@"metadata" andField:@"productionYear"];
	// NSString * text = [NSString stringWithFormat:@"%@ (%@)", title, year];

	// [self.overlayView.titleLabel setText:text];
}

// - (CGSize) getProperSizeForLabel:(FontLabel *)label {
//  //  CGSize size = [label.text sizeWithZFont:label.zFont
//   //                       constrainedToSize:CGSizeMake(label.frame.size.width, 100000)
//     //                         lineBreakMode:UILineBreakModeWordWrap];
//
//    //size.width = label.frame.size.width;
//    //return size;
// }

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	[super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
	[self rearrangeSubviewsForOrientation:toInterfaceOrientation];
}

- (void)rearrangeSubviewsForOrientation:(UIInterfaceOrientation)interfaceOrientation {
	if (!self.isViewLoaded) {
		return;
	}
	if (interfaceOrientation == UIInterfaceOrientationPortrait ||
		interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
	{
		//  self.overlayView.backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
	}
	else {
		//   self.overlayView.backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
	}

	//   if (self.overlayView.backgroundImage) {
	//    [self imageDidSet: self.overlayView.backgroundImage.image];
	// }
}

// - (void)imageDidSet:(UIImage*)image {
//    //if (IS_DEBUG_MODE)
//    //  NSLog(@"RemotePlayerViewController: imageDidSet called...");
//
//    double w = image.size.width;
//    double h = image.size.height;
//
//    double ratio = 0;
//
//    if (h != 0) {
//        ratio = w / h;
//    }
//
//    // CGRect frame = self.overlayView.backgroundImage.frame;
//    UIInterfaceOrientation interfaceOrientation = self.interfaceOrientation;
//    int height = 0;
//    int width = 0;
//
//    int screenH;
//    int screenW;
//    if (interfaceOrientation == UIInterfaceOrientationPortrait ||
//        interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
//        screenH = 1024;
//        screenW = 768;
//        if (self.statusRecorder.isStream) {
//            width = 315;
//            height = 152;
//        } else {
//            height = 1024;
//            width = 768;
//        }
//    } else {
//        screenH = 768;
//        screenW = 1024;
//        if (self.statusRecorder.isStream) {
//            width = 315;
//            height = 152;
//        } else {
//            height = 768;
//            width = 1024;
//        }
//    }
//    int top_pad = 42;
//    int bottom_pad = 0;
//    //if (!shouldShowCloseButton) {
//     //   bottom_pad = 93;
//    //}
//
//    //    if (self.statusRecorder.isStream) {
//    //        frame.size.width = width;
//    //        frame.size.height = height;
//    //        self.overlayView.backgroundImage.frame = frame;
//    //        self.overlayView.backgroundImage.center = self.overlayView.center;
//    //    } else {
//    //        frame.size.height = height - top_pad - bottom_pad;
//    //        frame.origin.y    = top_pad;
//    //        frame.size.width = frame.size.height * ratio;
//    //        frame.origin.x = (width - frame.size.width) / 2;
//    //        self.overlayView.backgroundImage.frame = frame;
//    //    }
// }

- (void)assetUpdated:(NSNotification *)n {
	if ([ACManager sharedInstance].statusRecorder.asset == nil) {
		//        self.overlayView.backgroundImageUrl = nil;
		//        [self.overlayView.backgroundImage setImageUrl: self.overlayView.backgroundImageUrl];
		//        [self.overlayView.titleLabel setText:@""];
		//        [self.overlayView.descriptionLabel setText:@""];
		//        self.overlayView.subtitles = nil;
		//        [self.overlayView resetSubtitleModel];
		// [self.overlayView hideHud];
		return;
	}
	else {
		//     NSString* imageUrl = [SimpleMediaItem getImageUrlForModel:self.statusRecorder.asset andImageKey:@"home_bg_landscape"];
		// if (self.statusRecorder.isStream) {
		//    imageUrl = [self.statusRecorder.asset valueForKeyPath:@"imageUrl"];
		// }
		// if (!self.overlayView.backgroundImageUrl || (self.overlayView.backgroundImageUrl && ![self.overlayView.backgroundImageUrl isEqual:imageUrl])) {
		//    self.overlayView.backgroundImageUrl = imageUrl;
		//   [self.overlayView.backgroundImage setImageUrl:imageUrl];
		// }
		//        [self updateTitleYearLabel:self.statusRecorder.asset];
		//       // CGSize size = [self getProperSizeForLabel:self.overlayView.titleLabel];
		//        CGRect frame = self.overlayView.titleLabel.frame;
		//        frame.size = size;
		//        self.overlayView.titleLabel.frame = frame;
		//        [self.overlayView.descriptionLabel setText:[self.statusRecorder.asset getSafeStringValueForKeyPath:@"metadata" andField:@"synopsis"]];
		//        size = [self getProperSizeForLabel:self.overlayView.descriptionLabel];
		//        frame = self.overlayView.descriptionLabel.frame;
		//        frame.origin.y = self.overlayView.titleLabel.frame.origin.y + self.overlayView.titleLabel.frame.size.height + 20;
		//        frame.size = size;
		//        self.overlayView.descriptionLabel.frame = frame;
		//        CGRect dFrame = self.overlayView.detailPanel.frame;
		//        [self.overlayView.detailPanel setContentSize:CGSizeMake(dFrame.size.width, frame.origin.y + frame.size.height + 20)];
		//        self.overlayView.subtitles = self.statusRecorder.subtitles;
		//        [self.overlayView resetSubtitleModel];
	}
}

- (void)stateUpdate:(NSNotification *)n {
	// [self.overlayView update:nil];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
