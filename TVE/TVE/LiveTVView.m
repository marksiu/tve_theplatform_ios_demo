//
//  LiveTVView.m
//  TVE
//
//  Created by Gabor Bottyan on 11/28/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "LiveTVView.h"
#import "Constants.h"
#import "AppView.h"
#import "VSRemoteImageView.h"
@implementation LiveTVView

- (id)initWithFrame:(CGRect)frame data:(NSDictionary * )__data {
	self = [super initWithFrame:frame];
	if (self) {
		[self configureView:Nil];
	}
	return self;
}

- (void)dealloc {
	self.movieConteiner = Nil;
	[super dealloc];
}

- (void)configureView:(NSDictionary * )item {
	float lblFS = 15.0f;
	float titleFS = 19.0f;
	float subtitleFS = 14.5f;

	for (UIView *v in [[[self subviews] copy] autorelease]) {
		[v removeFromSuperview];
	}

	UIView *channelContentView = [[UIView alloc] initWithFrame:CGRectInset(CGRectMake(0, 0, liveTVWidth, liveTVHeight), 2, 2)];
	channelContentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];

	UIImageView *live = [[UIImageView alloc] initWithFrame:CGRectInset(CGRectMake(0, 0, channelContentView.frame.size.width, channelContentView.frame.size.height), 3, 3)];
	live.image = [UIImage imageNamed:@"live.png"];
	[channelContentView addSubview:live];
	live.clipsToBounds = YES;
	// live.backgroundColor = [UIColor greenColor];
	live.contentMode = UIViewContentModeScaleAspectFill;

	[live release];

	if (!self.movieConteiner) {
		self.movieConteiner = [[[UIView alloc] initWithFrame:CGRectInset(CGRectMake(0, 0, channelContentView.frame.size.width, channelContentView.frame.size.height), 3, 3)] autorelease];
	}

	self.movieConteiner.backgroundColor = [UIColor blackColor];
	[channelContentView addSubview:self.movieConteiner];

	[self addSubview:channelContentView];
	[channelContentView release];

	UIView *channelInfoContentView = [[UIView alloc] initWithFrame:CGRectInset(CGRectMake(0, liveTVHeight, liveTVWidth, channelHeight), 1, 1)];
	channelInfoContentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];

	UILabel *channelNumber = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 100, 20)];
	channelNumber.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:lblFS];
	channelNumber.text = [NSString stringWithFormat:@"CH %@", item ? [item objectForKey:@"channel"]:@""];
	channelNumber.backgroundColor = [UIColor clearColor];
	channelNumber.textColor = [UIColor colorWithRed:141.0f / 255.0f green:141.0f / 255.0f blue:141.0f / 255.0f alpha:1.0f];
	[channelInfoContentView addSubview:channelNumber];
	[channelNumber release];

	UILabel *channelName = [[UILabel alloc] initWithFrame:CGRectMake(10, 25, 100, 20)];
	channelName.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:lblFS];
	channelName.text = item ? [item objectForKey:@"name"] : @"";
	channelName.backgroundColor = [UIColor clearColor];
	channelName.textColor = [UIColor colorWithRed:141.0f / 255.0f green:141.0f / 255.0f blue:141.0f / 255.0f alpha:1.0f];
	[channelInfoContentView addSubview:channelName];
	[channelName release];

	NSString *imageUrl = [item objectForKey:@"logo"];
	VSRemoteImageView *imageView = [[VSRemoteImageView alloc] initWithFrame:CGRectMake(140, 15, 60, 17)];
	[imageView setContentMode:UIViewContentModeScaleAspectFill];
	imageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
	[imageView setClipsToBounds:YES];
	// Use channel mockup images which were saved locally.
	if ([[item objectForKey:@"logo"] rangeOfString:@".png"].location != NSNotFound) {
		[imageView setLocalImage:[item objectForKey:@"logo"] withChannel:true];
	}
	else {
		[imageView setImageUrl:imageUrl withPlaceholder:nil animated:YES];
	}

	[channelInfoContentView addSubview:imageView];
	// imageView.center = CGPointMake(channelInfoContentView.center.x, imageView.center.y);
	[imageView release];

	[self addSubview:channelInfoContentView];

	[channelInfoContentView release];

	NSDictionary *currentProgram = [[AppView sharedInstance] currentEPGProgramOfChannel:item programsOffset:0];
	NSDictionary *nextProgram = [[AppView sharedInstance] currentEPGProgramOfChannel:item programsOffset:1];

	UIView *nowContentView = [[UIView alloc] initWithFrame:CGRectInset(CGRectMake(0, liveTVHeight + channelHeight, liveTVWidth, nextHeight), 1, 1)];
	nowContentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];

	UILabel *nowText = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, liveTVWidth, 20)];
	nowText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:lblFS];
	nowText.text = @"ON NOW";
	nowText.textColor = [UIColor whiteColor];

	// [UIColor colorWithRed:85.0/255.0 green:175.0/255.0 blue:236.0/255.0 alpha:1.0];
	nowText.backgroundColor = [UIColor clearColor];
	[nowContentView addSubview:nowText];
	[nowText release];

	UILabel *nowTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 22, liveTVWidth - 10, 30)];
	nowTitle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:titleFS];
	nowTitle.text = currentProgram ? [currentProgram objectForKey:@"title"] : @"";
	nowTitle.textColor = [UIColor whiteColor];
	nowTitle.backgroundColor = [UIColor clearColor];
	[nowContentView addSubview:nowTitle];
	[nowTitle release];

	UILabel *timeText = [[UILabel alloc] initWithFrame:CGRectMake(10, 49, liveTVWidth - 10, 20)];
	timeText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:subtitleFS];
	timeText.text = currentProgram ? [[AppView sharedInstance] getEPGPeriod:currentProgram] : @"";
	timeText.textColor = [UIColor colorWithRed:141.0f / 255.0f green:141.0f / 255.0f blue:141.0f / 255.0f alpha:1.0f];
	timeText.backgroundColor = [UIColor clearColor];
	[nowContentView addSubview:timeText];
	[timeText release];

	CGSize lastLabelSize = [timeText.text sizeWithFont:timeText.font];

	int watchNumber = [[item objectForKey:@"watching"] intValue];

	if (watchNumber > 0) {
		UIView *watchView = [[[UIView alloc] initWithFrame:CGRectMake(27 + lastLabelSize.width, 51, liveTVWidth, 20)] autorelease];

		UILabel *watchText = [[[UILabel alloc] initWithFrame:CGRectMake(3, 1, itemRectSize.width - (itemImageRectSize.width + 30), friendIconSize.height)] autorelease];
		watchText.text = [NSString stringWithFormat:@"%d", watchNumber];
		watchText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:subtitleFS];
		watchText.textColor = [UIColor colorWithRed:141.0f / 255.0f green:141.0f / 255.0f blue:141.0f / 255.0f alpha:1.0f];
		watchText.backgroundColor = [UIColor clearColor];
		[watchView addSubview:watchText];

		UIImage *watchImage = [UIImage imageNamed:@"friend_icon_gray.png"];
		UIImageView *friends = [[[UIImageView alloc] initWithFrame:CGRectMake(18, 1, friendIconSize.width, friendIconSize.height)] autorelease];
		friends.image = watchImage;
		[watchView addSubview:friends];


		// [watchText release];

		[nowContentView addSubview:watchView];
		//   [watchView release];
	}

	[self addSubview:nowContentView];
	[nowContentView release];

	UIView *nextContentView = [[UIView alloc] initWithFrame:CGRectInset(CGRectMake(0, liveTVHeight + channelHeight + nextHeight, liveTVWidth, nextHeight), 1, 1)];
	nextContentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];

	UILabel *nextText = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, liveTVWidth - 10, 20)];
	nextText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:lblFS];
	nextText.text = @"UP NEXT";
	nextText.textColor = [UIColor whiteColor];
	// [UIColor colorWithRed:85.0/255.0 green:175.0/255.0 blue:236.0/255.0 alpha:1.0];
	nextText.backgroundColor = [UIColor clearColor];
	[nextContentView addSubview:nextText];
	[nextText release];

	UILabel *nextTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 22, liveTVWidth - 10, 30)];
	nextTitle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:titleFS];
	nextTitle.text =  nextProgram ? [nextProgram objectForKey:@"title"] : @"";
	nextTitle.textColor = [UIColor whiteColor];
	nextTitle.backgroundColor = [UIColor clearColor];
	[nextContentView addSubview:nextTitle];
	[nextTitle release];

	UILabel *nextTimeText = [[UILabel alloc] initWithFrame:CGRectMake(10, 49, liveTVWidth - 10, 20)];
	nextTimeText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:subtitleFS];
	nextTimeText.text =  nextProgram ? [[AppView sharedInstance] getEPGPeriod:nextProgram] : @"";
	nextTimeText.textColor = [UIColor colorWithRed:141.0f / 255.0f green:141.0f / 255.0f blue:141.0f / 255.0f alpha:1.0f];
	nextTimeText.backgroundColor = [UIColor clearColor];
	[nextContentView addSubview:nextTimeText];
	[nextTimeText release];

	[self addSubview:nextContentView];
	[nextContentView release];

	UIImage *adImage = [UIImage imageNamed:@"car_ad.png"];
	UIImage *adImage2 = [UIImage imageNamed:@"gift_ad.png"];

	UIImageView *ad1 = [[UIImageView alloc] initWithFrame:CGRectInset(CGRectMake(0, liveTVHeight + channelHeight + nextHeight + nextHeight, adSize.width, adSize.height), 2, 1)];

	ad1.image = adImage;
	[self addSubview:ad1];
	[ad1 release];

	UIImageView *ad2 = [[UIImageView alloc] initWithFrame:CGRectOffset(ad1.frame, 0, adSize.height)];
	ad2.image = adImage2;
	[self addSubview:ad2];
	[ad2 release];

	// self.backgroundColor = [UIColor yellowColor];
}

@end
