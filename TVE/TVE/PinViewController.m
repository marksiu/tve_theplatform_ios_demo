//
//  PinViewController.m
//  TVE
//
//  Created by Mate Beres on 12/17/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "PinViewController.h"
#import "VSRemoteImageView.h"
#import "UIHelper.h"
#import "AppView.h"
// TVE thePlatform
#import <Security/Security.h>
#import "KeychainItemWrapper.h"
#import "TVEConfig.h"

@interface PinViewController () <UITextFieldDelegate>

@property (retain, nonatomic) IBOutlet UITextField *textField;
@property (retain, nonatomic) IBOutlet UIButton *pin1;
@property (retain, nonatomic) IBOutlet UIButton *pin2;
@property (retain, nonatomic) IBOutlet UIButton *pin3;
@property (retain, nonatomic) IBOutlet UIButton *pin4;
@property (retain, nonatomic) IBOutlet UILabel *message;
@property (retain, nonatomic) IBOutlet VSRemoteImageView *imageView;

- (IBAction)pinConfirm:(id)sender;

// TVE thePlatform
- (void)rentalMovie;
- (NSString *)retrieveProductID;
- (NSArray *)getKeyChainArr;
- (NSString *)getEndUserToken;
- (NSString *)getEndUserID;
- (NSString *)getEndUserPaymentInst;
- (NSString *)createPurchaseOrder;
- (void)commitPurchaseOrder:(NSString *)createOrderRef;
- (void)startIndicator;
- (void)stopIndicator;

@property (retain) NSMutableData *responseData;


@end

@implementation PinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (IBAction)pinConfirm:(id)sender {
	// Loading Indicator

	[self startSpinner];
	[[self.navigationController navigationBar] setTintColor:[UIColor blackColor]];

	UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];

	indicator.frame = CGRectMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2 - 40, 25, 25);
	//    indicator.frame = CGRectMake(0,0, 25, 25);
	[self.view addSubview:indicator];
	[indicator startAnimating];
	[indicator setHidden:NO];

	NSString *mediaId = [self.item objectForKey:@"id"];
	if (mediaId != nil) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		                   // create the purchase order and commit order
						   [self rentalMovie];

		                   // clear renew rental record
						   TVEConfig *config = [TVEConfig sharedConfig];
						   [config renewRentalMovie];

						   dispatch_async (dispatch_get_main_queue (), ^{
		                   // If PIN correct, run the below code
											   [self.delegate pinCorrect:self.item];
											   [self dismissViewControllerAnimated:YES
																		completion:nil];
											   [self stopSpinner];
										   });
					   });
	}
	else {
		dispatch_async(dispatch_get_main_queue(), ^{
		                   // If PIN correct, run the below code
						   [self.delegate pinCorrect:self.item];
						   [self dismissViewControllerAnimated:YES
													completion:nil];
						   [self stopSpinner];
					   });
	}
}

- (void)viewDidLoad {
	[super viewDidLoad];


	// set druation and price
	NSString *rentalPrice;
	NSString *rentalDuration;

	// TV channel image
	NSString *logoUrl = [UIHelper getImageUrl:self.item forSize:self.imageView.bounds.size];
	if (![logoUrl length]) {
		logoUrl = [self.item valueForKey:@"logo"];
	}

	NSString *mediaId = [self.item objectForKey:@"id"];
	NSLog(@"media ID: %@", mediaId);

	if (mediaId != nil) {
		NSString *urlStr = [NSString stringWithFormat:@"http://feed.product.theplatform.com/f/xBrLJC/accedo-product-feed?form=json&pretty=true&byScopeIds=%@", mediaId];
		//	NSString *urlStr = [NSString stringWithFormat:@"http://feed.product.theplatform.com/f/xBrLJC/accedo-product-feed?form=json&pretty=true&byScopeIds=http://data.media.theplatform.com/media/data/Media/13568067928"];
		NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlStr]];
		NSURLResponse *response = nil;
		NSError *error = nil;
		NSData *data = [NSURLConnection sendSynchronousRequest:request
											 returningResponse:&response
														 error:&error];

		if (error == nil) {
			// retrieve data from API response
			NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
			NSArray *entryArr = [jsonObject objectForKey:@"entries"];
			NSDictionary *entry = [entryArr objectAtIndex:0];
			NSDictionary *productPricingPlan = [entry objectForKey:@"plproduct$pricingPlan"];
			NSArray *productPricingTiersArr = [productPricingPlan objectForKey:@"plproduct$pricingTiers"];
			NSDictionary *productPricingTiers = [productPricingTiersArr objectAtIndex:0];

			// rental price
			NSDictionary *productAmont = [productPricingTiers objectForKey:@"plproduct$amounts"];
			rentalPrice = [productAmont objectForKey:@"USD"];

			// rental duration
			NSArray *productPricingTagsArr = [productPricingTiers objectForKey:@"plproduct$productTags"];
			NSDictionary *productPricingTags = [productPricingTagsArr objectAtIndex:0];
			rentalDuration = [productPricingTags objectForKey:@"plproduct$title"];
		}

		[self.imageView setImageUrl:logoUrl];
	}
	else {
		rentalPrice = [self.item objectForKey:@"pl1$price"];
		if (![rentalPrice length]) {
			rentalPrice = @"5";
		}
		rentalDuration = [[self.item objectForKey:@"pl1$rentalPeriod"] description];
		if ([rentalDuration length]) {
			rentalDuration = [NSString stringWithFormat:@"%@ days", rentalDuration];
		}
		else {
			rentalDuration = @"for a month";
		}

		[self.imageView setLocalImage:logoUrl withChannel:YES];
	}


	self.navigationItem.title = @"Enter Payment PIN";

	NSString *title = [self.item objectForKey:@"title"];
	if (![title length]) {
		title = [self.item objectForKey:@"name"];
	}
//	NSString *price = [self.item objectForKey:@"pl1$price"];
//	if (![price length]) {
//		price = @"5";
//	}
//	NSString *period = [[self.item objectForKey:@"pl1$rentalPeriod"] description];
//	if ([period length]) {
//		period = [NSString stringWithFormat:@"%@ days", period];
//	}
//	else {
//		period = @"for a month";
//	}



	if (self.item) {
		self.message.text = [NSString stringWithFormat:@"You have selected %@. Your registered credit card will be charded $%@ for %@. Payment details can be checked on your TV Operator's Account.",
							 title,
							 rentalPrice,
							 rentalDuration];
	}

	self.navigationItem.rightBarButtonItem  = [[[UIBarButtonItem alloc] initWithTitle:@"Close"
																				style:UIBarButtonItemStylePlain
																			   target:self
																			   action:@selector(close)]
											   autorelease];
	/*
	 * [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:14/255.0
	 *                                                                    green:25/255.0
	 *                                                                     blue:49/255.0
	 *                                                                    alpha:1.0]];
	 */
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)dealloc {
	self.pin1 = nil;
	self.pin1 = nil;
	self.pin1 = nil;
	self.pin1 = nil;
	self.message = nil;
	self.imageView = nil;
	[super dealloc];
}

- (void)close {
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)openKeyboard {
	[self.textField becomeFirstResponder];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
	NSString *original = textField.text;
	NSString *new = [original stringByReplacingCharactersInRange:range withString:string];

	if (4 < [new length]) {
		return NO;
	}

	// new = [new substringToIndex:MIN(3, [new length] - 1)];

	[self.pin1 setTitle:0 < [new length] ? @"•":@"" forState:UIControlStateNormal];
	[self.pin2 setTitle:1 < [new length] ? @"•":@"" forState:UIControlStateNormal];
	[self.pin3 setTitle:2 < [new length] ? @"•":@"" forState:UIControlStateNormal];
	[self.pin4 setTitle:3 < [new length] ? @"•":@"" forState:UIControlStateNormal];

	return YES;
}

#pragma mark - rental movie
// thePlatform API
- (void)rentalMovie {
	// combine create purchase order and commit purchase order
//	[self createAndCommitPurchaseOrder];

	// create purchase order
	NSString *createOrderRef = [self createPurchaseOrder];

	// commit purchase order
	[self commitPurchaseOrder:createOrderRef];
}

// Retrieve Product ID by Media ID
- (NSString *)retrieveProductID {
	NSString *mediaId = [self.item objectForKey:@"id"];
	NSURLRequest *url = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://feed.product.theplatform.com/f/xBrLJC/accedo-product-feed?form=json&pretty=true&byScopeIds=%@", mediaId]]];
	NSURLResponse *response = nil;
	NSError *error = nil;
	NSData *data = [NSURLConnection sendSynchronousRequest:url
										 returningResponse:&response
													 error:&error ];

	if (error == nil) {
		NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
		NSArray *entryArr = [jsonObject objectForKey:@"entries"];
		NSDictionary *entry = [entryArr objectAtIndex:0];
		NSString *productId = [entry objectForKey:@"id"];
		NSLog(@"product ID: %@", productId);
		return productId;
	}

	return @"";
}

- (NSArray *)getKeyChainArr {
	KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"login" accessGroup:nil];
	NSString *keyChainData = [keychainItem objectForKey:(id)kSecValueData];
	NSArray *keyChainArr = [keyChainData componentsSeparatedByString:@"#*#"];

	return keyChainArr;
}

- (NSString *)getEndUserToken {
	NSArray *keyChainArr = [self getKeyChainArr];
	NSString *endUserToken = [keyChainArr objectAtIndex:1];

	NSLog(@"End User Token: %@", endUserToken);
	return endUserToken;
}

- (NSString *)getEndUserID {
	NSArray *keyChainArr = [self getKeyChainArr];
	NSString *endUserId = [keyChainArr objectAtIndex:2];

	NSLog(@"End User ID: %@", endUserId);
	return endUserId;
}

- (NSString *)getEndUserPaymentInst {
	NSArray *keyChainArr = [self getKeyChainArr];
	NSString *endUserPaymentInst = [keyChainArr objectAtIndex:3];

	NSLog(@"End User Payment Instrument: %@", endUserPaymentInst);
	return endUserPaymentInst;
}

#pragma mark - thePlatform API - combine create and commit purchase order
// - (void)createAndCommitPurchaseOrder {
//	NSLog(@"createAndCommitPurchaseOrder");
//
//	// retrieve product ID, end user token, end user payment instrument
//	NSString *productID = [self retrieveProductID];
//	NSString *endUserToken = [self getEndUserToken];
//	NSString *endUserPaymentInst = [self getEndUserPaymentInst];
//	NSString *endUserId = [self getEndUserID];
//
//	__block NSString *createOrderRef = @"";
//
//	// create purchase order
//	NSURLRequest *createOrderUrl = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://storefront.commerce.theplatform.com/storefront/web/Checkout/createOrder?schema=1.2&token=%@&account=http://access.auth.theplatform.com/data/Account/2301538417&form=json&_purchaseItemInfos[0].productId=%@&_purchaseItemInfos[0].quantity=1&_purchaseItemInfos[0].currency=USD&_paymentRef=%@", endUserToken, productID, endUserPaymentInst]]];
//
//	[NSURLConnection sendAsynchronousRequest:createOrderUrl
//									   queue:[NSOperationQueue mainQueue]
//						   completionHandler: ^(NSURLResponse * response, NSData * data, NSError * error) {
//		 if (error == nil) {
//			 NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
//			 createOrderRef = [jsonObject objectForKey:@"createOrderResponse"];
//
//	         // commit purchase order
//			 NSURLRequest *commitOrderUrl = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://storefront.commerce.theplatform.com/storefront/web/Checkout/commitOrder?schema=1.2&token=%@&account=http://access.auth.theplatform.com/data/Account/2301538417&form=json&_userId=%@&_providerOrderRef=%@", endUserToken, endUserId, createOrderRef]]];
//
//			 [NSURLConnection sendAsynchronousRequest:commitOrderUrl
//												queue:[NSOperationQueue mainQueue]
//									completionHandler:^(NSURLResponse * response, NSData * data, NSError * error) {
//					  if (error == nil) {
//						  NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
//						  NSDictionary *commitOrderResp = [jsonObject objectForKey:@"commitOrderResponse"];
//						  NSString *status = [commitOrderResp objectForKey:@"status"];
//
//						  if ([status isEqualToString:@"Completed"]) {
//							  NSLog (@"Commit Order Success!!");
//						  }
//					  }
//				  }];
//		 }
//	 }];
// }


#pragma mark - thePlatform API - create purchase order
// Create purchase order by thePlatform API
- (NSString *)createPurchaseOrder {
	NSLog(@"createPurchaseOrder");

	// retrieve product ID, end user token, end user payment instrument
	NSString *productID = [self retrieveProductID];
	NSString *endUserToken = [self getEndUserToken];
	NSString *endUserPaymentInst = [self getEndUserPaymentInst];

	// create purchase order
	NSURLRequest *url = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://storefront.commerce.theplatform.com/storefront/web/Checkout/createOrder?schema=1.2&token=%@&account=http://access.auth.theplatform.com/data/Account/2301538417&form=json&_purchaseItemInfos[0].productId=%@&_purchaseItemInfos[0].quantity=1&_purchaseItemInfos[0].currency=USD&_paymentRef=%@", endUserToken, productID, endUserPaymentInst]]];

	NSURLResponse *response = nil;
	NSError *error = nil;
	NSData *data = [NSURLConnection sendSynchronousRequest:url
										 returningResponse:&response
													 error:&error];

	if (error == nil) {
		NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
		NSString *createOrderRef = [jsonObject objectForKey:@"createOrderResponse"];
		return createOrderRef;
	}


	return @"";
}

#pragma mark - thePlatform API - commit purchase order
- (void)commitPurchaseOrder:(NSString *)createOrderRef {
	NSLog(@"commitPurchaseOrder");

	// retrieve end user token, end user ID
	NSString *endUserToken = [self getEndUserToken];
	NSString *endUserId = [self getEndUserID];

	// commit purchase order
	NSURLRequest *url = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://storefront.commerce.theplatform.com/storefront/web/Checkout/commitOrder?schema=1.2&token=%@&account=http://access.auth.theplatform.com/data/Account/2301538417&form=json&_userId=%@&_providerOrderRef=%@", endUserToken, endUserId, createOrderRef]]];
	NSURLResponse *response = nil;
	NSError *error = nil;
	NSData *data = [NSURLConnection sendSynchronousRequest:url
										 returningResponse:&response
													 error:&error];

	if (error == nil) {
		NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
		NSDictionary *commitOrderResp = [jsonObject objectForKey:@"commitOrderResponse"];
		NSString *status = [commitOrderResp objectForKey:@"status"];

		if ([status isEqualToString:@"Completed"]) {
			NSLog(@"Commit Order Success!!");
		}
	}
}

@end
