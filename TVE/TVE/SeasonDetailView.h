//
//  SeasonDetailView.h
//  TVE
//
//  Created by Gabor Bottyan on 12/3/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetView.h"
#import "BaseViewController.h"
@interface SeasonDetailView : UIScrollView <UICollectionViewDataSource, UICollectionViewDelegate, AsetViewDelegate>{
}
@property (assign) BaseViewController *detaildelegate;
- (id)initWithFrame:(CGRect) frame config:(NSDictionary *)configuration;
- (void)setContent:(NSArray * )relatedContent type:(BOOL)episodes;
- (void)configureView:(NSDictionary *)configuration;
@end
