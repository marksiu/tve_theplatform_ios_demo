//
//  Constants.h
//  TVE
//
//  Created by Gabor Bottyan on 11/28/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#ifndef TVE_Constants_h
#define TVE_Constants_h


#define CURRENT_WINDOW [UIApplication sharedApplication].delegate.window

static const double timeUnit = 0.02;
static const CGSize itemRectSize = { 392, 107 };
static const CGSize subscribeRectSize = { 314, 300 };
static const CGSize itemImageRectSize = { 165, 94 };
static const CGSize listImageRectSize = { 140, 79 };
static const CGSize arrowRectSize = { 23, 13.5 };
static const CGSize rectSize = { 392, 225 };
static const CGSize listRectSize = { 320, 86 };
static const CGSize ratingStarSize = { 14, 13 };
static const CGSize friendIconSize = { 16, 15 };
static const CGSize adSize = { 242, 148 };
static const float gap = 5;
static const float detailLeftSize = 320;
static const float detailRightSize = 744;
static const float detailHeight = 655;
static const float promoHeight = 400;
static const float promoTop = 300;

static const float screenWidth = 1024;
static const float epgWidth = 783;
static const float liveTVWidth = 242;
static const float smallliveTVWidth = 213;
static const float liveTVHeight = 153;
static const float channelHeight = 54;
static const float nowHeight = 104;
static const float nextHeight = 79;
static double boxTimeDelta = 0;
static double loginButtonHeight = 40.0;

#endif // ifndef TVE_Constants_h

