//
//  PinViewController.h
//  TVE
//
//  Created by Mate Beres on 12/17/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "BaseViewController.h"


@protocol PinViewControllerDelegate <NSObject, NSURLConnectionDelegate>

- (void)pinCorrect:(NSDictionary *)item;

@end


@interface PinViewController : BaseViewController

@property (retain) NSDictionary *item;
@property (assign) id<PinViewControllerDelegate> delegate;

@end
