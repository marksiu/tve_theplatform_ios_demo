//
//  MyTVViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 12/20/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "MyTVViewController.h"
#import "AppView.h"
#import "LoginViewController.h"

// TVE thePlatform
#import "TVEConfig.h"
@interface MyTVViewController ()
@property (retain) UIViewController *loginController;
@property (retain) UIButton *favChannelsButton;
@property (retain) UIButton *favShowsButton;
@property (retain) UIButton *rentalButton;
@property (retain) UIButton *sortButton;
@property (retain) UIImageView *textBackgroundView;
@property (retain) UILabel *separationLabel1;
@property (retain) UILabel *separationLabel2;
@property (assign) BOOL shouldLoverLogin;
@property (assign) int mode;
@end

@implementation MyTVViewController

- (id)initWithFeed:(NSDictionary *)desc {
	self = [super initWithFeed:desc];
	if (self) {
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(favChanChanged:)
													 name:@"FAVORITE_CHANNEL_CHANGED"
												   object:nil];

		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(rentedChanged:)
													 name:@"RENTED_MOVIES_CHANGED"
												   object:nil];

		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(favShowChanged:)
													 name:@"FAVORITE_SHOWS_CHANGED"
												   object:nil];
	}
	return self;
}

- (void)favChanChanged:(id)sender {
	if (self.mode == 0) {
		[self fetchData];
	}
}

- (void)rentedChanged:(id)sender {
	if (self.mode == 2) {
		[self fetchData];
	}
}

- (void)favShowChanged:(id)sender {
	if (self.mode == 1) {
		[self fetchData];
	}
}

- (void)fetchData {
	// NSLog(@"FETCH DATA");
	int currentMode = self.mode;

	if ([AppView sharedInstance].fingerprint) {
//		dispatch_async(dispatch_get_main_queue(), ^{
//						   [self startSpinner];
//					   });

		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
						   NSArray *data = Nil;
						   if (self.mode == 0) {
							   NSMutableArray *arr = [NSMutableArray array];
							   NSArray *ids = [[AppView sharedInstance] getFavoriteChannels];
							   for (NSNumber *ch in ids) {
								   [arr addObject:[[AppView sharedInstance] channelByChannelId:[NSString stringWithFormat:@"%@", ch]]];
							   }

							   data = arr;
						   }
						   else if (self.mode == 1) {
							   data = [[AppView sharedInstance] getFavoriteShows];
						   }
						   else {
							   data = [[AppView sharedInstance] getRentedMovies];
						   }

						   self.data = [NSDictionary dictionaryWithObject:data forKey:@"entries"];
						   dispatch_async (dispatch_get_main_queue (), ^{
											   if (self.mode == currentMode) {
												   if (self.mode != 2) {
													   [self display:data];
												   }
												   else {
//													   [self displayMyRental];
													   [self displayMyRentalFromLocal];
												   }
											   }
											   [self stopSpinner];
										   });
					   });
	}
}

- (void)showFilters {
	[self.scrollView addSubview:self.textBackgroundView];
	[self.scrollView addSubview:self.separationLabel1];
	[self.scrollView addSubview:self.separationLabel2];
	[self.scrollView addSubview:self.favChannelsButton];
	[self.scrollView addSubview:self.favShowsButton];
	[self.scrollView addSubview:self.rentalButton];
	[self.scrollView addSubview:self.sortButton];

	// temp solutation to refresh rental record
	TVEConfig *config = [TVEConfig sharedConfig];
	[config renewRentalMovie];
}

- (void)setupFilters {
	float topY = 20;

	self.favChannelsButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.favChannelsButton.frame = CGRectMake(130, topY, 180, 31);
	self.favChannelsButton.backgroundColor = [UIColor clearColor];
	[self.favChannelsButton setTitle:@"Favorite TV Channels" forState:UIControlStateNormal];
	self.favChannelsButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
	self.favChannelsButton.titleLabel.textColor = [UIColor whiteColor];
	[self.favChannelsButton addTarget:self action:@selector(showFavChannels:) forControlEvents:UIControlEventTouchUpInside];

	self.favShowsButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.favShowsButton.frame = CGRectMake(330, topY, 130, 31);
	self.favShowsButton.backgroundColor = [UIColor clearColor];
	[self.favShowsButton setTitle:@"Favorite TV Shows" forState:UIControlStateNormal];
	self.favShowsButton.titleLabel.textColor = [UIColor whiteColor];
	self.favShowsButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
	[self.favShowsButton addTarget:self action:@selector(showFavShows:) forControlEvents:UIControlEventTouchUpInside];


	self.rentalButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.rentalButton.frame = CGRectMake(480, topY, 130, 31);
	self.rentalButton.backgroundColor = [UIColor clearColor];
	[self.rentalButton setTitle:@"My Rentals" forState:UIControlStateNormal];
	self.rentalButton.titleLabel.textColor = [UIColor whiteColor];
	self.rentalButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
	[self.rentalButton addTarget:self action:@selector(showRentals:) forControlEvents:UIControlEventTouchUpInside];



	self.textBackgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"OndemandBackgroundLayer"]] autorelease];
	self.textBackgroundView.frame = CGRectMake(110, topY, 510, 30);

	self.separationLabel1 = [[[UILabel alloc] initWithFrame:CGRectMake(310, topY + 1, 40, 30)] autorelease];
	self.separationLabel1.text = @"|";
	self.separationLabel1.font = [UIFont fontWithName:@"Helvetica" size:20];
	[self.separationLabel1 setTextColor:[UIColor whiteColor]];
	[self.separationLabel1 setBackgroundColor:[UIColor clearColor]];

	self.separationLabel2 = [[[UILabel alloc] initWithFrame:CGRectMake(480, topY + 1, 40, 30)] autorelease];
	self.separationLabel2.text = @"|";
	self.separationLabel2.font = [UIFont fontWithName:@"Helvetica" size:20];
	[self.separationLabel2 setTextColor:[UIColor whiteColor]];
	[self.separationLabel2 setBackgroundColor:[UIColor clearColor]];


	self.textBackgroundView.center = CGPointMake(self.scrollView.center.x, self.textBackgroundView.center.y);

	self.separationLabel1.frame = CGRectMake(self.textBackgroundView.frame.origin.x + 200, topY + 1, 40, 30);
	self.separationLabel1.frame = CGRectMake(self.textBackgroundView.frame.origin.x + 370, topY + 1, 40, 30);
	self.favChannelsButton.frame = CGRectMake(self.textBackgroundView.frame.origin.x + 20, topY, 180, 31);
	self.favShowsButton.frame = CGRectMake(self.textBackgroundView.frame.origin.x + 220, topY, 130, 31);
	self.rentalButton.frame = CGRectMake(self.textBackgroundView.frame.origin.x + 370, topY, 130, 31);

	self.top = 40 + topY;




	/*
	 * self.sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
	 * self.sortButton.frame = CGRectMake(651, 0, 131, 31);
	 * [self.sortButton setTitle:@"Sort By" forState:UIControlStateNormal];
	 * self.sortButton.backgroundColor = [UIColor blackColor];
	 * self.sortButton.titleLabel.textColor = [UIColor whiteColor];
	 * [self.sortButton addTarget:self action:@selector(showSort:) forControlEvents:UIControlEventTouchUpInside];
	 *
	 */

	// self.top = 50;
}

- (void)updateFilters {
	if (self.mode == 0) {
		[self.favChannelsButton setTitleColor:[UIColor colorWithRed:31.0 / 255.0 green:102.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
		[self.favShowsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[self.rentalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	}
	else if (self.mode == 1) {
		[self.rentalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[self.favShowsButton setTitleColor:[UIColor colorWithRed:31.0 / 255.0 green:102.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
		[self.favChannelsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	}
	else if (self.mode == 2) {
		[self.favChannelsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[self.favShowsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[self.rentalButton setTitleColor:[UIColor colorWithRed:31.0 / 255.0 green:102.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
	}
}

- (void)showFavChannels:(id)sender {
	[self startSpinner];
	self.mode = 0;
	[self updateFilters];
	[self fetchData];
}

- (void)showFavShows:(id)sender {
	[self startSpinner];
	self.mode = 1;
	[self updateFilters];
	[self fetchData];
}

- (void)showRentals:(id)sender {
	[self startSpinner];
	self.mode = 2;
	[self updateFilters];
	[self fetchData];
}

- (void)sortButtonTouched:(id)sender {
}

- (void)textFieldDidBeginEditing {
	// NSLog(@"BEGIN");
	if (self.loginController) {
		self.shouldLoverLogin = NO;
		[UIView animateWithDuration:0.5
						 animations:^{
			 self.loginController.view.center = CGPointMake (self.view.center.x, self.view.center.y - 150);
		 }
						 completion:^(BOOL finished) {
		 }];
	}
}

- (void)restoreLogin {
	if (self.shouldLoverLogin) {
		[UIView animateWithDuration:0.5
						 animations:^{
			 self.loginController.view.center = self.view.center;
		 }
						 completion:^(BOOL finished) {
		 }];
	}
}

- (void)textFieldDidEndEditing {
	// NSLog(@"END");
	if (self.loginController) {
		self.shouldLoverLogin = YES;
		[NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(restoreLogin) userInfo:Nil repeats:NO];
	}
}

- (void)appLogin:(id)sender {
	if (self.loginController) {
		[self.loginController.view removeFromSuperview];
	}
	[self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
//	if (self.mode == 2) {
//		dispatch_async(dispatch_get_main_queue(), ^{
//						   [self startSpinner];
//					   });
//	}
}

- (void)viewDidAppear:(BOOL)animated {
//	if (self.mode == 2) {
//		// disable the API call to get the latest rental record
//	[self displayMyRental];
//
//		dispatch_async(dispatch_get_main_queue(), ^{
//						   [self stopSpinner];
//					   });
//	}

	if ([AppView sharedInstance].fingerprint) {
		// NSLog(@"%@",[AppView sharedInstance].fingerprint);
	}
	else {
		// [self openLogin:Nil];
		if (!self.loginController) {
			self.loginController = [self getLogin:self];
			[self addChildViewController:self.loginController];
			[self.view addSubview:self.loginController.view];
			self.loginController.view.frame = CGRectMake(0, 0, 545, 264);
			self.loginController.view.center = self.view.center;
		}
	}
}

- (void)viewDidLoad {
	[super viewDidLoad];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appLogin:)
												 name:@"APP_LOGIN"
											   object:nil];
}

- (void)asetTapped:(id)sender {
	TVEConfig *config = [TVEConfig sharedConfig];
	NSDictionary *rentalMovieRecord = [config getRentalMovieDataFromMediaFeed];

	AssetView *asset = (AssetView *)sender;
	int selected = asset.tag;
	NSArray *entries = nil;

	if (self.mode != 2) {
		entries = [self.data objectForKey:@"entries"];
	}
	else {
		entries = [rentalMovieRecord objectForKey:@"entries"];
	}

	NSDictionary *content = [entries objectAtIndex:selected];

	if ([content objectForKey:@"channel"]) {
		[self openLiveDetail:content withCollection:entries title:@"Favorite Channels"];
	}
	else {
		[self openDetail:content withCollection:entries title:Nil];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
