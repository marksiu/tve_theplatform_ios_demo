//
//  LoginViewController.h
//  TVE
//
//  Created by Mate Beres on 12/10/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "BaseViewController.h"

@protocol LoginViewControllerDelegate <NSObject>

- (void)textFieldDidBeginEditing;
- (void)textFieldDidEndEditing;

@end


@interface LoginViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIAlertViewDelegate>

@property (retain) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UIButton *loginButton;
@property (assign) BOOL showCloseButton;
@property (assign) BOOL isModal;
@property (assign) id delegate;
@end
