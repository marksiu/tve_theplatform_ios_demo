//
//  LoginView.m
//  TVE
//
//  Created by Gabor Bottyan on 11/29/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "LoginView.h"
#import "Constants.h"
@implementation LoginView

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
		float scale = liveTVWidth / subscribeRectSize.width;

		UIImageView *subscribe = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, subscribeRectSize.width * scale, subscribeRectSize.height * scale)];
		subscribe.image = [UIImage imageNamed:@"FreeContent_with_btn.jpg"];
		[self addSubview:subscribe];
		[subscribe release];

		// UIView * subscribeContentView = [[UIView alloc] initWithFrame:CGRectInset(CGRectMake(0, 0, liveTVWidth, liveTVHeight),2,2)];
		// channelContentView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];

		// UIImageView * live = [[UIImageView alloc] initWithFrame:CGRectInset(CGRectMake(0, 0, channelContentView.frame.size.width, channelContentView.frame.size.height), 5, 5)];
		// live.image = [UIImage imageNamed:@"live.png"];
		// live.clipsToBounds = YES;
		// live.backgroundColor = [UIColor greenColor];
		// live.contentMode = UIViewContentModeScaleAspectFill;

		// [channelContentView addSubview:live];
		// [live release];

		// [self addSubview:channelContentView];
		// [channelContentView release];




		UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
		loginButton.frame = CGRectInset(CGRectMake(0, subscribe.frame.size.height + subscribe.frame.origin.y, liveTVWidth - 2, loginButtonHeight), 2, 2);
		[loginButton setBackgroundImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
		loginButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14];

		[loginButton setTitleColor:[UIColor colorWithRed:6.0 / 255.0 green:135.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
		[loginButton setTitle:@"Login" forState:UIControlStateNormal];
		[loginButton addTarget:self action:@selector(openLogin:) forControlEvents:UIControlEventTouchUpInside];
		[self addSubview:loginButton];




		UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
		registerButton.frame = CGRectInset(CGRectMake(0, subscribe.frame.size.height + subscribe.frame.origin.y + loginButtonHeight, liveTVWidth - 2, loginButtonHeight), 2, 2);
		[registerButton setBackgroundImage:[UIImage imageNamed:@"btn_right.png"] forState:UIControlStateNormal];
		registerButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14];
		[registerButton setTitle:@"Register" forState:UIControlStateNormal];
		registerButton.titleLabel.textColor = [UIColor whiteColor];
		[self addSubview:registerButton];


		UIImage *adImage = [UIImage imageNamed:@"car_ad.png"];
		UIImage *adImage2 = [UIImage imageNamed:@"gift_ad.png"];

		UIImageView *ad1 = [[UIImageView alloc] initWithFrame:CGRectInset(CGRectMake(0, subscribe.frame.size.height + subscribe.frame.origin.y + loginButtonHeight * 2, adSize.width, adSize.height), 1, 1)];
		ad1.image = adImage;
		[self addSubview:ad1];
		[ad1 release];

		UIImageView *ad2 = [[UIImageView alloc] initWithFrame:CGRectOffset(ad1.frame, 0, adSize.height)];
		ad2.image = adImage2;
		[self addSubview:ad2];
		[ad2 release];
	}
	return self;
}

- (void)openLogin:(id)sender {
	if (self.delegate) {
		[self.delegate openLogin:Nil];
	}
}

/*
 * // Only override drawRect: if you perform custom drawing.
 * // An empty implementation adversely affects performance during animation.
 * - (void)drawRect:(CGRect)rect
 * {
 *  // Drawing code
 * }
 */

@end
