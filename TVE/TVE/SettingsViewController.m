//
//  SettingsViewController.m
//  TVE
//
//  Created by Mate Beres on 12/5/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "SettingsViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppView.h"
#import <AccedoConnect/AccedoConnect.h>

#import "Constants.h"

#import <Security/Security.h>
#import "KeychainItemWrapper.h"
#import "TVEConfig.h"

@interface SettingsViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = Nil;

	NSString *cellType = @"SettingsCell";

	if ([AppView sharedInstance].fingerprint && indexPath.section == 0 && indexPath.row == 2) {
		cellType = @"OperatorCell";
	}
	cell = [tableView dequeueReusableCellWithIdentifier:cellType];

	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellType] autorelease];

		if ([AppView sharedInstance].fingerprint && indexPath.section == 0 && indexPath.row == 2) {
			UIImageView *view = [[[UIImageView alloc] initWithFrame:CGRectMake(340, 5, 120, 60)] autorelease];
			view.contentMode = UIViewContentModeScaleAspectFit;
			view.tag = 1;
			[cell addSubview:view];
		}
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
	}

	NSString *uname = [[AppView sharedInstance].fingerprint stringByReplacingOccurrencesOfString:@"fingerprint" withString:@""];

	if ([AppView sharedInstance].fingerprint && indexPath.section == 0) {
		if (indexPath.row == 0) {
			cell.textLabel.text = @"Username";
			cell.detailTextLabel.text = 0 < [uname length] ? uname : @"";
		}
		else if (indexPath.row == 1) {
			cell.textLabel.text = @"Subscription Term";
			cell.detailTextLabel.text = 0 < [uname length] ? @"Expires in 14 days" : @"";
		}
		else if (indexPath.row == 2) {
			cell.textLabel.text = @"Content Provider";
			((UIImageView *)[cell viewWithTag:1]).image = nil;
		}
	}
	else if (([AppView sharedInstance].fingerprint && indexPath.section == 1) || indexPath.section == 0) {
		cell.textLabel.text = @"Device Connection";
		if ([ACManager sharedInstance].isPaired) {
			cell.detailTextLabel.text  = [ACManager sharedInstance].statusRecorder.pairedDeviceName;
		}
		else {
			cell.detailTextLabel.text = @"No Connection";
		}
	}
	else if (([AppView sharedInstance].fingerprint && indexPath.section == 2) || indexPath.section == 1) {
		cell.textLabel.text = @"Version";
		cell.detailTextLabel.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"TVEVersion"];
	}
	return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if ([AppView sharedInstance].fingerprint && section == 0) {
		return 2;
	}
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if ([AppView sharedInstance].fingerprint && indexPath.section == 0 && indexPath.row == 2) {
		return 70;
	}
	return 42;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	if ([AppView sharedInstance].fingerprint) {
		return 3;
	}
	return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	if ([AppView sharedInstance].fingerprint && section == 0) {
		return @"Account Information";
	}
	return nil;
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}

- (void)removeEndUserDataFromKeychain {
	// retrieve the admin token
	KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"login" accessGroup:nil];
	NSString *keychainValueAfterEndUserLogin = [keychainItem objectForKey:(id)kSecValueData];
	NSString *keySeparator = @"#*#";
	NSArray *keychainArrAfterEndUserLogin = [keychainValueAfterEndUserLogin componentsSeparatedByString:keySeparator];
	NSString *adminToken = [keychainArrAfterEndUserLogin objectAtIndex:0];

	NSLog(@"adminToken: %@", adminToken);

	// *****************************************************************
	//                       The above code okay
	// *****************************************************************

	// reset the keychain value
	[keychainItem resetKeychainItem];

	// set the admin token and assign NULL value to end user info
	NSString *nullStr = @"NULL";
	NSString *loginKeychainValue = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", adminToken, keySeparator, nullStr, keySeparator, nullStr, keySeparator, nullStr];

	// set the value
	[keychainItem setObject:loginKeychainValue forKey:(id)kSecValueData];

	// check keychain value
	NSString *checkKeychainValue = [keychainItem objectForKey:(id)kSecValueData];
	NSLog(@"SettingsViewController.m keychain: %@", checkKeychainValue);
}

- (IBAction)logout:(id)sender {
	// NSLog(@"------------------------------- LOGOUT");

	// key rental movie record
	TVEConfig *config = [TVEConfig sharedConfig];

	[config clearRentalMovieRecord];

	// clear keychain
	[self removeEndUserDataFromKeychain];
	[[AppView sharedInstance] logout];

	[self close:Nil];
}

- (void)close:(id)sender {
	[CURRENT_WINDOW.rootViewController dismissViewControllerAnimated:YES completion:^{}];
}

- (void)viewDidLoad {
	[super viewDidLoad];
	self.title = @"Settings";
	self.navigationItem.rightBarButtonItem  = [[[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(close:)] autorelease];

	if ([AppView sharedInstance].fingerprint) {
		self.navigationItem.leftBarButtonItem  = [[[UIBarButtonItem alloc] initWithTitle:@"Logout"
																				   style:UIBarButtonItemStyleDone
																				  target:self
																				  action:@selector(logout:)]
												  autorelease];
	}
	else {
		self.navigationItem.leftBarButtonItem  = Nil;
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)clearSubscriptions:(id)sender {
	[[AppView sharedInstance] clearSubscription];
}

- (IBAction)clearRental:(id)sender {
	[[AppView sharedInstance] clearRentalData];
}

@end
