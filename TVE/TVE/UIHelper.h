//
//  UIHelper.h
//  TVE
//
//  Created by Gabor Bottyan on 12/4/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIHelper : NSObject {
}
+ (NSString *)getImageUrl:(NSDictionary *)entry forSize:(CGSize)size;
+ (NSString *)getTVShowImageUrl:(NSDictionary *)entry;
+ (NSString *)getVideoUrl:(NSDictionary *)entry;
+ (NSDictionary *)getVideo:(NSDictionary *)entry;
+ (NSString *)getEpisode:(NSDictionary *)item;
+ (NSString *)getSeason:(NSDictionary *)item;
@end
