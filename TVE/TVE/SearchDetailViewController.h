//
//  SearchDetailViewController.h
//  TVE
//
//  Created by Gabor Bottyan on 12/21/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "OnDemandViewController.h"

@interface SearchDetailViewController : OnDemandViewController
- (id)initWithQuery:(NSString *)__query;
@property (retain) NSString *query;
@end
