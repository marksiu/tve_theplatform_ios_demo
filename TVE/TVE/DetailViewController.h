//
//  DetailViewController.h
//  TVE
//
//  Created by Mate Beres on 11/29/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "BaseViewController.h"
#import "DetailsListView.h"


@interface DetailViewController : BaseViewController <DetailsListViewDelegate>{
}
- (id)initWithData:(NSDictionary *)__data withCollection:(NSArray *)col title:(NSString *)title;
@end
