//
//  BaseViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 11/28/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

@class BaseViewController;
#import "BaseViewController.h"
#import "DetailViewController.h"
#import "SettingsViewController.h"
#import "LoginViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import <Social/Social.h>
#import "RemoteViewController.h"
#import "ProgramInfoPopupViewController.h"
#import "SearchDetailViewController.h"
#import "SearchViewController.h"
#import "PinViewController.h"
#import "UIHelper.h"
#import "UIImage+Extension.h"
#import "SelectedLiveTVViewController.h"
#import <AccedoConnect/AccedoConnect.h>

#import "Constants.h"


#import "UIViewController+TVERemoteControlPanel.h"

@interface BaseViewController ()

@property (retain) UIView *smokey;

@property (nonatomic, retain) UIButton *acRemoteNavButton;

@end

@implementation BaseViewController

@synthesize acRemoteController;
@synthesize acPairingPopup;
@synthesize acRemoteNavButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.

	self.smokey = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1024, 655)];
	self.smokey.backgroundColor = [UIColor colorWithWhite:0 alpha:.8];
	UIActivityIndicatorView *a = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	a.tag = 1;
	a.center = self.smokey.center;
	[self.smokey addSubview:a];
	[self.view addSubview:self.smokey];
	self.smokey.hidden = YES;

	self.acRemoteController = [[[ACRemoteControlPopup alloc] initWithNibName:@"ACRemoteControlPopup"
																	  bundle:nil] autorelease];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackStatusUpdated:) name:@"PLAYBACK_STATUS_CHANGED" object:[ACManager sharedInstance]];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didClientDisconnected:) name:@"AC_CLIENT_DISCONNECTED" object:[ACManager sharedInstance]];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didACDisconnected) name:@"AC_DISCONNECTED" object:[ACManager sharedInstance]];


	if ([self respondsToSelector:@selector(playbackStatusUpdated:)]) {
		[self performSelector:@selector(playbackStatusUpdated:)
				   withObject:[ACManager sharedInstance].statusRecorder.lastPlaybackStatus];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)startSpinner {
	[self.view bringSubviewToFront:self.smokey];
	self.smokey.hidden = NO;
	self.smokey.alpha = 0;
	[((UIActivityIndicatorView *)[self.smokey viewWithTag:1])startAnimating];
	[UIView animateWithDuration:.3 animations:^{
		 self.smokey.alpha = 1;
	 }];
}

- (void)stopSpinner {
	self.smokey.hidden = NO;
	self.smokey.alpha = 1;
	[UIView animateWithDuration:.3 animations:^{
		 self.smokey.alpha = 0;
	 } completion:^(BOOL finished) {
		 [((UIActivityIndicatorView *)[self.smokey viewWithTag:1])stopAnimating];
		 self.smokey.hidden = YES;
	 }];
}


- (void)openLiveDetail:(NSDictionary *)data withCollection:(NSArray *)collection title:(NSString *)title {
	SelectedLiveTVViewController *det = [[SelectedLiveTVViewController alloc] initWithData:data withCollection:collection title:title];

	[self.navigationController pushViewController:det animated:YES];
	[det release];
}

- (void)openDetail:(NSDictionary *)data withCollection:(NSArray *)collection title:(NSString *)title {
	DetailViewController *det = [[DetailViewController alloc] initWithData:data withCollection:collection title:title];

	[self.navigationController pushViewController:det animated:YES];
	[det release];
}

- (void)openDetailOndemand:(NSDictionary *)data withCollection:(NSArray *)collection title:(NSString *)title {
	DetailViewController *det = [[DetailViewController alloc] initWithData:data withCollection:collection title:title
		];
	AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;

	self.tabBarController.selectedIndex = app.onDemandIndex;
	// NSLog(@"ON DEMAND INDEX: %d", app.onDemandIndex);
	[app.onDemandStack pushViewController:det animated:YES];
	[det release];
}

- (void)customNavigationBar {
	UINavigationBar *navBar = [self.navigationController navigationBar];

	[navBar setTintColor:[UIColor blackColor]];

	if ([navBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
		[navBar setBackgroundImage:[UIImage imageNamed:@"top.png"] forBarMetrics:UIBarMetricsDefault];
	}
	else {
		UIImageView *imageView = (UIImageView *)[navBar viewWithTag:12000];
		if (imageView == nil) {
			imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top.png"]];
			[imageView setTag:12000];
			[navBar insertSubview:imageView atIndex:0];
			[imageView release];
		}
	}


	UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[settingsButton addTarget:self action:@selector(openSettings) forControlEvents:UIControlEventTouchUpInside];
	[settingsButton setBackgroundImage:[UIImage imageNamed:@"setting.png"] forState:UIControlStateNormal];
	settingsButton.frame = CGRectMake(0, 0, 23, 23);
	UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[searchButton addTarget:self action:@selector(openSearch) forControlEvents:UIControlEventTouchUpInside];
	[searchButton setBackgroundImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
	searchButton.frame = CGRectMake(53, 2, 21, 21);


	UIView *parentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 23)];
	[parentView addSubview:settingsButton];
	[parentView addSubview:searchButton];
	parentView.backgroundColor = [UIColor clearColor];
	UIBarButtonItem *customBarButtomItem = [[UIBarButtonItem alloc] initWithCustomView:parentView];
	[parentView release];
	[self.navigationItem setRightBarButtonItem:customBarButtomItem animated:YES];

	UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 49, navBar.frame.size.height)];
	[leftView setBackgroundColor:[UIColor clearColor]];
	self.acRemoteNavButton = [self acRemoteButton];
	[leftView addSubview:self.acRemoteNavButton];
	[self.navigationItem setLeftBarButtonItem:[[[UIBarButtonItem alloc] initWithCustomView:leftView] autorelease]];
	[leftView release];
	[customBarButtomItem release];

	self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage externalImageNamed:@"logo"]];
}

- (void)customNavigationBar:(UIViewController *)vc {
	UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];

	[settingsButton addTarget:self action:@selector(openSettings) forControlEvents:UIControlEventTouchUpInside];
	[settingsButton setBackgroundImage:[UIImage imageNamed:@"setting.png"] forState:UIControlStateNormal];
	settingsButton.frame = CGRectMake(0, 0, 23, 23);

	UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[searchButton addTarget:self action:@selector(openSearch) forControlEvents:UIControlEventTouchUpInside];
	[searchButton setBackgroundImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
	searchButton.frame = CGRectMake(53, 2, 21, 21);

	UIView *parentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 130, 23)];
	[parentView addSubview:settingsButton];
	[parentView addSubview:searchButton];
	parentView.backgroundColor = [UIColor clearColor];

	UIBarButtonItem *customBarButtomItem = [[UIBarButtonItem alloc] initWithCustomView:parentView];
	[parentView release];

	[vc.navigationItem setRightBarButtonItem:customBarButtomItem animated:YES];

	[customBarButtomItem release];
}

- (void)openSettings {
	SettingsViewController *set = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:Nil];

	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:set];

	nav.modalPresentationStyle = UIModalPresentationFormSheet;
	// UIModalPresentationPageSheet;
	// nav.navigationItem.title = @"Settings";

	nav.view.layer.cornerRadius = 0;
	[CURRENT_WINDOW.rootViewController presentViewController:nav animated:YES completion:^{}];

	nav.view.superview.frame = CGRectMake (0, 100, 510, 540);
	CGPoint p = self.view.center;
	nav.view.superview.center = CGPointMake (p.x, p.y + 50);

	[nav release];
	[set release];
}

- (void)openSearch {
	SearchViewController *content = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil];

	content.delegate = self;
	UIPopoverController *controller = [[UIPopoverController alloc] initWithContentViewController:content];
	content.popover = controller;
	controller.popoverContentSize = CGSizeMake (334, 44);
	// controller.contentViewController.contentSizeForViewInPopover = CGSizeMake(337, 290);
	// content.contentSizeForViewInPopover = CGSizeMake(334, 400);

	UIBarButtonItem *bbi = self.navigationItem.rightBarButtonItem;

	if (bbi) {
		[controller presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionUp animated:NO];
	}
	else {
		[controller presentPopoverFromRect:CGRectMake(990, 20, 10, 10) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
	}
}

- (void)playEmbedded:(NSDictionary *)content inView:(UIView *)container {
	BOOL isStream = ([[content valueForKey:@"channel"] length] > 0);

	[self playMovie:content isStream:isStream isRemote:NO initialPosition:0 containerView:container];
}

- (void)startVideoIfPossible {
	NSLog(@"START VIDEO IF POSSIBLE");
}

- (void)doSearch:(NSString *)query {
	if ([self isKindOfClass:[SearchDetailViewController class]]) {
		((SearchDetailViewController *)self).query = query;
		[((SearchDetailViewController *)self)fetchData];
	}
	else {
		SearchDetailViewController *det = [[SearchDetailViewController alloc] initWithQuery:query ];
		AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
		self.tabBarController.selectedIndex = app.onDemandIndex;
		[app performSelector:@selector(tabBarController:didSelectViewController:) withObject:self.tabBarController withObject:app.onDemandStack];
		// NSLog(@"ON DEMAND INDEX: %d", app.onDemandIndex);
		[app.onDemandStack pushViewController:det animated:YES];
		// [det release];

		// [self.navigationController pushViewController:det animated:YES];
		[det release];
	}
}

- (void)fetchData {
	NSLog(@"THIS SHOULD NOT HAVE BEEN CALLED !!!!!!!!");
}

- (void)openLogin:(id)sender {
	[self openLogin:sender withCloseButton:YES];
}

- (UIViewController *)getLogin:(id)delegate {
	LoginViewController *set = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:Nil];

	set.showCloseButton = NO;
	UINavigationController *nav = [[[UINavigationController alloc] initWithRootViewController:set] autorelease];
	nav.modalPresentationStyle = UIModalPresentationFormSheet;
	// UIModalPresentationPageSheet;
	// nav.navigationItem.title = @"Settings";
	set.delegate  = delegate;
	nav.view.layer.cornerRadius = 0;
	nav.view.layer.borderColor = [[UIColor blackColor] CGColor];
	nav.view.superview.layer.borderColor = [[UIColor blackColor] CGColor];
	nav.view.superview.layer.cornerRadius = 10;
	nav.navigationItem.rightBarButtonItem = Nil;
	set.isModal  = NO;
	UINavigationBar *navBar = [nav navigationBar];
	navBar.barStyle = UIBarStyleBlack;
	NSDictionary *textTitleOptions = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, [UIColor clearColor], UITextAttributeTextShadowColor, [NSNumber numberWithInt:0], UITextAttributeTextShadowOffset, nil];
	navBar.layer.borderColor = [[UIColor clearColor] CGColor];
	navBar.titleTextAttributes = textTitleOptions;
	// [navBar setTintColor:[UIColor blackColor]];
	//    [[CURRENT_WINDOW.rootViewController presentViewController:nav animated:YES completion:^{
	//    }];



	// [nav release];
	[set release];

	return nav;
}

- (void)openLogin:(id)sender withCloseButton:(BOOL)showCloseButton {
	LoginViewController *set = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:Nil];

	set.showCloseButton = showCloseButton;
	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:set];
	nav.modalPresentationStyle = UIModalPresentationFormSheet;
	// UIModalPresentationPageSheet;
	// nav.navigationItem.title = @"Settings";
	set.isModal  = YES;
	nav.view.layer.cornerRadius = 0;
	nav.view.layer.borderColor = [[UIColor blackColor] CGColor];
	nav.view.superview.layer.borderColor = [[UIColor blackColor] CGColor];
	nav.navigationItem.rightBarButtonItem = Nil;

	UINavigationBar *navBar = [nav navigationBar];
	navBar.barStyle = UIBarStyleBlack;
	NSDictionary *textTitleOptions = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], UITextAttributeTextColor, [UIColor clearColor], UITextAttributeTextShadowColor, [NSNumber numberWithInt:0], UITextAttributeTextShadowOffset, nil];
	navBar.layer.borderColor = [[UIColor clearColor] CGColor];
	navBar.titleTextAttributes = textTitleOptions;
	// [navBar setTintColor:[UIColor blackColor]];
	[CURRENT_WINDOW.rootViewController presentViewController:nav animated:YES completion:^{
	 }];

	nav.view.superview.frame = CGRectMake(0, 0, 545, 264);
	CGPoint p = self.view.center;
	nav.view.superview.center = CGPointMake(p.x, p.y);

	[nav release];
	[set release];
}

- (void)shareFacebookPopup:(NSString *)text {
	SLComposeViewController *composeController = [SLComposeViewController
												  composeViewControllerForServiceType:SLServiceTypeFacebook];

	// composeController.title = @"Post to Facebook Wall";

	[composeController setInitialText:[NSString stringWithFormat:@"Check out %@! I watched it through Accedo TVE app #TVE", text]];

	[self presentViewController:composeController
					   animated:YES completion:nil];
}

- (void)shareTwitterPopup:(NSString *)text {
	SLComposeViewController *composeController = [SLComposeViewController
												  composeViewControllerForServiceType:SLServiceTypeTwitter];

	// composeController.title = @"Share to Twitter";

	// [composeController setInitialText:[NSString stringWithFormat:@"%@ via @TVE app", text]];
	[composeController setInitialText:[NSString stringWithFormat:@"Check out %@! I watched it through Accedo TVE app #TVE", text]];

	[self presentViewController:composeController
					   animated:YES completion:nil];
}

- (void)viewDidAppear:(BOOL)animated {
}

- (void)viewWillDisappear:(BOOL)animated {
}

- (NSUInteger)supportedInterfaceOrientations {
	return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotate {
	return YES;
}

@end
