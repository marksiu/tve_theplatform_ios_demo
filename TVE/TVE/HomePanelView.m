//
//  HomePanelView.m
//  TVE
//
//  Created by Gabor Bottyan on 11/28/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "HomePanelView.h"
#import "VSRemoteImageView.h"
#import "SBJson.h"
#import "Constants.h"
#import "AssetView.h"
#import "UIHelper.h"
#import "ThePlatform.h"
#import "BaseViewController.h"

@interface  HomePanelView ()

@property (retain) NSDictionary *metadata;
@property (retain) NSDictionary *data;
@property (retain) VSRemoteImageView *imageView;
@property (retain) UIButton *button;
@property (retain) NSString *title;

@end

@implementation HomePanelView

- (id)initWithFrame:(CGRect)frame title:(NSString * )title data:(NSDictionary * )__data {
	self = [super initWithFrame:frame];

	srand( (unsigned)time(NULL) );

	if (self) {
		self.title = title;
		self.metadata = __data;
		// @stef: inset set to 1,1 (old valuse 2,2)
		UIView *blackBackground = [[UIView alloc] initWithFrame:CGRectInset(CGRectMake(0, 0, frame.size.width, frame.size.height), 1, 1)];
		blackBackground.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
		[self addSubview:blackBackground];
		[blackBackground release];

		self.imageView = [[[VSRemoteImageView alloc] initWithFrame:CGRectMake(7, 7, frame.size.width - 14, frame.size.height - 25)] autorelease];
		[self addSubview:self.imageView];

		UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 140, frame.size.width, 100)];
		titleLabel.text = [title uppercaseString];
		titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:34];
		titleLabel.textAlignment = NSTextAlignmentLeft;
		titleLabel.backgroundColor = [UIColor clearColor];
		titleLabel.textColor = [UIColor whiteColor];
		[self addSubview:titleLabel];
		[titleLabel release];

		self.arrowView = [[[UIImageView alloc] initWithFrame:CGRectMake(itemRectSize.width / 2 - arrowRectSize.width / 2, self.imageView.frame.origin.y + self.imageView.frame.size.height + 1, arrowRectSize.width, arrowRectSize.height)] autorelease];
		[self addSubview:self.arrowView];
		// self.arrowView.image = [UIImage imageNamed:@"arrow_down.png"];
		self.arrowView.alpha = 0;

		self.button = [UIButton buttonWithType:UIButtonTypeCustom];
		self.button.userInteractionEnabled = NO;
		self.button.frame = self.imageView.frame;
		[self addSubview:self.button];
		[self.button addTarget:self action:@selector(pushed:) forControlEvents:UIControlEventTouchUpInside];
		self.clipsToBounds = YES;

		UIView *dropDownView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 0)];
		dropDownView.clipsToBounds = YES;
		// dropDownView.backgroundColor = [UIColor clearColor];
		[self addSubview:dropDownView];
		self.contentView = dropDownView;
		[dropDownView release];
		[self fetchData];
	}
	return self;
}

- (void)fetchData {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   NSDictionary *c = [[ThePlatform sharedInstance] getData:self.metadata];
	                   // (NSDictionary *)[content JSONValue];


	                   // NSLog(@"%@", self.metadata);
					   NSArray *entries = [c objectForKey:@"entries"];

					   int count = [entries count];

					   if (count > 5) {
						   self.itemCount = MIN (count, 6);
						   self.contentHeight = 5 * itemRectSize.height + itemRectSize.height / 2;
					   }
					   else {
						   self.itemCount = MIN (count, 6);
						   self.contentHeight = MIN (count, 6) * itemRectSize.height;
					   }


					   self.data = c;

					   dispatch_async (dispatch_get_main_queue (), ^{
										   for (int i = 0; i < MIN (count, 6); i++) {
											   NSDictionary *entry = [entries objectAtIndex:i];

											   if (i == 0) {
												   NSString *imageUrl = [UIHelper getImageUrl:entry forSize:CGSizeMake (self.frame.size.width - 14, self.frame.size.height - 25)];
												   [self.imageView setImageUrl:imageUrl withPlaceholder:nil animated:YES];
											   }

											   if (i > 4) {
												   AssetView *assetView = [[[AssetView alloc] initWithStyle:ASSET_TYPE_HOME_LIST_MORE data:entry] autorelease];
												   assetView.frame = CGRectMake (0, i * assetView.frame.size.height * 2, assetView.frame.size.width, assetView.frame.size.height);

												   assetView.delegate = self;
												   assetView.tag = i;
												   [self.contentView addSubview:assetView];
												   break;
											   }
											   else {
												   AssetView *assetView = [[[AssetView alloc] initWithStyle:ASSET_TYPE_HOME_LIST data:entry] autorelease];

												   assetView.frame = CGRectMake (0, i * assetView.frame.size.height, assetView.frame.size.width, assetView.frame.size.height);
												   assetView.tag = i;
												   assetView.delegate = self;

												   [self.contentView addSubview:assetView];
											   }

											   self.button.userInteractionEnabled = YES;
										   }
									   });
				   });
}

- (void)asetTapped:(id)sender {
	AssetView *asset = (AssetView *)sender;
	int selected = asset.tag;

	// Fixed the home page
//	if (selected > 2) {
	if (selected > 4) {
		NSArray *entries = [self.data objectForKey:@"entries"];
		NSDictionary *content = [entries objectAtIndex:0];

		[self.detaildelegate openDetail:content withCollection:entries title:self.title];
	}
	else {
		if ([[self.metadata objectForKey:@"type"] isEqualToString:@"episodeCollection"]) {
			BaseViewController *bvc = (BaseViewController *)self.detaildelegate;
			NSArray *entries = [self.data objectForKey:@"entries"];
			NSDictionary *content = [entries objectAtIndex:selected];
			[bvc playMovie:content isStream:NO isRemote:NO initialPosition:0 containerView:Nil];
		}
		else {
			NSArray *entries = [self.data objectForKey:@"entries"];
			NSDictionary *content = [entries objectAtIndex:selected];
			[self.detaildelegate openDetail:content withCollection:entries title:self.title];
		}
	}
}

- (void)pushed:(id)sender {
	if (self.open) {
		self.open = NO;
		// self.arrowView.image = [UIImage imageNamed:@"arrow_down.png"];
		// self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height- self.contentHeight);
		[self.delegate panelWillClose:self];
	}
	else {
		self.open = YES;
		self.arrowView.image = [UIImage imageNamed:@"arrow_up.png"];
		// self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height+ self.contentHeight);
		[self.delegate panelWillOpen:self];
	}

	// NSLog(self.open ? @"YES": @"NO");
}

@end
