//
//  FriendsPopupViewController.h
//  TVE
//
//  Created by Mate Beres on 12/20/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "BaseViewController.h"

@interface FriendsPopupViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>

@end
