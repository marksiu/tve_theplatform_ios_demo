//
//  RemoteViewController.h
//  TVE
//
//  Created by Gabor Bottyan on 12/11/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import "JSONViewController.h"
#import <AccedoConnect/ACStatusRecorder.h>
@interface RemoteViewController : JSONViewController


@end
