//
//  DataHelper.m
//  TVE
//
//  Created by Gabor Bottyan on 12/18/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "ThePlatform.h"
#import "SBJson.h"

#define ADMIN                        1

#define SIGNIN_URL                   @"/signIn?"
#define AUTHENTICATION_URL_KEY       @"endUserAuthenticationEndpoint"
#define ADMIN_AUTHENTICATION_URL_KEY @"adminAuthenticationEndpoint"
#define DIR_ID_KEY                   @"endUserAuthenticationDirectoryId"
#define ADMIN_DIR_ID_KEY             @"adminAuthenticationDirectoryId"
#define AUTH_SCHEMA_KEY              @"authenticationEndpointSchema"
#define SCHEMA_KEY                   @"schema"
#define FORMAT_KEY                   @"format"
#define PRETTY_KEY                   @"pretty"
#define MEDIA_URL_KEY                @"mediaObjectUrl"
#define FEED_URL_KEY                 @"feedBaseUrl"
#define CATHEGORY_BASE_KEY           @"categoryBaseUrl"
#define FEED_PID_KEY                 @"feedPid"
#define FEED_REGISTRY_KEY            @"feedRegistryId"

@interface  ThePlatform   ()
@property (retain) NSDictionary *authData;

@end



@implementation ThePlatform
static ThePlatform *sharedInstance = nil;

/**
 * "com.accedo.plugin.ThePlatformOnDemand":{
 *  "pluginKey":"com.accedo.plugin.ThePlatformOnDemand",
 *  "enabled":true,
 *  "config":{
 *      "tvSeriesCatId":"9768515932",
 *      "accountId":"http://access.auth.theplatform.com/data/Account/2301538417",
 *      "mediaObjectUrl":"http://data.media.theplatform.com/media/data/Media",
 *      "tvEpisodeCatId":"11240515733",
 *      "adminAuthenticationEndpoint":"https://identity.auth.theplatform.com/idm/web/Authentication",
 *      "pageSize":"6",
 *      "authenticationEndpointSchema":"1.0",
 *      "feedPid":"DwE21wX1pGqZ",
 *      "format":"json",
 *      "endUserAuthenticationEndpoint":"https://euid.theplatform.com/idm/web/Authentication",
 *      "pretty":"true",
 *      "schema":"1.2",
 *      "movieCatId":"9768515931",
 *      "endUserAuthenticationDirectoryId":"IJHRpd4czHsVFdob",
 *      "tvClipCatId":"11975747634",
 *      "feedBaseUrl":"http://feed.theplatform.com/f",
 *      "adminAuthenticationDirectoryId":"mpx",
 *      "categoryBaseUrl":"http://data.media.theplatform.com/media/data/Category/",
 *      "feedRegistryId":"xBrLJC",
 *      "ratingEndPointUrl":"http://data.social.community.theplatform.com/social/data/Rating"
 *  }
 * }
 * 11240515733
 */


+ (ThePlatform *)sharedInstance {
	if (sharedInstance == nil) {
		sharedInstance = [[super allocWithZone:NULL] init];
	}

	return sharedInstance;
}

// We can still have a regular init method, that will get called the first time the Singleton is used.
- (id)init {
	self = [super init];

	if (self) {
		// Work your initialising magic here as you normally would
	}

	return self;
}

// Your dealloc method will never be called, as the singleton survives for the duration of your app.
// However, I like to include it so I know what memory I'm using (and incase, one day, I convert away from Singleton).
- (void)dealloc {
	// I'm never called!
	[super dealloc];
}

// We don't want to allocate a new instance, so return the current one.
+ (id)allocWithZone:(NSZone *)zone {
	return [[self sharedInstance] retain];
}

// Equally, we don't want to generate multiple copies of the singleton.
- (id)copyWithZone:(NSZone *)zone {
	return self;
}

// Once again - do nothing, as we don't have a retain counter for this object.
- (id)retain {
	return self;
}

// Replace the retain counter so we can never release this object.
- (NSUInteger)retainCount {
	return NSUIntegerMax;
}

// This function is empty, as we don't want to let the user release this object.
- (oneway void)release {
}

// Do nothing, other than return the shared instance - as this is expected from autorelease.
- (id)autorelease {
	return self;
}

// http://feed.theplatform.com/f/xBrLJC/DwE21wX1pGqZ?form=json&schema=1.2&byCategoryIds=http%3A%2F%2Fdata.media.theplatform.com%2Fmedia%2Fdata%2FCategory%2F9768515931



+ (NSString *)getRelatedQueryString1:(NSDictionary *)dict {
	NSMutableString *str = [NSMutableString string];

	NSArray *credits = [dict objectForKey:@"media$credits"];


	if (credits.count > 0) {
		NSDictionary *credit = [credits objectAtIndex:0];
		NSString *value = [credit objectForKey:@"media$value"];
		if (value) {
			NSString *inter = [value stringByReplacingOccurrencesOfString:@"  " withString:@" "];
			inter =  [inter stringByReplacingOccurrencesOfString:@" " withString:@"+"];
			[str appendFormat:@"%@", inter];
		}
	}



	return str;
}

+ (NSString *)getRelatedQueryString:(NSDictionary *)dict {
	NSMutableString *str = [NSMutableString string];

	NSArray *credits = [dict objectForKey:@"media$credits"];

	for (int i = 0; i < 2 || i < [credits count]; i++) {
		NSDictionary *credit = [credits objectAtIndex:i];
		NSString *value = [credit objectForKey:@"media$value"];
		if (value) {
			if (str.length > 0) {
				[str appendFormat:@" %@", value];
			}
			else {
				[str appendFormat:@"%@", value];
			}
		}
	}


	NSString *inter = [str stringByReplacingOccurrencesOfString:@"  " withString:@" "];
	return [inter stringByReplacingOccurrencesOfString:@" " withString:@"+"];
}

+ (NSString *)urlencode:(NSString *)str {
	return [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	/*
	 *
	 * NSMutableString *output = [NSMutableString string];
	 * const unsigned char *source = (const unsigned char *)[str UTF8String];
	 * int sourceLen = strlen((const char *)source);
	 * for (int i = 0; i < sourceLen; ++i) {
	 *  const unsigned char thisChar = source[i];
	 *  if (thisChar == ' '){
	 *      [output appendString:@"+"];
	 *  } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
	 *             (thisChar >= 'a' && thisChar <= 'z') ||
	 *             (thisChar >= 'A' && thisChar <= 'Z') ||
	 *             (thisChar >= '0' && thisChar <= '9')) {
	 *      [output appendFormat:@"%c", thisChar];
	 *  } else {
	 *      [output appendFormat:@"%%%02X", thisChar];
	 *  }
	 * }
	 * return output;
	 */
}

- (NSDictionary *)getTVClips {
	NSDictionary *cfg = [self.config objectForKey:@"config"];
	NSString *cathegoryId = [cfg objectForKey:@"tvClipCatId"];
	NSString *query =  [NSString stringWithFormat:@"&byCategoryIds=%@", [ThePlatform urlencode:[self getAssetsByCategory:cathegoryId]]];

	return [self getFeed:query];
}

- (NSDictionary *)getTVSeries:(NSString *)q {
	NSDictionary *cfg = [self.config objectForKey:@"config"];
	NSString *cathegoryId = [cfg objectForKey:@"tvSeriesCatId"];
	NSString *query =  [NSString stringWithFormat:@"&byCategoryIds=%@", [ThePlatform urlencode:[self getAssetsByCategory:cathegoryId]]];

	if (q) {
		query = [query stringByAppendingString:[ThePlatform urlencode:q]];
	}
	return [self getFeed:query];
}

- (NSDictionary *)getEpisodes {
	NSDictionary *cfg = [self.config objectForKey:@"config"];
	NSString *cathegoryId = [cfg objectForKey:@"tvEpisodeCatId"];
	NSString *query =  [NSString stringWithFormat:@"&byCategoryIds=%@", [ThePlatform urlencode:[self getAssetsByCategory:cathegoryId]]];

	return [self getFeed:query];
}

- (NSDictionary *)getMovies:(NSString *)q {
	NSDictionary *cfg = [self.config objectForKey:@"config"];
	NSString *cathegoryId = [cfg objectForKey:@"movieCatId"];
	NSString *query =  [NSString stringWithFormat:@"&byCategoryIds=%@", [ThePlatform urlencode:[self getAssetsByCategory:cathegoryId]]];

	if (q) {
		query = [query stringByAppendingString:[ThePlatform urlencode:q]];
	}

	return [self getFeed:query];
}

- (NSDictionary *)getFeed {
	return [self getFeed:Nil];
}

- (NSDictionary *)getDataForId:(NSString *)asetid {
	NSString *query =  [NSString stringWithFormat:@"&byId=%@", [ThePlatform urlencode:asetid]];

	return [self getFeed:query];
}

- (NSDictionary *)getData:(NSDictionary *)feedConfig {
	// http://feed.theplatform.com/f/xBrLJC/DwE21wX1pGqZ?count=true&byCategoryIds=http://data.media.theplatform.com/media/data/Category/11975235945&startIndex=1&endIndex=15&form=json&schema=1.2
	// NSLog(@"Fetching: \n %@ \n", feedConfig);


	NSDictionary *cfg = [self.config objectForKey:@"config"];

	NSDictionary *feed = [feedConfig objectForKey:@"feed"];

	NSDictionary *options = [feed objectForKey:@"options"];

	NSString *form = [cfg objectForKey:FORMAT_KEY];
	NSString *pretty = [cfg objectForKey:PRETTY_KEY];
	NSString *schema = [cfg objectForKey:SCHEMA_KEY];
	NSString *sortBy = [options objectForKey:@"sortBy"];

	NSString *opid = [options objectForKey:@"id"];

	NSString *baseUrl = [self getFeedBaseUrl];
	// [feed objectForKey:@"url"];  ////url;//



	NSString *query = @"";

	if (opid) {
		query  = [NSString stringWithFormat:@"&byCategoryIds=%@", opid];
		// [ThePlatform urlencode: opid]];
	}


	baseUrl = [baseUrl stringByAppendingString:query];
	baseUrl = [[[baseUrl stringByAppendingFormat:@"&form=%@&", form ] stringByAppendingFormat:@"schema=%@&", schema] stringByAppendingFormat:@"pretty=%@", pretty];

	if (sortBy) {
		if ([sortBy isEqualToString:@"rating"]) {
			baseUrl = [baseUrl stringByAppendingString:[@"&sort=:starRating|desc" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
		}
		if ([sortBy isEqualToString:@"availableDate"]) {
			baseUrl = [baseUrl stringByAppendingString:[@"&sort=availableDate|desc" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
		}
	}
	NSLog(@"URL: \n %@ \n", baseUrl);

	NSLog(@"NSURL: \n %@ \n", [NSURL URLWithString:baseUrl]);

	/*
	 * options =                     {
	 *   id = "http://data.media.theplatform.com/media/data/Category/11240515733";
	 *   startIndex = 1;
	 * };
	 * url = "http://feed.theplatform.com/f/xBrLJC/cbomawJ4sB5n";
	 *
	 *
	 * feed =                 {
	 *   options =                     {
	 *   };
	 *   url = "http://feed.theplatform.com/f/xBrLJC/cbomawJ4sB5n";
	 * };
	 * type = channelCollection;
	 */



	NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:baseUrl] encoding:NSUTF8StringEncoding error:Nil];
	NSDictionary *res = [result JSONValue];

	// NSLog(@"DATA: %@", res);

	return res;
}

- (NSString * )getFeedBaseUrl {
	NSDictionary *cfg = [self.config objectForKey:@"config"];
	NSString *endPoint = [cfg objectForKey:FEED_URL_KEY];
	NSString *feedPid = [cfg objectForKey:FEED_PID_KEY];
	NSString *feedRegistry = [cfg objectForKey:FEED_REGISTRY_KEY];

	NSString *baseUrl = [[[endPoint stringByAppendingString:@"/"] stringByAppendingFormat:@"%@/", feedRegistry] stringByAppendingFormat:@"%@?", feedPid ];

	return baseUrl;
}

- (NSDictionary *)getFeed:(NSString *)query {
	// http://feed.theplatform.com/f/xBrLJC/S5aJeXR8giur

	NSDictionary *cfg = [self.config objectForKey:@"config"];
	NSString *endPoint = [cfg objectForKey:FEED_URL_KEY];
	NSString *feedPid = [cfg objectForKey:FEED_PID_KEY];
	NSString *feedRegistry = [cfg objectForKey:FEED_REGISTRY_KEY];


	NSString *form = [cfg objectForKey:FORMAT_KEY];
	NSString *pretty = [cfg objectForKey:PRETTY_KEY];
	NSString *schema = [cfg objectForKey:SCHEMA_KEY];
	NSString *token = [self.authData objectForKey:@"token"];

	NSString *baseUrl = [[[[[[endPoint stringByAppendingString:@"/"] stringByAppendingFormat:@"%@/", feedRegistry] stringByAppendingFormat:@"%@?", feedPid ] stringByAppendingFormat:@"form=%@&", form ] stringByAppendingFormat:@"schema=%@&", schema] stringByAppendingFormat:@"pretty=%@", pretty];

	if (token) {
		baseUrl = [baseUrl stringByAppendingFormat:@"&token=%@", token ];
	}

	if (query) {
		baseUrl = [baseUrl stringByAppendingFormat:@"%@", query];
	}

	NSLog(@"URL: \n %@ \n", baseUrl);



//    http://feed.theplatform.com/f/xBrLJC/DwE21wX1pGqZ?form=json&schema=1.2&byCategoryIds=http%3A%2F%2Fdata.media.theplatform.com%2Fmedia%2Fdata%2FCategory%2F9768515931

	if (![NSURL URLWithString:baseUrl]) {
		NSLog(@"\n\n\n\n\n\n\n\n\n\n\n\n URL WRONG: %@ \n\n\n\n\n\n\n\n\n\n\n\n", baseUrl);
	}
	// NSLog(@"%@",[NSURL URLWithString:[ThePlatform urlencode:baseUrl]]);

	NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:baseUrl] encoding:NSUTF8StringEncoding error:Nil];


	// NSLog(@"CAT: %@", result);


	return [result JSONValue];
	// http://data.media.theplatform.com/media/data/Category/9768515931
}

/*http://data.media.theplatform.com/media/data/Category/9768515931*/
- (NSString *)getAssetsByCategory:(NSString *)cat {
	NSDictionary *cfg = [self.config objectForKey:@"config"];
	NSString *endPoint = [cfg objectForKey:CATHEGORY_BASE_KEY];
	NSString *baseUrl = [[endPoint stringByAppendingString:@""] stringByAppendingFormat:@"%@", cat];

	// NSLog(@"QUERY:\n\n %@ \n\n", baseUrl);
	return baseUrl;
}

// http://feed.theplatform.com/f/xBrLJC/DwE21wX1pGqZ?form=json&schema=1.2&pretty=true&byCredits=Liam+Neeson
- (NSDictionary *)related:(NSString *)query {
	NSString *q =  [NSString stringWithFormat:@"&byCredits=%@", query];

	NSLog(@"RELATED QUERY %@", q);
	return [self getFeed:q];
}

- (NSDictionary *)getEpisodesBySeries:(NSString *)seriesRef {
	// http://feed.theplatform.com/f/xBrLJC/DwE21wX1pGqZ?count=true&byCustomValue={seriesRef}{theWorks}&byCategoryIds=http://data.media.theplatform.com/media/data/Category/11240515733&startIndex=1&endIndex=15&form=json&schema=1.2
	NSDictionary *cfg = [self.config objectForKey:@"config"];

	// NSString* escapedSeriesRef =[seriesRef stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];


	NSString *cathegoryId = [cfg objectForKey:@"tvEpisodeCatId"];
	NSString *query =  [NSString stringWithFormat:@"&startIndex=1&endIndex=15&byCategoryIds=%@", [self getAssetsByCategory:cathegoryId]];
	NSString *customQuery = [NSString stringWithFormat:@"&count=true&byCustomValue={seriesRef}{%@}", seriesRef];

	query =  [query stringByAppendingString:customQuery];
	NSLog(@"EPISODES URL: %@", query);

	NSDictionary *res =  [self getFeed:[ThePlatform urlencode:query]];

	// NSLog(@"%@",res);

	return res;
}

- (NSDictionary *)getClipsBySeries:(NSString *)seriesRef {
// http://feed.theplatform.com/f/xBrLJC/DwE21wX1pGqZ?count=true&byCustomValue={seriesRef}{theWorks}&byCategoryIds=http://data.media.theplatform.com/media/data/Category/11975747634&startIndex=1&endIndex=15&form=json&schema=1.2
	NSDictionary *cfg = [self.config objectForKey:@"config"];

	NSString *cathegoryId = [cfg objectForKey:@"tvClipCatId"];
	NSString *query =  [NSString stringWithFormat:@"&startIndex=1&endIndex=15&byCategoryIds=%@", [self getAssetsByCategory:cathegoryId]];
	NSString *customQuery = [NSString stringWithFormat:@"&count=true&byCustomValue={seriesRef}{%@}", seriesRef];

	query = [query stringByAppendingString:customQuery];
//     [ThePlatform urlencode:

	NSLog(@"Q: %@", query);
	NSDictionary *res =  [self getFeed:[ThePlatform urlencode:query]];

	// NSLog(@"%@",res);

	return res;
}

// http://feed.theplatform.com/f/xBrLJC/UGOWLOj8blVu?q=the&form=json&schema=1.2
- (NSDictionary *)search:(NSString *)query sort:(NSString *)so {
	NSString *q =  [NSString stringWithFormat:@"&q=%@", query];

	if (so) {
		q = [q stringByAppendingString:[ThePlatform urlencode:so]];
	}

	return [self getFeed:q];
}

// http://feed.theplatform.com/f/xBrLJC/S5aJeXR8giur/categories.
- (NSArray *)getCategories {
	NSDictionary *cfg = [self.config objectForKey:@"config"];
	NSString *endPoint = [cfg objectForKey:FEED_URL_KEY];
	NSString *feedPid = [cfg objectForKey:FEED_PID_KEY];
	NSString *feedRegistry = [cfg objectForKey:FEED_REGISTRY_KEY];


	NSString *form = [cfg objectForKey:FORMAT_KEY];
	NSString *pretty = [cfg objectForKey:PRETTY_KEY];
	NSString *schema = [cfg objectForKey:SCHEMA_KEY];
	NSString *token = [self.authData objectForKey:@"token"];

	NSString *baseUrl = [[[[[[[endPoint stringByAppendingString:@"/"] stringByAppendingFormat:@"%@/", feedRegistry] stringByAppendingFormat:@"%@/categories?", feedPid ] stringByAppendingFormat:@"form=%@&", form ] stringByAppendingFormat:@"schema=%@&", schema] stringByAppendingFormat:@"pretty=%@&", pretty] stringByAppendingFormat:@"token=%@", token];

	// NSLog(@"URL: %@", baseUrl);
	//    http://feed.theplatform.com/f/xBrLJC/DwE21wX1pGqZ?form=json&schema=1.2&byCategoryIds=http%3A%2F%2Fdata.media.theplatform.com%2Fmedia%2Fdata%2FCategory%2F9768515931

	NSLog(@"CALLING: \n %@ \n", baseUrl);

	NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:baseUrl] encoding:NSUTF8StringEncoding error:Nil];

	// NSLog(@"CATEGORIES:\n %@  \n", result);

	NSDictionary *res = [result JSONValue];
	return [res objectForKey:@"entries"];
}

/*filter:
 * https://help.theplatform.com/display/vms2/Retrieving+Media+objects
 *
 * byAdded=1297797044000~1297797059000
 * byAddedByUserId
 * byAdminTags
 * byApproved
 * byAvailability
 * byAvailableDate
 * byCategories
 * byCategoryIds
 * byContent
 * byCredits
 * byCustomValue
 * byExcludeId
 * byExpirationDate
 * byGuid
 * byGuidPrefix
 * byId
 * byOwnerId
 * byProgramId
 * byProvider
 * byProviderId
 * byPubDate
 * byRatings
 * byRelatedId
 * byRelatedReleasePid
 * byReleaseGuid
 * byReleaseId
 * byReleasePid
 * bySeriesId
 * byTitle
 * byTitlePrefix
 * byUpdated
 */




- (NSArray *)getMedia:(NSDictionary *)filter query:(NSString *)query config:(NSDictionary *)config {
	NSMutableArray *arr = [NSMutableArray array];

	NSDictionary *cfg = [config objectForKey:@"config"];

	NSString *endPoint = [cfg objectForKey:MEDIA_URL_KEY];
	NSString *schema = [cfg objectForKey:SCHEMA_KEY];
	NSString *form = [cfg objectForKey:FORMAT_KEY];
	NSString *pretty = [cfg objectForKey:PRETTY_KEY];


	NSString *token = [self.authData objectForKey:@"token"];

	NSString *baseUrl = [[[[[endPoint stringByAppendingString:@"/"] stringByAppendingFormat:@"schema=%@&", schema ] stringByAppendingFormat:@"form=%@&", form ] stringByAppendingFormat:@"pretty=%@&", pretty] stringByAppendingFormat:@"token=%@", token];


	if (query) {
		baseUrl = [baseUrl stringByAppendingFormat:@"&q=%@", query];
	}





	return arr;
}

// http://data.media.theplatform.com/media/data/Media?schema=1.2&form=json&token=6Kpa353-183XQnnApLhU0YCh8DBikIC6&byCategories=Action|NGA&q=title:of



- (NSDictionary * )login:( NSString * )username password:(NSString *)password {
	NSDictionary *cfg = [self.config objectForKey:@"config"];


#if ADMIN

	NSString *dir = [cfg objectForKey:ADMIN_DIR_ID_KEY];
	NSString *endPoint = [cfg objectForKey:ADMIN_AUTHENTICATION_URL_KEY];
#else
	NSString *dir = [cfg objectForKey:DIR_ID_KEY];
	NSString *endPoint = [cfg objectForKey:AUTHENTICATION_URL_KEY];

#endif

	NSString *schema = [cfg objectForKey:AUTH_SCHEMA_KEY];
	NSString *form = [cfg objectForKey:FORMAT_KEY];
	NSString *pretty = [cfg objectForKey:PRETTY_KEY];



	NSString *URL = [[[[[[endPoint stringByAppendingString:SIGNIN_URL] stringByAppendingFormat:@"schema=%@&", schema ] stringByAppendingFormat:@"form=%@&", form ] stringByAppendingFormat:@"pretty=%@&", pretty] stringByAppendingFormat:@"username=%@/%@&", dir, username] stringByAppendingFormat:@"password=%@", password];

	NSLog(@"%@", URL);

	NSString *authResult = [NSString stringWithContentsOfURL:[NSURL URLWithString:URL] encoding:NSUTF8StringEncoding error:Nil];

	NSDictionary *auth = [authResult JSONValue];

	if (self.authData) {
		[self.authData release];
		self.authData = Nil;
	}

	self.authData = [auth objectForKey:@"signInResponse"];



	if (self.authData) {
		return [self.authData retain];
	}
	else {
		return Nil;
	}
}

@end
