//
//  AppDelegate.m
//  TVE
//
//  Created by Gabor Bottyan on 11/26/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "AppDelegate.h"
#import "MaintenanceViewController.h"
#import "HomeViewController.h"
#import "LiveTVViewController.h"
#import "OnDemandViewController.h"
#import "TVGuideViewController.h"

#import "ConnectViewController.h"
#import "DetailViewController.h"

// #import "OnDemandDetailViewController.h"
#import <AccedoConnect/AccedoConnect.h>
#import "JSONViewController.h"
#import "SBJson.h"
#import "RemotePlayerViewController.h"
#import <AccedoConnect/ACManager.h>
#import "ThePlatform.h"
#import "AppView.h"
#import "UIImage+Extension.h"
#import "MyTVViewController.h"
#import "ASIHTTPRequest.h"

#import "TVEStatusRecorderDataSource.h"

#import <QuartzCore/QuartzCore.h>

// TVE thePlatform
#import <Security/Security.h>
#import "KeychainItemWrapper.h" // from Apple Sample Code

#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>




@interface AppDelegate ()
@property (retain) NSMutableDictionary *config;
@property (retain) NSMutableArray *protectedControllerIndexes;
@property (retain) NSMutableDictionary *protectedControllers;

@property (retain) NSDate *forRetention;

// TVE thePlatform
@property (retain) NSMutableData *responseData;
- (void)saveAdminTokenToKeyChain:(NSString *)token;

@end

@implementation AppDelegate

@synthesize playOnAccedoConnect;

+ (NSString *)getMacAddress {
	int mgmtInfoBase[6];
	char *msgBuffer = NULL;
	size_t length;
	unsigned char macAddress[6];
	struct if_msghdr *interfaceMsgStruct;
	struct sockaddr_dl *socketStruct;
	NSString *errorFlag = NULL;

	// Setup the management Information Base (mib)
	mgmtInfoBase[0] = CTL_NET;
	// Request network subsystem
	mgmtInfoBase[1] = AF_ROUTE;
	// Routing table info
	mgmtInfoBase[2] = 0;
	mgmtInfoBase[3] = AF_LINK;
	// Request link layer information
	mgmtInfoBase[4] = NET_RT_IFLIST;
	// Request all configured interfaces

	// With all configured interfaces requested, get handle index
	if ((mgmtInfoBase[5] = if_nametoindex("en0")) == 0) {
		errorFlag = @"if_nametoindex failure";
	}
	else {
		// Get the size of the data available (store in len)
		if (sysctl(mgmtInfoBase, 6, NULL, &length, NULL, 0) < 0) {
			errorFlag = @"sysctl mgmtInfoBase failure";
		}
		else {
			// Alloc memory based on above call
			if ((msgBuffer = malloc(length)) == NULL) {
				errorFlag = @"buffer allocation failure";
			}
			else {
				// Get system information, store in buffer
				if (sysctl(mgmtInfoBase, 6, msgBuffer, &length, NULL, 0) < 0) {
					errorFlag = @"sysctl msgBuffer failure";
				}
			}
		}
	}

	// Befor going any further...
	if (errorFlag != NULL) {
		NSLog(@"Error: %@", errorFlag);
		free(msgBuffer);
		return errorFlag;
	}

	// Map msgbuffer to interface message structure
	interfaceMsgStruct = (struct if_msghdr *)msgBuffer;

	// Map to link-level socket structure
	socketStruct = (struct sockaddr_dl *)(interfaceMsgStruct + 1);

	// Copy link layer address data in socket structure to an array
	memcpy(&macAddress, socketStruct->sdl_data + socketStruct->sdl_nlen, 6);

	// Read from char array into a string object, into traditional Mac address format
	NSString *macAddressString = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
								  macAddress[0], macAddress[1], macAddress[2],
								  macAddress[3], macAddress[4], macAddress[5]];
	NSLog(@"Mac Address: %@", macAddressString);

	// Release the buffer memory
	free(msgBuffer);

	return macAddressString;
}

- (void)dealloc {
	[_window release];
	[super dealloc];
}

- (void)testData:(NSDictionary *)feed {
	ThePlatform *p = [ThePlatform sharedInstance];

	p.config = feed;
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
	return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
	tabBarController.selectedViewController = viewController;
	for (UINavigationController *c in tabBarController.viewControllers) {
		[c popToRootViewControllerAnimated:NO];
		UIViewController *vc = c.topViewController;
		if ([vc isKindOfClass:[BaseViewController class]]) {
			[((BaseViewController *)vc)closePlayer];
		}
	}

	UINavigationController *nc = (UINavigationController *)viewController;
	[((BaseViewController *)nc.topViewController)view];

	[((BaseViewController *)nc.topViewController)startVideoIfPossible];
}

// Call me on main thread!!!
- (void)maintenanceApplication:(NSString * )message {
	MaintenanceViewController *mvc = [[[MaintenanceViewController alloc] initWithNibName:@"MaintenanceViewController" bundle:nil] autorelease];

	mvc.message = message;
	self.window.rootViewController = mvc;
}

- (void)buildApplication:(NSDictionary * )metadata {
	NSArray *navigation = [metadata objectForKey:@"navigation"];
	NSMutableArray *viewsArray = [NSMutableArray array];

	for (NSDictionary *navitem in navigation) {
		NSString *text = [navitem objectForKey:@"text"];
		NSString *link = [navitem objectForKey:@"link"];
		NSString *type = [navitem objectForKey:@"type"];
		NSString *icon = [navitem objectForKey:@"icon"];
		BOOL enabled = [((NSNumber *)[navitem objectForKey:@"enabled"])boolValue];

		if (enabled) {
			UINavigationController *nav = nil;

			if (type) {
				if ([type isEqualToString:@"home"]) {
					nav = [[[UINavigationController alloc] initWithRootViewController:[[[HomeViewController alloc] initWithDescription:metadata] autorelease]] autorelease];
					self.homeStack = nav;
					nav.delegate = (BaseViewController *)nav.topViewController;
				}
				if ([type isEqualToString:@"tvGuide"]) {
					nav = [[[UINavigationController alloc] initWithRootViewController:[[[TVGuideViewController alloc] init] autorelease]] autorelease];
				}
				if ([type isEqualToString:@"onDemand"]) {
					NSDictionary *feed = Nil;
					if (metadata) {
						feed = [metadata objectForKey:@"com.accedo.plugin.ThePlatformOnDemand"];
						NSNumber *enabled = [feed objectForKey:@"enabled"];
						if (![enabled boolValue]) {
							feed = Nil;
						}
					}
					if (!feed) {
						feed = [metadata objectForKey:@"defaultOnDemand"];
					}
					if (feed) {
						[self testData:feed];
						nav = [[[UINavigationController alloc] initWithRootViewController:[[[OnDemandViewController alloc] initWithFeed:feed] autorelease]] autorelease];
						self.onDemandStack = nav;
						nav.delegate = (BaseViewController *)nav.topViewController;
						self.onDemandIndex = [viewsArray count];
					}
				}

				if ([type isEqualToString:@"connect"]) {
					// NSString *filePath = [[NSBundle mainBundle] pathForResource:@"MoviePlayer" ofType:@"bundle"];
					// NSBundle *bundle = [NSBundle bundleWithPath:filePath];
					RemotePlayerViewController *js = [[[RemotePlayerViewController alloc] init] autorelease];
					js.shouldShowCloseButton = NO;
					ConnectViewController *con = [[[ConnectViewController alloc] initWithPlayerController:js] autorelease];
					js.overlayView.delegate = con;
					nav = [[[UINavigationController alloc] initWithRootViewController:con] autorelease];
				}

				if ([type isEqualToString:@"myTv"]) {
					NSNumber *index = [NSNumber numberWithInt:[self.protectedControllerIndexes count] + [viewsArray count]];
					[self.protectedControllerIndexes addObject:index];
					nav = [[[UINavigationController alloc] initWithRootViewController:[[[MyTVViewController alloc] initWithFeed:Nil] autorelease]] autorelease];
					[self.protectedControllers setObject:nav forKey:index];
					nav.delegate = (BaseViewController *)nav.topViewController;
				}

				if ([type isEqualToString:@"player"]) {
					// NSString *filePath = [[NSBundle mainBundle] pathForResource:@"MoviePlayer" ofType:@"bundle"];
					// NSBundle *bundle = [NSBundle bundleWithPath:filePath];
					RemotePlayerViewController *js = [[[RemotePlayerViewController alloc] init] autorelease];
					nav = [[[UINavigationController alloc] initWithRootViewController:js] autorelease];
					// TODO: what is it?
				}
			}
			else if (link) {
				NSDictionary *linkData = [metadata valueForKeyPath:link];
				NSString *linkType = [linkData objectForKey:@"type"];

				if ([linkType isEqualToString:@"movieCollection"]) {
					nav = [[[UINavigationController alloc] initWithRootViewController:[[[OnDemandViewController alloc] initWithFeed:linkData] autorelease]] autorelease];
					nav.delegate = (BaseViewController *)nav.topViewController;
				}
				else if ([linkType isEqualToString:@"episodeCollection"]) {
					nav = [[[UINavigationController alloc] initWithRootViewController:[[[OnDemandViewController alloc] initWithFeed:linkData] autorelease]] autorelease];
					nav.delegate = (BaseViewController *)nav.topViewController;
				}
				else if ([linkType isEqualToString:@"showCollection"]) {
					nav = [[[UINavigationController alloc] initWithRootViewController:[[[OnDemandViewController alloc] initWithFeed:linkData] autorelease]] autorelease];
					nav.delegate = (BaseViewController *)nav.topViewController;
				}
				else if ([linkType isEqualToString:@"channelCollection"]) {
					nav = [[[UINavigationController alloc] initWithRootViewController:[[[LiveTVViewController alloc] initWithFeed:linkData] autorelease]] autorelease];
					nav.delegate = (BaseViewController *)nav.topViewController;
				}
			}


			if (nav) {
				if (icon) {
					if ([UIImage externalImageNamed:icon] == nil) {
						nav.tabBarItem.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", icon]];
					}
					else {
						nav.tabBarItem.image = [UIImage externalImageNamed:icon];
					}
				}
				nav.tabBarItem.title = text;
				if (![[self.protectedControllers allValues] containsObject:nav]) {
					[viewsArray addObject:nav];
				}
				// [nav release];
			}
		}
	}



	if ([viewsArray count] > 0) {
		UITabBarController *tabbarController = [[UITabBarController alloc] init];
		tabbarController.delegate = self;
		// then tell the tabbarcontroller to use our array of views
		[tabbarController setViewControllers:viewsArray];
		// then the last step is to add the our tabbarcontroller as subview of the window
		self.window.rootViewController = tabbarController;
		[tabbarController release];
	}
}

- (void)launch {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   [self checkAvailability];
				   });
}

- (void)checkAvailability {
	AppView *appview = [AppView sharedInstance];


	if ([appview isActive]) {
		[self appviewRemote];
	}
	else {
		NSString *message = [appview getMaintainanceMessage];
		if (0 < [message length]) {
			dispatch_sync(dispatch_get_main_queue(), ^{
							  [self maintenanceApplication:message];
						  });
		}
	}
}

- (void)appviewRemote1 {
	__block NSDictionary *cfg = Nil;
	__block NSDictionary *meta = Nil;





	dispatch_group_t group = dispatch_group_create();
	dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

	dispatch_group_async(group, queue, ^{
							 NSString *string = [NSString stringWithFormat:@"{\"eventType\":\"START\", \"userIdentifier\":\"%@\"}", [AppDelegate getMacAddress]];
							 NSMutableData *data = [NSMutableData dataWithData:[string dataUsingEncoding:NSUTF8StringEncoding]];


//							 NSString *url = [[APPVIEW_URL stringByAppendingString:API_KEY] stringByAppendingString:@"/event/log"];
//							 ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:url]];
//							 [request addRequestHeader:@"Accept" value:@"application/json"];
//							 [request addRequestHeader:@"Content-type" value:@"application/json"];
//							 [request setPostBody:data];
//							 [request startSynchronous];
//							 NSLog (@"Called .../event/log with response: %@", [request responseString]);
						 });

	dispatch_group_async(group, queue, ^{
							 meta = [[AppView sharedInstance] loadMetadata];
							 @synchronized (self) {
								 [self.config addEntriesFromDictionary:meta];
							 }
							 [[AppView sharedInstance] loadAssets];
						 });

	dispatch_group_async(group, queue, ^{
							 cfg = [[AppView sharedInstance] loadConfig];
							 @synchronized (self) {
								 [self.config addEntriesFromDictionary:cfg];
							 }
						 });

	dispatch_group_async(group, queue, ^{
							 [[AppView sharedInstance] loadChannelsAndEPG];

	                         // NSLog(@"ALL CHANNELS %d",[[[AppView sharedInstance] allChannels] count]);
						 });

	dispatch_group_wait(group, DISPATCH_TIME_FOREVER);
	dispatch_release(group);

	dispatch_async(dispatch_get_main_queue(), ^{
					   if (meta && cfg) {
						   NSLog (@"BUILDING APPLICATION");
						   [self buildApplication:self.config];
					   }
					   else {
						   [self maintenanceApplication:nil];
					   }
				   });
}

- (void)appviewRemote {
	NSDictionary *cfg = Nil;
	NSDictionary *meta = Nil;

	NSString *string = [NSString stringWithFormat:@"{\"eventType\":\"START\", \"userIdentifier\":\"%@\"}", [AppDelegate getMacAddress]];
	NSMutableData *data = [NSMutableData dataWithData:[string dataUsingEncoding:NSUTF8StringEncoding]];

//	NSString *url = [[APPVIEW_URL stringByAppendingString:API_KEY] stringByAppendingString:@"/event/log"];
//	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:url]];
//
//	[request addRequestHeader:@"Accept" value:@"application/json"];
//	[request addRequestHeader:@"Content-type" value:@"application/json"];
//	[request addRequestHeader:@"X-Session" value:[[AppView sharedInstance] getSession]];
//	[request setPostBody:data];
//	[request startSynchronous];
//
// //	NSString *eventLogString = [request responseString];
//	NSLog(@"Called .../event/log with response: %@", [request responseString]);


	meta = [[AppView sharedInstance] loadMetadata];
	[self.config addEntriesFromDictionary:meta];

//	cfg = [[AppView sharedInstance] loadConfig];
//  The config is hardcode locally
	cfg = [[AppView sharedInstance] createConfig];
	[self.config addEntriesFromDictionary:cfg];

	[[AppView sharedInstance] loadChannelsAndEPG];

	[[AppView sharedInstance] loadAssets];

	dispatch_async(dispatch_get_main_queue(), ^{
					   if (meta && cfg) {
						   NSLog (@"BUILDING APPLICATION");
						   [self buildApplication:self.config];
					   }
					   else {
						   [self maintenanceApplication:nil];
					   }
				   });
}

- (void)appLogin:(id)sender {
	if (self.protectedControllers) {
		NSMutableArray *vcs = [NSMutableArray
							   arrayWithArray:[((UITabBarController *)self.window.rootViewController)viewControllers]];

		for (NSNumber *nr in self.protectedControllerIndexes) {
			int index = [nr intValue];
			UIViewController *vc = [self.protectedControllers objectForKey:nr];
			[vcs insertObject:vc atIndex:index];
		}

		[((UITabBarController *)self.window.rootViewController) setViewControllers:vcs animated:YES];
		// [vcs addObjectsFromArray:self.protectedControllers];
	}
}

- (void)appLogout:(id)sender {
	if (self.protectedControllers) {
		NSMutableArray *vcs = [NSMutableArray
							   arrayWithArray:[((UITabBarController *)self.window.rootViewController)viewControllers]];

		[vcs removeObjectsInArray:[self.protectedControllers allValues]];


//        for (NSNumber * nr in self.protectedControllerIndexes) {
//            int index = [nr intValue];
//            UIViewController * vc = [self.protectedControllers objectForKey:nr];
//            [vcs insertObject:vc atIndex:index];
//        }

		[((UITabBarController *)self.window.rootViewController) setViewControllers:vcs animated:YES];
		// [vcs addObjectsFromArray:self.protectedControllers];
	}
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
#if DEVELOPMENT
	[TestFlight takeOff:@"1b0cc29fa192bc027bdb862dc77813fe_MTYwNzIwMjAxMi0xMS0yOSAwMzo0ODo1MC45NTE0Mzg"];
#endif

	[self registerDefaults];

	NSSetUncaughtExceptionHandler(&onUncaughtException);

	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appLogin:)
												 name:@"APP_LOGIN"
											   object:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appLogout:)
												 name:@"APP_LOGOUT"
											   object:nil];

	self.forRetention = [NSDate date];

	self.config = [NSMutableDictionary dictionary];
	self.protectedControllers = [NSMutableDictionary dictionary];
	self.protectedControllerIndexes = [NSMutableArray array];

	for (NSString *familyName in [UIFont familyNames]) {
		for (NSString *fontName in [UIFont fontNamesForFamilyName : familyName]) {
			// NSLog(@"%@ - %@", familyName, fontName);
		}
	}


	self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
	self.window.backgroundColor = [UIColor blackColor];

	UIViewController *s = [[[UIViewController alloc] init] autorelease];
	s.view = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Default-Landscape"]] autorelease];

	UIActivityIndicatorView *aiv = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	CGPoint c = s.view.center;
	c.y = c.y + 40;
	aiv.center = c;
	[s.view addSubview:aiv];
	[aiv startAnimating];

	self.window.rootViewController = s;

	[self.window makeKeyAndVisible];

	[self launch];

	[[ACManager sharedInstance] connectWithAPIKey:AC_KEY server:AC_URL];
	[[[TVEStatusRecorderDataSource alloc] initForStatusRecorder:[ACManager sharedInstance].statusRecorder] autorelease];

	// TVE thePlatform admin account
	NSString *urlStr = @"https://identity.auth.theplatform.com/idm/web/Authentication/signIn?schema=1.0&form=json&pretty=true&username=mpx/mark.siu@accedo.tv&password=Accedo1405";
//	NSTimeInterval *reqTime = 20;

//	self.responseData = [[NSMutableData alloc] init];
	NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlStr]];
	NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];



	return YES;
}

- (BaseViewController *)currentViewController {
	if ([self.window.rootViewController isKindOfClass:[UITabBarController class]]) {
		return (BaseViewController *)((UINavigationController *)((UITabBarController *)self.window.rootViewController).selectedViewController).topViewController;
	}
	return nil;
}

- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
	NSString *string = [NSString stringWithFormat:@"{\"eventType\":\"QUIT\", \"userIdentifier\":\"%@\", \"retentionTime\":%d}", [AppDelegate getMacAddress], (int)fabs([self.forRetention timeIntervalSinceNow])];
	NSMutableData *data = [NSMutableData dataWithData:[string dataUsingEncoding:NSUTF8StringEncoding]];

//	NSString *url = [[APPVIEW_URL stringByAppendingString:API_KEY] stringByAppendingString:@"/event/log"];
//
//	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:url]];
//
//	[request addRequestHeader:@"Accept" value:@"application/json"];
//	[request addRequestHeader:@"Content-type" value:@"application/json"];
//	[request setPostBody:data];
//	[request startSynchronous];
//	NSLog(@"Called .../event/log with response: %@", [request responseString]);
}

- (void)registerDefaults {
	NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];

	if (!settingsBundle) {
		NSLog(@"Could not find Settings.bundle");
		return;
	}

	NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
	NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];

	NSMutableDictionary *defaultsToRegister = [[[NSMutableDictionary alloc] initWithCapacity:[preferences count]] autorelease];
	for (NSDictionary *prefSpecification in preferences) {
		NSString *key = [prefSpecification objectForKey:@"Key"];
		if (key) {
			[defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
		}
	}

	[[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
}

void onUncaughtException(NSException *exception) {
	NSLog(@"Stack Trace: %@", [exception callStackSymbols]);
	NSLog(@"uncaught exception: %@", exception.description);
}

#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	NSLog(@"admin request make");
	self.responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	NSError *error = nil;

//    NSDictionary *dictionary = [[NSDictionary alloc] init];

	if (self.responseData == nil) {
		NSLog(@"responseData is nil");
		return;
	}

	NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableContainers error:&error];

	NSDictionary *loginDataArr = [jsonObject objectForKey:@"signInResponse"];
	NSString *adminToken = [loginDataArr objectForKey:@"token"];
	NSLog(@"admin token: %@", adminToken);

//	// convert to JSON
//	NSError *myError = nil;
//	NSDictionary *res = [NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableContainers error:&myError];
//
//	// show all values
//	for (id key in res) {
//		id value = [res objectForKey:key];
//
//		NSString *keyAsString = (NSString *)key;
//		NSString *valueAsString = (NSString *)value;
//
//		NSLog(@"key: %@", keyAsString);
//		NSLog(@"value: %@", valueAsString);
//	}
//
//	// extract specific value...
//	NSArray *results = [res objectForKey:@"results"];
//
//	for (NSDictionary *result in results) {
//		NSString *icon = [result objectForKey:@"icon"];
//		NSLog(@"icon: %@", icon);
//	}

	[self saveAdminTokenToKeyChain:adminToken];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	NSLog(@"Admin Token Failure");
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
	// Return nil to indicate not necessary to store a cached response for this connection
	return nil;
}

#pragma mark - keychain
- (void)saveAdminTokenToKeyChain:(NSString *)token {
	// handle the relogin issue
	KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"login" accessGroup:nil];

	[keychainItem resetKeychainItem];

	// set keychain value
	NSString *keySeparator = @"#*#";
	NSString *nullStr = @"NULL";
	NSString *loginKeychainValue = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", token, keySeparator, nullStr, keySeparator, nullStr, keySeparator, nullStr];
	[keychainItem setObject:loginKeychainValue forKey:(id)kSecValueData];

	// check keychain value
	NSString *checkKeychainValue = [keychainItem objectForKey:(id)kSecValueData];
	NSLog(@"AppDelegate.m keychain: %@", checkKeychainValue);



//	NSMutableDictionary *adminLogin = [[NSMutableDictionary alloc] init];
//
//	NSData *data = [[NSData alloc] init];
//
//
// //	[adminLogin setObject:token forKey:@"adminToken"];
// //	adminLogin[(id)kSecValueData] = data;
//
// //    [adminLogin setObject:(id)kSecClassKey forKey:(id)kSecClass];
// //	[adminLogin setObject:(id)token forKey:(id)kSecValueData];
//	[adminLogin setObject:@"" forKey:(id)kSecClassGenericPassword];
//
//	NSLog(@"count: %d", adminLogin.count);
//
//	OSStatus status = -101;
//	OSStatus duplicatedStatus = -25299;
//	status = SecItemAdd((CFDictionaryRef)adminLogin, NULL);
//
//	if (status == noErr) {
//		NSLog(@"Save success");
//	}
//
//	if (status == duplicatedStatus) {
//		status = SecItemUpdate((CFDictionaryRef)adminLogin, NULL);
//		NSLog(@"update value success");
//	}
//    SecItemDelete((CFDictionaryRef)adminLogin);
}

@end
