//
//  HomeViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 11/26/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "HomeViewController.h"
#import "HomePanelView.h"
#import "Constants.h"
#import "LiveTVView.h"
#import "LoginView.h"
#import "AppView.h"
#import "UIImage+Extension.h"
#import "ThePlatform.h"

@interface HomeViewController ()

@property (retain) NSDictionary *config;
@property (retain) UIScrollView *scrollView;
@property (retain) LoginView *loginView;
@property (retain) LiveTVView *liveTv;

@end

@implementation HomeViewController

- (id)initWithDescription:(NSDictionary *)desc {
	self = [super init];
	if (self) {
		self.config = desc;
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(appLogin:)
													 name:@"APP_LOGIN"
												   object:nil];


		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(appLogout:)
													 name:@"APP_LOGOUT"
												   object:nil];



		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(updateHomeChannel)
													 name:@"HOME_CHANNEL_CHANGED"
												   object:nil];
	}

	return self;
}

- (void)updateHomeChannel {
	if ([AppView sharedInstance].fingerprint) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
						   NSString *homeChannelId =  [[AppView sharedInstance] getHomeChannel];
						   NSLog (@"HOME CHANNEL: %@", homeChannelId);
						   NSDictionary *homeChannel = [[AppView sharedInstance] channelByChannelId:homeChannelId];

						   if (!homeChannelId || !homeChannel) {
							   NSLog (@"SETTING FIRST CHANNEL AS HOME CHANNEL");
							   homeChannel = [[[AppView sharedInstance] allChannels] objectAtIndex:0];
						   }

						   if (homeChannel) {
							   dispatch_async (dispatch_get_main_queue (), ^{
												   [self.liveTv configureView:homeChannel];
												   if (self.navigationController == ((UITabBarController *)CURRENT_WINDOW.rootViewController).selectedViewController && self.navigationController.topViewController == self) {
													   NSLog (@"START PLAY ON HOME LIVE");
													   if (!self.cmorePlayer) {
														   [self playEmbedded:homeChannel inView:self.liveTv.movieConteiner];
													   }
												   }
											   });
						   }
					   });
	}
}

- (void)viewWillAppear:(BOOL)animated {
	self.navigationController.navigationBarHidden = NO;
}

- (void)appLogin:(id)sender {
	// [self updateHomeChannel];

	[UIView animateWithDuration:0.3
					 animations:^{
		 self.loginView.hidden  = YES;
		 self.liveTv.hidden = NO;
		 self.scrollView.contentSize = CGSizeMake (screenWidth, [self getMaxHeight]);
	 }
					 completion:^(BOOL finished) {
	 }];
}

- (void)appLogout:(id)sender {
	[self closePlayer];

	[UIView animateWithDuration:0.3
					 animations:^{
		 self.loginView.hidden = NO;
		 self.liveTv.hidden = YES;
		 self.scrollView.contentSize = CGSizeMake (screenWidth, [self getMaxHeight]);
	 }
					 completion:^(BOOL finished) {
	 }];
}




- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	self.config = Nil;
	self.scrollView = Nil;
	self.loginView = Nil;
	self.liveTv = Nil;
	[super dealloc];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	NSString *homeChannelId =  [[AppView sharedInstance] getHomeChannel];
	if (homeChannelId) {
		NSDictionary *homeChannel = [[AppView sharedInstance] channelByChannelId:homeChannelId];
		if (homeChannel) {
			if (!self.cmorePlayer) {
				[self playEmbedded:homeChannel inView:self.liveTv.movieConteiner];
			}
		}
	}
}

- (NSUInteger)supportedInterfaceOrientations {
	return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotate {
	return YES;
}

- (void)openPromo:(id)sender {
	NSLog(@"OPEN PROMO");
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   NSDictionary *promo = [self.config objectForKey:@"promotion"];
					   NSDictionary *c = [[ThePlatform sharedInstance] getData:promo];
	                   // (NSDictionary *)[content JSONValue];
					   NSArray *entries = [c objectForKey:@"entries"];
					   if (entries && [entries count] > 0) {
						   dispatch_async (dispatch_get_main_queue (), ^{
											   [self openDetail:[entries objectAtIndex:0] withCollection:entries title:@""];
										   });
					   }
				   });
}

- (void)createTiles {
	float actualPromoTop = 0;
	CGPoint cursor = CGPointZero;
	NSDictionary *promo = [self.config objectForKey:@"promotion"];
	// NSLog(@"PROMO: %@", promo);
	// @stef: introduce promoTopZer0 var to avoid liveTv and loginView panels y-offset(ed) positioning.
	float promoTopZer0 = 0;


	// @stef: commenten out for promo...

	if (promo) {
		NSNumber *promoenabled = [promo objectForKey:@"enabled"];

		if ([promoenabled boolValue]) {
			NSString *promoKey = [promo objectForKey:@"background"];
			UIImageView *promoImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, promoHeight)];
			UIButton *promoButton = [UIButton buttonWithType:UIButtonTypeCustom];
			[promoButton addTarget:self action:@selector(openPromo:) forControlEvents:UIControlEventTouchUpInside];
			promoButton.frame = CGRectMake(0, 0, screenWidth, promoHeight);
			promoImage.contentMode = UIViewContentModeScaleAspectFill;
			promoImage.image = [UIImage externalImageNamed:promoKey];

			[self.scrollView addSubview:promoImage];
			[self.scrollView addSubview:promoButton];
			[promoImage release];
			actualPromoTop = promoTop;
			cursor = CGPointMake(0, promoTop);

			promoTopZer0 = promoTop;
		}
	}


	NSArray *panels = [self.config objectForKey:@"homePanel"];

	for (NSDictionary *p in panels) {
		NSString *title = [p objectForKey:@"text"];
		NSString *link = [p objectForKey:@"link"];
		// NSLog(@"%@", link);
		NSDictionary *data = [self.config valueForKeyPath:link];

		if (cursor.x + rectSize.width > screenWidth) {
			cursor.x = 0;
			cursor.y = cursor.y + rectSize.height;
		}

		CGRect frame = CGRectInset(CGRectMake(cursor.x, cursor.y, rectSize.width, rectSize.height), 0, 0);
		HomePanelView *v = [[HomePanelView alloc] initWithFrame:frame title:title data:data];
		v.detaildelegate = self;
		v.delegate =  self;

		[self.scrollView addSubview:v];

		cursor.x = cursor.x + rectSize.width;
	}


	self.liveTv = [[[LiveTVView alloc] initWithFrame:CGRectOffset(CGRectMake(screenWidth - liveTVWidth, actualPromoTop, liveTVWidth, 660), 2, 0) data:Nil] autorelease];

	[self.scrollView addSubview:self.liveTv];
	// [self.liveTv release];
	self.liveTv.hidden = YES;

	self.loginView = [[[LoginView alloc] initWithFrame:CGRectOffset(CGRectMake(screenWidth - liveTVWidth, actualPromoTop, liveTVWidth, 605), 2, 0) ] autorelease];
	self.loginView.backgroundColor = [UIColor clearColor];
	[self.scrollView addSubview:self.loginView];
	self.loginView.delegate = self;
	// [self.loginView release];
}

// -(void)



- (CGFloat)getMaxHeight {
	// NSLog(@"-MH");
	CGFloat maxHeight = 0.0;

	for (UIView *panelView in self.scrollView.subviews) {
		if (![panelView isKindOfClass:[HomePanelView class]]) {
			continue;
		}

		// NSLog(@"%f %f", panelView.frame.origin.y,panelView.frame.size.height);
		maxHeight = MAX(maxHeight, panelView.frame.origin.y + panelView.frame.size.height);
	}

	if (self.liveTv.hidden == NO) {
		maxHeight = MAX(maxHeight, self.liveTv.frame.size.height + self.liveTv.frame.origin.y);
	}

	if (self.loginView.hidden == NO) {
		maxHeight = MAX(maxHeight, self.loginView.frame.size.height + self.loginView.frame.origin.y);
	}

	// NSLog(@"-MH %f", maxHeight);
	return maxHeight;
}

- (NSArray *)getIntersectingPanels:(CGRect)rect withIgnore:(HomePanelView *)ignorePanel {
	NSMutableArray *intersectingPanels = [NSMutableArray array];

	for (UIView *panelView in self.scrollView.subviews) {
		if (![panelView isKindOfClass:[HomePanelView class]]) {
			continue;
		}
		if (panelView == ignorePanel) {
			continue;
		}

		if (CGRectIntersectsRect(rect, panelView.frame)) {
			[intersectingPanels addObject:panelView];
		}
	}

	return intersectingPanels;
}

- (void)panelWillOpen:(id)sender {
	HomePanelView *view = (HomePanelView *)sender;
	CGRect frame = view.frame;
	CGRect contentFrame = view.contentView.frame;

	// CGFloat maxHeight = [self getMaxHeight];

	CGRect intersectionRect = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, self.scrollView.contentSize.height);
	NSArray *intersectingPanels = [self getIntersectingPanels:intersectionRect withIgnore:view];

	double time = timeUnit * view.itemCount;

	[UIView animateWithDuration:time
					 animations:^{
		 view.arrowView.alpha = 1;
		 self.scrollView.contentOffset = CGPointMake (0, view.frame.origin.y);
		 view.frame = CGRectMake (frame.origin.x, frame.origin.y, frame.size.width, frame.size.height + view.contentHeight);
		 view.contentView.frame = CGRectMake (contentFrame.origin.x, contentFrame.origin.y, contentFrame.size.width, view.contentHeight);

		 CGFloat d = view.contentHeight;
		 for (HomePanelView * panel in intersectingPanels) {
			 panel.frame = CGRectMake (panel.frame.origin.x, panel.frame.origin.y + d, panel.frame.size.width, panel.frame.size.height);
		 }
	 }
					 completion:^(BOOL finished) {
		 [UIView animateWithDuration:0.3
						  animations:^{
				  self.scrollView.contentSize = CGSizeMake (screenWidth, [self getMaxHeight]);
			  }
						  completion:^(BOOL finished) {
				  self.scrollView.contentSize = CGSizeMake (screenWidth, [self getMaxHeight]);
			  }];
	 }];
}

- (void)panelWillClose:(id)sender {
	HomePanelView *view = (HomePanelView *)sender;
	CGRect frame = view.frame;
	CGRect contentFrame = view.contentView.frame;

	CGFloat maxHeight = [self getMaxHeight];
	CGRect intersectionRect = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, maxHeight);
	NSArray *intersectingPanels = [self getIntersectingPanels:intersectionRect withIgnore:view];

	// self.scrollView.contentSize = CGSizeMake(screenWidth, maxHeight - view.contentHeight);

	double time = timeUnit * view.itemCount;

	[UIView animateWithDuration:time
					 animations:^{
		 view.arrowView.alpha = 0;
		 view.frame = CGRectMake (frame.origin.x, frame.origin.y, frame.size.width, rectSize.height);
		 view.contentView.frame = CGRectMake (contentFrame.origin.x, contentFrame.origin.y, contentFrame.size.width, 0.0);

		 CGFloat d = view.contentHeight;
		 for (HomePanelView * panel in intersectingPanels) {
			 panel.frame = CGRectMake (panel.frame.origin.x, panel.frame.origin.y - d, panel.frame.size.width, panel.frame.size.height);
		 }
	 }
					 completion:^(BOOL finished) {
		 [UIView animateWithDuration:0.3
						  animations:^{
				  self.scrollView.contentSize = CGSizeMake (screenWidth, [self getMaxHeight]);
			  }
						  completion:^(BOOL finished) {
				  self.scrollView.contentSize = CGSizeMake (screenWidth, [self getMaxHeight]);
			  }];
	 }];
}




- (void)viewDidLoad {
	[self customNavigationBar];

	[super viewDidLoad];

	[[ACManager sharedInstance].statusRecorder reprocessLastPlaybackStatus];

	self.view.backgroundColor = [UIColor clearColor];
	UIImage *backgroundImage;
	if ([UIImage externalImageNamed:@"background"]) {
		backgroundImage = [UIImage externalImageNamed:@"background"];
	}
	else {
		backgroundImage = [UIImage imageNamed:@"background.png"];
	}
	UIImageView *backgroundImageView = [[[UIImageView alloc] initWithImage:backgroundImage] autorelease];
	// [backgroundImage release];
	[self.view addSubview:backgroundImageView];

	// CGRect bf = backgroundImageView.frame;

	self.scrollView = [[[UIScrollView alloc] initWithFrame:self.view.bounds] autorelease];
	self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	self.scrollView.backgroundColor = [UIColor clearColor];
	[self.view addSubview:self.scrollView];
	//    [backgroundImageView release];
	[self createTiles];
	self.scrollView.contentSize = CGSizeMake(screenWidth, [self getMaxHeight]);
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
