//
//  LiveView.h
//  TVE
//
//  Created by Mate Beres on 12/4/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface LiveView : UIView
@property (assign) BaseViewController *delegate;
@property (retain) UIView *movieConteiner;
- (id)initWithFrame:(CGRect) frame data:(NSDictionary * )configuration;
- (void)configureView:(NSDictionary *)item;
@end
