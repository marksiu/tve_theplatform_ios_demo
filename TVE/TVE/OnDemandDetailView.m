//
//  OnDemandDetailView.m
//  TVE
//
//  Created by Gabor Bottyan on 11/30/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "OnDemandDetailView.h"
#import "VSRemoteImageView.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"
#import "AssetView.h"
#import "UIHelper.h"
#import "PopupHelper.h"
#import "AppView.h"
#import "AppDelegate.h"
#import "BaseViewController.h"
#import "ThePlatform.h"

// TVE thePlatform
#import <Security/Security.h>
#import "KeychainItemWrapper.h"
#import "TVEConfig.h"
#import "RentalMovie.h"

const int kNumStars = 5;

const CGRect kTitleLabelFrame = { 35, 24, 300, 40 };
const CGSize kButtonSize = { 205, 40 };
const CGSize kStarSize = { 19, 18 };
const CGFloat kSpaceBetweenStars = 5.0f;
const CGSize kFriendLogoSize = { 22, 18 };


@interface OnDemandDetailView ()
@property (retain) VSRemoteImageView *logoImageView;
@property (retain) UILabel *titleLabel;
@property (retain) UIView *ratingView;
@property (retain) UIImageView *ageRatingImageView;
@property (retain) UILabel *subtitleLabel2;
@property (retain) UIImageView *rentPriceView;
@property (retain) UIImageView *rentalPeriodView;
@property (retain) UILabel *rentalPriceLabel1;
@property (retain) UILabel *rentalPriceLabel2;
@property (retain) UILabel *rentalPeriodLabel1;
@property (retain) UILabel *rentalPeriodLabel2;
// @property (retain) UILabel* descriptionLabel;

@property (retain) UIButton *buttonRentMovie;
@property (retain) UIButton *buttonWatchTrailer;
@property (retain) UIButton *buttonRateThisMove;
@property (retain) UIButton *buttonShare;
@property (retain) UIButton *buttonFriendsShared;
@property (retain) UILabel *friendsShareLabel;
@property (retain) UIImageView *backgroundImageView;

@property (retain) UIView *lineView;
@property (retain) UIView *buttonSeparator;

@property (retain) UIButton *descriptionButton;
@property (retain) UIButton *relatedButton;
@property (assign) BOOL relatedVisible;
@property (retain) NSArray *relatedData;
@property (retain) UIView *relatedView;
@property (retain) UIView *descriptionView;

@property (retain) NSDictionary *subscriptionData;
@property (retain) NSDictionary *config;
@property (assign) int ratingValue;

// TVE thePlatform
@property (retain) NSMutableData *responseData;
@property (retain) UIActivityIndicatorView *indicator;
@end

@implementation OnDemandDetailView




- (id)initWithFrame:(CGRect)frame config:(NSDictionary *)configuration {
	self = [super initWithFrame:frame];
	if (self) {
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(appLogin:)
													 name:@"APP_LOGIN"
												   object:nil];


		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(updateRental)
													 name:@"RENTED_MOVIES_CHANGED"
												   object:nil];


		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(updateRating)
													 name:@"MOVIE_RATINGS_CHANGED"
												   object:nil];


		self.config = configuration;
		self.backgroundImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 737, 415)] autorelease];
		self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
		self.backgroundImageView.alpha = 0.1;

		self.titleLabel = [[[UILabel alloc] initWithFrame:kTitleLabelFrame] autorelease];
		self.ratingView = [[[UIView alloc] initWithFrame:CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y + 40, kStarSize.width, kStarSize.height)] autorelease];



		// self.logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(370, 35, 302, 185)];

		self.logoImageView = [[[VSRemoteImageView alloc] initWithFrame:CGRectMake(366, 35, 302, 185)] autorelease];




		[self.logoImageView setContentMode:UIViewContentModeScaleAspectFill];
		self.logoImageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
		[self.logoImageView setClipsToBounds:YES];

		self.logoImageView.layer.shadowOffset = CGSizeMake(0, 0);
		self.logoImageView.layer.shadowColor = [[UIColor colorWithRed:0.0f / 255.0f
																green:0
																 blue:0.0f / 255.0f
																alpha:1] CGColor];
		self.logoImageView.layer.shadowRadius = 20.0;
		self.logoImageView.layer.shadowOpacity = 0.8;
		// self.layer.shouldRasterize = YES;
		self.logoImageView.layer.shadowPath = [[UIBezierPath bezierPathWithRect:self.logoImageView.layer.bounds] CGPath];

		self.ageRatingImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(self.ratingView.frame.origin.x, self.ratingView.frame.origin.y + 40, 46, 17)] autorelease];
		self.subtitleLabel2 = [[[UILabel alloc] initWithFrame:CGRectMake(self.ageRatingImageView.frame.origin.x + 52, self.ageRatingImageView.frame.origin.y + 2, 250, 14)] autorelease];

		self.rentPriceView = [[[UIImageView alloc] initWithFrame:CGRectMake(self.ageRatingImageView.frame.origin.x, self.subtitleLabel2.frame.origin.y + 40, 281, 35)] autorelease];
		self.rentalPeriodView = [[[UIImageView alloc] initWithFrame:CGRectMake(self.ageRatingImageView.frame.origin.x, self.rentPriceView.frame.origin.y + 38, 281, 35)] autorelease];
		self.rentPriceView.backgroundColor = [UIColor clearColor];
		self.rentalPriceLabel1 = [[[UILabel alloc] initWithFrame:CGRectMake(self.rentPriceView.frame.origin.x + 10, self.rentPriceView.frame.origin.y - 2, 150, 40)] autorelease];
		self.rentalPriceLabel2 = [[[UILabel alloc] initWithFrame:CGRectMake(self.rentPriceView.frame.origin.x + 130, self.rentPriceView.frame.origin.y - 2, 150, 40)] autorelease];
		self.rentalPeriodLabel1 = [[[UILabel alloc] initWithFrame:CGRectMake(self.rentalPeriodView.frame.origin.x + 10, self.rentalPeriodView.frame.origin.y - 2, 150, 40)] autorelease];
		self.rentalPeriodLabel2 = [[[UILabel alloc] initWithFrame:CGRectMake(self.rentalPeriodView.frame.origin.x + 130, self.rentalPeriodView.frame.origin.y - 2, 150, 40)] autorelease];
		self.rentalPeriodView.backgroundColor = [UIColor clearColor];

		self.buttonWatchTrailer = [UIButton buttonWithType:UIButtonTypeCustom];
		self.buttonRentMovie = [UIButton buttonWithType:UIButtonTypeCustom];
		self.buttonShare = [UIButton buttonWithType:UIButtonTypeCustom];
		self.buttonRateThisMove = [UIButton buttonWithType:UIButtonTypeCustom];
		self.buttonFriendsShared = [UIButton buttonWithType:UIButtonTypeCustom];

		self.friendsShareLabel = [[[UILabel alloc] initWithFrame:CGRectMake(450, 295, 150, 20)] autorelease];



		self.relatedButton = [UIButton buttonWithType:UIButtonTypeCustom];
		self.descriptionButton = [UIButton buttonWithType:UIButtonTypeCustom];

		self.buttonSeparator = [[[UIView alloc] initWithFrame:CGRectMake(190, 362, 1, 15)] autorelease];
		self.buttonSeparator.backgroundColor = [UIColor grayColor];

		self.lineView = [[[UIView alloc] initWithFrame:CGRectInset(CGRectMake(0, 410, detailRightSize, 1), 30, 0)] autorelease];
		self.lineView.backgroundColor = [UIColor grayColor];

		self.relatedView = [[[UIView alloc] initWithFrame:CGRectMake(40, 395, detailRightSize, 200)] autorelease];
		[self.relatedView setBackgroundColor:[UIColor clearColor]];
		self.relatedView.alpha = 0;

		self.descriptionView = [[[UIView alloc] initWithFrame:CGRectMake(35, 390, detailRightSize - 20, 200)] autorelease];
		[self.descriptionView setBackgroundColor:[UIColor clearColor]];

		// [self.logoImageView setImage: backgroundImage];

		// [self configureView:configuration];




		[self addSubview:self.relatedView];
		[self addSubview:self.backgroundImageView];
		[self addSubview:self.logoImageView];
		[self addSubview:self.titleLabel];
		[self addSubview:self.ratingView];
		[self addSubview:self.ageRatingImageView];
		[self addSubview:self.subtitleLabel2];
		[self addSubview:self.descriptionView];
		[self addSubview:self.rentPriceView];
		[self addSubview:self.rentalPeriodView];
		[self addSubview:self.rentalPriceLabel1];
		[self addSubview:self.rentalPriceLabel2];
		[self addSubview:self.rentalPeriodLabel1];
		[self addSubview:self.rentalPeriodLabel2];
		[self addSubview:self.buttonSeparator];

		[self addSubview:self.buttonRentMovie];
		[self addSubview:self.buttonWatchTrailer];
		[self addSubview:self.buttonRateThisMove];
		[self addSubview:self.buttonShare];
		[self addSubview:self.buttonFriendsShared];
		[self addSubview:self.descriptionButton];
		[self addSubview:self.relatedButton];
		[self addSubview:self.lineView];

		// TVE thePlatform loading indicator for the whole page display
		self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
		self.indicator.frame = CGRectMake(self.frame.size.width / 2 - 50, self.frame.size.height / 2 - 50, 25, 25);
		self.indicator.tag = 1;
		self.indicator.hidden = YES;
		[self.indicator setHidesWhenStopped:YES];
		[self addSubview:self.indicator];
	}
	return self;
}

- (void)configureView:(NSDictionary *)configuration {
	//  NSLog(@"MOVIE %@", configuration);

	[self.indicator startAnimating];

	[self setDetailPageElementHidden:YES];

//    self.logoImageView.backgroundColor = [UIColor yellowColor];
	self.config = configuration;
	[self.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:25]];
	[self.titleLabel setText:[configuration objectForKey:@"title"]];
	[self.titleLabel setTextColor:[UIColor whiteColor]];
	[self.titleLabel setBackgroundColor:[UIColor clearColor]];

	[self.logoImageView setImageUrl:[UIHelper getImageUrl:configuration forSize:CGSizeMake(self.logoImageView.frame.size.width, self.logoImageView.frame.size.width)]];
	int rating = [[configuration objectForKey:@"pl1$starRating"] intValue];
	UIImage *star_blue = [UIImage imageNamed:@"star_focus.png"];
	UIImage *star_gray = [UIImage imageNamed:@"star.png"];

	for (UIView *subview in [[[self.ratingView subviews] copy] autorelease]) {
		[subview removeFromSuperview];
	}

	for (int j = 0; j < rating; j++) {
		UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectMake(j * kStarSize.width + j * kSpaceBetweenStars, 0, kStarSize.width, kStarSize.height)];
		star.image = star_blue;
		[self.ratingView addSubview:star];
		[star release];
	}

	for (int j = rating; j < kNumStars; j++) {
		UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectMake(j * kStarSize.width + j * kSpaceBetweenStars, 0, kStarSize.width, kStarSize.height)];
		star.image = star_gray;
		[self.ratingView addSubview:star];
		[star release];
	}


	// NSString * pgrating = [configuration objectForKey:@"pl1$parentalControlRating"];
	// UIImage* ageRatingImage = [UIImage imageNamed:[NSString stringWithFormat:@"RATED_%@.png", pgrating]];
	UIImage *ageRatingImage = [UIImage imageNamed:@"PG-13.png"];

	self.ageRatingImageView.image = ageRatingImage;

	NSDictionary *genreList = [configuration objectForKey:@"media$categories"];
	NSMutableArray *genres = [NSMutableArray array];
	for (NSDictionary *key in genreList) {
		NSString *genre = [key objectForKey:@"media$name"];
		genre = [genre stringByReplacingOccurrencesOfString:@"Movies" withString:@""];
		genre = [genre stringByReplacingOccurrencesOfString:@"/" withString:@""];
		genre = [genre stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		if (0 < [genre length]) {
			[genres addObject:genre];
		}
	}
	NSMutableString *str = [NSMutableString string];
	for (int i = 0; i < [genres count]; ++i) {
		[str appendString:genres[i]];
		if (i != [genres count] - 1) {
			[str appendString:@", "];
		}
	}

	NSString *durationText = @"";

	NSDictionary *media = [UIHelper getVideo:configuration];

	if (media) {
		NSNumber *duration = [media objectForKey:@"plfile$duration"];
		durationText = [NSString stringWithFormat:@"%d mins", [duration intValue]];
	}

	NSTimeInterval pubDate = [[[configuration valueForKey:@"pubDate"] description] longLongValue] / 1000;
	NSDateFormatter *f = [[NSDateFormatter alloc] init];
	f.dateFormat = @"yyyy";
	NSString *year = [f stringFromDate:[NSDate dateWithTimeIntervalSince1970:pubDate]];
	[f release];

	[self.subtitleLabel2 setText:[durationText stringByAppendingFormat:@" | %@ | %@", year, str]];
	[self.subtitleLabel2 setFont:[UIFont fontWithName:@"Helvetica" size:15]];
	[self.subtitleLabel2 setTextColor:[UIColor whiteColor]];
	[self.subtitleLabel2 setBackgroundColor:[UIColor clearColor]];

	for (UIView *subview in [[[self.rentalPeriodView subviews] copy] autorelease]) {
		[subview removeFromSuperview];
	}

	for (UIView *subview in [[[self.rentPriceView subviews] copy] autorelease]) {
		[subview removeFromSuperview];
	}

	UIImage *viewBackground = [UIImage imageNamed:@"Expiry.png"];
	self.rentPriceView.image  = viewBackground;
	self.rentalPeriodView.image = viewBackground;
	//  [viewBackground release];

	//   self.rentalPriceLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(10, -3, 150, 40)];



	UIImage *buttonBackgroundImage = [UIImage imageNamed:@"btn.png"];
	UIImage *buttonBackgroundImageSelected = [UIImage imageNamed:@"button_selected.png"];
	self.buttonRentMovie.frame = CGRectMake(self.rentalPeriodView.frame.origin.x, self.rentalPeriodView.frame.origin.y + 60, kButtonSize.width, kButtonSize.height);


	[self.rentalPriceLabel1 setFont:[UIFont fontWithName:@"Helvetica" size:14]];
	[self.rentalPriceLabel1 setTextColor:[UIColor whiteColor]];
	[self.rentalPriceLabel1 setBackgroundColor:[UIColor clearColor]];

	//   self.rentalPriceLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(150, -3, 150, 40)];
	[self.rentalPriceLabel2 setFont:[UIFont fontWithName:@"Helvetica" size:14]];
	[self.rentalPriceLabel2 setTextColor:[UIColor whiteColor]];

	[self.rentalPriceLabel2 setBackgroundColor:[UIColor clearColor]];

	//    self.rentalPeriodLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(10, -3, 150, 40)];
	[self.rentalPeriodLabel1 setFont:[UIFont fontWithName:@"Helvetica" size:14]];
	[self.rentalPeriodLabel1 setTextColor:[UIColor whiteColor]];
	[self.rentalPeriodLabel1 setBackgroundColor:[UIColor clearColor]];

	//  self.rentalPeriodLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(150, -3, 150, 40)];
	[self.rentalPeriodLabel2 setFont:[UIFont fontWithName:@"Helvetica" size:14]];
	[self.rentalPeriodLabel2 setTextColor:[UIColor whiteColor]];

	[self.rentalPeriodLabel2 setBackgroundColor:[UIColor clearColor]];
	[self.buttonRentMovie.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];

	[self.buttonRentMovie setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.buttonRentMovie setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
	[self.buttonRentMovie setBackgroundImage:buttonBackgroundImageSelected forState:UIControlStateHighlighted];

	[self.rentalPriceLabel1 setFont:[UIFont fontWithName:@"Helvetica" size:14]];
	[self.rentalPriceLabel1 setTextColor:[UIColor whiteColor]];
	[self.rentalPriceLabel1 setBackgroundColor:[UIColor clearColor]];

	//   self.rentalPriceLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(150, -3, 150, 40)];
	[self.rentalPriceLabel2 setFont:[UIFont fontWithName:@"Helvetica" size:14]];
	[self.rentalPriceLabel2 setTextColor:[UIColor whiteColor]];

	[self.rentalPriceLabel2 setBackgroundColor:[UIColor clearColor]];

	[self.buttonRentMovie.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];

	[self.buttonRentMovie setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.buttonRentMovie setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
	[self.buttonRentMovie setBackgroundImage:buttonBackgroundImageSelected forState:UIControlStateHighlighted];
	// [self.buttonRentMovie setImage:[UIImage imageNamed:@"buy.png"] forState:UIControlStateNormal];
	// [self.buttonRentMovie setImage:[UIImage imageNamed:@"buy.png"] forState:UIControlStateHighlighted];
	// [self.buttonRentMovie setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];



	// NSDictionary * rentalData = [[AppView sharedInstance] getRentalData:self.config];



	self.buttonWatchTrailer.frame = CGRectMake(self.buttonRentMovie.frame.origin.x + kButtonSize.width + 10, self.buttonRentMovie.frame.origin.y, kButtonSize.width, kButtonSize.height);
	[self.buttonWatchTrailer addTarget:self action:@selector(watchMovieTouched:) forControlEvents:UIControlEventTouchUpInside];
	[self.buttonWatchTrailer.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
	[self.buttonWatchTrailer setTitle:@"Watch Trailer" forState:UIControlStateNormal];
	[self.buttonWatchTrailer setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.buttonWatchTrailer setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
	[self.buttonWatchTrailer setBackgroundImage:buttonBackgroundImageSelected forState:UIControlStateHighlighted];
	[self.buttonWatchTrailer setImage:[UIImage imageNamed:@"play_button.png"] forState:UIControlStateNormal];
	[self.buttonWatchTrailer setImage:[UIImage imageNamed:@"play_button.png"] forState:UIControlStateHighlighted];
	[self.buttonWatchTrailer setImageEdgeInsets:UIEdgeInsetsMake(0, -55, 0, 0)];

	[self.buttonRateThisMove addTarget:self action:@selector(rateButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
	self.buttonRateThisMove.frame = CGRectMake(self.buttonRentMovie.frame.origin.x + (kButtonSize.width + 10) * 2, self.buttonRentMovie.frame.origin.y, kButtonSize.width, kButtonSize.height);
	[self.buttonRateThisMove.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
	[self.buttonRateThisMove setTitle:@"Rate Movie" forState:UIControlStateNormal];
	[self.buttonRateThisMove setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.buttonRateThisMove setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
	[self.buttonRateThisMove setBackgroundImage:buttonBackgroundImageSelected forState:UIControlStateHighlighted];

	[self.buttonShare addTarget:self action:@selector(shareButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
	self.buttonShare.frame = CGRectMake(self.buttonRentMovie.frame.origin.x, self.buttonRentMovie.frame.origin.y + 47, kButtonSize.width, kButtonSize.height);
	[self.buttonShare.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
	[self.buttonShare setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.buttonShare setTitle:@"Share" forState:UIControlStateNormal];
	[self.buttonShare setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
	[self.buttonShare setBackgroundImage:buttonBackgroundImageSelected forState:UIControlStateHighlighted];
	[self.buttonShare setImage:[UIImage imageNamed:@"share.png"] forState:UIControlStateNormal];
	[self.buttonShare setImage:[UIImage imageNamed:@"share.png"] forState:UIControlStateHighlighted];
	[self.buttonShare setImageEdgeInsets:UIEdgeInsetsMake(0, -105, 0, 0)];

	self.buttonFriendsShared.frame = CGRectMake(self.buttonShare.frame.origin.x + kButtonSize.width + 10, self.buttonRentMovie.frame.origin.y + 47, kButtonSize.width, kButtonSize.height);
	[self.buttonFriendsShared addTarget:self action:@selector(friendsButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
	[self.buttonFriendsShared setBackgroundImage:buttonBackgroundImage forState:UIControlStateNormal];
	[self.buttonFriendsShared setBackgroundImage:buttonBackgroundImageSelected forState:UIControlStateHighlighted];
	[self.buttonFriendsShared setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.buttonFriendsShared setTitle:@"3 Friends Shared" forState:UIControlStateNormal];
	[self.buttonFriendsShared.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:15]];
	[self.buttonFriendsShared setImage:[UIImage imageNamed:@"friend_btn.png"] forState:UIControlStateNormal];
	[self.buttonFriendsShared setImage:[UIImage imageNamed:@"friend_btn.png"] forState:UIControlStateHighlighted];
	[self.buttonFriendsShared setImageEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
//    [self.buttonFriendsShared setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];

	self.descriptionButton.frame = CGRectMake(40, 350, 140, 37);
	[self.descriptionButton setTitle:@"Synopsis & Credits" forState:UIControlStateNormal];
	self.descriptionButton.titleLabel.font = [UIFont systemFontOfSize:15];
	[self.descriptionButton setTitleColor:[UIColor colorWithRed:35.0 / 255.0 green:117.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
	[self.descriptionButton addTarget:self action:@selector(showDescription:) forControlEvents:UIControlEventTouchUpInside];

	self.lineView.frame = CGRectInset(CGRectMake(0, 385, detailRightSize - 40, 1), 35, 0);
	self.lineView.alpha = 0.5f;

	self.relatedButton.frame = CGRectMake(190, 350, 140, 37);
	self.relatedButton.titleLabel.font = [UIFont systemFontOfSize:15];
	[self.relatedButton setTitle:@"Related Movies" forState:UIControlStateNormal];
	[self.relatedButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[self.relatedButton addTarget:self action:@selector(showRelated:) forControlEvents:UIControlEventTouchUpInside];
	// self.relatedButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;

	[self setDescription:configuration];
	// UIButton* episodesButton = [UIButton buttonWithType:UIButtonTypeCustom];

	[self updateScrollArea];
	// UIView* tablineView = [[UIView alloc] initWithFrame:CGRectMake(220, 480, 900, 40)];
	// [tablineView setBackgroundColor:[UIColor blackColor]];
	[self showDescription:Nil];
	[self updateRating];
	[self updateRental];
}

// set media price and rental price
- (void)setMediaRentalPriceAndDuration {
	NSDictionary *jsonObject = self.config;
	NSString *mediaId = [jsonObject objectForKey:@"id"];

	NSLog(@"media ID: %@", mediaId);

	NSString *urlStr = [NSString stringWithFormat:@"http://feed.product.theplatform.com/f/xBrLJC/accedo-product-feed?form=json&fields=plproduct:pricingPlan&pretty=true&byScopeIds=%@", mediaId];
//	NSString *urlStr = [NSString stringWithFormat:@"http://feed.product.theplatform.com/f/xBrLJC/accedo-product-feed?form=json&pretty=true&byScopeIds=http://data.media.theplatform.com/media/data/Media/13568067928"];
	NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlStr]];
	NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];

	NSLog(@"product feed URL: %@", urlStr);
}

- (BOOL)checkLogin {
	KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"login" accessGroup:nil];
	NSString *keyChainData = [keychainItem objectForKey:(id)kSecValueData];
	NSArray *keyChainArr = [keyChainData componentsSeparatedByString:@"#*#"];

	NSString *endUserToken = [keyChainArr objectAtIndex:1];
	NSString *endUserId = [keyChainArr objectAtIndex:2];
	NSString *endUserPaymentInst = [keyChainArr objectAtIndex:3];

	NSString *nullStr = @"NULL";

	if ([endUserToken isEqualToString:nullStr] || [endUserId isEqualToString:nullStr] || [endUserPaymentInst isEqualToString:nullStr]) {
		return NO;
	}

	return YES;
}

- (NSArray *)getKeyChainArr {
	KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"login" accessGroup:nil];
	NSString *keyChainData = [keychainItem objectForKey:(id)kSecValueData];
	NSArray *keyChainArr = [keyChainData componentsSeparatedByString:@"#*#"];

	return keyChainArr;
}

- (NSString *)getAdminToken {
	NSArray *keyChainArr = [self getKeyChainArr];
	NSString *adminToken = [keyChainArr objectAtIndex:0];

	NSLog(@"Admin Token: %@", adminToken);
	return adminToken;
}

- (NSString *)getEndUserID {
	NSArray *keyChainArr = [self getKeyChainArr];
	NSString *endUserId = [keyChainArr objectAtIndex:2];

	NSLog(@"End User ID: %@", endUserId);
	return endUserId;
}

- (long long)checkRentalRecord {
	NSDictionary *jsonObject = self.config;
	NSString *mediaId = [jsonObject objectForKey:@"id"];

	double now = [[NSDate date] timeIntervalSince1970] * 1000;
	NSString *nowStr = [NSString stringWithFormat:@"%.0f", now];
	NSString *adminToken;
	NSString *endUserId;

	if ([self checkLogin]) {
		NSLog(@"checkLogin: pass");
		adminToken = [self getAdminToken];
		endUserId = [self getEndUserID];
	}
	else {
		return 0;
	}

	NSURLRequest *url = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://data.entitlement.theplatform.com/eds/data/Entitlement?schema=1.1&form=json&pretty=true&token=%@&byUserId=%@&byScopeId=%@&byInWindow=%@", adminToken, endUserId, mediaId, nowStr]]];
	NSURLResponse *response = nil;
	NSError *error = nil;
	NSData *data = [NSURLConnection sendSynchronousRequest:url
										 returningResponse:&response
													 error:&error ];

	if (error == nil) {
		NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

		int mediaCount = [[jsonObject objectForKey:@"entryCount"] intValue];

		if (mediaCount == 1) {
			NSArray *entryArr = [jsonObject objectForKey:@"entries"];
			NSDictionary *entry = [entryArr objectAtIndex:0];
			NSNumber *expireDateSec = [entry objectForKey:@"ent$expirationDate"];
			long long value = [expireDateSec longLongValue];
			return value;
		}
	}
	return 0;
}

- (void)setDetailPageElementHidden:(BOOL)visiable {
	self.logoImageView.hidden = visiable;
	self.titleLabel.hidden = visiable;
	self.ratingView.hidden = visiable;
	self.ageRatingImageView.hidden = visiable;
	self.subtitleLabel2.hidden = visiable;
	self.rentPriceView.hidden = visiable;
	self.rentalPeriodView.hidden = visiable;
	self.rentalPriceLabel1.hidden = visiable;
	self.rentalPriceLabel2.hidden = visiable;
	self.rentalPeriodLabel1.hidden = visiable;
	self.rentalPeriodLabel2.hidden = visiable;
	self.buttonRentMovie.hidden = visiable;
	self.buttonWatchTrailer.hidden = visiable;
	self.buttonRateThisMove.hidden = visiable;
	self.buttonShare.hidden = visiable;
	self.buttonFriendsShared.hidden = visiable;
	self.friendsShareLabel.hidden = visiable;
	self.backgroundImageView.hidden = visiable;
	self.lineView.hidden = visiable;
	self.buttonSeparator.hidden = visiable;
	self.descriptionButton.hidden = visiable;
	self.relatedButton.hidden = visiable;
	self.relatedView.hidden = visiable;
	self.descriptionView.hidden = visiable;
}

- (void)updateRental {
	// NSLog(@"UPDATING RENTAL DATA");

	[self.buttonRentMovie setEnabled:NO];

	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   NSDictionary *rentalData = [[AppView sharedInstance] getRentalData:self.config];

//	                   // retrieve entitlement record 1 by 1 (API)
//					   BOOL hasRentalRecord = NO;
//					   long long rentalExpireSec = [self checkRentalRecord];
//					   if (rentalExpireSec != 0) {
//						   hasRentalRecord = YES;
//					   }

	                   // retrieve rental movie record from TVEConfig
					   BOOL hasRentalRecord = NO;
					   TVEConfig *config = [TVEConfig sharedConfig];
					   NSMutableDictionary *rentalMovieDict = config.getRentalMovieRecord;

	                   // get media ID from self.config
					   NSDictionary *jsonObject = self.config;
					   NSString *mediaId = [jsonObject objectForKey:@"id"];

	                   //
					   RentalMovie *detailPageMovie = nil;

					   if ([rentalMovieDict objectForKey:mediaId] != nil) {
						   detailPageMovie = [rentalMovieDict objectForKey:mediaId];
						   hasRentalRecord = YES;
					   }


					   dispatch_async (dispatch_get_main_queue (), ^{
//										   [self setDetailPageElementHidden:YES];
//										   if (!rentalData) {

	                                       // no rental by user
										   if (!hasRentalRecord) {
											   [self.rentalPriceLabel1 setText:@"Rental Price"];

//											   NSString *price =  [self.config objectForKey:@"pl1$price"];
//											   NSString *period =  [self.config objectForKey:@"pl1$rentalPeriod"];

											   [self setMediaRentalPriceAndDuration];

//											   [self.rentalPriceLabel2 setText:price ? [NSString stringWithFormat:@"$%@", price ]:@""];
											   [self.rentalPeriodLabel1 setText:@"Rental Period"];
//											   [self.rentalPeriodLabel2 setText:period ? [NSString stringWithFormat:@"%@ days", period]:@"" ];
											   [self.buttonRentMovie removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
											   [self.buttonRentMovie setTitle:@"Rent Movie" forState:UIControlStateNormal];
											   [self.buttonRentMovie setImage:[UIImage imageNamed:@"buy.png"] forState:UIControlStateNormal];
											   [self.buttonRentMovie setImage:[UIImage imageNamed:@"buy.png"] forState:UIControlStateHighlighted];
											   [self.buttonRentMovie setImageEdgeInsets:UIEdgeInsetsMake (0, -70, 0, 0)];
											   [self.buttonRentMovie addTarget:self action:@selector(rentMovieTouched:) forControlEvents:UIControlEventTouchUpInside];
//											   self.rentalPeriodView.hidden = NO;
//											   self.rentalPeriodLabel1.hidden = NO;
//											   self.rentalPeriodLabel2.hidden = NO;
										   }
										   else {
	                                           // already rental by user

	                                           // milliseconds to seconds
//											   NSNumber *expiry = [NSNumber numberWithDouble:[[rentalData objectForKey:@"rentalExpiry"] doubleValue] / 1000];
//											   NSNumber *expiry = [NSNumber numberWithLongLong:rentalExpireSec / 1000];
											   NSNumber *expiry = [NSNumber numberWithLongLong:detailPageMovie.expiryDateInSec / 1000];
											   NSDate *expiryDate = [NSDate dateWithTimeIntervalSince1970:[expiry doubleValue]];
											   NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

	                                           // @stef #50 Expiry Time Format: 1Jan, 12:02pm (No year and all PM should be pm)
	                                           // [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
	                                           // [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
											   [dateFormatter setDateFormat:@"dd MMM, HH:mm"];
//											   [dateFormatter setAMSymbol:@"am"];
//											   [dateFormatter setPMSymbol:@"pm"];

											   NSString *formattedDateString = [dateFormatter stringFromDate:expiryDate];
											   [dateFormatter release];
											   [self.rentalPriceLabel1 setText:@"Expiry Time"];
											   [self.rentalPriceLabel2 setText:[NSString stringWithFormat:@"%@", formattedDateString]];
											   self.rentalPeriodView.hidden = YES;
											   self.rentalPeriodLabel1.hidden = YES;
											   self.rentalPeriodLabel2.hidden = YES;
											   [self.buttonRentMovie setTitle:@"Watch Movie" forState:UIControlStateNormal];
											   [self.buttonRentMovie setImageEdgeInsets:UIEdgeInsetsMake (0, -58, 0, 0)];
											   [self.buttonRentMovie removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
											   [self.buttonRentMovie addTarget:self action:@selector(watchMovieTouched:) forControlEvents:UIControlEventTouchUpInside];

											   [self.indicator stopAnimating];
											   [self setDetailPageElementHidden:NO];
											   self.rentalPeriodView.hidden = YES;
											   self.rentalPeriodLabel1.hidden = YES;
											   self.rentalPeriodLabel2.hidden = YES;
										   }
										   [self.buttonRentMovie setEnabled:YES];
									   });
				   });
}

- (void)updateRating {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   int rating = [[AppView sharedInstance] getRating:self.config];
					   dispatch_async (dispatch_get_main_queue (), ^{
										   if (rating > 0) {
											   self.ratingValue = rating;
											   [self.buttonRateThisMove setTitle:[NSString stringWithFormat:@"Your Rating: %d ", rating] forState:UIControlStateNormal];
											   [self.buttonRateThisMove setTitleColor:[UIColor colorWithRed:35.0 / 255.0 green:117.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
											   [self.buttonRateThisMove setImage:[UIImage imageNamed:@"rating_star_focus.png"] forState:UIControlStateNormal];
											   [self.buttonRateThisMove setImageEdgeInsets:UIEdgeInsetsMake (0, 140, 0, 0)];
											   [self.buttonRateThisMove setTitleEdgeInsets:UIEdgeInsetsMake (0, -50, 0, 0)];
	                                           // NSLog(@"Rating Value :%d", rating);
										   }
										   else {
											   self.ratingValue = 0;
											   [self.buttonRateThisMove setImage:Nil forState:UIControlStateNormal];
											   [self.buttonRateThisMove setTitle:@"Rate Movie" forState:UIControlStateNormal];
											   [self.buttonRateThisMove setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
											   [self.buttonRateThisMove setTitleEdgeInsets:UIEdgeInsetsMake (0, 0, 0, 0)];
										   }
									   });
				   });
}

- (int)getCurrentRating {
	return self.ratingValue;
}

- (void)setCurrentRating:(int)rating {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   self.ratingValue = rating;
					   [[AppView sharedInstance] setRating:self.config rating:rating];
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self updateRating];
//
//        });
				   });
}

- (void)asetTapped:(id)sender {
	AssetView *asset = (AssetView *)sender;
	int selected = asset.tag;
	// NSLog(@"Selected %d", selected);
	NSDictionary *content = [self.relatedData objectAtIndex:selected];

	[self.detaildelegate itemSelected:content];
	// [self configureView:content];
}

- (NSString *)getCastStringByRole:(NSDictionary *)cast role:(NSString *)role {
	NSMutableString *str = [NSMutableString string];
	NSMutableArray *filteredCast = [NSMutableArray array];

	for (NSDictionary *item in cast) {
		NSString *name = [item objectForKey:@"media$value"];
		NSString *role2 = [item objectForKey:@"media$role"];

		if (name && [role2 compare:role] == NSOrderedSame) {
			[filteredCast addObject:item];
		}
	}

	for (int i = 0; i < [filteredCast count]; ++i) {
		NSDictionary *castItem = [filteredCast objectAtIndex:i];
		NSString *name = [castItem objectForKey:@"media$value"];
		// NSString* role2 = [castItem objectForKey:@"media$role"];

		[str appendString:name];

		if (i != [filteredCast count] - 1) {
			[str appendString:@", "];
		}
	}


	return str;
}

- (void)setDescription:(NSDictionary * )content {
	for (UIView *v in [[[self.descriptionView subviews] copy] autorelease]) {
		[v removeFromSuperview];
	}


	NSMutableString *descriptionText = [NSMutableString string];
	NSString *text = [content objectForKey:@"description"];

	if (text) {
		[descriptionText appendString:text];
	}

	NSString *directors = [self getCastStringByRole:[content objectForKey:@"media$credits"] role:@"director"];
	if ([directors length] > 0) {
		[descriptionText appendString:@"\n\nDirector\n"];
		[descriptionText appendString:directors];
	}

	NSString *actors = [self getCastStringByRole:[content objectForKey:@"media$credits"] role:@""];
	if ([actors length] > 0) {
		[descriptionText appendString:@"\n\nCast\n"];
		[descriptionText appendString:actors];
		[descriptionText appendString:@"\n"];
	}

	UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, detailRightSize, 200)];
	label.font = [UIFont fontWithName:@"Helvetica" size:15];
	label.numberOfLines = 0;
	[label setText:descriptionText];

	CGSize textSize = [label.text sizeWithFont:label.font
							 constrainedToSize:CGSizeMake(detailRightSize - 100, 1000)
								 lineBreakMode:NSLineBreakByWordWrapping];

	label.frame = CGRectMake(10, 10, textSize.width, textSize.height);

	self.descriptionView.frame = CGRectMake(self.descriptionView.frame.origin.x, self.descriptionView.frame.origin.y, textSize.width, textSize.height);


	[label setTextColor:[UIColor whiteColor]];
	[label setBackgroundColor:[UIColor clearColor]];
	[[self descriptionView] addSubview:label];

	[label release];
}

- (void)clearRelated {
	for (UIView *v in [[[self.relatedView subviews] copy] autorelease]) {
		[v removeFromSuperview];
	}
}

- (void)setRelatedContent:(NSArray * )c {
	self.relatedData = c;

	for (UIView *v in [[[self.relatedView subviews] copy] autorelease]) {
		[v removeFromSuperview];
	}

	float maxHeight = 0;

	for (int i = 0; i < [self.relatedData count]; i++) {
		NSDictionary *entry = [self.relatedData objectAtIndex:i];
		AssetView *assetView = Nil;


		assetView = [[[AssetView alloc] initWithStyle:ASSET_TYPE_RELATED_GRID data:entry] autorelease];
		assetView.frame = CGRectInset(CGRectMake((i % 3) * assetView.frame.size.width, (i / 3) * assetView.frame.size.height, assetView.frame.size.width, assetView.frame.size.height), 0, 0);

		assetView.tag = i;
		assetView.delegate = self;

		[ self.relatedView addSubview:assetView];


		maxHeight = assetView.frame.origin.y + assetView.frame.size.height;
	}
	// self.relatedView.backgroundColor = [UIColor yellowColor];
	self.relatedView.frame = CGRectMake(self.relatedView.frame.origin.x, self.relatedView.frame.origin.y, self.relatedView.frame.size.width, maxHeight);

	// @stef #51 Remove number behind "Related Movies"
	// [self.relatedButton setTitle:[NSString stringWithFormat:@"Related Movies (%d)", [self.relatedData count]] forState:UIControlStateNormal];
}

- (void)rentMovieTouched:(id)sender {
	if ([AppView sharedInstance].fingerprint) {
		[PopupHelper showRentalPinPopupForItem:self.config delegate:self.detaildelegate];
	}
	else {
		self.subscriptionData = self.config;
		[((BaseViewController *)self.detaildelegate) openLogin:Nil];
	}
}

- (void)watchMovieTouched:(id)sender {
	[(BaseViewController *)self.detaildelegate playEmbedded:self.config inView:nil];
}

- (void)appLogin:(id)sender {
	// NSLog(@"ONDEMAND VIEW AFTER APP LOGIN");
	// NSLog(@"ONDEMAND VIEW AUTO RENTAL");
	if (self.subscriptionData) {
		NSDictionary *rentalData = [[AppView sharedInstance] getRentalData:self.config];

		BOOL hasRentalRecord = NO;
		long long rentalExpireSec = [self checkRentalRecord];
		if (rentalExpireSec != 0) {
			hasRentalRecord = YES;
		}

		if (!hasRentalRecord) {
//			[PopupHelper showRentalPinPopupForItem:self.config delegate:self.detaildelegate];
			[PopupHelper showRentalPinPopupForItem:self.subscriptionData delegate:self.detaildelegate];
		}
		self.subscriptionData = Nil;
	}
}

- (void)updateScrollArea {
	[UIView animateWithDuration:0.3
					 animations:^{
		 if (self.relatedVisible) {
			 float h = self.relatedView.frame.origin.y + self.relatedView.frame.size.height;
	         // NSLog(@"Height: %f", h);
			 self.contentSize = CGSizeMake (detailRightSize, self.relatedView.frame.origin.y + self.relatedView.frame.size.height);
		 }
		 else {
			 self.contentSize = CGSizeMake (detailRightSize, self.descriptionView.frame.origin.y + self.descriptionView.frame.size.height);
		 }
	 }
					 completion:^(BOOL finished) {
	 }];
}


- (void)showDescription:(id)sender {
	if (self.relatedVisible) {
		[UIView animateWithDuration:0.3
						 animations:^{
			 self.relatedView.alpha = 0;
			 self.descriptionView.alpha = 1;
			 [self.descriptionButton setTitleColor:[UIColor colorWithRed:35.0 / 255.0 green:117.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
			 [self.relatedButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		 }
						 completion:^(BOOL finished) {
			 self.relatedVisible = NO;
			 [self updateScrollArea];
		 }];
	}
}

- (void)showRelated:(id)sender {
	if (!self.relatedVisible) {
		[UIView animateWithDuration:0.3
						 animations:^{
			 self.relatedView.alpha = 1;
			 self.descriptionView.alpha = 0;
			 [self.relatedButton setTitleColor:[UIColor colorWithRed:35.0 / 255.0 green:117.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
			 [self.descriptionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		 }
						 completion:^(BOOL finished) {
			 self.relatedVisible = YES;
			 [self updateScrollArea];
		 }];
	}
}

- (void)shareButtonTouched:(id)sender {
	[PopupHelper showSharePopupInView:self atButton:self.buttonShare orientedToTop:NO asset:[self.config objectForKey:@"title"]];
}

- (void)rateButtonTouched:(id)sender {
	if ([AppView sharedInstance].fingerprint) {
		[PopupHelper showRatePopupInView:self atButton:self.buttonRateThisMove delegate:self];
	}
	else {
		[((BaseViewController *)self.detaildelegate) openLogin:Nil];
	}
}

- (void)friendsButtonTouched:(id)sender {
	[PopupHelper showFriendsPopupInView:self atButton:self.buttonFriendsShared orientedToTop:NO];
}

#pragma mark - NSURLConnection
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	self.responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[self.responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	NSError *error = nil;

	if (self.responseData == nil) {
		NSLog(@"responseData is nil");
		return;
	}

	// retrieve data from API response
	NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableContainers error:&error];
	NSArray *entryArr = [jsonObject objectForKey:@"entries"];
	if ([entryArr count] != 0) {
		NSDictionary *entry = [entryArr objectAtIndex:0];
		NSDictionary *productPricingPlan = [entry objectForKey:@"plproduct$pricingPlan"];
		NSArray *productPricingTiersArr = [productPricingPlan objectForKey:@"plproduct$pricingTiers"];
		NSDictionary *productPricingTiers = [productPricingTiersArr objectAtIndex:0];

		// rental price
		NSDictionary *productAmont = [productPricingTiers objectForKey:@"plproduct$amounts"];
		NSString *rentalPrice = [productAmont objectForKey:@"USD"];

		// rental duration
		NSArray *productPricingTagsArr = [productPricingTiers objectForKey:@"plproduct$productTags"];
		NSDictionary *productPricingTags = [productPricingTagsArr objectAtIndex:0];
		NSString *rentalDuration = [productPricingTags objectForKey:@"plproduct$title"];

		// set rental price and duration
		[self.rentalPriceLabel2 setText:rentalPrice ? [NSString stringWithFormat:@"$%@", rentalPrice ]:@""];
		[self.rentalPeriodLabel2 setText:rentalDuration ? [NSString stringWithFormat:@"%@", rentalDuration]:@"" ];
	}

	[self.indicator stopAnimating];
	[self setDetailPageElementHidden:NO];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	NSLog(@"product feed call media & rental duration Failure");
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
	// Return nil to indicate not necessary to store a cached response for this connection
	return nil;
}

#pragma mark = dealloc
- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	self.logoImageView = Nil;
	self.titleLabel = Nil;
	self.ratingView = Nil;
	self.ageRatingImageView = Nil;
	self.subtitleLabel2 = Nil;
	self.rentPriceView = Nil;
	self.rentalPeriodView = Nil;
	self.rentalPeriodLabel1 = Nil;
	self.rentalPeriodLabel2 = Nil;
	self.rentalPriceLabel1 = Nil;
	self.rentalPeriodLabel2 = Nil;
	self.buttonRentMovie = Nil;
	self.buttonWatchTrailer = Nil;
	self.buttonRateThisMove = Nil;
	self.buttonShare = Nil;
	self.buttonFriendsShared = Nil;
	self.friendsShareLabel = Nil;
	self.backgroundImageView = Nil;
	self.relatedView = Nil;
	self.lineView = Nil;
	self.buttonSeparator = Nil;
	self.descriptionView = Nil;
	self.descriptionButton = Nil;
	self.relatedButton = Nil;

	[super dealloc];
}

/*
 * // Only override drawRect: if you perform custom drawing.
 * // An empty implementation adversely affects performance during animation.
 * - (void)drawRect:(CGRect)rect
 * {
 * // Drawing code
 * }
 */

@end
