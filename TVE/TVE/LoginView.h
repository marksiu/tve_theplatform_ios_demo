//
//  LoginView.h
//  TVE
//
//  Created by Gabor Bottyan on 11/29/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginViewDelegate <NSObject>

- (void)openLogin:(id)sender;

@end


@interface LoginView : UIView

@property (assign) id <LoginViewDelegate> delegate;

@end
