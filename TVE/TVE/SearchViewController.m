//
//  SearchViewController.m
//  TVE
//
//  Created by Mate Beres on 12/19/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController () <UISearchDisplayDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>
@property (retain) NSMutableArray *data;
@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		_data = [[NSMutableArray alloc] init];
	}
	return self;
}

- (void)viewDidAppear:(BOOL)animated {
	//   [self customNavigationBar];
}

- (void)viewDidLoad {
	[super viewDidLoad];

	// Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (CGSize)contentSizeForViewInPopover {
	return CGSizeMake(335, 277);
}

- (void)mockSearch:(NSTimer *)timer {
	[_data removeAllObjects];
	int count = 1 + random() % 20;
	for (int i = 0; i < count; i++) {
		[_data addObject:timer.userInfo];
	}
	[self.searchDisplayController.searchResultsTableView reloadData];
}

#pragma mark - UISearchDisplayDelegate

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
	[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(mockSearch:) userInfo:searchString repeats:NO];
	return NO;
}

#pragma mark - UISearchBarDelegate

/*- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
 *  //NSLog(@"DO SEARCH");
 *
 * }*/

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	if (self.delegate) {
		[self.delegate doSearch:searchBar.text];
	}
	[self.popover dismissPopoverAnimated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[self.popover dismissPopoverAnimated:YES];
}

/*- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar {
 *
 * }*/

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [_data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

	if (!cell) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"] autorelease];
		cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
	}
	cell.textLabel.text = [NSString stringWithFormat:@"%@", [_data objectAtIndex:indexPath.row]];
	return cell;
}

#pragma mark - UITableViewDelegate

- (void)dealloc {
	_data = Nil;
	[super dealloc];
}

@end
