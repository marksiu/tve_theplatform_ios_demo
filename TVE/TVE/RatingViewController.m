//
//  RatingViewController.m
//  TVE
//
//  Created by Mate Beres on 12/11/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "RatingViewController.h"
#import "Constants.h"

const int MAX_STARS = 5;
const CGSize CONTENT_SIZE = { 160, 45 };

@interface RatingViewController ()
@property (assign) int ratingValue;
@property (retain) UIImage *starBlueImage;
@property (retain) UIImage *starGrayImage;

- (IBAction)touchDown:(id)sender;
@end

@implementation RatingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		self.ratingStarArray = [[NSMutableArray alloc] init];
		self.starBlueImage = [UIImage imageNamed:@"star_blue.png"];
		self.starGrayImage = [UIImage imageNamed:@"star_grey.png"];
		self.ratingValue = 0;

		// positioning stars in the middle
		CGFloat dx = (CONTENT_SIZE.width / 2 - ratingStarSize.width * 1.8 / 2) / (MAX_STARS + 1) - 5;
		CGFloat dy = CONTENT_SIZE.height / 2 - ratingStarSize.height * 1.8 / 2;
		for (int j = 0; j < MAX_STARS; j++) {
			UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectMake(j * (5 + ratingStarSize.width * 1.8) + dx, dy, ratingStarSize.width * 1.8, ratingStarSize.height * 1.8)];
			star.image = self.starGrayImage;
			[self.view addSubview:star];
			[self.ratingStarArray addObject:star];
			[star release];
		}
	}
	return self;
}

- (CGSize)contentSizeForViewInPopover {
	return CONTENT_SIZE;
}

- (void)viewDidLoad {
	[super viewDidLoad];


	// Do any additional setup after loading the view from its nib.
}

- (void)viewWillAppear:(BOOL)animated {
	if (self.delegate) {
		int current = [self.delegate getCurrentRating];
		[self setRating:current];
	}
	else {
		[self setRating:0];
	}
}

- (void)viewWillDisappear:(BOOL)animated {
	if (self.delegate) {
		[self.delegate setCurrentRating:self.ratingValue];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)setRating:(int)index {
	if (index < -1 || index > [self.ratingStarArray count]) {
		return;
	}

	for (int i = 0; i < MAX_STARS; ++i) {
		UIImageView *imageView = [self.ratingStarArray objectAtIndex:i];
		imageView.image = self.starGrayImage;
	}

	for (int i = 0; i < index; ++i) {
		UIImageView *imageView = [self.ratingStarArray objectAtIndex:i];
		imageView.image = self.starBlueImage;
	}
	self.ratingValue = index;
}

- (IBAction)touchDown:(id)sender {
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	CGFloat dx = (CONTENT_SIZE.width / 2 - ratingStarSize.width * 1.8 / 2) / (MAX_STARS + 1) - 5;

	for (UITouch *touch in touches) {
		CGPoint p = [touch locationInView:self.view];

		if (CGRectContainsPoint(self.view.frame, p)) {
			CGFloat index = round((p.x - dx) / (ratingStarSize.width * 1.8));

			// NSLog(@"%f %f %d", p.x, index, (int) index);
			[self setRating:(int)index];
			break;
		}
	}
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	CGFloat dx = (CONTENT_SIZE.width / 2 - ratingStarSize.width * 1.8 / 2) / (MAX_STARS + 1) - 5;

	for (UITouch *touch in touches) {
		CGPoint p = [touch locationInView:self.view];

		if (CGRectContainsPoint(self.view.frame, p)) {
			CGFloat index = round((p.x - dx) / (ratingStarSize.width * 1.8));

			// NSLog(@"%f %f %d", p.x, index, (int) index);
			[self setRating:(int)index];
			break;
		}
	}
}

@end
