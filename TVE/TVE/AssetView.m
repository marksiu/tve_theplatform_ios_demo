//
//  AssetView.m
//  TVE
//
//  Created by Gabor Bottyan on 12/4/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//



static const CGSize asset_home_list_size = { 392, 107 };
static const CGSize asset_home_image_size = { 165, 103 };

static const CGSize asset_ondemand_size = { 332, 99 };
static const CGSize asset_ondemand_image_size = { 139, 85 };

static const CGSize asset_ondemand_new_size = { 465, 118 };
static const CGSize asset_ondemand__new_image_size = { 166, 102 };

static const CGSize asset_list_left_size = { 320, 86 };
static const CGSize asset_list_left_size_small = { 320, 50 };
static const CGSize asset_list_image_size = { 140, 82 };

static const CGSize asset_related_size = { 205, 126 };
static const CGSize asset_related_image_size = { 193, 114 };

static const CGSize asset_episode_size = { 640, 124 };
static const CGSize asset_episode_image_size = { 180, 110 };

// @stef: val set to 1 (old value 2).
static const int small_assett_border = 1;
static const int big_assett_border = 6;
static const int assett_border_8 = 8;
// static UIFont * neueBold18 = ;[UIFont fontWithName:@"HelveticaNeue-Medium" size:18];

#import "AssetView.h"
#import "Constants.h"
#import "VSRemoteImageView.h"
#import "UIHelper.h"
#import <QuartzCore/QuartzCore.h>
#import "AppView.h"

@interface AssetView ()
@property (retain) UILabel *title;
@property (retain) UILabel *subtitle;
@property (retain) UILabel *description;
@property (retain) UIView *background;
@property (retain) UIView *ratingView;
@end


@implementation AssetView

- (void)dealloc {
	self.title = Nil;
	self.background = Nil;
	self.subtitle = Nil;
	self.description = Nil;

	[super dealloc];
}

- (void)setBackColor:(UIColor *)color {
	self.background.backgroundColor = color;
}

+ (CGSize)sizeOfStyle:(int)style {
	CGSize rectSize = CGSizeZero;

	switch (style)
	{
		case ASSET_TYPE_HOME_LIST :
			rectSize = asset_home_list_size;
			break;
		case ASSET_TYPE_HOME_LIST_MORE :
			rectSize = CGSizeMake(asset_home_list_size.width, asset_home_list_size.height / 2);
			break;
		case ASSET_TYPE_LEFT_LIST :
			rectSize = asset_list_left_size;
			break;
		case ASSET_TYPE_LEFT_LIST_SMALL :
			rectSize = asset_list_left_size_small;
			break;
		case ASSET_TYPE_ON_DEMAND :
			rectSize = asset_ondemand_size;
			break;
		case ASSET_TYPE_RELATED_GRID :
			rectSize = asset_related_size;
			break;
		case ASSET_TYPE_EPISODE :
			rectSize = asset_episode_size;
			break;
		case ASSET_TYPE_ON_DEMAND_NEW :
			rectSize = asset_ondemand_new_size;
			break;


		default :
			break;
	}


	return rectSize;
}

// thePlatform
- (id)initWithStyleWithMyRental:(int)style data:(NSDictionary *)entry {
    
    CGSize rectSize = CGSizeZero;
    
    CGSize imageSize = CGSizeZero;
    
	int border = 0;
	int outborder = 0;
	float fontSize = 0;

	rectSize = asset_ondemand_new_size;
	imageSize = asset_ondemand__new_image_size;
	border = 5;
	outborder = 8;
	fontSize = 20;

	self = [super initWithFrame:(CGRect) {CGPointMake(0, 0), rectSize }];
	NSString *imageUrl = Nil;
//	imageUrl =  [UIHelper getImageUrl:entry forSize:imageSize];
	imageUrl = [entry objectForKey:@"image"];

	UIView *lineView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, rectSize.width, 1)] autorelease];
	[self addSubview:lineView];


	self.backgroundColor = [UIColor clearColor];


	self.background = [[[UIView alloc] initWithFrame:CGRectInset(CGRectMake(0, 0, rectSize.width, rectSize.height), outborder, outborder)] autorelease];

	self.background.backgroundColor = [UIColor colorWithRed:13.0f / 255.0f green:18.0f / 255.0f blue:22.0f / 255.0f alpha:1.0];

	[self addSubview:self.background];

	CGRect imageRect = CGRectZero;
	imageRect = CGRectInset(CGRectMake(0, 0, imageSize.width, imageSize.height), border, border);

	VSRemoteImageView *imageView = [[VSRemoteImageView alloc] initWithFrame:imageRect];
	[imageView setContentMode:UIViewContentModeScaleAspectFill];
	imageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
	[imageView setClipsToBounds:YES];

	[imageView setImageUrl:imageUrl withPlaceholder:nil animated:YES];

	[self.background addSubview:imageView];
	[imageView release];

	float padding = 5;

	CGSize textBlockSize = (style == ASSET_TYPE_LEFT_LIST) ? CGSizeZero : CGSizeMake(0, 5);

	CGRect titleRect = CGRectZero;
	titleRect = CGRectMake(imageSize.width + padding,  textBlockSize.height, imageSize.width - (imageSize.width + 30), imageSize.height / 2);

	self.title = [[[UILabel alloc] initWithFrame:titleRect] autorelease];
	self.title.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize];
	self.title.textColor = [UIColor whiteColor];
	self.title.backgroundColor = [UIColor clearColor];

	self.title.numberOfLines = 2;
	// title here
	self.title.text = [entry objectForKey:@"title"];

	self.title.userInteractionEnabled = NO;

	CGSize textSize = [self.title.text sizeWithFont:self.title.font
								  constrainedToSize:CGSizeMake(rectSize.width - (imageSize.width + 30), rectSize.height / 2)
									  lineBreakMode:NSLineBreakByWordWrapping];

	titleRect = CGRectMake(imageSize.width + padding, textBlockSize.height, textSize.width, textSize.height);

	titleRect.origin.y -= 1;

	self.title.frame =  titleRect;


	// title.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
	textBlockSize = CGSizeMake(textBlockSize.width, self.title.frame.size.height + self.title.frame.origin.y);
	[self.background addSubview:self.title];

	UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
	b.frame = CGRectMake(0, 0, rectSize.width, rectSize.height);
	[b addTarget:self action:@selector(tapped:) forControlEvents:UIControlEventTouchUpInside];

	[self.background addSubview:b];


	int rating = [[entry objectForKey:@"starRating"] intValue];
	self.ratingView = [[UIView alloc] initWithFrame:CGRectMake(self.title.frame.origin.x, self.title.frame.origin.y + self.title.frame.size.height + 3, textSize.width, ratingStarSize.height + gap)];
	// ratingView.backgroundColor = [UIColor yellowColor];
	self.ratingView.userInteractionEnabled = NO;
	UIImage *star_blue = [UIImage imageNamed:@"star_small_focus.png"];
	UIImage *star_gray = [UIImage imageNamed:@"star_small.png"];

	for (int j = 0; j < rating; j++) {
		UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectMake(j * (ratingStarSize.width + 3), 0, ratingStarSize.width, ratingStarSize.height)];
		star.image = star_blue;
		[self.ratingView addSubview:star];
		[star release];
	}

	// no sure the loop need or not
	for (int j = rating; j < 5; j++) {
		UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectMake(j * (ratingStarSize.width + 3), 0, ratingStarSize.width, ratingStarSize.height)];
		star.image = star_gray;
		[self.ratingView addSubview:star];
		[star release];
	}

	[self.background addSubview:self.ratingView];

	[self.ratingView release];

	UIView *lastView = self.subtitle ? self.subtitle : self.ratingView;
	NSNumber *friendsShared = [NSNumber numberWithInt:[[entry objectForKey:@"friendShare"] integerValue]];

	UIView *shareView = [[[UIView alloc] initWithFrame:CGRectOffset(lastView.frame, 0, lastView.frame.size.height)] autorelease];
	shareView.userInteractionEnabled = NO;

	UILabel *shareText = [[[UILabel alloc] initWithFrame:CGRectMake(0, 3, rectSize.width - (imageSize.width + 30), friendIconSize.height)] autorelease];
	shareText.text = [NSString stringWithFormat:@"%d", [friendsShared intValue]];
	shareText.textColor = [UIColor colorWithRed:141.0 / 255.0 green:141.0 / 255.0 blue:141.0 / 255.0 alpha:1.0];

	shareText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize - 5];

	shareText.backgroundColor = [UIColor clearColor];
	[shareView addSubview:shareText];

	CGSize textSizeF = [shareText.text sizeWithFont:shareText.font];

	UIImage *shareImage = [UIImage imageNamed:@"friend_icon_gray.png"];
	UIImageView *friends = [[[UIImageView alloc] initWithFrame:CGRectMake(textSizeF.width + gap, 3, friendIconSize.width, friendIconSize.height)] autorelease];
	friends.image = shareImage;
	[shareView addSubview:friends];

	// [shareText release];

	[self.background addSubview:shareView];

	NSLog(@"You did it, it is the rental record");

	return self;
}

/*
 *
 * "pl1$featured" = 0;
 * "pl1$friendsShared" = 8;
 * "pl1$isTVSeries" = 0;
 * "pl1$parentalControlRating" = G;
 * "pl1$price" = "10.00";
 * "pl1$rentalPeriod" = 2;
 * "pl1$showTime" = "Sunday 10pm ET";
 * "pl1$starRating" = 4;
 *
 */

- (id)initWithStyle:(int)style data:(NSDictionary *)entry {
	BOOL isChannelData = NO;
	BOOL isTVShow = NO;
	BOOL isEpisode = NO;
	BOOL isAd = NO;

	if ([entry objectForKey:@"ad"]) {
		isAd = YES;
	}


	if ([entry objectForKey:@"channel"]) {
		isChannelData = YES;
	}

	if ([entry objectForKey:@"pl1$isTVSeries"] && !isChannelData) {
		isTVShow = [[entry objectForKey:@"pl1$isTVSeries"] boolValue];
	}
	if ([entry objectForKey:@"pl1$seriesRef"] && !isTVShow && !isChannelData) {
		isEpisode = YES;
	}

	if (isEpisode) {
		// NSLog(@"%@", entry);
	}

	CGSize rectSize = CGSizeZero;
	CGSize imageSize = CGSizeZero;
	int border = 0;
	int outborder = 0;
	float fontSize = 0;

	// NSString * styleSTR = @"NO_STYLE";

	switch (style)
	{
		case ASSET_TYPE_HOME_LIST :
			// styleSTR = @"ASSET_TYPE_HOME_LIST";
			rectSize = asset_home_list_size;
			imageSize = asset_home_image_size;
			fontSize = 20;
			border = 7;
			outborder = small_assett_border;
			break;
		case ASSET_TYPE_HOME_LIST_MORE :
			// styleSTR = @"ASSET_TYPE_HOME_LIST_MORE";
			rectSize = CGSizeMake(asset_home_list_size.width, asset_home_list_size.height / 2);
			imageSize = asset_home_image_size;
			fontSize = 18;
			border = 7;
			outborder = small_assett_border;
			break;
		case ASSET_TYPE_LEFT_LIST :
			// styleSTR = @"ASSET_TYPE_LEFT_LIST";
			rectSize = asset_list_left_size;
			imageSize = asset_list_image_size;
			border = small_assett_border;
			outborder = small_assett_border;
			fontSize = 15.8;
			break;
		case ASSET_TYPE_LEFT_LIST_SMALL :
			rectSize = asset_list_left_size_small;
			imageSize = asset_list_image_size;
			border = small_assett_border;
			outborder = small_assett_border;
			fontSize = 15.8;
			break;
		case ASSET_TYPE_ON_DEMAND :
			// styleSTR = @"ASSET_TYPE_ON_DEMAND";
			rectSize = asset_ondemand_size;
			imageSize = asset_ondemand_image_size;
			border = small_assett_border;
			outborder = big_assett_border;
			fontSize = 17;
			break;
		case ASSET_TYPE_RELATED_GRID :
			// styleSTR = @"ASSET_TYPE_RELATED_GRID";
			rectSize = asset_related_size;
			imageSize = asset_related_image_size;
			border = 2;
			outborder = 6;
			fontSize = 17;
			break;
		case ASSET_TYPE_EPISODE :
			// styleSTR = @"ASSET_TYPE_EPISODE";
			rectSize = asset_episode_size;
			imageSize = asset_episode_image_size;
			border = 4;
			outborder = 6;
			fontSize = 17;
			break;
		case ASSET_TYPE_ON_DEMAND_NEW :
			// styleSTR = @"ASSET_TYPE_ON_DEMAND_NEW";
			rectSize = asset_ondemand_new_size;
			imageSize = asset_ondemand__new_image_size;
			border = 5;
			outborder = 8;
			fontSize = 20;

			break;

		default :
			break;
	}

	// NSLog(@"::: fontSize: %f for assetType: %d (%@)", fontSize, style, styleSTR);

	self = [super initWithFrame:(CGRect) {CGPointMake(0, 0), rectSize }];

	if (self) {
		NSString *imageUrl = Nil;

		if (isChannelData) {
			imageUrl = [entry objectForKey:@"logo"];
		}
		else {
			imageUrl =  [UIHelper getImageUrl:entry forSize:imageSize];
		}


		UIView *lineView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, rectSize.width, 1)] autorelease];
		[self addSubview:lineView];


		self.backgroundColor = [UIColor clearColor];


		if (style == ASSET_TYPE_EPISODE) {
			self.background = [[[UIView alloc] initWithFrame:CGRectInset(CGRectMake(10, 10, imageSize.width, imageSize.height), 0, 0)] autorelease];
		}
		else {
			self.background = [[[UIView alloc] initWithFrame:CGRectInset(CGRectMake(0, 0, rectSize.width, rectSize.height), outborder, outborder)] autorelease];
		}



		// self.background.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
		self.background.backgroundColor = [UIColor colorWithRed:13.0f / 255.0f green:18.0f / 255.0f blue:22.0f / 255.0f alpha:1.0];

		[self addSubview:self.background];

		if (style == ASSET_TYPE_HOME_LIST_MORE) {
			UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.background.frame.size.width, self.background.frame.size.height)];
			label.backgroundColor = [UIColor clearColor];
			label.textColor = [UIColor whiteColor];
			label.textAlignment = NSTextAlignmentCenter;
			[self.background addSubview:label];
			label.text = @"More...";
			[label release];


			UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
			b.frame = CGRectMake(0, 0, rectSize.width, rectSize.height);
			[b addTarget:self action:@selector(tapped:) forControlEvents:UIControlEventTouchUpInside];
			[self.background addSubview:b];
		}
		else {
			CGRect imageRect = CGRectZero;

			if (isChannelData) {
				if (style == ASSET_TYPE_LEFT_LIST_SMALL) {
					imageRect = CGRectMake(170, 10, 85, 30);
				}
				else {
					imageRect = CGRectMake(250, 10, 140, 60);
				}
			}
			else {
				if (isAd) {
					imageRect = CGRectMake(0, 0, self.background.frame.size.width, self.background.frame.size.height);
				}
				else {
					imageRect = CGRectInset(CGRectMake(0, 0, imageSize.width, imageSize.height), border, border);
				}
			}

			if (isAd) {
				UIImageView *imageView = [[UIImageView alloc] initWithFrame:imageRect];
				imageView.image = [UIImage imageNamed:[entry objectForKey:@"ad"]];
				imageView.clipsToBounds = YES;
				imageView.contentMode = UIViewContentModeScaleToFill;
				[self.background addSubview:imageView];
				[imageView release];
			}
			else {
				VSRemoteImageView *imageView = [[VSRemoteImageView alloc] initWithFrame:imageRect];
				[imageView setContentMode:UIViewContentModeScaleAspectFill];
				imageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
				[imageView setClipsToBounds:YES];

				// Use channel mockup images which were saved locally.
				if (isChannelData && [imageUrl rangeOfString:@".png"].location != NSNotFound) {
					[imageView setLocalImage:imageUrl withChannel:isChannelData];
				}
				else {
					[imageView setImageUrl:imageUrl withPlaceholder:nil animated:YES];
				}

				[self.background addSubview:imageView];
				[imageView release];
			}

			float padding = 5;

			CGSize textBlockSize = (style == ASSET_TYPE_LEFT_LIST) ? CGSizeZero : CGSizeMake(0, 5);

			if (style == ASSET_TYPE_EPISODE) {
				self.subtitle = [[[UILabel alloc] initWithFrame:CGRectMake(imageSize.width + padding, 0, imageSize.width - (imageSize.width + 30), imageSize.height / 2)] autorelease];
				self.subtitle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize - 2];

				NSMutableString *episode = [NSMutableString string];
				if ([entry objectForKey:@"pl1$season"]) {
					[episode appendFormat:@"Season %@", [entry objectForKey:@"pl1$season"]];
				}
				else {
					if ([UIHelper getSeason:entry]) {
						[episode appendFormat:@"Season %@", [UIHelper getSeason:entry]];
					}
				}
				if ([episode length] > 0) {
					[episode appendString:@" "];
				}

				if ([entry objectForKey:@"pl1$episode"]) {
					[episode appendFormat:@"Episode %@", [entry objectForKey:@"pl1$episode"]];
				}
				else {
					if ([UIHelper getSeason:entry]) {
						[episode appendFormat:@"Episode %@", [UIHelper getEpisode:entry]];
					}
				}

				self.subtitle.textColor = [UIColor whiteColor];
				self.subtitle.backgroundColor = [UIColor clearColor];
				self.subtitle.text = episode;
				// [entry objectForKey:@"title"];
				self.subtitle.userInteractionEnabled = NO;
				CGSize subtitletextSize = [self.subtitle.text sizeWithFont:self.subtitle.font
														 constrainedToSize:CGSizeMake(rectSize.width - (imageSize.width + 30), rectSize.height / 2)
															 lineBreakMode:NSLineBreakByWordWrapping];

				self.subtitle.frame = CGRectMake(imageSize.width + padding, 0, subtitletextSize.width, subtitletextSize.height);
				self.subtitle.numberOfLines = 0;
				// title.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
				[self.background addSubview:self.subtitle];
				textBlockSize = subtitletextSize;
			}

			CGRect titleRect = CGRectZero;

			if (isChannelData) {
				if (style == ASSET_TYPE_LEFT_LIST_SMALL) {
					titleRect = CGRectMake(30,  textBlockSize.height + padding, imageSize.width - (imageSize.width + 30), imageSize.height / 2);
				}
				else {
					titleRect = CGRectMake(50,  textBlockSize.height, imageSize.width - (imageSize.width + 30), imageSize.height / 2);
				}
			}
			else {
				titleRect = CGRectMake(imageSize.width + padding,  textBlockSize.height, imageSize.width - (imageSize.width + 30), imageSize.height / 2);
			}

			self.title = [[[UILabel alloc] initWithFrame:titleRect] autorelease];
			self.title.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize];
			self.title.textColor = [UIColor whiteColor];
			self.title.backgroundColor = [UIColor clearColor];

			if (isEpisode) {
				self.title.numberOfLines = 1;
			}
			else {
				self.title.numberOfLines = 2;
			}

			if (isChannelData) {
				self.title.text = [entry objectForKey:@"name"];
			}
			else {
				if (isEpisode) {
					self.title.text = [entry objectForKey:@"pl1$seriesTitle"];
				}
				else {
					self.title.text = [entry objectForKey:@"title"];
                    // My TV - My Rental - title
				}
			}

			self.title.userInteractionEnabled = NO;

			CGSize textSize = [self.title.text sizeWithFont:self.title.font
										  constrainedToSize:CGSizeMake(rectSize.width - (imageSize.width + 30), rectSize.height / 2)
											  lineBreakMode:NSLineBreakByWordWrapping];

			if (isChannelData) {
				if (style == ASSET_TYPE_LEFT_LIST_SMALL) {
					titleRect = CGRectMake(5,  23, textSize.width, textSize.height);
				}
				else {
					titleRect = CGRectMake(50,  40, textSize.width, textSize.height);
				}
			}
			else {
				titleRect = CGRectMake(imageSize.width + padding, textBlockSize.height, textSize.width, textSize.height);

				if (style != ASSET_TYPE_LEFT_LIST) {
					titleRect.origin.y -= 1;
				}
				else {
					titleRect.origin.y += 1;
				}
			}

			self.title.frame =  titleRect;


			// title.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
			textBlockSize = CGSizeMake(textBlockSize.width, self.title.frame.size.height + self.title.frame.origin.y);
			[self.background addSubview:self.title];

			if (isTVShow) {
				self.subtitle = [[[UILabel alloc] initWithFrame:CGRectMake(imageSize.width + padding, self.title.frame.origin.y + titleRect.size.height, imageSize.width - (imageSize.width + 30), imageSize.height / 2)] autorelease];

				float fs = style == ASSET_TYPE_LEFT_LIST ? fontSize - 1 : fontSize - 5;

				self.subtitle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fs];

				self.subtitle.textColor = [UIColor colorWithRed:141.0f / 255.0f green:141.0f / 255.0f blue:141.0f / 255.0f alpha:1.0f];
				self.subtitle.backgroundColor = [UIColor clearColor];
				self.subtitle.text = [entry objectForKey:@"pl1$showTime"];
				self.subtitle.userInteractionEnabled = NO;
				CGSize subtitletextSize = [self.subtitle.text sizeWithFont:self.subtitle.font
														 constrainedToSize:CGSizeMake(rectSize.width - (imageSize.width + 30), rectSize.height / 2)
															 lineBreakMode:NSLineBreakByWordWrapping];

				self.subtitle.frame = CGRectMake(imageSize.width + padding, self.title.frame.origin.y + titleRect.size.height, subtitletextSize.width, subtitletextSize.height);
				self.subtitle.numberOfLines = 0;
				// title.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
				[self.background addSubview:self.subtitle];
				textBlockSize = subtitletextSize;
			}


			if (isEpisode && style != ASSET_TYPE_EPISODE) {
				self.subtitle = [[[UILabel alloc] initWithFrame:CGRectMake(imageSize.width + padding, self.title.frame.origin.y + titleRect.size.height, imageSize.width - (imageSize.width + 30), imageSize.height / 2)] autorelease];
				self.subtitle.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize - 2];

				if (isEpisode) {
					self.subtitle.textColor = [UIColor whiteColor];
				}
				else {
					self.subtitle.textColor = [UIColor colorWithRed:141.0f / 255.0f green:141.0f / 255.0f blue:141.0f / 255.0f alpha:1.0f];
				}
				self.subtitle.backgroundColor = [UIColor clearColor];

				NSMutableString *episode = [NSMutableString string];
				if ([entry objectForKey:@"pl1$season"]) {
					[episode appendFormat:@"Season %@", [entry objectForKey:@"pl1$season"]];
				}
				else {
					if ([UIHelper getSeason:entry]) {
						[episode appendFormat:@"Season %@", [UIHelper getSeason:entry]];
					}
				}

				if ([episode length] > 0) {
					[episode appendString:@" "];
				}

				if ([entry objectForKey:@"pl1$episode"]) {
					[episode appendFormat:@"Episode %@", [entry objectForKey:@"pl1$episode"]];
				}
				else {
					if ([UIHelper getSeason:entry]) {
						[episode appendFormat:@"Episode %@", [UIHelper getEpisode:entry]];
					}
				}


				NSString *bulkTitle = [entry objectForKey:@"title"];

				NSRange titleStart  = [bulkTitle rangeOfString:@"("];
				NSRange titleEnd  = [bulkTitle rangeOfString:@")"];

				if (titleStart.location != NSNotFound && titleEnd.location != NSNotFound) {
					NSString *actualTitle = [bulkTitle substringWithRange:NSMakeRange(titleStart.location + titleStart.length, titleEnd.location - titleStart.location - titleEnd.length)];

					[episode appendFormat:@"\n%@", actualTitle ];
				}



				self.subtitle.text = episode;
				self.subtitle.userInteractionEnabled = NO;
				CGSize subtitletextSize = [self.subtitle.text sizeWithFont:self.subtitle.font
														 constrainedToSize:CGSizeMake(rectSize.width - (imageSize.width + 30), rectSize.height / 2)
															 lineBreakMode:NSLineBreakByWordWrapping];

				self.subtitle.frame = CGRectMake(imageSize.width + padding, self.title.frame.origin.y + titleRect.size.height, subtitletextSize.width, subtitletextSize.height);
				self.subtitle.numberOfLines = 0;
				// title.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
				[self.background addSubview:self.subtitle];
				textBlockSize = CGSizeMake(subtitletextSize.width, self.subtitle.frame.origin.y + self.subtitle.frame.size.height);
			}

			if (isChannelData && style == ASSET_TYPE_LEFT_LIST_SMALL) {
				NSNumber *payd = [entry objectForKey:@"pay"];
				if (payd && [payd boolValue]) {
					// NSLog(@"CHANNEL IS PAYD");
					NSArray *paidChannels = [[AppView sharedInstance] getPaidChannels];
					// NSLog(@"SUBSCRIBED CHANNELS: %@", paidChannels);
					NSNumber *chan = [NSNumber numberWithInt:[[entry objectForKey:@"channel"] intValue]];
					if (!paidChannels || ![paidChannels containsObject:chan]) {
						// NSLog(@"NO SUBSCRIPTION FOUND");
						UIImageView *lock  = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lock.png"]] autorelease];
						// lock.frame = CGRectMake(rectSize.width-25, 0, 12.5,15);
						lock.frame = CGRectMake(rectSize.width - 25, 0, 12.5, 15);
						[self.background addSubview:lock];
						lock.center = CGPointMake(lock.center.x, self.background.center.y);
					}
				}
			}

			if (style == ASSET_TYPE_EPISODE || isChannelData) {
				CGRect subtitleRect = CGRectZero;

				if (isChannelData) {
					subtitleRect = CGRectMake(5, 5, imageSize.width - (imageSize.width + 30), imageSize.height / 2);

					if (style == ASSET_TYPE_LEFT_LIST_SMALL) {
						self.description.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize - 4];
					}
					else if (style == ASSET_TYPE_LEFT_LIST) {
						self.description.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize - 1];
					}
					else {
						self.description.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize];
					}
				}
				else {
					subtitleRect = CGRectMake(imageSize.width + padding, self.title.frame.origin.y + self.title.frame.size.height, imageSize.width - (imageSize.width + 30), imageSize.height / 2);

					if (style == ASSET_TYPE_LEFT_LIST) {
						self.description.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize - 1];
					}
					else {
						self.description.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize - 5];
					}
				}


				self.description = [[[UILabel alloc] initWithFrame:subtitleRect] autorelease];

				self.description.numberOfLines = 2;

				self.description.textColor = [UIColor whiteColor];
				self.description.backgroundColor = [UIColor clearColor];

				if (isChannelData) {
					self.description.text = [NSString stringWithFormat:@"CH %@", [entry objectForKey:@"channel"]];
				}
				else {
					self.description.text = [entry objectForKey:@"description"];
				}

				// self.description.text = @"In the Season 1 finale...";

				self.description.userInteractionEnabled = NO;
				/*
				 * CGSize subtitletextSize = [self.description.text sizeWithFont:self.description.font
				 * constrainedToSize:CGSizeMake(rectSize.width-(imageSize.width+30), rectSize.height - textBlockSize.height)
				 * lineBreakMode:NSLineBreakByWordWrapping];
				 */
				CGSize subtitletextSize = [self.description.text sizeWithFont:self.description.font
															constrainedToSize:CGSizeMake(rectSize.width - (imageSize.width + 30), 40)
																lineBreakMode:NSLineBreakByWordWrapping];

				if (isChannelData && style == ASSET_TYPE_LEFT_LIST_SMALL) {
					textBlockSize = CGSizeMake(0, 0);
				}

				self.description.frame = CGRectMake(imageSize.width + padding, textBlockSize.height, subtitletextSize.width, subtitletextSize.height);


				if (isChannelData) {
					if (style == ASSET_TYPE_LEFT_LIST_SMALL) {
						self.description.frame = CGRectMake(5, 3, subtitletextSize.width, subtitletextSize.height);
					}
					else {
						self.description.frame = CGRectMake(50, 20, subtitletextSize.width, subtitletextSize.height);
					}
				}
				else {
					self.description.frame = CGRectMake(imageSize.width + padding, textBlockSize.height, subtitletextSize.width, subtitletextSize.height);
				}


				self.description.numberOfLines = 0;
				// title.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
				[self.background addSubview:self.description];

				textBlockSize = CGSizeMake(textBlockSize.width, self.description.frame.size.height + self.description.frame.origin.y);
			}

			if (isEpisode) {
				NSNumber *expiry = [entry objectForKey:@"media$expirationDate"];
				NSDate *expiryDate = [NSDate dateWithTimeIntervalSince1970:[expiry doubleValue] / 1000];
				double now = [[NSDate date] timeIntervalSince1970];
				// NSLog(@"%f %@", [expiry doubleValue] ,expiryDate);

				if ([expiry doubleValue] > 0) {
					int days = floor(([expiry doubleValue] / 1000 - now) / 24 / 60 / 60);
					CGRect subtitleRect = CGRectZero;
					subtitleRect = CGRectMake(imageSize.width + padding, textBlockSize.height, 100, imageSize.height / 2);

					self.description = [[[UILabel alloc] initWithFrame:subtitleRect] autorelease];

					if (style == ASSET_TYPE_EPISODE) {
						self.description.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize - 2];
					}
					else {
						self.description.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize - 4];
					}
					self.description.numberOfLines = 1;
					self.description.text = [NSString stringWithFormat:@"Expires %d days", days];
					self.description.textColor = [UIColor colorWithRed:141.0f / 255.0f green:141.0f / 255.0f blue:141.0f / 255.0f alpha:1.0f];
					self.description.backgroundColor = [UIColor clearColor];
					CGSize desctextSize = [self.description.text sizeWithFont:self.description.font
															constrainedToSize:CGSizeMake(rectSize.width - (imageSize.width + 30), rectSize.height / 2)
																lineBreakMode:NSLineBreakByWordWrapping];

					self.description.frame = CGRectMake(imageSize.width + padding, textBlockSize.height, desctextSize.width, desctextSize.height);

					[self.background addSubview:self.description];
				}
			}

			UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
			b.frame = CGRectMake(0, 0, rectSize.width, rectSize.height);
			[b addTarget:self action:@selector(tapped:) forControlEvents:UIControlEventTouchUpInside];

			[self.background addSubview:b];

			if (style != ASSET_TYPE_EPISODE && style != ASSET_TYPE_RELATED_GRID && !isChannelData && !isTVShow && !isEpisode && !isAd) {
				int rating = [[entry objectForKey:@"pl1$starRating"] intValue];
				self.ratingView = [[UIView alloc] initWithFrame:CGRectMake(self.title.frame.origin.x, self.title.frame.origin.y + self.title.frame.size.height + 3, textSize.width, ratingStarSize.height + gap)];
				// ratingView.backgroundColor = [UIColor yellowColor];
				self.ratingView.userInteractionEnabled = NO;
				UIImage *star_blue = [UIImage imageNamed:@"star_small_focus.png"];
				UIImage *star_gray = [UIImage imageNamed:@"star_small.png"];

				for (int j = 0; j < rating; j++) {
					UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectMake(j * (ratingStarSize.width + 3), 0, ratingStarSize.width, ratingStarSize.height)];
					star.image = star_blue;
					[self.ratingView addSubview:star];
					[star release];
				}

				for (int j = rating; j < 5; j++) {
					UIImageView *star = [[UIImageView alloc] initWithFrame:CGRectMake(j * (ratingStarSize.width + 3), 0, ratingStarSize.width, ratingStarSize.height)];
					star.image = star_gray;
					[self.ratingView addSubview:star];
					[star release];
				}

				[self.background addSubview:self.ratingView];

				[self.ratingView release];
			}

			if (style == ASSET_TYPE_RELATED_GRID) {
				UIView *blackBand = [[UIView alloc] initWithFrame:CGRectMake(0, imageSize.height - 30, imageSize.width, 30)];
				blackBand.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
				[self.background addSubview:blackBand];
				[blackBand release];

				UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectInset(CGRectMake(0, imageSize.height - 30, imageSize.width, 30), 4, 0)];
				titleLabel.textColor = [UIColor whiteColor];
				titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize - 1];
				titleLabel.text  = [entry objectForKey:@"title"];
				titleLabel.textAlignment = NSTextAlignmentLeft;
				titleLabel.backgroundColor = [UIColor clearColor];
				[self.background addSubview:titleLabel];
				[titleLabel release];
			}

			if (!isChannelData && style != ASSET_TYPE_RELATED_GRID) {
				UIView *lastView = self.subtitle ? self.subtitle : self.ratingView;
				NSNumber *friendsShared = [entry objectForKey:@"pl1$friendsShared"];

				// int shareNumber = rand() % 10;
				if (friendsShared && [friendsShared intValue] > 0) {
					UIView *shareView = [[[UIView alloc] initWithFrame:CGRectOffset(lastView.frame, 0, lastView.frame.size.height)] autorelease];
					shareView.userInteractionEnabled = NO;

					UILabel *shareText = [[[UILabel alloc] initWithFrame:CGRectMake(0, 3, rectSize.width - (imageSize.width + 30), friendIconSize.height)] autorelease];
					shareText.text = [NSString stringWithFormat:@"%d", [friendsShared intValue]];
					shareText.textColor = [UIColor colorWithRed:141.0 / 255.0 green:141.0 / 255.0 blue:141.0 / 255.0 alpha:1.0];

					if (style == ASSET_TYPE_LEFT_LIST) {
						shareText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize - 1];
					}
					else {
						shareText.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:fontSize - 5];
					}

					shareText.backgroundColor = [UIColor clearColor];
					[shareView addSubview:shareText];

					CGSize textSize = [shareText.text sizeWithFont:shareText.font];

					UIImage *shareImage = [UIImage imageNamed:@"friend_icon_gray.png"];
					UIImageView *friends = [[[UIImageView alloc] initWithFrame:CGRectMake(textSize.width + gap, 3, friendIconSize.width, friendIconSize.height)] autorelease];
					friends.image = shareImage;
					[shareView addSubview:friends];

					// [shareText release];

					[self.background addSubview:shareView];
					// [shareView release];
				}
			}

			if (isEpisode /*&& style == ASSET_TYPE_HOME_LIST*/) {
				UIImageView *playThumb  = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play_thumb.png"]] autorelease];

				playThumb.frame = CGRectMake(10, 10, 46, 30);
				playThumb.alpha = 0.5;
				playThumb.contentMode = UIViewContentModeScaleAspectFill;

				[self.background addSubview:playThumb];
			}
		}

		// Initialization code
	}

	return self;
}

- (void)tapped:(id)sender {
	if (self.delegate) {
		[self.delegate asetTapped:self];
	}
}

- (void)setData:(NSDictionary *)data {
}

@end
