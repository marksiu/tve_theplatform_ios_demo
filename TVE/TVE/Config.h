//
//  Config.h
//  TVE
//
//  Created by Zoltan Gobolos on 12/20/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#ifndef TVE_Config_h
#define TVE_Config_h

#define DEVELOPMENT    1
#define VS_DEVELOPMENT 1

#if DEVELOPMENT
#import "TestFlight.h"
#endif

#define APPVIEW_URL    @ "http://api.appview.demo.accedo.tv/"
// #define API_KEY        @ "5175945be4b05c62ec9ddda8"
 #define API_KEY       @ "51f61fc0e4b0bbd03aa7b8ba"
// #define API_KEY        @ "5175a243e4b05c62ec9ddde4"

// #define APPVIEW_URL                          @ "http://192.168.3.31/api/"
// #define API_KEY                              @ "514b02fde4b0f066c357b52d"
// #define API_KEY                              @ "51774878e4b0c0bec9412b3b"

// appview 1.1 demo
// #define APPVIEW_URL                          @ "http://api.appview.demo.accedo.tv/"
// #define API_KEY                              @ "5175945be4b05c62ec9ddda8"

// #define APPVIEW_URL                         @ "http://api.appview.cloud.accedo.tv/"
// #define API_KEY                             @ "5152ac7de4b051acb384b506"


#define AC_KEY                               @ "99b925f0d1a2aff503f40226c06838c0"
#define AC_URL                               @ "http://api.connect.accedo.tv"

#define LINEAR_DATA_URL                      @ "http://api.appview.demo.accedo.tv/50dc037be4b0e503f98471f7/asset"
#define LIVE_VIDEO_URL                       @ "http://share.tve.demo.accedo.tv/live.php?maxWidth=720"

// OVP configuration hardcode

#define ACCOUNT_ID                           @ "http://access.auth.theplatform.com/data/Account/2301538417"
#define END_USER_AUTHENTICATION_DIRECTORY_ID @ "IJHRpd4czHsVFdob"
#define FEED_PID                             @ "DwE21wX1pGqZ"
#define FEED_REGISTRY_ID                     @ "xBrLJC"
#define MOVIE_CAT_ID                         @ "9768515931"
#define TV_CLIP_CAT_ID                       @ "11975747634"
#define TV_EPISODE_CAT_ID                    @ "11240515733"
#define TV_SERIES_CAT_ID                     @ "9768515932"

// Account configuration

#define MEDIA_OBJECT_URL                     @ "http://data.media.theplatform.com/media/data/Media"
#define ADMIN_AUTHENTICATION_ENDPOINT        @ "https://identity.auth.theplatform.com/idm/web/Authentication"
#define AUTHENTICATION_ENDPOINT_SCHEMA       @ "1.0"
#define PAGE_SIZE                            @ "6"
#define FORMAT                               @ "json"
#define END_USER_AUTHENTICATION_ENDPOINT     @ "https://euid.theplatform.com/idm/web/Authentication"
#define PRETTY                               @ "true"
#define SCHEMA                               @ "1.2"
#define FEED_BASE_URL                        @ "http://feed.theplatform.com/f"
#define ADMIN_AUTHENTICATION_DIRECTORY_ID    @ "mpx"
#define CATEGORY_BASE_URL                    @ "http://data.media.theplatform.com/media/data/Category/"
#define RATING_ENDPOINT_URL                  @ "http://data.social.community.theplatform.com/social/data/Rating"

// com.accedo.plugin.ThePlatformOnDemand other config

#define PLUGIN_KEY                           @ "com.accedo.plugin.ThePlatformOnDemand"
#define ENABLED                              @ "1"

#endif // ifndef TVE_Config_h
