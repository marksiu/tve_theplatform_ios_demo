//
//  JSONViewController.h
//  TVE
//
//  Created by Gabor Bottyan on 12/13/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^ EventCallback)(id sender);



@interface JSONViewController : UIViewController {
}
@property (retain) NSMutableDictionary *controls;
@property (assign) id delegate;
- (id)initWithBundle:(NSBundle *)__bundle;
- (void)on:(NSString * )key run:(EventCallback)callback;
@end
