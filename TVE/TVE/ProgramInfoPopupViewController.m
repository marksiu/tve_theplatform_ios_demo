//
//  ProgramInfoPopupViewController.m
//  TVE
//
//  Created by Mate Beres on 12/13/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "ProgramInfoPopupViewController.h"
#import "AppView.h"

#import "Constants.h"

@interface ProgramInfoPopupViewController ()
@property (assign) int mode;
@end

@implementation ProgramInfoPopupViewController

@synthesize metaData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil mode:(int)mode meta:(NSDictionary *)meta {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

	if (self) {
		self.mode = mode;
		self.metaData = meta;
	}

	return self;
}

- (void)configureView:(int)mode {
	// 0 - catchup
	// 1 - future
	// 2 - nontv
	BOOL needNonTVStuff = mode == 2;

	self.tvTextView.hidden = !needNonTVStuff;
	self.tvTitleLabel.hidden = !needNonTVStuff;
	self.tvWatchChannelButton.hidden = !needNonTVStuff;
	self.tvWatchChannelButton.enabled = needNonTVStuff;

	self.button1.hidden = needNonTVStuff;
	self.button2.hidden = needNonTVStuff;
	self.button3.hidden = needNonTVStuff;
	self.button4.hidden = needNonTVStuff;
	self.button1.enabled = !needNonTVStuff;
	self.button2.enabled = !needNonTVStuff;
	self.button3.enabled = !needNonTVStuff;
	self.button4.enabled = !needNonTVStuff;

	self.imageView.hidden = needNonTVStuff;
	self.textView.hidden = needNonTVStuff;

	self.demandLabel.hidden = needNonTVStuff;
	self.expiresLabel.hidden = needNonTVStuff;
	self.titleLabel.hidden = needNonTVStuff;
	self.seasonLabel.hidden = needNonTVStuff;
	self.thirdLabel.hidden = needNonTVStuff;

	self.expiresLabel.hidden = YES;
	self.demandLabel.hidden = YES;
	self.expiresImageView.hidden = YES;

	if (mode == 0) {
		[self.button1 setTitle:@"Watch Episode" forState:UIControlStateNormal];
		[self.button2 setTitle:@"Show Details" forState:UIControlStateNormal];
		[self.button3 setTitle:@"Watch Channel" forState:UIControlStateNormal];
		[self.button4 setTitle:@"Add to Favorites" forState:UIControlStateNormal];

		self.expiresLabel.hidden = NO;
		self.demandLabel.hidden = NO;
		self.expiresImageView.hidden = NO;
	}
	else if (mode == 1) {
		[self.button1 setTitle:@"SMS Reminder" forState:UIControlStateNormal];
		[self.button2 setTitle:@"Show Details" forState:UIControlStateNormal];
		[self.button3 setTitle:@"Watch Channel" forState:UIControlStateNormal];
		[self.button4 setTitle:@"Add to Favorites" forState:UIControlStateNormal];

		self.expiresLabel.hidden = YES;
		self.demandLabel.hidden = YES;
		self.expiresImageView.hidden = YES;
	}

	self.seasonLabel.hidden = YES;
	self.thirdLabel.hidden = YES;

	self.titleLabel.text = [metaData objectForKey:@"title"];
	self.tvTitleLabel.text = [metaData objectForKey:@"title"];

	self.textView.text = [metaData objectForKey:@"desc"];
	self.tvTextView.text = [metaData objectForKey:@"desc"];

	self.textView.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15];
	self.tvTextView.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:15];

	self.textView.textColor     = [UIColor colorWithRed:76.0 / 255.0f green:86.0f / 255.0f blue:106.0f / 255.0f alpha:1];
	self.tvTextView.textColor   = [UIColor colorWithRed:76.0 / 255.0f green:86.0f / 255.0f blue:106.0f / 255.0f alpha:1];
	self.tvTitleLabel.textColor = [UIColor colorWithRed:76.0 / 255.0f green:86.0f / 255.0f blue:106.0f / 255.0f alpha:1];
	self.titleLabel.textColor   = [UIColor colorWithRed:76.0 / 255.0f green:86.0f / 255.0f blue:106.0f / 255.0f alpha:1];

	self.tvTitleLabel.textColor = [UIColor redColor];

	NSString *channelID = [metaData objectForKey:@"channel"];

	if (channelID) {
		NSDictionary *channelData = [[AppView sharedInstance] channelByChannelId:channelID];
		NSString *chImgUrl = [channelData objectForKey:@"logo"];

		[self.contentProviderImageView setImageUrl:chImgUrl];
	}
}

- (void)viewDidLoad {
	[super viewDidLoad];

	self.navigationItem.title = @"Program Info";

	self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Close"
																			   style:UIBarStyleDefault
																			  target:self
																			  action:@selector(close:)] autorelease];

	// [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:76.0 / 255.0 green:129.0 / 255.0 blue:227.0 / 255.0 alpha:1.0]];
	// [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:195.0 / 255.0 green:198.0 / 255.0 blue:207.0 / 255.0 alpha:1.0]];

	[self configureView:self.mode];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)close:(id)sender {
	[CURRENT_WINDOW.rootViewController dismissViewControllerAnimated:YES completion:^{}];
}
- (IBAction)smsIfFuture:(id)sender {
	if (1 == self.mode) {
		UIAlertView *a = [[[UIAlertView alloc] initWithTitle:@"SMS Reminder Set"
													 message:@"Your SMS reminder was set.\nYou can cancel this SMS reminder at any time."
													delegate:nil
										   cancelButtonTitle:@"OK"
										   otherButtonTitles:nil] autorelease];
		[a show];
	}
	[CURRENT_WINDOW.rootViewController dismissViewControllerAnimated:YES completion:^{}];
}

- (void)dealloc {
	[_button1 release];
	[_button2 release];
	[_button3 release];
	[_button4 release];
	[_imageView release];
	[_titleLabel release];
	[_seasonLabel release];
	[_thirdLabel release];
	[_contentProviderImageView release];
	[_dateLabel release];
	[_tvWatchChannelButton release];
	[_tvTitleLabel release];
	[_tvTextView release];
	[_textView release];
	[_expiresImageView release];

	[metaData release];

	[super dealloc];
}

@end
