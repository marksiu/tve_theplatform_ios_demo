//
//  FilterViewController.h
//  TVE
//
//  Created by Gabor Bottyan on 12/20/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FilterViewControllerDelegate <NSObject>

- (void)filterChanged:(NSDictionary *)filter;
@end

@interface FilterViewController : UITableViewController
- (id)initWithData:(NSArray * )__data delegate:(id <FilterViewControllerDelegate>)__delegate;
- (void)setPopover:(UIPopoverController *)popoverController;
@property (retain) NSArray *data;
@property (assign) id <FilterViewControllerDelegate> delegate;


@end
