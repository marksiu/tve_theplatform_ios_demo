//
//  DetailsListView.m
//  TVE
//
//  Created by Gabor Bottyan on 11/30/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "DetailsListView.h"
#import "VSRemoteImageView.h"
#import "Constants.h"
#import "AssetView.h"
#import "TVEConfig.h"

@interface DetailsListView ()
@property (retain) NSArray *data;
@property (assign) int selectedIndex;
@property (retain) UICollectionView *collectionView;
@end

@implementation DetailsListView


- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
		UICollectionViewFlowLayout *flowLayout = [[[UICollectionViewFlowLayout alloc] init] autorelease];
		// [flowLayout setItemSize:CGSizeMake(detailRightSize, 1)];
		[flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
		flowLayout.minimumInteritemSpacing = 0;
		flowLayout.minimumLineSpacing = 0;

		[self.collectionView setCollectionViewLayout:flowLayout];
		self.collectionView = [[[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout] autorelease];
		self.collectionView.delegate = self;
		self.collectionView.dataSource = self;
		// [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"topCell"];
		[self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
		self.collectionView.backgroundColor = [UIColor clearColor];
		[self addSubview:self.collectionView];
	}
	return self;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *entry = [self.data objectAtIndex:indexPath.row];

	if ([entry objectForKey:@"channel"]) {
		return CGSizeMake(self.frame.size.width, [AssetView sizeOfStyle:ASSET_TYPE_LEFT_LIST_SMALL].height);
	}
	else {
		return CGSizeMake(self.frame.size.width, [AssetView sizeOfStyle:ASSET_TYPE_LEFT_LIST].height);
	}
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	UICollectionViewCell *cell = Nil;

	cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];

	AssetView *assetView = Nil;

	NSDictionary *entry = [self.data objectAtIndex:indexPath.row];

	if ([entry objectForKey:@"channel"]) {
		assetView = [[[AssetView alloc] initWithStyle:ASSET_TYPE_LEFT_LIST_SMALL data:entry] autorelease];
	}
	else {
		assetView = [[[AssetView alloc] initWithStyle:ASSET_TYPE_LEFT_LIST data:entry] autorelease];
	}
	assetView.frame = CGRectMake(0, 0, assetView.frame.size.width, assetView.frame.size.height);
	assetView.tag = indexPath.row;
	assetView.delegate = self;
	for (UIView *v in [[[cell.contentView subviews] copy] autorelease]) {
		[v removeFromSuperview];
	}

	if (indexPath.row == self.selectedIndex) {
		[assetView setBackColor:[UIColor colorWithRed:11.0 / 255.0 green:52.0 / 255.0 blue:105.0 / 255.0 alpha:0.5]];
	}
	else {
		[assetView setBackColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
	}
	assetView.userInteractionEnabled = NO;


	[cell.contentView addSubview:assetView];
	// cell.

	cell.backgroundColor = [UIColor clearColor];
	return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
	return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
	return [self.data count];
}

- (void)jumpToChannel:(NSString *)channelId {
	for (int i = 0; i < [self.data count]; i++) {
		NSDictionary *currentItem = [self.data objectAtIndex:i];
		NSString *currentChannelId = [currentItem objectForKey:@"channel"];
		if (currentChannelId && [currentChannelId isEqualToString:channelId]) {
			self.selectedIndex = i;
			[self reloadSelected];
			[self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.selectedIndex inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
			break;
		}
	}
}

- (void)asetTapped:(id)sender {
	return;

	AssetView *asset = (AssetView *)sender;
	int selected = asset.tag;

	// NSLog(@"Selected %d", selected);
	if (self.listdelegate) {
		[self.listdelegate itemSelected:[self.data objectAtIndex:selected]];
		for (int i = 0; i < [[self subviews] count]; i++) {
			AssetView *a = (AssetView *)[self.subviews objectAtIndex:i];
			if ([a isKindOfClass:[AssetView class]]) {
				if (i == selected) {
					[a setBackColor:[UIColor colorWithRed:11.0 / 255.0 green:52.0 / 255.0 blue:105.0 / 255.0 alpha:0.5]];
				}
				else {
					[a setBackColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
				}
			}
		}
	}
}

- (void)reloadItems {
	[self.collectionView reloadData];
	if (self.listdelegate && [self.data count] > 0) {
		TVEConfig *config = [TVEConfig sharedConfig];

		if ([config getShowSubscriptionMessage]) {
			[self.listdelegate itemSelected:[self.data objectAtIndex:self.selectedIndex]];
		}
	}
}

- (void)reloadSelected {
	NSArray *dirtypaths = [NSArray arrayWithObjects:[NSIndexPath indexPathForItem:self.selectedIndex inSection:0], nil];

	[self.collectionView reloadItemsAtIndexPaths:dirtypaths];
	if (self.listdelegate && [self.data count] > 0) {
		[self.listdelegate itemSelected:[self.data objectAtIndex:self.selectedIndex]];
	}
}

// - (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//
// }

// - (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	if (self.selectedIndex == indexPath.row) {
		// return;
		return NO;
	}

	NSArray *dirtypaths = [NSArray arrayWithObjects:[NSIndexPath indexPathForItem:self.selectedIndex inSection:0], [NSIndexPath indexPathForItem:indexPath.row inSection:0], nil];

	self.selectedIndex = indexPath.row;

	[self.collectionView reloadItemsAtIndexPaths:dirtypaths];

	NSDictionary *selectedEntry = [self.data objectAtIndex:indexPath.row];

	if (![selectedEntry objectForKey:@"ad"]) {
		if (self.listdelegate) {
			[self.listdelegate itemSelected:[self.data objectAtIndex:indexPath.row]];
		}
	}
	else {
		return NO;
	}



	return YES;
}

- (void)setData:(NSArray * )entries index:(int)index {
	self.data = entries;
	self.selectedIndex = index;
	[self.collectionView reloadData];
	if ([self.data count] > 0) {
		[self.listdelegate itemSelected:[self.data objectAtIndex:index]];
	}
	/*
	 *
	 * int count = [entries count];
	 * float maxHeight = 0;
	 * for (int i = 0 ; i < count; i ++){
	 *
	 *
	 *  NSDictionary * entry = [entries objectAtIndex:i];
	 *
	 *  AssetView * assetView = [[[AssetView alloc] initWithStyle: ASSET_TYPE_LEFT_LIST data: entry] autorelease];
	 *  assetView.frame = CGRectMake(0, i * assetView.frame.size.height, assetView.frame.size.width, assetView.frame.size.height);
	 *  assetView.tag = i;
	 *
	 *  if (i==index){
	 *      [assetView setBackColor: [UIColor colorWithRed:11.0/255.0 green:52.0/255.0 blue:105.0/255.0 alpha:0.5]];
	 *  }else{
	 *      [assetView setBackColor: [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
	 *  }
	 *  assetView.delegate = self;
	 *
	 *  ///[assetView addTarget:self action:@selector(itemSelected:) forControlEvents:UIControlEventTouchUpInside];
	 *
	 *  [self addSubview:assetView];
	 *
	 *
	 *  maxHeight = assetView.frame.origin.y+assetView.frame.size.height;
	 *
	 *
	 *
	 *
	 * }
	 * self.contentSize = CGSizeMake(self.frame.size.width, maxHeight);
	 * if (self.data){
	 *  [self.listdelegate itemSelected:[self.data objectAtIndex:index]];
	 * }
	 *
	 */
}

/*
 * // Only override drawRect: if you perform custom drawing.
 * // An empty implementation adversely affects performance during animation.
 * - (void)drawRect:(CGRect)rect
 * {
 * // Drawing code
 * }
 */

@end
