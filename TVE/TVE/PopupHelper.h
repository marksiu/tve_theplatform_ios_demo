//
//  PopupHelper.h
//  TVE
//
//  Created by Mate Beres on 12/12/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RatingViewController.h"
@interface PopupHelper : NSObject
+ (void)showSharePopupInView:(UIView *)view atButton:(UIButton *)button orientedToTop:(BOOL) orientation asset:(NSString *)title;
+ (void)showRatePopupInView:(UIView *)view atButton:(UIButton *)button delegate:(id <RatingViewControllerDelegate>)delegate;
+ (void)showFriendsPopupInView:(UIView *)view atButton:(UIButton *)button orientedToTop:(BOOL)orientation;
+ (void)showFilterPopupInView:(UIView *)view atButton:(UIButton *)button withData:(NSArray *)data delegate:(id)delegate;
+ (void)showProgramInfo:(NSDictionary *)program viewController:(UIViewController *)vc;
+ (void)showSubscribeAlert:(UIViewController *)vc;
+ (void)showSubscribePinPopupForItem:(NSDictionary * )item delegate:(id)delegate;
+ (void)showRentalPinPopupForItem:(NSDictionary * )item delegate:(id)delegate;
+ (void)showSortPopupInView:(UIView *)view atButton:(UIButton *)button withData:(NSArray *)data delegate:(id)delegate;
@end
