//
//  SortViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 1/1/13.
//  Copyright (c) 2013 Accedo Broadband. All rights reserved.
//

#import "SortViewController.h"

@interface SortViewController ()

@end

@implementation SortViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

	if (!cell) {
		cell = [[[UITableViewCell alloc] init] autorelease];
		cell.restorationIdentifier = CellIdentifier;
		cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
	}
	NSDictionary *secData = [self.data objectAtIndex:indexPath.section];
	NSArray *rowDataArray = [secData objectForKey:@"data"];
	// NSString * title = [secData objectForKey:@"title"];
	NSString *rowData = [rowDataArray objectAtIndex:indexPath.row];



	// ThePlatform * platform = [ThePlatform sharedInstance];
	// NSDictionary * config = platform.config;
	// NSDictionary * cfg = [config objectForKey:@"config"];

	// NSString * catBaseUrl = [cfg objectForKey:@"categoryBaseUrl"];
	// NSLog(@"%@", rowData);


	// NSString * moviescathegoryId = [catBaseUrl stringByAppendingString:[cfg objectForKey:@"movieCatId"]];

	// NSString * seriescathegoryId = [catBaseUrl stringByAppendingString:[cfg objectForKey:@"tvSeriesCatId"]];

	// if ([[rowData objectForKey:@"id"] isEqualToString:moviescathegoryId] || [[rowData objectForKey:@"id"] isEqualToString:seriescathegoryId]){
	cell.textLabel.text = rowData;
	// [rowData objectAtIndex:indexPath.row];//@"All";

	// }else{
	//    cell.textLabel.text = [rowData objectForKey:@"title"];
	// }


	// cell.textLabel.text =[rowData objectForKey:@"plcategory$fullTitle"];

	return cell;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
