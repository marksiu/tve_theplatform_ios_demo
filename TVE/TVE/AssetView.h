//
//  AssetView.h
//  TVE
//
//  Created by Gabor Bottyan on 12/4/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>
const static int ASSET_TYPE_HOME_LIST = 0;
const static int ASSET_TYPE_LEFT_LIST = 1;
const static int ASSET_TYPE_ON_DEMAND = 2;
const static int ASSET_TYPE_RELATED_GRID = 3;
const static int ASSET_TYPE_EPISODE = 4;
const static int ASSET_TYPE_ON_DEMAND_NEW = 5;
const static int ASSET_TYPE_HOME_LIST_MORE = 6;
const static int ASSET_TYPE_LEFT_LIST_SMALL = 7;
@protocol AsetViewDelegate <NSObject>

- (void)asetTapped:(id)sender;

@end


@interface AssetView : UIView {
}
@property (assign) id <AsetViewDelegate> delegate;
- (id)initWithStyle:(int)style data:(NSDictionary *)entry;
- (id)initWithStyleWithMyRental:(int)style data:(NSDictionary *)entry;
- (void)setBackColor:(UIColor *)color;
+ (CGSize)sizeOfStyle:(int)style;
@end
