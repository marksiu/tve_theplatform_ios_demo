//
//  FilterViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 12/20/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "FilterViewController.h"
#import "ThePlatform.h"
@interface FilterViewController ()
@property (assign, nonatomic) UIPopoverController *parentPopoverController;
@end

@implementation FilterViewController

- (id)initWithData:(NSArray * )__data delegate:(id <FilterViewControllerDelegate>)__delegate {
	self = [super initWithStyle:UITableViewStylePlain];
	if (self) {
		self.data = __data;
		self.delegate = __delegate;
	}
	return self;
}

- (void)setPopover:(UIPopoverController *)popoverController {
	self.parentPopoverController = popoverController;
}

- (void)viewDidLoad {
	[super viewDidLoad];

	// Uncomment the following line to preserve selection between presentations.
	// self.clearsSelectionOnViewWillAppear = NO;

	// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
	// self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return [self.data count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	NSDictionary *secData = [self.data objectAtIndex:section];
	NSArray *rowData = [secData objectForKey:@"data"];

	return [rowData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

	if (!cell) {
		cell = [[[UITableViewCell alloc] init] autorelease];
		cell.restorationIdentifier = CellIdentifier;
		cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
	}
	NSDictionary *secData = [self.data objectAtIndex:indexPath.section];
	NSArray *rowDataArray = [secData objectForKey:@"data"];
	// NSString * title = [secData objectForKey:@"title"];
	NSDictionary *rowData = [rowDataArray objectAtIndex:indexPath.row];



	ThePlatform *platform = [ThePlatform sharedInstance];
	NSDictionary *config = platform.config;
	NSDictionary *cfg = [config objectForKey:@"config"];

	NSString *catBaseUrl = [cfg objectForKey:@"categoryBaseUrl"];



	NSString *moviescathegoryId = [catBaseUrl stringByAppendingString:[cfg objectForKey:@"movieCatId"]];

	NSString *seriescathegoryId = [catBaseUrl stringByAppendingString:[cfg objectForKey:@"tvSeriesCatId"]];

	if ([[rowData objectForKey:@"id"] isEqualToString:moviescathegoryId] || [[rowData objectForKey:@"id"] isEqualToString:seriescathegoryId]) {
		cell.textLabel.text = @"All";
	}
	else {
		cell.textLabel.text = [rowData objectForKey:@"title"];
	}

	// cell.textLabel.text =[rowData objectForKey:@"plcategory$fullTitle"];

	return cell;
}

/*
 * // Override to support conditional editing of the table view.
 * - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 * {
 *  // Return NO if you do not want the specified item to be editable.
 *  return YES;
 * }
 */

/*
 * // Override to support editing the table view.
 * - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 * {
 *  if (editingStyle == UITableViewCellEditingStyleDelete) {
 *      // Delete the row from the data source
 *      [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 *  }
 *  else if (editingStyle == UITableViewCellEditingStyleInsert) {
 *      // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 *  }
 * }
 */

/*
 * // Override to support rearranging the table view.
 * - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 * {
 * }
 */

/*
 * // Override to support conditional rearranging of the table view.
 * - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 * {
 *  // Return NO if you do not want the item to be re-orderable.
 *  return YES;
 * }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (self.delegate) {
		NSDictionary *secData = [self.data objectAtIndex:indexPath.section];
		NSArray *rowDataArray = [secData objectForKey:@"data"];
		// NSString * title = [secData objectForKey:@"title"];
		NSDictionary *rowData = [rowDataArray objectAtIndex:indexPath.row];
		[self.delegate filterChanged:rowData];
		[self.parentPopoverController dismissPopoverAnimated:YES];
	}
}

@end
