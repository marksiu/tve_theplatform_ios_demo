//
//  MaintenanceViewController.m
//  TVE
//
//  Created by Zoltan Gobolos on 12/31/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "MaintenanceViewController.h"

@interface MaintenanceViewController ()
@property (retain, nonatomic) IBOutlet UILabel *messageLabel;
@end

@implementation MaintenanceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view from its nib.

	if (0 < [self.message length]) {
		self.messageLabel.text = self.message;
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
