//
//  LoginViewController.m
//  TVE
//
//  Created by Mate Beres on 12/10/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "LoginViewController.h"
#import "AppView.h"
#import <QuartzCore/QuartzCore.h>

#import "Constants.h"

#import <Security/Security.h>
#import "KeychainItemWrapper.h"

#import "TVEConfig.h"

@interface LoginViewController ()
@property (retain) UITextField *userTextField;
@property (retain) UITextField *passwordTextField;

// TVE thePlatform
@property (retain) NSMutableData *responseData;
- (void)saveEndUserTokenToKeyChain:(NSString *)token userId:(NSString *)userId paymentInstrument:(NSString *)paymentInst;

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	self.title = @"Login to your account";
	if (self.showCloseButton) {
		self.navigationItem.rightBarButtonItem  = [[[UIBarButtonItem alloc] initWithTitle:@"Close"
																					style:UIBarButtonItemStylePlain
																				   target:self
																				   action:@selector(close:)]
												   autorelease];
	}
	[self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:14 / 255.0
																		  green:25 / 255.0
																		   blue:49 / 255.0
																		  alpha:1.0]];

	self.loginButton.frame = CGRectMake(30, self.loginButton.frame.origin.y, 482, 42);
	[self.loginButton setBackgroundImage:[UIImage imageNamed:@"large_login_button.png"] forState:UIControlStateNormal];
	[self.loginButton setTintColor:[UIColor colorWithRed:14 / 255.0 green:25 / 255.0 blue:49 / 255.0 alpha:1.0]];
	[self.loginButton addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];

//	_indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//	_indicator.frame = CGRectMake((self.view.frame.size.width / 2) - 12, (self.view.frame.size.height / 2) - 28, 25, 25);
//	_indicator.tag  = 1;
//	[self.view addSubview:_indicator];
//	[_indicator setHidden:YES];
}

/*
 * -(void) viewDidAppear:(BOOL)animated {
 *  [super viewDidAppear:animated];
 *
 *  [self.userTextField becomeFirstResponder];
 * }
 */

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	if (self.delegate) {
		[self.delegate textFieldDidBeginEditing];
	}
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	if (self.delegate) {
		[self.delegate textFieldDidEndEditing];
	}
}

- (void)showLoginFailure {
	UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Login Failure" message:@"Incorrect Username or Password!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];

	[alertview show];
}

// Editable for the login pop up box
- (void)setLoginInputFieldEditable:(BOOL)enable {
	[self.userTextField setEnabled:enable];
	[self.passwordTextField setEnabled:enable];
}

- (void)login:(id)sender {
	[self setLoginInputFieldEditable:NO];

	NSString *username = [self.userTextField.text lowercaseString];
	NSString *password = self.passwordTextField.text;

	if (username.length == 0 || password.length == 0) {
		[self showLoginFailure];
		[self setLoginInputFieldEditable:YES];
		return;
	}

	// TVE thePlatform admin account
	//	NSString *urlStr = [NSString stringWithFormat:@"https://euid.theplatform.com/idm/web/Authentication/signIn?schema=1.0&form=json&pretty=true&username=IJHRpd4czHsVFdob/%@&password=%@", self.userTextField.text, self.passwordTextField.text];
	//	//	NSTimeInterval *reqTime = 20;
	//	NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:urlStr]];
	//	NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];

	//	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://euid.theplatform.com/idm/web/Authentication/signIn?schema=1.0&form=json&pretty=true&username=IJHRpd4czHsVFdob/%@&password=%@", self.userTextField.text, self.passwordTextField.text]]];


	// end user login
//	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://euid.theplatform.com/idm/web/Authentication/signIn?schema=1.0&form=json&pretty=true&username=IJHRpd4czHsVFdob/testuser&password=testuser"]];


	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://euid.theplatform.com/idm/web/Authentication/signIn?schema=1.0&form=json&pretty=true&username=IJHRpd4czHsVFdob/%@&password=%@", username, password]]];
	NSURLResponse *response = nil;
	NSError *error = nil;
	NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest
										 returningResponse:&response
													 error:&error];

	if (error == nil) {
		// Parse data here
		NSLog(@"you get the data");

		self.responseData = [[NSMutableData alloc] init];
		[self.responseData appendData:data];

		// end user login json handle
		NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableContainers error:&error];
		NSDictionary *loginDataArr = [jsonObject objectForKey:@"signInResponse"];
		NSString *endUserToken = [loginDataArr objectForKey:@"token"];
		NSString *userId = [loginDataArr objectForKey:@"userId"];
		NSLog(@"end user token: %@", endUserToken);
		NSLog(@"end user userId: %@", userId);

		if (endUserToken.length != 0 && endUserToken != nil && userId.length != 0 && userId != nil) {
			// retrieve payment instrument
			NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://storefront.commerce.theplatform.com/storefront/data/PaymentInstrumentInfo?token=%@&pretty=true&form=json&schema=1.0&account=http://access.auth.theplatform.com/data/Account/2301538417", endUserToken]]];
			response = nil;
			error = nil;
			data = nil;
			data = [NSURLConnection sendSynchronousRequest:request
										 returningResponse:&response
													 error:&error];

			if (error == nil) {
				[self.responseData setLength:0];
				[self.responseData appendData:data];

				// end user payment instrument json handle
				NSDictionary *jsonObject = nil;
				jsonObject = [NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableLeaves error:&error];
				NSArray *entryArr = [jsonObject objectForKey:@"entries"];
				NSDictionary *entry = [entryArr objectAtIndex:0];
				NSString *paymentInst = [entry objectForKey:@"id"];
				NSLog(@"payment instrument: %@", paymentInst);

				// save end user ID and token to keychain
				[self saveEndUserTokenToKeyChain:endUserToken userId:userId paymentInstrument:paymentInst];
                
                // retrieve entitlement record to local by TVEConfig
                TVEConfig* config = [TVEConfig sharedConfig];
                [config downloadRentalRecord];

				// generate the md5
				[[AppView sharedInstance] updateLoginHome:username];
				[self close:nil];
			}
		}
		else {
			[self showLoginFailure];
			[self setLoginInputFieldEditable:YES];

			return;
		}
	}


	//	if (_endUserLoginValid) {
	//		NSLog(@"self.userTextField.text: %@", self.userTextField.text);
	//		NSLog(@"self.passwordTextField.text: %@", self.passwordTextField.text);
	//
	//		if ([[AppView sharedInstance] login:self.userTextField.text password:self.passwordTextField.text]) {
	//			//	if (_endUserLoginValid) {
	//			[self close:Nil];
	//		}
	//	}
	//
	//    if ([[AppView sharedInstance] login:self.userTextField.text password:self.passwordTextField.text]) {
	//        [self close:Nil];
	//    }


	else {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 // end-user login failure
		CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];

		[animation setDuration:0.05];
		[animation setRepeatCount:4];
		[animation setAutoreverses:YES];

		float deltax = 0;
		float deltay = 5;

		UIView *shakeView = Nil;

		if (self.isModal) {
			shakeView = self.navigationController.view.superview;
			[animation setFromValue:[NSValue valueWithCGPoint:
									 CGPointMake([shakeView center].x - deltax, [shakeView center].y + deltax)]];
			[animation setToValue:[NSValue valueWithCGPoint:
								   CGPointMake([shakeView center].x + deltax, [shakeView center].y + deltay)]];
		}
		else {
			shakeView = self.navigationController.view;
			[animation setFromValue:[NSValue valueWithCGPoint:
									 CGPointMake([shakeView center].x - deltax, [shakeView center].y - deltay)]];
			[animation setToValue:[NSValue valueWithCGPoint:
								   CGPointMake([shakeView center].x + deltax, [shakeView center].y + deltay)]];
		}
		[[shakeView layer] addAnimation:animation forKey:@"position"];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 50.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	UIView *v = [[[UIView alloc] initWithFrame:CGRectMake(0, 20, 500, 30)] autorelease];

	[v setBackgroundColor:[UIColor clearColor]];

	UILabel *l = [[[UILabel alloc] initWithFrame:CGRectMake(35, 20, 500, 30)] autorelease];
	[l setBackgroundColor:[UIColor clearColor]];
	[l setTextColor:[UIColor blackColor]];
	[l setText:@"Enter a username and password:"];
	[v addSubview:l];

	return v;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView {
	return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	return nil;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
	return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView_ cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"ULoginCell";
	UITableViewCell *cell = (UITableViewCell *)[tableView_ dequeueReusableCellWithIdentifier:CellIdentifier];

	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] autorelease];
	}

	[cell setSelectionStyle:UITableViewCellSelectionStyleNone];

	CGRect cellr = cell.frame;

	cellr.origin.x   += 130;
	cellr.size.width += 20;

	NSUInteger r = [indexPath row];

	if (r == 0) {
		cell.textLabel.text = @"Username";

		if (!self.userTextField) {
			self.userTextField  = [[[UITextField alloc] initWithFrame:cellr] autorelease];

			self.userTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
			self.userTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
			[self.userTextField setAutocapitalizationType:UITextAutocapitalizationTypeWords];
			[self.userTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
			self.userTextField.delegate = self;
			// self.userTextField.backgroundColor = [UIColor redColor];
		}

		[cell.contentView addSubview:self.userTextField];
		[cell setAccessoryType:UITableViewCellAccessoryNone];
	}
	else if (r == 1) {
		cell.textLabel.text = @"Password";

		if (!self.passwordTextField) {
			self.passwordTextField = [[[UITextField alloc] initWithFrame:cellr] autorelease];

			self.passwordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
			self.passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
			self.passwordTextField.secureTextEntry = YES;
			[self.passwordTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
			[self.passwordTextField setAutocorrectionType:UITextAutocorrectionTypeNo];
			self.passwordTextField.delegate = self;
			// self.passwordTextField.backgroundColor = [UIColor yellowColor];
		}

		[cell.contentView addSubview:self.passwordTextField];
		[cell setAccessoryType:UITableViewCellAccessoryNone];
	}

	return cell;
}

- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section {
	return 2;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	if (indexPath.row == 0) {
		//  [self.userTextField becomeFirstResponder];
	}
	else if (indexPath.row == 1) {
		// [self.passwordTextField becomeFirstResponder];
	}
	else if (indexPath.row == 2) {
		/*   OperatorPanelViewController* op =  [[OperatorPanelViewController alloc] initWithNibName:@"OperatorPanelViewController"
		 * bundle:nil];
		 * op.delegate = self;
		 * op.lastOperator = self.operator;
		 * [self.navigationController pushViewController:op animated:YES];
		 * [op release];*/
	}
}

- (void)close:(id)sender { // close the popup windows
	if (self.isModal) {
		[CURRENT_WINDOW.rootViewController dismissViewControllerAnimated:YES completion:^{
			 if ([AppView sharedInstance].fingerprint) {
				 [[NSNotificationCenter defaultCenter] postNotificationName:@"APP_LOGIN" object:Nil];
			 }
		 }];
	}
	else {
		[self.navigationController.view removeFromSuperview];
	}
}

- (void)dealloc {
	[_loginButton release];
	[super dealloc];
}

// #pragma mark - NSURLConnection
// - (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
//	NSLog(@"end user request make");
//	self.responseData = [[NSMutableData alloc] init];
// }
//
// - (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
//	[self.responseData appendData:data];
// }
//
// - (void)connectionDidFinishLoading:(NSURLConnection *)connection {
//	NSError *error = nil;
//
//	//    NSDictionary *dictionary = [[NSDictionary alloc] init];
//
//	if (self.responseData == nil) {
//		NSLog(@"responseData is nil");
//		return;
//	}
//
//	NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableContainers error:&error];
//
//	NSDictionary *loginDataArr = [jsonObject objectForKey:@"signInResponse"];
//	NSString *adminToken = [loginDataArr objectForKey:@"token"];
//	NSLog(@"end user token: %@", adminToken);
//
//	[self saveEndUserTokenToKeyChain:adminToken];
// }
//
// - (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
//	NSLog(@"end user Token Failure");
// }
//
// - (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
//	// Return nil to indicate not necessary to store a cached response for this connection
//	return nil;
// }

#pragma mark - keychain
- (void)saveEndUserTokenToKeyChain:(NSString *)token userId:(NSString *)userId paymentInstrument:(NSString *)paymentInst {
	// retrieve admin token
	KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"login" accessGroup:nil];
	NSString *keychainValueBeforeEndUserLogin = [keychainItem objectForKey:(id)kSecValueData];
	NSString *keySeparator = @"#*#";
	NSArray *keychainArrBeforeEndUserLogin = [keychainValueBeforeEndUserLogin componentsSeparatedByString:keySeparator];
	NSString *adminToken = [keychainArrBeforeEndUserLogin objectAtIndex:0];

	NSLog(@"LoginViewController.m keychain for admin only: %@", adminToken);

	// reset keychain
	[keychainItem resetKeychainItem];

	// form string with end user data
	NSString *keychainWithEndUser = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", adminToken, keySeparator, token, keySeparator, userId, keySeparator, paymentInst];

	// set keychain value
	[keychainItem setObject:keychainWithEndUser forKey:(id)kSecValueData];

	// check the value
	NSString *checkKeychainValue = [keychainItem objectForKey:(id)kSecValueData];
	NSLog(@"LoginViewController.m keychain: %@", checkKeychainValue);
}

@end
