//
//  BaseViewController.h
//  TVE
//
//  Created by Gabor Bottyan on 11/28/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailOpenerDelegate.h"
#import "SearchViewController.h"

#import "UIViewController+TVEMoviePlayer.h"

@interface BaseViewController : UIViewController <DetailOpenerDelegate, SearchViewControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate>{
}

@property (retain, nonatomic) CMOREPlayerViewController *cmorePlayer;
@property (nonatomic, retain) NSTimer *remoteTimeoutTimer;
@property (retain) UIPopoverController *acRemotePopup;
@property (retain) ACRemoteControlPopup *acRemoteController;
@property (retain, nonatomic) ACPairingViewController *acPairingPopup;

- (void)fetchData;
- (void)startVideoIfPossible;
- (void)openSearch;
- (void)openSettings;

- (void)customNavigationBar;
- (void)customNavigationBar:(UIViewController *)vc;

- (void)shareTwitterPopup:(NSString *)text;
- (void)shareFacebookPopup:(NSString *)assettitle;
- (void)openLogin:(id)sender;
- (void)doSearch:(NSString *)query;
- (UIViewController *)getLogin:(id)delegate;
- (void)openDetailOndemand:(NSDictionary *)data withCollection:(NSArray *)collection title:(NSString *)title;
- (void)openLiveDetail:(NSDictionary *)data withCollection:(NSArray *)collection title:(NSString *)title;
- (void)playEmbedded:(NSDictionary *)content inView:(UIView *)container;

- (void)startSpinner;
- (void)stopSpinner;



@end
