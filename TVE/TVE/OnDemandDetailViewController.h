//
//  OnDemandDetailViewController.h
//  TVE
//
//  Created by Gabor Bottyan on 12/4/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "BaseViewController.h"
#import "DetailsListView.h"
@interface OnDemandDetailViewController : BaseViewController <DetailsListViewDelegate>{
}
- (id)initWithFeed:(NSDictionary *)desc;
@end
