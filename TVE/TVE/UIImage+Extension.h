//
//  UIImage+Extension.h
//  TVE
//
//  Created by Zoltan Gobolos on 12/20/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extension)
+ (UIImage *)externalImageNamed:(NSString *)name;
@end
