//
//  SelectedLiveTVViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 1/2/13.
//  Copyright (c) 2013 Accedo Broadband. All rights reserved.
//

#import "SelectedLiveTVViewController.h"
#import "Constants.h"
@interface SelectedLiveTVViewController ()
@property (retain) NSArray *collection;
@property (retain) NSDictionary *data;


@end

@implementation SelectedLiveTVViewController


- (id)initWithData:(NSDictionary *)__data withCollection:(NSArray *)col title:(NSString *)title {
	self = [super init];
	if (self) {
		self.data = __data;
		self.title = title;
		self.collection = col;
	}
	return self;
}

- (void)fetchData {
	// NSArray *entries = [[AppView sharedInstance] searchChannels: self.searchPhrase];
	// if ([entries count]) {
	[self.listView setData:self.collection index:0];
	// }
}

- (void)viewWillAppear:(BOOL)animated {
	// NSLog(@"%@",[self.view.subviews objectAtIndex:0]);



	self.navigationController.navigationBarHidden = YES;

	self.leftViewController.navigationController.view.frame = CGRectMake(0, 0, detailLeftSize, self.leftViewController.navigationController.view.frame.size.height);
	self.rightViewController.navigationController.view.frame = CGRectMake(320, 0, 1024 - 320, self.rightViewController.navigationController.view.frame.size.height);


	UINavigationBar *navBar = [self.rightViewController.navigationController navigationBar];
	[navBar setTintColor:[UIColor blackColor]];



	if ([navBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
		[navBar setBackgroundImage:[UIImage imageNamed:@"top.png"] forBarMetrics:UIBarMetricsDefault];
	}
	else {
		UIImageView *imageView = (UIImageView *)[navBar viewWithTag:12000];
		if (imageView == nil) {
			imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top.png"]];
			[imageView setTag:12000];
			[navBar insertSubview:imageView atIndex:0];
			[imageView release];
		}
	}


	navBar = [self.leftViewController.navigationController navigationBar];
	[navBar setTintColor:[UIColor blackColor]];



	if ([navBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
		[navBar setBackgroundImage:[UIImage imageNamed:@"top.png"] forBarMetrics:UIBarMetricsDefault];
	}
	else {
		UIImageView *imageView = (UIImageView *)[navBar viewWithTag:12000];
		if (imageView == nil) {
			imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top.png"]];
			[imageView setTag:12000];
			[navBar insertSubview:imageView atIndex:0];
			[imageView release];
		}
	}

	if (self.title) {
		self.leftViewController.navigationItem.title = self.title;
	}


	UIButton *settingsButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[settingsButton addTarget:self action:@selector(openSettings) forControlEvents:UIControlEventTouchUpInside];
	[settingsButton setBackgroundImage:[UIImage imageNamed:@"setting.png"] forState:UIControlStateNormal];
	settingsButton.frame = CGRectMake(0, 0, 23, 23);
	UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
	[searchButton addTarget:self action:@selector(openSearch) forControlEvents:UIControlEventTouchUpInside];
	[searchButton setBackgroundImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
	searchButton.frame = CGRectMake(53, 2, 21, 21);


	UIView *parentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 90, 23)];
	[parentView addSubview:settingsButton];
	[parentView addSubview:searchButton];
	parentView.backgroundColor = [UIColor clearColor];
	UIBarButtonItem *customBarButtomItem = [[UIBarButtonItem alloc] initWithCustomView:parentView];
	[parentView release];
	[self.rightViewController.navigationItem setRightBarButtonItem:customBarButtomItem animated:YES];
	[customBarButtomItem release];

	self.leftViewController.navigationItem.title = @"Channels";
	self.rightViewController.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage externalImageNamed:@"logo"]];
	// self.leftViewController.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Categories" UIBarButtonItemStylePlain target:self action:@selector(showCategories:)]autorelease];

	self.rightViewController.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Home Channel" style:UIBarButtonItemStylePlain target:self action:@selector(homeChannel:)] autorelease];


	// [[self.view.subviews objectAtIndex:0] setFrame:CGRectMake(0, 0, 1024, 768)];
	self.leftViewController.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(goback:)] autorelease];
}

- (void)goback:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
