//
//  OnDemandViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 11/26/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "OnDemandViewController.h"
#import "OnDemandDetailView.h"
#import "DetailsListView.h"
#import "Constants.h"
#import "SBJson.h"
#import "ThePlatform.h"
#import "AssetView.h"
#import "PopupHelper.h"
#import "AppView.h"
#import "UIImage+Extension.h"

// thePlatform
#import <Security/Security.h>
#import "KeychainItemWrapper.h"
#import "TVEConfig.h"

@interface OnDemandViewController ()

@property (retain) NSDictionary *feed;
@property (retain) NSDictionary *filtedData;

@end

@implementation OnDemandViewController

- (id)initWithFeed:(NSDictionary *)desc {
	self = [super init];
	if (self) {
		self.feed = desc;
		// Custom initialization
		self.top = 0;
	}
	return self;
}

- (BOOL)checkLogin {
	KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"login" accessGroup:nil];
	NSString *keyChainData = [keychainItem objectForKey:(id)kSecValueData];
	NSArray *keyChainArr = [keyChainData componentsSeparatedByString:@"#*#"];

	NSString *endUserToken = [keyChainArr objectAtIndex:1];
	NSString *endUserId = [keyChainArr objectAtIndex:2];
	NSString *endUserPaymentInst = [keyChainArr objectAtIndex:3];

	NSString *nullStr = @"NULL";

	if ([endUserToken isEqualToString:nullStr] || [endUserId isEqualToString:nullStr] || [endUserPaymentInst isEqualToString:nullStr]) {
		return NO;
	}

	return YES;
}

- (NSArray *)getKeyChainArr {
	KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"login" accessGroup:nil];
	NSString *keyChainData = [keychainItem objectForKey:(id)kSecValueData];
	NSArray *keyChainArr = [keyChainData componentsSeparatedByString:@"#*#"];

	return keyChainArr;
}

- (NSString *)getAdminToken {
	NSArray *keyChainArr = [self getKeyChainArr];
	NSString *adminToken = [keyChainArr objectAtIndex:0];

	NSLog(@"Admin Token: %@", adminToken);
	return adminToken;
}

- (NSString *)getEndUserID {
	NSArray *keyChainArr = [self getKeyChainArr];
	NSString *endUserId = [keyChainArr objectAtIndex:2];

	NSLog(@"End User ID: %@", endUserId);
	return endUserId;
}

// copy from display:entries
// customize for the thePlatform demo
- (void)displayMyRental {
	BOOL emptyRentalRecord = NO;

	// check number of rental history of thePlatform API
	double now = [[NSDate date] timeIntervalSince1970] * 1000;
	NSString *nowStr = [NSString stringWithFormat:@"%.0f", now];
	NSString *adminToken = @"";
	NSString *endUserId = @"";

	// check rental record
	NSMutableArray *rentalRecordArr = [[NSMutableArray alloc] init];

	// dict for rental record
	NSMutableDictionary *rentalRecordDict = [[NSMutableDictionary alloc] init];

	if ([self checkLogin]) {
		NSLog(@"checkLogin: pass");
		adminToken = [self getAdminToken];
		endUserId = [self getEndUserID];

		NSURLRequest *url = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://data.entitlement.theplatform.com/eds/data/Entitlement?schema=1.1&form=json&pretty=true&token=%@&byUserId=%@&byInWindow=%@&fields=ent$scopeId", adminToken, endUserId, nowStr]]];
		NSURLResponse *response = nil;
		NSError *error = nil;
		NSData *data = [NSURLConnection sendSynchronousRequest:url
											 returningResponse:&response
														 error:&error ];

		if (error == nil) {
			NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

			int mediaCount = [[jsonObject objectForKey:@"entryCount"] intValue];

			if (mediaCount != 0) {
				// have rental record
				NSArray *entryArr = [jsonObject objectForKey:@"entries"];
				// retrieve rental record one by one

//				NSMutableDictionary *rentalRecord = [[NSMutableDictionary alloc] init];

				for (int i = 0; i < mediaCount; i++) {
					NSDictionary *entry = [entryArr objectAtIndex:i];
					NSString *mediaID = [entry objectForKey:@"ent$scopeId"];
					NSArray *mediaIDArr = [mediaID componentsSeparatedByString:@"/Media/"];
					NSString *mediaIDNum = [mediaIDArr objectAtIndex:1];
//					[rentalRecord setValue:mediaIDNum forKey:key];
					[rentalRecordArr insertObject:mediaIDNum atIndex:i];
				}
			}
			else {
				emptyRentalRecord = YES;
			}
		}
	}
	else {
		emptyRentalRecord = YES;
	}

//	if (emptyRentalRecord) {
//		// stop the thePlatform API call
//		[self display:entries];
//		return;
//	}else {
	if (!emptyRentalRecord) {
		// retrieve the rental record from thePlatform API

		// form rental record string

		NSString *rentalRecordStr = [rentalRecordArr componentsJoinedByString:@"|"];
		NSString *urlStr = [NSString stringWithFormat:@"http://feed.theplatform.com/f/xBrLJC/DwE21wX1pGqZ?form=json&schema=1.2&byId=%@&pretty=true", rentalRecordStr];
		NSURLRequest *url = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
		NSURLResponse *response = nil;
		NSError *error = nil;
		NSData *data = [NSURLConnection sendSynchronousRequest:url
											 returningResponse:&response
														 error:&error ];

		if (error == nil) {
			NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

			int mediaCount = [[jsonObject objectForKey:@"entryCount"] intValue];

			if (mediaCount != 0) {
				// have rental record
				NSArray *entryArr = [jsonObject objectForKey:@"entries"];
				// retrieve rental record one by one

				NSString *title;
				NSString *image;
				NSString *friendShare;
				NSString *starRating;

				NSString *titleKey;
				NSString *imageKey;
				NSString *friendShareKey;
				NSString *starRatingKey;

				for (int i = 0; i < mediaCount; i++) {
					NSDictionary *entry = [entryArr objectAtIndex:i];

					// key
					titleKey = [NSString stringWithFormat:@"%dtitle", i];
					imageKey = [NSString stringWithFormat:@"%dimage", i];
					friendShareKey = [NSString stringWithFormat:@"%dfriendShare", i];
					starRatingKey = [NSString stringWithFormat:@"%dstarRating", i];

//					// value checking
//					title = [NSString stringWithFormat:@"%@%@", [entry objectForKey:@"title"], @"yaya"];
//					image = [NSString stringWithFormat:@"%@%@", [entry objectForKey:@"plmedia$defaultThumbnailUrl"], @""];
//					friendShare = [NSString stringWithFormat:@"%@%@", [NSString stringWithFormat:@"%d", [[entry objectForKey:@"pl1$friendsShared"] integerValue]], @"yaya"];
//					starRating = [NSString stringWithFormat:@"%@%@", [NSString stringWithFormat:@"%d", [[entry objectForKey:@"pl1$starRating"] integerValue]], @"yaya"];

					// value
					title = [entry objectForKey:@"title"];
					image = [entry objectForKey:@"plmedia$defaultThumbnailUrl"];
					friendShare = [NSString stringWithFormat:@"%d", [[entry objectForKey:@"pl1$friendsShared"] integerValue]];
					starRating = [NSString stringWithFormat:@"%d", [[entry objectForKey:@"pl1$starRating"] integerValue]];

					// put the value to dict
					[rentalRecordDict addEntriesFromDictionary:@ { titleKey:title, imageKey:image, friendShareKey:friendShare, starRatingKey:starRating }];

					NSLog(@"Get Rental Info for My Rental");
				}
			}
			else {
				emptyRentalRecord = YES;
			}
		}
	}



	for (UIView *v in [[[self.scrollView subviews] copy] autorelease]) {
		[v removeFromSuperview];
	}

	[self showFilters];
	float maxHeight = 0;
	float leftIndent = 50;

	CGPoint cursor = CGPointMake(leftIndent, self.top);
	int tagCounter = 0;

	int numberOfRentalRecord = [rentalRecordDict count];
	numberOfRentalRecord = numberOfRentalRecord / 4;

	for (int i = 0; i < numberOfRentalRecord; i++) {
		NSMutableDictionary *rentalRecord = [[NSMutableDictionary alloc] init];

		// key
		NSString *titleKey = [NSString stringWithFormat:@"%dtitle", i];
		NSString *imageKey = [NSString stringWithFormat:@"%dimage", i];
		NSString *friendShareKey = [NSString stringWithFormat:@"%dfriendShare", i];
		NSString *starRatingKey = [NSString stringWithFormat:@"%dstarRating", i];

		[rentalRecord addEntriesFromDictionary:@ { @"title":[rentalRecordDict objectForKey:titleKey], @"image":[rentalRecordDict objectForKey:imageKey], @"friendShare":[rentalRecordDict objectForKey:friendShareKey], @"starRating":[rentalRecordDict objectForKey:starRatingKey] }];

//		NSDictionary *entry = [entries objectAtIndex:i];

		AssetView *assetView = [[[AssetView alloc] initWithStyleWithMyRental:ASSET_TYPE_ON_DEMAND_NEW data:rentalRecord] autorelease];

		assetView.frame = CGRectMake(cursor.x, cursor.y, assetView.frame.size.width, assetView.frame.size.height);

		assetView.tag = tagCounter;
		tagCounter++;

		assetView.delegate = self;

		// [assetView addTarget:self action:@selector(itemSelected:) forControlEvents:UIControlEventTouchUpInside];

		[self.scrollView addSubview:assetView];
		maxHeight = assetView.frame.origin.y + assetView.frame.size.height;
		cursor.x = cursor.x + assetView.frame.size.width;

		if (cursor.x > assetView.frame.size.width * 2) {
			cursor.x = leftIndent;
			cursor.y = cursor.y + assetView.frame.size.height - 2;
		}
	}

	self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, maxHeight + 10);
}

// use the local rental record and old display method to show the rental record
- (void)displayMyRentalFromLocal {
	NSArray *rentalRecordArr = [[NSArray alloc] init];

	if ([self checkLogin]) {
		NSLog(@"checkLogin: pass");

		TVEConfig *config = [TVEConfig sharedConfig];
		NSDictionary *rentalRecordDict = [config getRentalMovieDataFromMediaFeed];
		rentalRecordArr = [rentalRecordDict objectForKey:@"entries"];
	}

	for (UIView *v in [[[self.scrollView subviews] copy] autorelease]) {
		[v removeFromSuperview];
	}

	[self showFilters];
	float maxHeight = 0;
	float leftIndent = 50;

	CGPoint cursor = CGPointMake(leftIndent, self.top);
	int tagCounter = 0;

	for (int i = 0; i < [rentalRecordArr count]; i++) {
		NSDictionary *entry = [rentalRecordArr objectAtIndex:i];

		AssetView *assetView = [[[AssetView alloc] initWithStyle:ASSET_TYPE_ON_DEMAND_NEW data:entry] autorelease];

		assetView.frame = CGRectMake(cursor.x, cursor.y, assetView.frame.size.width, assetView.frame.size.height);
		if (![entry objectForKey:@"ad"]) {
			assetView.tag = tagCounter;
			tagCounter++;
		}
		else {
			assetView.tag = -1;
		}
		assetView.delegate = self;

		// [assetView addTarget:self action:@selector(itemSelected:) forControlEvents:UIControlEventTouchUpInside];

		[self.scrollView addSubview:assetView];
		maxHeight = assetView.frame.origin.y + assetView.frame.size.height;
		cursor.x = cursor.x + assetView.frame.size.width;

		if (cursor.x > assetView.frame.size.width * 2) {
			cursor.x = leftIndent;
			cursor.y = cursor.y + assetView.frame.size.height - 2;
		}
	}

	self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, maxHeight + 10);
}

- (void)display:(NSArray * )entries {
	// dispatch_async(dispatch_get_main_queue(), ^{




	for (UIView *v in [[[self.scrollView subviews] copy] autorelease]) {
		[v removeFromSuperview];
	}




	[self showFilters];

	float maxHeight = 0;
	float leftIndent = 50;

	CGPoint cursor = CGPointMake(leftIndent, self.top);
	int tagCounter = 0;

	for (int i = 0; i < [entries count]; i++) {
		NSDictionary *entry = [entries objectAtIndex:i];

		AssetView *assetView = [[[AssetView alloc] initWithStyle:ASSET_TYPE_ON_DEMAND_NEW data:entry] autorelease];

		assetView.frame = CGRectMake(cursor.x, cursor.y, assetView.frame.size.width, assetView.frame.size.height);
		if (![entry objectForKey:@"ad"]) {
			assetView.tag = tagCounter;
			tagCounter++;
		}
		else {
			assetView.tag = -1;
		}
		assetView.delegate = self;

		// [assetView addTarget:self action:@selector(itemSelected:) forControlEvents:UIControlEventTouchUpInside];

		[self.scrollView addSubview:assetView];
		maxHeight = assetView.frame.origin.y + assetView.frame.size.height;
		cursor.x = cursor.x + assetView.frame.size.width;

		if (cursor.x > assetView.frame.size.width * 2) {
			cursor.x = leftIndent;
			cursor.y = cursor.y + assetView.frame.size.height - 2;
		}
	}

	self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, maxHeight + 10);

	// });
}

- (void)fetchData {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   dispatch_async (dispatch_get_main_queue (), ^{
										   [self startSpinner];
									   });

					   NSDictionary *c = Nil;
					   NSString *query =  Nil;

					   NSString *sortQuery = Nil;
					   if (self.sortData) {
						   if ([self.sortData isEqualToString:@"A-Z"]) {
							   sortQuery = @"&sort=title";
						   }
						   if ([self.sortData isEqualToString:@"Date"]) {
							   sortQuery = @"&sort=added,title|desc";
						   }
						   if ([self.sortData isEqualToString:@"Rating"]) {
							   sortQuery = @"&sort=:starRating|desc";
						   }
					   }


					   if (self.filtedData) {
						   query = [NSString stringWithFormat:@"&byCategoryIds=%@", [ThePlatform urlencode:[self.filtedData objectForKey:@"id"]]];
						   if (sortQuery) {
							   query = [query stringByAppendingString:[ThePlatform urlencode:sortQuery]];
						   }

						   c =  [[ThePlatform sharedInstance] getFeed:query];
					   }
					   else {
						   if (self.movies) {
							   c = [[ThePlatform sharedInstance] getMovies:sortQuery];
						   }
						   else {
							   c = [[ThePlatform sharedInstance] getTVSeries:sortQuery];
						   }
					   }



					   self.data = c;

					   NSArray *entries = [c objectForKey:@"entries"];

					   NSMutableArray *entriesWithAds = [NSMutableArray array];

					   int a = 1;

					   for (int i = 0; i <  [entries count]; i++) {
						   [entriesWithAds addObject:[entries objectAtIndex:i]];

						   if ((i + 1) % 4 == 0) {
	                           // NSLog(@"MATCHES %d", i);
							   NSString *addName = [NSString stringWithFormat:@"cad%d.png", a];
							   [entriesWithAds addObject:[NSDictionary dictionaryWithObject:addName forKey:@"ad"]];

							   a++;

							   if (a > 4)
								   a = 1;
						   }
					   }



					   dispatch_async (dispatch_get_main_queue (), ^{
										   [self display:entriesWithAds];
										   [self stopSpinner];
									   });
				   });
}

- (void)filterChanged:(NSDictionary *)filter {
	if ([filter isKindOfClass:[NSString class]]) {
		self.sortData = (NSString *)filter;
		[self.sortButton setTitle:[NSString stringWithFormat:@"Sort By: %@", filter] forState:UIControlStateNormal];
	}
	else {
		self.filtedData = filter;

		NSString *filterTitle = [filter objectForKey:@"title"];
		if ([filterTitle isEqualToString:@"Movies"]) {
			filterTitle = @"All";
		}

		if ([filterTitle isEqualToString:@"TV Series"]) {
			filterTitle = @"All";
		}

		[self.filterButton setTitle:[NSString stringWithFormat:@"Filter By: %@", filterTitle] forState:UIControlStateNormal];
	}
	[self fetchData];
}

- (void)showFilters {
	[self.scrollView addSubview:self.textBackgroundView];
	[self.scrollView addSubview:self.separationLabel];
	[self.scrollView addSubview:self.textBackgroundView];
	[self.scrollView addSubview:self.separationLabel];
	[self.scrollView addSubview:self.moviesButton];
	[self.scrollView addSubview:self.cachupButton];
	[self.scrollView addSubview:self.filterButton];
	[self.scrollView addSubview:self.sortButton];
}

- (void)updateFilters {
	if (self.movies) {
		[self.moviesButton setTitleColor:[UIColor colorWithRed:31.0 / 255.0 green:102.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
		[self.cachupButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	}
	else {
		[self.moviesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
		[self.cachupButton setTitleColor:[UIColor colorWithRed:31.0 / 255.0 green:102.0 / 255.0 blue:255.0 / 255.0 alpha:1.0] forState:UIControlStateNormal];
	}
}

- (void)showMovies:(id)sender {
	self.movies = YES;
	self.filtedData = Nil;
	[self.filterButton setTitle:[NSString stringWithFormat:@"Filter By: Featured"] forState:UIControlStateNormal];
	[self updateFilters];
	[self fetchData];
}

- (void)showTVShows:(id)sender {
	self.movies = NO;
	self.filtedData = Nil;
	[self.filterButton setTitle:[NSString stringWithFormat:@"Filter By: Featured"] forState:UIControlStateNormal];
	[self updateFilters];
	[self fetchData];
}

- (void)showSort:(id)sender {
	NSArray *sortArray = [NSArray arrayWithObjects:@"A-Z", @"Date", @"Rating", nil];
	NSMutableDictionary *genreFilter = [NSMutableDictionary dictionary];

	[genreFilter setObject:sortArray forKey:@"data"];
	[genreFilter setObject:@"Genre" forKey:@"title"];

	NSArray *panelData = [NSArray arrayWithObjects:genreFilter, nil];

	[PopupHelper showSortPopupInView:self.scrollView atButton:self.sortButton withData:panelData delegate:self];
}

- (void)showFilter:(id)sender {
	NSArray *filters = [[ThePlatform sharedInstance] getCategories];
	NSMutableArray *relatedFilters = [NSMutableArray array];
	NSMutableArray *finalrelatedFilters = [NSMutableArray array];
	ThePlatform *platform = [ThePlatform sharedInstance];
	NSDictionary *config = platform.config;
	NSDictionary *cfg = [config objectForKey:@"config"];
	NSString *cathegoryId = Nil;
	NSString *catBaseUrl = [cfg objectForKey:@"categoryBaseUrl"];


	if (self.movies) {
		cathegoryId = [catBaseUrl stringByAppendingString:[cfg objectForKey:@"movieCatId"]];
	}
	else {
		cathegoryId = [catBaseUrl stringByAppendingString:[cfg objectForKey:@"tvSeriesCatId"]];
	}

	// int allIndex = -1;

	for (int i = 0; i < [filters count]; i++) {
		NSDictionary *d = [filters objectAtIndex:i];
		NSString *parentCatId = [d objectForKey:@"plcategory$parentId"];
		if ([parentCatId isEqualToString:cathegoryId]) {
			[relatedFilters addObject:d];
		}
		NSString *catId = [d objectForKey:@"id"];
		if ([catId isEqualToString:cathegoryId]) {
			// allIndex = [relatedFilters count];
			[finalrelatedFilters addObject:d];
		}
	}

	[finalrelatedFilters addObjectsFromArray:relatedFilters];


	NSMutableDictionary *genreFilter = [NSMutableDictionary dictionary];
	[genreFilter setObject:finalrelatedFilters forKey:@"data"];
	[genreFilter setObject:@"Genre" forKey:@"title"];

	NSArray *panelData = [NSArray arrayWithObjects:genreFilter, nil];

	[PopupHelper showFilterPopupInView:self.scrollView atButton:self.filterButton withData:panelData delegate:self];
}

- (void)setupFilters {
	float topY = 20;

	self.textBackgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"OndemandBackgroundLayer"]] autorelease];
	self.textBackgroundView.frame = CGRectMake(170, topY, 267, 30);
	self.separationLabel = [[[UILabel alloc] initWithFrame:CGRectMake(310, topY + 1, 40, 30)] autorelease];
	self.separationLabel.text = @"|";
	self.separationLabel.font = [UIFont fontWithName:@"Helvetica" size:20];
	[self.separationLabel setTextColor:[UIColor whiteColor]];
	[self.separationLabel setBackgroundColor:[UIColor clearColor]];

	float filterTextFontSize = 15.0f;

	self.cachupButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.cachupButton.frame = CGRectMake(180, topY, 131, 31);
	self.cachupButton.backgroundColor = [UIColor clearColor];
	[self.cachupButton setTitle:@"Catch Up TV" forState:UIControlStateNormal];
	self.cachupButton.titleLabel.textColor = [UIColor whiteColor];
	self.cachupButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:filterTextFontSize];
	[self.cachupButton addTarget:self action:@selector(showTVShows:) forControlEvents:UIControlEventTouchUpInside];

	self.moviesButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.moviesButton.frame = CGRectMake(311, topY, 131, 31);
	[self.moviesButton setTitle:@"Movies" forState:UIControlStateNormal];
	self.moviesButton.backgroundColor = [UIColor clearColor];
	self.moviesButton.titleLabel.textColor = [UIColor whiteColor];
	self.moviesButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:filterTextFontSize];
	[self.moviesButton addTarget:self action:@selector(showMovies:) forControlEvents:UIControlEventTouchUpInside];

	self.filterButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.filterButton.frame = CGRectMake(501, topY, 175, 30);
	[self.filterButton setBackgroundImage:[UIImage imageNamed:@"OnDemandButton"] forState:UIControlStateNormal];
	[self.filterButton setTitle:@"Filter By: Featured" forState:UIControlStateNormal];
	self.filterButton.backgroundColor = [UIColor clearColor];
	self.filterButton.titleLabel.textColor = [UIColor whiteColor];
	self.filterButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:filterTextFontSize];
	[self.filterButton addTarget:self action:@selector(showFilter:) forControlEvents:UIControlEventTouchUpInside];

	self.sortButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.sortButton.frame = CGRectMake(686, topY, 175, 30);
	[self.sortButton setBackgroundImage:[UIImage imageNamed:@"OnDemandButton"] forState:UIControlStateNormal];
	[self.sortButton setTitle:@"Sort By: A-Z" forState:UIControlStateNormal];
	self.sortButton.backgroundColor = [UIColor clearColor];
	self.sortButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:filterTextFontSize];
	self.sortButton.titleLabel.textColor = [UIColor whiteColor];
	[self.sortButton addTarget:self action:@selector(showSort:) forControlEvents:UIControlEventTouchUpInside];

	self.top = 40 + topY;
}

- (void)viewWillAppear:(BOOL)animated {
	self.navigationController.navigationBarHidden = NO;
	[[self.view.subviews objectAtIndex:0] setFrame:self.view.bounds];
}

- (void)viewDidLoad {
	[super viewDidLoad];
	[self customNavigationBar];
	// self.view.backgroundColor = [UIColor clearColor];
	UIImage *backgroundImage;
	if ([UIImage externalImageNamed:@"background"]) {
		backgroundImage = [UIImage externalImageNamed:@"background"];
	}
	else {
		backgroundImage = [UIImage imageNamed:@"background.png"];
	}
	UIImageView *backgroundImageView = [[[UIImageView alloc] initWithImage:backgroundImage] autorelease];
	// [backgroundImage release];

	[self.view addSubview:backgroundImageView];

	self.scrollView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, detailRightSize + detailLeftSize, detailHeight) ] autorelease];
	[self.view addSubview:self.scrollView];

	[self setupFilters];
	[self updateFilters];
//    [backgroundImageView release];
	[self fetchData];
}

- (void)asetTapped:(id)sender {
	AssetView *asset = (AssetView *)sender;
	int selected = asset.tag;

	if (selected >= 0) {
		NSArray *entries = [self.data objectForKey:@"entries"];
		NSMutableArray *entriesWithAds = [NSMutableArray array];

		int a = 1;

		for (int i = 0; i <  [entries count]; i++) {
			[entriesWithAds addObject:[entries objectAtIndex:i]];
			if ((i + 1) % 4 == 0) {
				NSString *addName = [NSString stringWithFormat:@"cad%d.png", a];
				[entriesWithAds addObject:[NSDictionary dictionaryWithObject:addName forKey:@"ad"]];

				a++;

				if (a > 4) {
					a = 1;
				}
			}
		}
		NSDictionary *content = [entries objectAtIndex:selected];
		[self openDetail:content withCollection:entriesWithAds title:Nil];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
