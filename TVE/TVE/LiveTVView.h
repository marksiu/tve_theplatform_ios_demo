//
//  LiveTVView.h
//  TVE
//
//  Created by Gabor Bottyan on 11/28/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LiveTVView : UIView {
}
@property (retain) UIView *movieConteiner;
- (void)configureView:(NSDictionary * )channel;
- (id)initWithFrame:(CGRect) frame data:(NSDictionary * )__data;
@end
