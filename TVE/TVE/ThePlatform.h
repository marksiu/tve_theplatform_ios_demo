//
//  DataHelper.h
//  TVE
//
//  Created by Gabor Bottyan on 12/18/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThePlatform : NSObject {
}
- (NSDictionary *)getFeed:(NSString *)query;
- (NSDictionary *)getFeed;
+ (NSString *)urlencode:(NSString *)str;
- (NSArray *)getCategories;
- (NSDictionary *)search:(NSString *)query sort:(NSString *)so;
+ (NSString *)getRelatedQueryString:(NSDictionary *)dict;
- (NSDictionary *)related:(NSString *)query;
- (NSDictionary *)getEpisodes;
- (NSDictionary *)getTVSeries:(NSString *)query;
- (NSDictionary *)getMovies:(NSString *)query;
- (NSDictionary *)getTVClips;
- (NSDictionary *)login:( NSString * )username password:(NSString *)password;
- (NSDictionary *)getEpisodesBySeries:(NSString *)seriesRef;
- (NSDictionary *)getClipsBySeries:(NSString *)seriesRef;
- (NSDictionary *)getData:(NSDictionary *)feedConfig;
- (NSDictionary *)getDataForId:(NSString *)asetid;
- (NSString *)getAssetsByCategory:(NSString *)cat;
@property (retain) NSDictionary *config;

+ (id)sharedInstance;
@end
