//
//  TVEConfig.h
//  TVE
//
//  Created by Mark Siu on 15/8/13.
//  Copyright (c) 2013 Accedo Broadband. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TVEConfig : NSObject

+ (TVEConfig *)sharedConfig;
- (void)downloadRentalRecord;
- (NSMutableDictionary *)getRentalMovieRecord;
- (void)clearRentalMovieRecord;
- (void)renewRentalMovie;
- (NSDictionary *)getRentalMovieDataFromMediaFeed;

// only for Live TV Tab pop up message fix
- (BOOL)getShowSubscriptionMessage;
- (void)updateShowSubscriptionMessage:(BOOL)showMessage;

@end
