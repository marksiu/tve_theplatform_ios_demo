//
//  SeriesViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 12/3/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "SeriesViewController.h"
#import "DetailsListView.h"
#import "SeasonDetailView.h"
#import "Constants.h"
#import "SBJson.h"
#import "AppView.h"
#import "UIImage+Extension.h"

@interface SeriesViewController ()
@property (retain) NSDictionary *data;
@property (retain) NSArray *collection;
@property (retain) SeasonDetailView *detailView;
@property (retain) DetailsListView *listView;
@property (retain) NSDictionary *feed;
@end

@implementation SeriesViewController

- (id)initWithData:(NSDictionary *)__data withCollection:(NSArray *)col {
	self = [super init];
	if (self) {
		self.data = __data;
		self.collection = col;
	}
	return self;
}

- (id)initWithFeed:(NSDictionary *)desc {
	self = [super init];
	if (self) {
		self.feed = desc;
		// Custom initialization
	}
	return self;
}

- (void)fetchData {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   NSString *url = Nil;
	                   // NSLog(@"%@", self.feed);
					   NSDictionary *f = [self.feed objectForKey:@"feed"];
					   if (f) {
						   url = [f objectForKey:@"url"];
					   }
					   else {
						   url = [self.feed objectForKey:@"url"];
					   }




					   NSString *u = [NSString stringWithFormat:@"%@?form=json&pretty=true", url];

					   NSString *content = [NSString stringWithContentsOfURL:[NSURL URLWithString:u] encoding:NSUTF8StringEncoding error:Nil];
					   NSDictionary *c = (NSDictionary *)[content JSONValue];
					   NSArray *entries = [c objectForKey:@"entries"];

	                   // NSLog(@"Calling %@ - > %d", url, [entries count]);
					   dispatch_async (dispatch_get_main_queue (), ^{
										   [self.listView setData:entries index:0];

										   [self.detailView setContent:entries type:YES];
										   [self.detailView setContent:entries type:NO];
									   });
				   });
}

- (void)viewDidLoad {
//    [super viewDidLoad];
//    [self customNavigationBar];
//
//    self.view.backgroundColor = [UIColor clearColor];
//    UIImage * backgroundImage = [UIImage externalImageNamed:@"background"];
//    UIImageView * backgroundImageView = [[UIImageView alloc] initWithImage: backgroundImage];
//    backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
//    [backgroundImage release];
//    [self.view addSubview:backgroundImageView];
//    self.detailView = [[[SeasonDetailView alloc] initWithFrame:CGRectMake(detailLeftSize, 0, detailRightSize, detailHeight) config:nil] autorelease];
//    self.listView = [[[DetailsListView alloc] initWithFrame:CGRectMake(0, 0, detailLeftSize, detailHeight)] autorelease];
//    self.listView.listdelegate = self;
//    self.detailView.backgroundColor = [UIColor clearColor];
//    [self.view addSubview:self.listView];
//    [self.view addSubview:self.detailView];
//
//    [backgroundImageView release];
//    [self fetchData];

	[super viewDidLoad];
	[self customNavigationBar];




	self.view.backgroundColor = [UIColor clearColor];
	UIImage *backgroundImage;
	if ([UIImage externalImageNamed:@"background"]) {
		backgroundImage = [UIImage externalImageNamed:@"background"];
	}
	else {
		backgroundImage = [UIImage imageNamed:@"background.png"];
	}
	UIImageView *backgroundImageView = [[[UIImageView alloc] initWithImage:backgroundImage] autorelease];
	// [backgroundImage release];
	[self.view addSubview:backgroundImageView];
	self.detailView = [[[SeasonDetailView alloc] initWithFrame:CGRectMake(detailLeftSize, 0, detailRightSize, detailHeight) config:nil] autorelease];
	self.listView = [[[DetailsListView alloc] initWithFrame:CGRectMake(0, 0, detailLeftSize, detailHeight)] autorelease];



	self.listView.listdelegate = self;


	self.detailView.backgroundColor = [UIColor clearColor];
	// self.detailView.detaildelegate = self;
	[self.view addSubview:self.listView];
	[self.view addSubview:self.detailView];

	// [backgroundImageView release];
	// [self updateOnDemand];
}

- (void)itemSelected:(NSDictionary *)item {
	[self.detailView configureView:item];
}

- (void)updateOnDemand {
	[self.listView setData:self.collection index:0];
	//  [self.detailView setRelatedContent:self.collection];
	[self.detailView configureView:self.data];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
