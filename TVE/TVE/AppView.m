//
//  AppView.m
//  TVE
//
//  Created by Gabor Bottyan on 12/19/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "AppView.h"
#import "SBJson.h"
#import "ASIHTTPRequest.h"
#import "ThePlatform.h"
#import "NSDate+Extras.h"
#import "VSRemoteImageView.h"

static NSString *channelsMutex = @"channelMutex";

@interface AppView () {
	NSArray *_channels;
}

@property (retain) NSDictionary *appStatus;


@property (retain) NSDictionary *groupData;
@property (retain) NSString *lastFetchedGroupJSON;
@property BOOL firstUpdate;

@property (retain) NSDictionary *userData;
@property (retain) NSMutableDictionary *assetCache;

@property (retain) NSString *sessionKey;
@property (retain) NSDate *sessionExpiry;

@property (retain) NSArray *epg;
@property (retain) NSArray *channels;
@property (retain) NSArray *categories;

@property NSString *assetString;


@end

static NSString * myAppViewUrl = APPVIEW_URL;
static NSString *myApiKey = API_KEY;


@implementation AppView
static AppView *sharedInstance = Nil;

- (NSString *)getAppViewUrl {
	return myAppViewUrl;
}

- (void)setAppViewUrl:(NSString *)newAppViewUrl {
	myAppViewUrl = newAppViewUrl;
}

- (NSString *)getApiKey {
	return myApiKey;
}

- (void)setApiKey:(NSString *)newApiKey {
	myApiKey = newApiKey;
}

- (BOOL)hasSession {
	// check if the session is there and valid
	if (self.sessionKey != Nil && self.sessionExpiry != Nil && [self.sessionExpiry timeIntervalSinceNow] > 0) {
		return YES;
	}

	return NO;
}

- (void)createSession {
//	NSString *uuid = [[NSUUID init] UUIDString];
	NSString *uuid = @"testingabcdefg";

	NSString *url = [APPVIEW_URL stringByAppendingFormat:@"/session?appKey=%@&uuid=%@", myApiKey, uuid ];

	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:url]];

	[request startSynchronous];

	NSError *error = [request error];
	if (!error) {
		NSString *responseStr = [request responseString];
		SBJsonParser *parser = [[SBJsonParser alloc] init];
		NSDictionary *sessionObj = [parser objectWithString:responseStr];
		self.sessionKey = [sessionObj objectForKey:@"sessionKey"];


		NSString *sessionExpiryStr = [sessionObj objectForKey:@"expiration"];

		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"yyyyMMdd'T'HH:mm:ssZ"];

		self.sessionExpiry = [dateFormatter dateFromString:sessionExpiryStr];
		[dateFormatter release];
		[parser release];
	}
}

- (NSString *)getSession {
	int cc = 0;

	while (![self hasSession] && cc < 2)
	{
		[self createSession];
		cc++;
	}

	return self.sessionKey;
}

- (BOOL)isActive {
	if (self.appStatus == Nil) {
		[self checkAppStatus];
	}

	// return false if failed to retreive app status
	if (self.appStatus == Nil) {
		return NO;
	}

	return [((NSString *)[self.appStatus objectForKey:@"status"]) isEqual:@"Active"];
}

- (void)checkAppStatus {
	NSString *url = [APPVIEW_URL stringByAppendingString:@"/status"];

	NSLog(@"APPVIEW URL: %@", url);

	ASIHTTPRequest *sentRequest = [self sendRequest:url];

	SBJsonParser *parser = [[SBJsonParser alloc] init];
	NSString *responseStr = [sentRequest responseString];
	self.appStatus = [parser objectWithString:responseStr];
	[parser release];
}

- (NSString *)getMaintainanceMessage {
	if (self.appStatus == Nil) {
		[self checkAppStatus];
	}

	// return fallback message on connection failure
	if (self.appStatus == Nil) {
		return @"Unable to connect to AppView";
	}



	return [self.appStatus objectForKey:@"message"];
}

- (void)setChannels:(NSArray *)channels {
	@synchronized(channelsMutex)
	{
		_channels = [channels retain];
	}
}

- (NSArray *)channels {
	@synchronized(channelsMutex)
	{
		return [[_channels retain] autorelease];
	}
}

- (void)periodicalUpdate:(id)sender {
	if (self.fingerprint) {
		NSLog(@"PERIODIC UPDATE APPVIEW ...");

		BOOL flag = NO;
		if (self.firstUpdate) {
			flag = YES;
			self.firstUpdate = NO;
		}

		__block BOOL favoriteShowsChanged = flag;
		__block BOOL paydChannelsChanged  = flag;
		__block BOOL rentedMoviesChanged  = flag;
		__block BOOL movieRatingsChanged  = flag;
		__block BOOL homeChannelChanged  = flag;
		__block BOOL favoriteChannelChanged  = flag;


		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
						   NSDictionary *groupData = [self getGroupDataFromRemote];

						   NSLog (@"%@", groupData);
						   NSDictionary *currentGroupData = Nil;
						   if (groupData) {
							   currentGroupData = [NSDictionary dictionaryWithDictionary:[self getGroupData]];

							   @synchronized (self) {
								   self.groupData = groupData;
							   }

		                       // NSLog(@"UPDATED APPVIEW DATA");
		                       // if (currentGroupData){
		                       //  NSLog(@"COMPARE TO CURRENT DATA");

							   NSArray *currentFavoriteShows = [currentGroupData objectForKey:@"favShow"];
							   NSArray *favoriteShows = [groupData objectForKey:@"favShow"];
							   if (![currentFavoriteShows isEqualToArray:favoriteShows]) {
								   favoriteShowsChanged = YES;
								   NSLog (@"FAVORITE SHOWS CHANGED");
							   }


							   NSArray *currentPaydChannels = [currentGroupData objectForKey:@"paidChannels"];
							   NSArray *paydChannels = [groupData objectForKey:@"paidChannels"];
							   if (![currentPaydChannels isEqualToArray:paydChannels]) {
								   paydChannelsChanged = YES;
								   NSLog (@"PAYD CHANNELS CHANGED");
							   }

							   NSArray *currentRentedMovies = [currentGroupData objectForKey:@"rentedMovies"];
							   NSArray *rentedMovies = [groupData objectForKey:@"rentedMovies"];
							   if (![currentRentedMovies isEqualToArray:rentedMovies]) {
								   rentedMoviesChanged = YES;
								   NSLog (@"RENTED MOVIES CHANGED");
							   }

							   NSArray *currentMovieRatings = [currentGroupData objectForKey:@"movieRating"];
							   NSArray *movieRatings = [groupData objectForKey:@"movieRating"];
							   if (![currentMovieRatings isEqualToArray:movieRatings]) {
								   movieRatingsChanged = YES;
								   NSLog (@"MOVIES RATINGS CHANGED");
							   }

							   NSArray *currentFavChannels = [currentGroupData objectForKey:@"favChannels"];
							   NSArray *favChannels = [groupData objectForKey:@"favChannels"];
							   if (![currentFavChannels isEqualToArray:favChannels]) {
								   favoriteChannelChanged = YES;
								   NSLog (@"FAVORITE CHANNEL CHANGED");
							   }





							   NSString *currentHomeChannel = [currentGroupData objectForKey:@"homeChannel"];
							   NSString *homeChannel =  [groupData objectForKey:@"homeChannel"];
		                       // NSLog(@"HOME CHANNEL : %@  WAS %@", currentHomeChannel, homeChannel);

							   if (![currentHomeChannel isEqualToString:homeChannel]) {
								   homeChannelChanged = YES;
								   NSLog (@"HOME CHANNEL CHANGED");
							   }
		                       // }

							   dispatch_async (dispatch_get_main_queue (), ^{
												   if (favoriteShowsChanged) {
													   NSLog (@"Favorite shows changed");
													   [[NSNotificationCenter defaultCenter] postNotificationName:@"FAVORITE_SHOWS_CHANGED" object:self];
												   }
												   if (paydChannelsChanged) {
													   NSLog (@"Payd channels changed");
													   [[NSNotificationCenter defaultCenter] postNotificationName:@"PAYD_CHANNELS_CHANGED"  object:self];
												   }

												   if (rentedMoviesChanged) {
													   NSLog (@"Rented movies changed");
													   [[NSNotificationCenter defaultCenter] postNotificationName:@"RENTED_MOVIES_CHANGED"  object:self];
												   }

												   if (movieRatingsChanged) {
													   NSLog (@"Movie ratings changed");
													   [[NSNotificationCenter defaultCenter] postNotificationName:@"MOVIE_RATINGS_CHANGED"  object:self];
												   }

												   if (homeChannelChanged) {
													   NSLog (@"Homer channel changed");
													   [[NSNotificationCenter defaultCenter] postNotificationName:@"HOME_CHANNEL_CHANGED"  object:self];
												   }


												   if (favoriteChannelChanged) {
													   NSLog (@"Favorite channel changed");
													   [[NSNotificationCenter defaultCenter] postNotificationName:@"FAVORITE_CHANNEL_CHANGED"  object:self];
												   }
											   });
						   }
					   });
	}
}

+ (AppView *)sharedInstance {
	if (sharedInstance == Nil) {
		sharedInstance = [[super allocWithZone:NULL] init];
	}

	return sharedInstance;
}

// We can still have a regular init method, that will get called the first time the Singleton is used.
- (id)init {
	self = [super init];

	if (self) {
		// NSDictionary *dict = [matchingDicts lastObject];



		self.assetCache = [NSMutableDictionary dictionary];
		NSTimer *timer = [NSTimer timerWithTimeInterval:10.0f target:self selector:@selector(periodicalUpdate:) userInfo:Nil repeats:YES];
		[[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
		// Work your initialising magic here as you normally would
	}

	return self;
}

// Your dealloc method will never be called, as the singleton survives for the duration of your app.
// However, I like to include it so I know what memory I'm using (and incase, one day, I convert away from Singleton).
- (void)dealloc {
	// I'm never called!
	[super dealloc];
}

// We don't want to allocate a new instance, so return the current one.
+ (id)allocWithZone:(NSZone *)zone {
	return [[self sharedInstance] retain];
}

// Equally, we don't want to generate multiple copies of the singleton.
- (id)copyWithZone:(NSZone *)zone {
	return self;
}

// Once again - do nothing, as we don't have a retain counter for this object.
- (id)retain {
	return self;
}

// Replace the retain counter so we can never release this object.
- (NSUInteger)retainCount {
	return NSUIntegerMax;
}

// This function is empty, as we don't want to let the user release this object.
- (oneway void)release {
}

// Do nothing, other than return the shared instance - as this is expected from autorelease.
- (id)autorelease {
	return self;
}

- (NSDictionary *)mockUsers {
	return [NSDictionary dictionaryWithObjectsAndKeys:@"ho_hardCodedFingerprint001", @"marco" /*,@"fingerprintMichael", @"Michael",@"fingerprintFredrik", @"Fredrik",@"fingerprintReuben", @"Reuben"*/, Nil];
}

- (void)logout {
	self.fingerprint = Nil;
	self.groupData = Nil;
	[[NSNotificationCenter defaultCenter] postNotificationName:@"APP_LOGOUT" object:Nil];
	NSLog(@"Favorite shows changed");
	[[NSNotificationCenter defaultCenter] postNotificationName:@"FAVORITE_SHOWS_CHANGED" object:self];
	NSLog(@"Payd channels changed");
	[[NSNotificationCenter defaultCenter] postNotificationName:@"PAYD_CHANNELS_CHANGED"  object:self];
	NSLog(@"Rented movies changed");
	[[NSNotificationCenter defaultCenter] postNotificationName:@"RENTED_MOVIES_CHANGED"  object:self];
	NSLog(@"Movie ratings changed");
	[[NSNotificationCenter defaultCenter] postNotificationName:@"MOVIE_RATINGS_CHANGED"  object:self];
	NSLog(@"Movie ratings changed");
	[[NSNotificationCenter defaultCenter] postNotificationName:@"HOME_CHANNEL_CHANGED"  object:self];
	NSLog(@"Movie ratings changed");
	[[NSNotificationCenter defaultCenter] postNotificationName:@"FAVORITE_CHANNEL_CHANGED"  object:self];
}

- (BOOL)login:(NSString *)userName password:(NSString * )password {
	if ([userName isEqualToString:@"marco"]) {
		self.fingerprint = @"ho_hardCodedFingerprint001";
	}
	else {
		NSArray *knownUsers = [NSArray arrayWithObjects:@"michael", @"fredrik", @"reuben", Nil];
		if (![knownUsers containsObject:[userName lowercaseString]]) {
			return NO;
		}

		NSString *realUserName = [[userName lowercaseString] capitalizedString];

//		self.fingerprint = [@"fingerprint" stringByAppendingString:realUserName];
		NSString *fingerprintMD5 = [[VSRemoteImageView md5:realUserName] lowercaseString];
		self.fingerprint = fingerprintMD5;
	}

	self.firstUpdate = YES;

	[self periodicalUpdate:Nil];

	return YES;
}

// TVE thePlatform - update home interface
- (void)updateLoginHome:(NSString *)username {
//	NSString *userName = @"reuben";
//	NSString *password = @"";
//
//	NSArray *knownUsers = [NSArray arrayWithObjects:@"michael", @"fredrik", @"reuben", Nil];
//	if (![knownUsers containsObject:[userName lowercaseString]]) {
//		return;
//	}
//
//	NSString *realUserName = [[userName lowercaseString] capitalizedString];
//	self.fingerprint = [@"fingerprint" stringByAppendingString:realUserName];

	NSString *fingerprintMD5 = [[VSRemoteImageView md5:username] lowercaseString];

	self.fingerprint = fingerprintMD5;
	self.firstUpdate = YES;
	[self periodicalUpdate:Nil];
}

- (BOOL)createGroupData {
	NSString *json = [self createGroupJSON];

	return [self postGroupData:json];
}

- (id)parseJSON:(NSString *)json {
	SBJsonParser *parse = [[SBJsonParser alloc] init];
	NSMutableDictionary *obj = [parse objectWithString:json];

	[parse release];

	return [[obj copy] autorelease];
}

- (NSString *)writeJSON:(id)data {
	SBJsonWriter *write = [[SBJsonWriter alloc] init];
	NSString *json = [write stringWithObject:data];

	[write release];
	return json;
}

- (NSDictionary *)parseGroupJSON:(NSString *)json {
	NSMutableDictionary *configData = [self parseJSON:json];
	NSMutableDictionary *newConfigData = [NSMutableDictionary dictionary];

	for (NSString *key in configData) {
		id newValue = [self parseJSON:[configData valueForKey:key]];

		// in case unable to parse value
		if (newValue == Nil) {
			newValue = [configData valueForKey:key];
		}

		[newConfigData setValue:newValue forKey:key];
	}

	return newConfigData;
}

- (NSString *)writeGroupJSON:(NSDictionary *)groupData {
	NSMutableDictionary *newGroupData = [NSMutableDictionary dictionary];

	for (NSString *key in groupData) {
		NSString *json = [self writeJSON:[groupData objectForKey:key] ];

		if (json == Nil) {
			json = [groupData objectForKey:key];
		}

		[newGroupData setValue:json forKey:key];
	}

	NSString *json = [self writeJSON:newGroupData];

	return json;
}

- (NSString *)createGroupJSON {
	return @"{\"favChannels\": \"[]\",\"paidChannels\": \"[]\",\"favShow\": \"[]\",\"rentedMovies\": \"[]\",\"movieRating\": \"[]\", \"homeChannel\":\"501\" }";
}

- (NSDictionary *)createGroupDict {
	NSString *json =   [self createGroupJSON];

	return [self parseJSON:json];
}

- (BOOL)postGroupData:(NSDictionary *)groupData {
	NSString *currentJSON;

	@synchronized(self)
	{
		currentJSON  =  [self writeGroupJSON:groupData];
	}
	NSString *url = [[APPVIEW_URL stringByAppendingString:@"/group"] stringByAppendingFormat:@"/%@", self.fingerprint];

	NSData *postData  = [currentJSON dataUsingEncoding:NSUTF8StringEncoding];
	NSMutableData *md = [NSMutableData dataWithData:postData];

	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:url]];

	[request addRequestHeader:@"Content-Type" value:@"application/json"];
	[request addRequestHeader:@"X-Session" value:self.sessionKey];
	[request setPostBody:md];

	[request setCompletionBlock:^{
		 NSString *responseString = [request responseString];
		 NSLog (@"Response: %@", responseString);
	 }];
	[request setFailedBlock:^{
		 NSError *error = [request error];
		 NSLog (@"Error: %@", error.localizedDescription);
	 }];

	[request startSynchronous];

	[self periodicalUpdate:Nil];

	return YES;
}

- (BOOL)postGroupDataWithKey:(NSString *)key andValue:(id)value {
//    (id) backupValue = [self.groupData objectForKey:key];

	// NSString *json = [self getGroupJSONFromRemote];

	// force update from server if self data not up to date
	// if (json != Nil && ![json isEqualToString:self.lastFetchedGroupJSON]) {
	//	[self updateGroupDataWithJSON:json];
	// NSLog(@"postGroupData but data updated on server side. Exited.");
	// return NO;
	// }

	// download latest and update
	[self fetchGroupData];

	NSMutableDictionary *groupData;

	@synchronized(self)
	{
		groupData = [NSMutableDictionary dictionaryWithDictionary:self.groupData];
	}

	[groupData setValue:value forKey:key];

	if ([self postGroupData:groupData]) {
		return YES;
	}

	// restore previous data
//    [self.groupData setValue:backupValue forKey:key];

	return NO;
}

- (NSDictionary *)getGroupDataFromRemote {
	return [self parseGroupJSON:[self getGroupJSONFromRemote]];
}

- (NSString *)getGroupJSONFromRemote {
	if (self.fingerprint) {
		NSString *url = [[APPVIEW_URL stringByAppendingString:@"/group"] stringByAppendingFormat:@"/%@", self.fingerprint];
		ASIHTTPRequest *request = [self sendRequest:url];

		NSError *error = [request error];

		if (error) {
			if ([request responseStatusCode] == 404) {
				return Nil;
			}

			// pretend as empty
			return @"";
		}

		NSString *responseStr = [request responseString];


		if ([responseStr rangeOfString:@"500"].location != NSNotFound) {
			return @"";
		}

		return responseStr;
	}
	return Nil;
}

- (id)getGroupDataByKey:(NSString *)key {
	return [self parseJSON:[[self getGroupData] objectForKey:key]];
}

- (NSDictionary *)getGroupData {
	@synchronized(self)
	{
		if (!self.groupData && ![self fetchGroupData]) {
			// Issue fetching group data besides from empty data
			return Nil;
		}

		// return groupData
		if (self.groupData) {
			return self.groupData;
		}

		// createGroupData if group data doesn't exists
		if ([self createGroupData]) {
			return self.groupData;
		}
	}

	// couldn't create
	return Nil;
}

- (BOOL)fetchGroupData {
	NSString *json = [self getGroupJSONFromRemote];

	// issue retrevie json data other than empty, fetch failed
	if (json == Nil) {
		return NO;
	}

	if ([json isEqualToString:@""]) {
		return [self createGroupData];
	}

	self.lastFetchedGroupJSON = json;

	@synchronized(self)
	{
		self.groupData = [self parseGroupJSON:json];
	}

	return YES;
}

- (void)fetchUserData {
	NSString *url = [[APPVIEW_URL stringByAppendingString:@"/user"] stringByAppendingFormat:@"/%@", self.fingerprint];

	// NSLog(@"%@", url);
	NSString *config_str = [self contentsOfUrl:url];
	// NSLog(@"CONFIGSTR : %@", config_str);
	SBJsonParser *parse = [[SBJsonParser alloc] init];
	NSDictionary *conf =  [parse objectWithString:config_str];

	if ([conf objectForKey:@"error"]) {
		NSLog(@"UPLOADING NEW USER DATA");
		// [self createUserData];
	}
	[parse release];
	self.userData =  [[conf copy] autorelease];
}

- (NSArray *)loadSet:(NSArray * )set {
	NSMutableArray *bakedArray = [NSMutableArray array];

	for (int i = 0; i < [set count]; i++) {
		NSDictionary *dict = [set objectAtIndex:i];
		NSString *fullId = [dict objectForKey:@"id"];
		NSDictionary *d = Nil;


		if ([self.assetCache objectForKey:[fullId lastPathComponent]]) {
			d = [self.assetCache objectForKey:[fullId lastPathComponent]];
		}
		else {
			d = [[ThePlatform sharedInstance] getDataForId:[fullId lastPathComponent]];
			if (d && [fullId lastPathComponent]) {
				[self.assetCache setObject:d forKey:[fullId lastPathComponent]];
			}
		}


		NSArray *entries = [d objectForKey:@"entries"];
		if (d) {
			[bakedArray addObjectsFromArray:entries];
		}
	}
	// NSLog(@"%@",bakedArray);
	return bakedArray;
}

- (NSArray *)getRentedMoviesRemote {
	NSString *url = [[[APPVIEW_URL stringByAppendingString:@"/group"] stringByAppendingFormat:@"/%@", self.fingerprint] stringByAppendingString:@"/rentedMovies"];
	NSString *config_str = [self contentsOfUrl:url];
	SBJsonParser *parse = [[SBJsonParser alloc] init];
	NSArray *conf =  [parse objectWithString:config_str];

	// NSLog(@"CONFIG : %@", conf);
	[parse release];
	return [self loadSet:conf];
}

- (NSArray *)getRentedMovies {
	NSArray *res = Nil;


	@synchronized(self)
	{
		res = [self.groupData objectForKey:@"rentedMovies"];
	}
	// NSLog(@"RENTED MOVIES: %@", res);
	return [self loadSet:res];
}

- (NSArray *)getFavoriteShowRecords {
	NSArray *res = Nil;

	@synchronized(self)
	{
		res = [self.groupData objectForKey:@"favShow"];
	}
	// NSLog(@"FAVORITE SHOWS: %@", res);
	return res;
}

- (NSArray * )getFavoriteShowRecordsRemote {
	NSString *url = [[[APPVIEW_URL stringByAppendingString:@"/group"] stringByAppendingFormat:@"/%@", self.fingerprint] stringByAppendingString:@"/favShow"];
	NSString *config_str = [self contentsOfUrl:url];
	SBJsonParser *parse = [[SBJsonParser alloc] init];
	NSArray *conf =  [parse objectWithString:config_str];

	[parse release];
	return conf;
}

- (NSArray *)getFavoriteShows {
	NSArray *conf = [self getFavoriteShowRecords];

	return [self loadSet:conf];
}

- (NSArray *)getFavoriteChannels {
	NSArray *res = Nil;

	@synchronized(self)
	{
		res = [[[self.groupData objectForKey:@"favChannels"] copy] autorelease];
	}
	return res;
}

- (NSArray *)getFavoriteChannelsRemote {
	NSString *url = [[[APPVIEW_URL stringByAppendingString:@"/group"] stringByAppendingFormat:@"/%@", self.fingerprint] stringByAppendingString:@"/favChannels"];
	NSString *config_str = [self contentsOfUrl:url];
	SBJsonParser *parse = [[SBJsonParser alloc] init];
	NSArray *conf =  [parse objectWithString:config_str];

	[parse release];
	return conf;
}

- (BOOL)isFavoriteChannel:(NSDictionary *)channel {
	if (self.fingerprint) {
		NSArray *favoriteChannels = [self getFavoriteChannels];
		NSString *str = [channel objectForKey:@"channel"];
		for (int i = 0; i < [favoriteChannels count]; i++) {
			NSNumber *nr = [favoriteChannels objectAtIndex:i];
			if ([str intValue] == [nr intValue]) {
				return YES;
			}
		}
	}
	return NO;
}

- (BOOL)isFavoriteShow:(NSDictionary *)show {
	if (self.fingerprint) {
		NSString *showId = [show objectForKey:@"id"];
		// NSLog(@"ISFAVORITE: %@", showId);
		NSArray *favoriteChannels = [self getFavoriteShowRecords];
		for (int i = 0; i < [favoriteChannels count]; i++) {
			NSDictionary *dict = [favoriteChannels objectAtIndex:i];
			NSString *str = [dict objectForKey:@"id"];
			if ([str isEqualToString:showId]) {
				// NSLog(@"YES ISFAVORITE: %@", showId);
				return YES;
			}
		}
		// NSLog(@"NO ISFAVORITE: %@", showId);
	}

	return NO;
}

- (NSString *)getHomeChannel {
	if (self.fingerprint) {
		return [[self getGroupData] objectForKey:@"homeChannel"];
	}
	return Nil;
}

- (void)setHomeChannel:(NSString *)channeld {
	[self postGroupDataWithKey:@"homeChannel" andValue:channeld];
}

- (void)addAssetToFavorites:(NSDictionary *)asset {
	if (self.fingerprint) {
		NSArray *conf =  [[self getGroupData] objectForKey:@"favShow"];
		NSLog(@"CURRENT TO FAVORITE SHOWS: %@", conf);


		NSString *aid = [asset objectForKey:@"id"];

		NSMutableDictionary *good = Nil;
		int goodIndex = 0;

		NSMutableArray *bakedArray = [NSMutableArray arrayWithArray:conf];

		for (int i = 0; i < [bakedArray count]; i++) {
			NSDictionary *dict = [bakedArray objectAtIndex:i];
			NSString *str = [dict objectForKey:@"id"];
			if ([str isEqualToString:aid]) {
				goodIndex = i;

				good = [NSMutableDictionary dictionaryWithDictionary:dict];
				;

				break;
			}
		}

		if (!good) {
			good = [NSMutableDictionary dictionaryWithObject:aid forKey:@"id"];
			[bakedArray addObject:good];
		}
		else {
			[bakedArray removeObjectAtIndex:goodIndex];
		}

		[self postGroupDataWithKey:@"favShow" andValue:bakedArray];
	}
}

- (void)addChannelToFavorites:(NSDictionary *)channel {
	if (self.fingerprint) {
		NSArray *conf =  [[self getGroupData] objectForKey:@"favChannels"];

		NSLog(@"CURRENT FAVORITE CHANNELS : %@", conf);

		NSString *aid = [channel objectForKey:@"channel"];

		NSNumber *good = Nil;


		int goodIndex = 0;


		NSMutableArray *bakedArray = [NSMutableArray arrayWithArray:conf];

		for (int i = 0; i < [bakedArray count]; i++) {
			NSNumber *nr = [bakedArray objectAtIndex:i];

			if ([aid intValue] == [nr intValue]) {
				goodIndex = i;
				good = nr;
				;

				break;
			}
		}
		// [bakedArray removeAllObjects];

		if (!good) {
			NSString *channelId = [channel objectForKey:@"channel"];
			// good = channel;
			[bakedArray addObject:[NSNumber numberWithInt:[channelId intValue]]];
		}
		else {
			[bakedArray removeObjectAtIndex:goodIndex];
		}

		[self postGroupDataWithKey:@"favChannels" andValue:bakedArray];
	}
}

- (void)clearRentalData {
	if (self.fingerprint) {
		// use last fetched data
		NSArray *array = [[self parseJSON:self.lastFetchedGroupJSON] objectForKey:@"rentedMovies"];

		[self postGroupDataWithKey:@"rentedMovies" andValue:array];
	}
}

- (void)clearSubscription {
	if (self.fingerprint) {
		// use last fetched data
		NSArray *array = [[self parseJSON:self.lastFetchedGroupJSON] objectForKey:@"paidChannels"];

		[self postGroupDataWithKey:@"paidChannels" andValue:array];
	}
}

- (void)addChannelToPaidChannels:(NSDictionary *)channel {
	if (self.fingerprint) {
		NSArray *conf =  [[self getGroupData] objectForKey:@"paidChannels"];

		NSString *aid = [channel objectForKey:@"channel"];

		NSNumber *good = Nil;
		int goodIndex = 0;
		NSMutableArray *bakedArray = [NSMutableArray arrayWithArray:conf];

		for (int i = 0; i < [bakedArray count]; i++) {
			NSNumber *nr = [bakedArray objectAtIndex:i];

			if ([aid intValue] == [nr intValue]) {
				goodIndex = i;
				good = nr;
				;

				break;
			}
		}
		// [bakedArray removeAllObjects];

		if (!good) {
			NSString *channelId = [channel objectForKey:@"channel"];
			// good = channel;
			[bakedArray addObject:[NSNumber numberWithInt:[channelId intValue]]];
		}

		[self postGroupDataWithKey:@"paidChannels" andValue:bakedArray];
	}
}

- (NSArray *)getPaidChannels {
	NSArray *res = Nil;

	@synchronized(self)
	{
		res = [self.groupData objectForKey:@"paidChannels"];
	}
	return res;
}

- (NSArray *)getPaidChannelsRemote {
	if (self.fingerprint) {
		NSString *url = [[[APPVIEW_URL stringByAppendingString:@"/group"] stringByAppendingFormat:@"/%@", self.fingerprint] stringByAppendingString:@"/paidChannels"];
		NSString *config_str = [self contentsOfUrl:url];
		SBJsonParser *parse = [[SBJsonParser alloc] init];
		NSArray *conf =  [parse objectWithString:config_str];
		// NSLog(@"CONFIG : %@", conf);
		[parse release];

		return conf;
	}
	return [NSArray array];
}

- (NSDictionary *)getRentalData:(NSDictionary *)asset {
	if (self.fingerprint) {
		NSArray *conf = Nil;
		@synchronized(self)
		{
			conf =  [self.groupData objectForKey:@"rentedMovies"];
		}
		NSMutableArray *bakedArray = [NSMutableArray arrayWithArray:conf];
		NSString *aid = [asset objectForKey:@"id"];
		for (int i = 0; i < [bakedArray count]; i++) {
			NSDictionary *dict = [bakedArray objectAtIndex:i];
			NSString *str = [dict objectForKey:@"id"];
			if ([str isEqualToString:aid]) {
				return dict;

				break;
			}
		}
	}
	return Nil;
}

- (NSDictionary *)getRentalDataRemote:(NSDictionary *)asset {
	if (self.fingerprint) {
		NSString *url = [[[APPVIEW_URL stringByAppendingString:@"/group"] stringByAppendingFormat:@"/%@", self.fingerprint] stringByAppendingString:@"/rentedMovies"];
		NSString *config_str = [self contentsOfUrl:url];
		SBJsonParser *parse = [[SBJsonParser alloc] init];
		NSArray *conf =  [parse objectWithString:config_str];
		// NSLog(@"CONFIG : %@", conf);
		[parse release];


		NSMutableArray *bakedArray = [NSMutableArray arrayWithArray:conf];
		NSString *aid = [asset objectForKey:@"id"];
		for (int i = 0; i < [bakedArray count]; i++) {
			NSDictionary *dict = [bakedArray objectAtIndex:i];
			NSString *str = [dict objectForKey:@"id"];
			if ([str isEqualToString:aid]) {
				return dict;

				break;
			}
		}
	}
	return Nil;
}

- (int)getRatingRemote:(NSDictionary *)asset {
	if (self.fingerprint) {
		NSString *url = [[[APPVIEW_URL stringByAppendingString:@"/group"] stringByAppendingFormat:@"/%@", self.fingerprint] stringByAppendingString:@"/movieRating"];
		NSString *config_str = [self contentsOfUrl:url];
		SBJsonParser *parse = [[SBJsonParser alloc] init];
		NSArray *conf =  [parse objectWithString:config_str];
		// NSLog(@"CONFIG : %@", conf);
		[parse release];

		NSMutableArray *bakedArray = [NSMutableArray arrayWithArray:conf];
		NSString *aid = [asset objectForKey:@"id"];
		for (int i = 0; i < [bakedArray count]; i++) {
			NSDictionary *dict = [bakedArray objectAtIndex:i];
			NSString *str = [dict objectForKey:@"id"];
			if ([str isEqualToString:aid]) {
				NSNumber *rating = [dict objectForKey:@"rating"];
				return [rating intValue];

				break;
			}
		}
	}
	return 0;
}

- (int)getRating:(NSDictionary *)asset {
	if (self.fingerprint) {
		NSArray *conf = Nil;
		@synchronized(self)
		{
			conf = [self.groupData objectForKey:@"movieRating"];
		}
		NSMutableArray *bakedArray = [NSMutableArray arrayWithArray:conf];
		NSString *aid = [asset objectForKey:@"id"];
		for (int i = 0; i < [bakedArray count]; i++) {
			NSDictionary *dict = [bakedArray objectAtIndex:i];
			NSString *str = [dict objectForKey:@"id"];
			if ([str isEqualToString:aid]) {
				NSNumber *rating = [dict objectForKey:@"rating"];
				return [rating intValue];

				break;
			}
		}
	}
	return -1;
}

- (void)setRating:(NSDictionary *)asset rating:(int)rating {
	if (self.fingerprint) {
		NSArray *conf = [[self getGroupData] objectForKey:@"movieRating"];

		// NSLog(@"CONFIG : %@", conf);
		NSString *aid = [asset objectForKey:@"id"];
		NSMutableDictionary *good = Nil;
		int goodIndex = 0;
		NSMutableArray *bakedArray = [NSMutableArray arrayWithArray:conf];

		for (int i = 0; i < [bakedArray count]; i++) {
			NSDictionary *dict = [bakedArray objectAtIndex:i];
			NSString *str = [dict objectForKey:@"id"];
			if ([str isEqualToString:aid]) {
				goodIndex = i;

				good = [NSMutableDictionary dictionaryWithDictionary:dict];

				break;
			}
		}

		if (!good) {
			good = [NSMutableDictionary dictionary];
			[good setObject:aid forKey:@"id"];
			[bakedArray addObject:good];
			goodIndex = [bakedArray count] - 1;
		}


		[good setObject:[NSNumber numberWithInt:rating] forKey:@"rating"];
		[bakedArray replaceObjectAtIndex:goodIndex withObject:good];

		[self postGroupDataWithKey:@"movieRating" andValue:bakedArray];
	}
}

- (void)setRental:(NSDictionary *)asset rental:(NSDictionary * )rental {
	if (self.fingerprint) {
		NSArray *conf =  [[self getGroupData] objectForKey:@"rentedMovies"];

		NSString *aid = [asset objectForKey:@"id"];
		NSMutableDictionary *good = Nil;
		int goodIndex = 0;
		NSMutableArray *bakedArray = [NSMutableArray arrayWithArray:conf];

		for (int i = 0; i < [bakedArray count]; i++) {
			NSDictionary *dict = [bakedArray objectAtIndex:i];
			NSString *str = [dict objectForKey:@"id"];
			if ([str isEqualToString:aid]) {
				goodIndex = i;

				good = [NSMutableDictionary dictionaryWithDictionary:dict];
				;

				break;
			}
		}

		if (!good) {
			good = [NSMutableDictionary dictionaryWithDictionary:rental];
			[bakedArray addObject:good];
			goodIndex = [bakedArray count] - 1;
		}

		[bakedArray replaceObjectAtIndex:goodIndex withObject:rental];

		[self postGroupDataWithKey:@"rentedMovies" andValue:bakedArray];
	}
}

- (ASIHTTPRequest *)sendRequest:(NSString *)url {
	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:url]];

	[request addRequestHeader:@"X-Session" value:[self getSession] ];
	// [request addRequestHeader:@"Content-Type" value:@"application/json"];
	[request startSynchronous];
	NSLog(@"URL: %@", request.url);
	NSLog(@"Session: %@", [self getSession]);

	return request;
}

- (NSString *)contentsOfUrl:(NSString *)url {
	ASIHTTPRequest *sentRequest = [self sendRequest:url];

	NSError *error = [sentRequest error];

	if (!error) {
		NSString *res = [[sentRequest responseString] copy];
		return res;
	}

	NSLog(@"ContentsOfUrl Error: %@", error.localizedDescription);
	return Nil;
}

- (NSDictionary *)loadConfig {
	NSString *url = [APPVIEW_URL stringByAppendingString:@"/config"];
	// NSLog(@"CONFIG URL: %@", url);
	NSString *config_str = [self contentsOfUrl:url];

	// [NSString stringWithContentsOfURL:[NSURL URLWithString: url] encoding:NSUTF8StringEncoding error:Nil];

	NSLog(@"CONFIGSTR : %@", config_str);
	SBJsonParser *parse = [[SBJsonParser alloc] init];
	NSDictionary *conf =  [parse objectWithString:config_str];

	// NSLog(@"CONFIG :\n\n\n %@\n\n\n", conf);


	[parse release];


	return [[conf copy] autorelease];
}

- (NSDictionary *)createConfig {
	NSArray *configValueArray = [[NSArray alloc] initWithObjects:ACCOUNT_ID, ADMIN_AUTHENTICATION_DIRECTORY_ID, ADMIN_AUTHENTICATION_ENDPOINT,
								 AUTHENTICATION_ENDPOINT_SCHEMA, CATEGORY_BASE_URL, END_USER_AUTHENTICATION_DIRECTORY_ID, END_USER_AUTHENTICATION_ENDPOINT, FEED_BASE_URL,
								 FEED_PID, FEED_REGISTRY_ID, FORMAT, MEDIA_OBJECT_URL, MOVIE_CAT_ID, PAGE_SIZE, PRETTY, RATING_ENDPOINT_URL, SCHEMA, TV_CLIP_CAT_ID,
								 TV_EPISODE_CAT_ID, TV_SERIES_CAT_ID, Nil];
	NSArray *configKeyArray = [[NSArray alloc] initWithObjects:@"accountId", @"adminAuthenticationDirectoryId", @"adminAuthenticationEndpoint",
							   @"authenticationEndpointSchema", @"categoryBaseUrl", @"endUserAuthenticationDirectoryId", @"endUserAuthenticationEndpoint",
							   @"feedBaseUrl", @"feedPid", @"feedRegistryId", @"format", @"mediaObjectUrl", @"movieCatId", @"pageSize", @"pretty",
							   @"ratingEndPointUrl", @"schema", @"tvClipCatId", @"tvEpisodeCatId", @"tvSeriesCatId", Nil];

	NSMutableDictionary *configDict = [[NSMutableDictionary alloc] initWithCapacity:20];

	for (int i = 0; i < [configKeyArray count]; i++) {
		[configDict setObject:[configValueArray objectAtIndex:i] forKey:[configKeyArray objectAtIndex:i]];
	}
	[configValueArray release];
	[configKeyArray release];

	NSMutableDictionary *thePlatformOnDemandDict = [[NSMutableDictionary alloc] init];
	[thePlatformOnDemandDict setObject:configDict forKey:@"config"];
	[thePlatformOnDemandDict setObject:PLUGIN_KEY forKey:@"pluginKey"];
	[thePlatformOnDemandDict setObject:ENABLED forKey:@"enabled"];
	NSMutableDictionary *confDict =  [[NSMutableDictionary alloc] init];
	[confDict setObject:thePlatformOnDemandDict forKey:@"com.accedo.plugin.ThePlatformOnDemand"];

	[thePlatformOnDemandDict release];

	NSDictionary *conf = [NSDictionary dictionaryWithDictionary:confDict];

	[confDict release];

	return [[conf copy] autorelease];
}

- (NSDictionary *)loadMetadata {
	NSString *url = [APPVIEW_URL stringByAppendingString:@"/metadata"];

	// NSLog(@"METADATA URL: %@ \n", url);
	// NSString * config_str = [NSString stringWithContentsOfURL:[NSURL URLWithString: url] encoding:NSUTF8StringEncoding error:Nil];
	NSString *config_str = [self contentsOfUrl:url];

	if (config_str) {
		NSDictionary *metadata = [config_str JSONValue];

		NSMutableDictionary *newMetadata = [[NSMutableDictionary alloc] init];
		{
			NSString *homePanel = [metadata objectForKey:@"homePanel"];
			NSArray *homePanelArray = [homePanel JSONValue];
			NSString *link = [metadata objectForKey:@"link"];
			NSDictionary *linkDict = [link JSONValue];
			NSString *promotion = [metadata objectForKey:@"promotion"];
			NSDictionary *promotionDict = [promotion JSONValue];
			NSString *navigation = [metadata objectForKey:@"navigation"];
			NSArray *navigationArray = [navigation JSONValue];

			[newMetadata setObject:homePanelArray forKey:@"homePanel"];
			[newMetadata setObject:linkDict forKey:@"link"];
			[newMetadata setObject:promotionDict forKey:@"promotion"];
			[newMetadata setObject:navigationArray forKey:@"navigation"];
		}

		return [[newMetadata copy] autorelease];

		//		else {
		//			SBJsonParser *parse = [[SBJsonParser alloc] init];
//			NSDictionary *metadata =  [parse objectWithString:config_str];
//
//			// NSLog(@"METADATASTR : %@ \n", config_str);
//			NSLog(@"METADATA : \n\n\n%@ \n\n\n", metadata);
//
//			[parse release];
//			return [[metadata copy] autorelease];
//		}
	}
	else {
		return [NSDictionary dictionary];
	}
}

- (void)loadAssets {
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSString *localLastModified = [defaults valueForKey:@"TVEAssetsDate"];

	NSString *url = [APPVIEW_URL stringByAppendingString:@"/asset"];

	NSLog(@"ASSET URL: %@", url);

	ASIHTTPRequest *request = [self sendRequest:url];
	NSString *lastModified = [[request responseHeaders] valueForKey:@"Last-Modified"];


//	if ([lastModified isEqualToString:localLastModified]) {
//		return;
//	}
	if ([lastModified isEqualToString:localLastModified]) {
		// check whether there is cache directory or not
		NSString *saveDirectory = [NSSearchPathForDirectoriesInDomains (NSCachesDirectory, NSUserDomainMask, YES)lastObject];
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSError *error;
		NSArray *cacheFiles = [fileManager contentsOfDirectoryAtPath:[saveDirectory stringByAppendingPathComponent:@"AppViewAssets"] error:&error];
		if ([cacheFiles count] != 0) {
			for (NSString *file in cacheFiles) {
				NSString *cachePath = [[saveDirectory stringByAppendingPathComponent:@"AppViewAssets"] stringByAppendingPathComponent:file];
				// check cached data is valid or not
				if ([file isEqualToString:@"promotion"] && [UIImage imageWithData:[NSData dataWithContentsOfFile:cachePath]]) {
					return;
				}
			}
			/* handle error */
		}
	}

//	NSDictionary *assets = [[[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease] JSONValue];

	SBJsonParser *parser = [[SBJsonParser alloc] init];
	NSDictionary *assets = [parser objectWithString:[request responseString]];


	if (!assets) {
		return;
	}

	dispatch_group_t group2 = dispatch_group_create();
	dispatch_queue_t queue2 = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

	for (NSString *key in [assets allKeys]) {
		dispatch_group_async(group2, queue2, ^{
								 NSString *path = [assets valueForKey:key];
								 NSString *url = [APPVIEW_URL stringByAppendingString:path];
								 ASIHTTPRequest *request = [self sendRequest:url];
								 NSData *d = [request responseData];
								 if (!d) {
									 NSLog (@"ASSET FETCH FAILED: %@, %@", key, url);
								 }
								 else {
									 if ([UIImage imageWithData:d]) {
										 NSString *cacheFilePath = [[AppView assetsCacheDirectory] stringByAppendingPathComponent:key];
										 [d writeToFile:cacheFilePath atomically:YES];
										 NSLog (@"ASSET FETCH SUCCESS: %@, %@", key, url);
									 }
								 }
							 });
	}

	dispatch_group_wait(group2, DISPATCH_TIME_FOREVER);
	dispatch_release(group2);

	[defaults setValue:lastModified forKey:@"TVEAssetsDate"];
	[defaults synchronize];
}

- (NSData *)assetByKey:(NSString *)key {
	NSString *cacheFilePath = [[AppView assetsCacheDirectory] stringByAppendingPathComponent:key];

	return [NSData dataWithContentsOfFile:cacheFilePath];
}

+ (NSString *)assetsCacheDirectory {
	static NSString *_cacheDirectory;

	static dispatch_once_t onceToken;

	dispatch_once(&onceToken, ^{
					  NSString *systemCacheDirectory = [NSSearchPathForDirectoriesInDomains (NSCachesDirectory, NSUserDomainMask, YES)lastObject];
					  _cacheDirectory = [systemCacheDirectory stringByAppendingPathComponent:@"AppViewAssets"];

					  BOOL directory;
					  BOOL exists = [[NSFileManager defaultManager] fileExistsAtPath:_cacheDirectory isDirectory:&directory];

					  if (exists && directory) {
	                      // we are cool
					  }
					  else {
						  [[NSFileManager defaultManager] createDirectoryAtPath:_cacheDirectory
													withIntermediateDirectories:YES
																	 attributes:Nil
																		  error:Nil];
					  }

					  [_cacheDirectory retain];
				  });

	return _cacheDirectory;
}

- (void)loadChannelsAndEPG {
	NSLog(@"LOADING EPG DATA.......");

	NSString *chaCatStr;
	NSString *epgStr;

	NSString *chaCatPath = [[NSBundle mainBundle] pathForResource:@"channel_category" ofType:@"json"];

	NSError *chaCatError;
	chaCatStr = [NSString stringWithContentsOfFile:chaCatPath encoding:NSUTF8StringEncoding error:&chaCatError];
	// error checking omitted

	NSString *epgPath = [[NSBundle mainBundle] pathForResource:@"epg" ofType:@"json"];

	NSError *epgError;
	epgStr = [NSString stringWithContentsOfFile:epgPath encoding:NSUTF8StringEncoding error:&epgError];
	// error checking omitted

	epgStr = [epgStr stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
//	}

	static NSString *c = @"channel";

	NSDictionary *chaCat = [chaCatStr JSONValue];

	// NSLog(@"\n\n\n\nPARSED CATEGORIES: %@", chaCat);

	self.categories = [chaCat allKeys];

	NSMutableArray *channels = [NSMutableArray array];
	for (NSArray *categoryChannels in [chaCat allValues]) {
		for (NSDictionary *_channel in categoryChannels) {
			NSMutableDictionary *channel = [NSMutableDictionary dictionaryWithDictionary:_channel];
			NSString *channelId = [[_channel valueForKey:c] description];
			[channel setValue:channelId forKey:c];
			[channels addObject:channel];
		}
	}

	self.channels = [NSMutableArray arrayWithArray:[channels sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"channel" ascending:YES]]]];


	// NSLog(@"\n\n\n\nPARSED CHANNELS: %@", channels);

	// @synchronized(self){
	//   self.channels = [[channels copy] autorelease];
	// }



	// NSLog(@"XXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
	// NSLog(@"XXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
	// NSLog(@"XXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
	// NSLog(@"XXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
	// NSLog(@"XXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
	// NSLog(@"CHANNELS:\n%d", [self.channels count]);

	NSDate *localDay = [NSDate localDay];
	NSTimeInterval localDayTimeInterval = [localDay timeIntervalSince1970];
	int localDayDays = (int)localDayTimeInterval / 86400;
	localDayTimeInterval = (double)localDayDays * 86400.0;

	NSDateFormatter *f1 = [[[NSDateFormatter alloc] init] autorelease];
	f1.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
	f1.dateFormat = @"KK:mma";

	NSArray *__epg = [epgStr JSONValue];
	NSMutableArray *epg = [NSMutableArray array];
	for (NSDictionary *_channel in __epg) {
		NSString *channelId = [[_channel valueForKey:@"channel"] description];
		NSString *category = [_channel valueForKey:@"category"];
		NSMutableDictionary *channel = [NSMutableDictionary dictionaryWithDictionary:_channel];
		[channel setValue:category forKey:@"category"];
		[channel setValue:channelId forKey:@"channel"];


		NSArray *__programs = [channel valueForKey:@"events"];
		NSMutableArray *programs = [NSMutableArray array];
		for (NSDictionary *__program in __programs) {
			NSMutableDictionary *program = [NSMutableDictionary dictionary];

			NSRange r;

			NSString *title = [__program valueForKey:@"title"];
			r = [title rangeOfString:@"####"];
			if (NSNotFound != r.location) {
				title = [title substringToIndex:r.location];
			}

			NSString *desc = [__program valueForKey:@"desc"];
			r = [desc rangeOfString:@"####"];
			if (NSNotFound != r.location) {
				desc = [desc substringToIndex:r.location];
			}

			[program setValue:title forKey:@"title"];
			[program setValue:desc forKey:@"desc"];

			NSNumber *startTime = [__program valueForKey:@"startTime"];
			NSNumber *endTime = [__program valueForKey:@"endTime"];

			NSTimeInterval startTimeInterval = [startTime doubleValue] / 1000.0;
			NSTimeInterval endTimeInterval = [endTime doubleValue] / 1000.0;
			NSTimeInterval durationInterval = endTimeInterval - startTimeInterval;

			if (durationInterval < 1) {
				continue;
			}

			int originalDays = (int)startTimeInterval / 86400;
			NSTimeInterval originalHours = startTimeInterval  - (double)(originalDays * 86400);

			startTimeInterval = localDayTimeInterval + originalHours;

			[program setValue:@ (startTimeInterval) forKey:@"startTimeSeconds"];
			[program setValue:@ (durationInterval) forKey:@"durationSeconds"];
			[program setValue:channelId forKey:@"channel"];

			NSDate *sd = [NSDate dateWithTimeIntervalSince1970:startTimeInterval];
			NSDate *ed = [NSDate dateWithTimeIntervalSince1970:startTimeInterval + durationInterval];

			[program setValue:sd forKey:@"startDate"];
			[program setValue:ed forKey:@"endDate"];
			[program setValue:[NSString stringWithFormat:@"%@ - %@", [f1 stringFromDate:sd], [f1 stringFromDate:ed]] forKey:@"KKmma"];

			[programs addObject:program];
		}

		[channel setValue:programs forKey:@"events"];

		[epg addObject:channel];
	}
	self.epg = [epg sortedArrayUsingComparator:^NSComparisonResult (id id1, id id2) {
					static NSString *c = @"category";
					NSString *c1 = [((NSDictionary *)id1) valueForKey:c];
					NSString *c2 = [((NSDictionary *)id2) valueForKey:c];
					return [c1 compare:c2];
				}];
}

- (NSArray *)searchChannels:(NSString *)query {
	NSArray *all =  [NSArray arrayWithArray:self.channels];

	if (!query || [@"" isEqualToString:query]) {
		return all;
	}
	// NSLog(@"ALL %@", all);

	NSPredicate *predicateString = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"name", query];
	// keySelected is NSString itself
	// NSLog(@"predicate %@",predicateString);
	// NSPredicate *predicate = [NSPredicate predicateWithFormat:
	//                        @"name like %@", query];
	NSArray *filteredArray = [all filteredArrayUsingPredicate:predicateString];
	// NSLog(@"%@", filteredArray);


	return filteredArray;
}

- (NSArray *)allChannels {
	if ([self.channels count] == 0) {
		return Nil;
	}
	return [NSArray arrayWithArray:self.channels];
}

- (NSArray *)allCategories {
	return [NSArray arrayWithArray:self.categories];
}

- (NSDictionary *)channelByChannelId:(NSString *)channelId {
	for (NSDictionary *channel in self.channels) {
		if ([[channel valueForKey:@"channel"] isEqualToString:channelId]) {
			return channel;
		}
	}
	return Nil;
}

- (NSArray *)channelsOfCategory:(NSString *)category {
	NSMutableArray *result = [NSMutableArray array];

	for (NSDictionary *channel in self.channels) {
		if ([[channel valueForKey:@"category"] isEqualToString:category]) {
			[result addObject:channel];
		}
	}
	return result;
}

- (NSArray *)epgByCategories:(NSArray *)categories {
	if (![categories count]) {
		categories = self.categories;
	}
	static NSString *c = @"category";
	NSMutableArray *epg = [NSMutableArray array];

	for (NSDictionary *channel in self.channels) {
		NSString *category = [channel valueForKey:c];
		if ([categories containsObject:category]) {
			[epg addObject:channel];
		}
	}
	return epg;
}

- (NSString *)getEPGPeriod:(NSDictionary *)program {
	NSDate *from = [NSDate dateWithTimeIntervalSinceReferenceDate:[[program objectForKey:@"startTimeSeconds"] doubleValue]];
	NSDate *to = [NSDate dateWithTimeIntervalSinceReferenceDate:[[program objectForKey:@"startTimeSeconds"] doubleValue] + [[program objectForKey:@"durationSeconds"] doubleValue]];


	NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];

	[dateFormat setDateFormat:@"hh:mma"];
	[dateFormat setAMSymbol:@"am"];
	[dateFormat setPMSymbol:@"pm"];
	NSString *fromString = [dateFormat stringFromDate:from];
	NSString *toString = [dateFormat stringFromDate:to];
	NSString *res = [NSString stringWithFormat:@"%@ - %@", fromString, toString];
	[dateFormat release];
	// NSLog(@"INTERVAL: %@",res);
	return res;
}

- (NSDictionary *)currentEPGProgramOfChannel:(NSDictionary *)channel programsOffset:(int)offset {
	return [self currentEPGProgramOfChannelId:[channel valueForKey:@"channel"] programsOffset:offset];
}

- (NSDictionary *)currentEPGProgramOfChannelId:(NSString *)channelId programsOffset:(int)offset {
	NSTimeInterval elapsedIntervalSinceEpoch = [[[NSDate date] dateByAddingTimeInterval:[[NSTimeZone localTimeZone] secondsFromGMT]] timeIntervalSince1970];

	for (NSDictionary *channel in self.epg) {
		NSString *_channelId = [channel valueForKey:@"channel"];
		if ([channelId isEqualToString:_channelId]) {
			NSArray *programs = [channel valueForKey:@"events"];
			for (int i = 0; i < [programs count]; i++) {
				NSDictionary *program = [programs objectAtIndex:i];
				NSTimeInterval startTimeInterval = [[program valueForKey:@"startTimeSeconds"] doubleValue];
				NSTimeInterval durationInterval = [[program valueForKey:@"durationSeconds"] doubleValue];
				NSTimeInterval endTimeInterval = startTimeInterval + durationInterval;
				if (startTimeInterval < elapsedIntervalSinceEpoch && elapsedIntervalSinceEpoch <= endTimeInterval) {
					i += offset;
					NSDictionary *result = Nil;
					if (0 <= i && i < [programs count]) {
						result = [programs objectAtIndex:i];
					}
					return result;
				}
			}
		}
	}
	return Nil;
}

- (NSArray *)scheduleForChannelIds:(NSArray *)channelIds startTime:(NSTimeInterval)startTimeInterval durationSeconds:(NSTimeInterval)durationInterval {
	NSTimeInterval endTimeInterval = startTimeInterval + durationInterval;
	NSMutableArray *result = [NSMutableArray array];

	for (NSString *channelId in channelIds) {
		for (NSDictionary *channel in self.epg) {
			if ([[channel valueForKey:@"channel"] isEqualToString:channelId]) {
				for (NSDictionary *program in [channel valueForKey : @"events"]) {
					NSTimeInterval pStartTimeInterval = [[program valueForKey:@"startTimeSeconds"] doubleValue];
					NSTimeInterval pDurationInterval = [[program valueForKey:@"durationSeconds"] doubleValue];
					NSTimeInterval pEndTimeInterval = pStartTimeInterval + pDurationInterval;
					if (pStartTimeInterval <= endTimeInterval && startTimeInterval <= pEndTimeInterval) {
						[result addObject:program];
					}
				}
			}
		}
	}
	return result;
}

@end
