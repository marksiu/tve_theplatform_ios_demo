//
//  FriendsPopupViewController.m
//  TVE
//
//  Created by Mate Beres on 12/20/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "FriendsPopupViewController.h"

@interface FriendsPopupViewController ()
@property (retain) NSMutableArray *friends;
@end

@implementation FriendsPopupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		_friends = [[NSMutableArray alloc] init];

		[_friends addObject:@"Alice Smith"];
		[_friends addObject:@"Andrew Gonzalez"];
		[_friends addObject:@"Joseph Brown"];
		[_friends addObject:@"Ethan Thompson"];
		[_friends addObject:@"Daniel White"];
		[_friends addObject:@"Christopher Martinez"];
		[_friends addObject:@"Michael Baker"];
		[_friends addObject:@"William Rogers"];
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (CGSize)contentSizeForViewInPopover {
	return CGSizeMake(200, 130);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 3;
	// [_friends count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

	if (!cell) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
									   reuseIdentifier:@"cell"] autorelease];
		cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
	}

	cell.textLabel.text = [NSString stringWithFormat:@"%@", [_friends objectAtIndex:indexPath.row]];

	return cell;
}

- (void)dealloc {
	_friends = Nil;

	[super dealloc];
}

@end
