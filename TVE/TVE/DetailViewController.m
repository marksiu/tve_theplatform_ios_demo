//
//  DetailViewController.m
//  TVE
//
//  Created by Mate Beres on 11/29/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "DetailViewController.h"
#import "Constants.h"
#import "OnDemandDetailView.h"
#import "DetailsListView.h"
#import "Constants.h"
#import "AssetView.h"
#import "ThePlatform.h"
#import "SeasonDetailView.h"
#import "AppView.h"
#import "UIImage+Extension.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed : ((float)((rgbValue & 0xFF0000) >> 16)) / 255.0 green : ((float)((rgbValue & 0xFF00) >> 8)) / 255.0 blue : ((float)(rgbValue & 0xFF)) / 255.0 alpha : 1.0]

@interface DetailViewController ()

@property (retain) NSArray *collection;
@property (retain) NSDictionary *data;


@property (retain) SeasonDetailView *sdetailView;
@property (retain) OnDemandDetailView *detailView;
@property (retain) DetailsListView *listView;
@property (retain) UIViewController *leftViewController;
@property (retain) UIViewController *rightViewController;
@property (retain) NSString *title;


@end



@implementation DetailViewController

- (id)initWithData:(NSDictionary *)__data withCollection:(NSArray *)col title:(NSString *)title {
	self = [super init];
	if (self) {
		self.data = __data;
		self.title = title;
		self.collection = col;
	}
	return self;
}

- (void)appLogin:(id)sender {
	// NSLog(@"OnDemandDetailViewController appLogin");
	[self itemSelected:self.data];
}

- (void)viewDidLoad {
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appLogin:)
												 name:@"APP_LOGIN"
											   object:nil];

	self.view.backgroundColor = [UIColor blackColor];
	UIImage *backgroundImage;
	if ([UIImage externalImageNamed:@"background"]) {
		backgroundImage = [UIImage externalImageNamed:@"background"];
	}
	else {
		backgroundImage = [UIImage imageNamed:@"background.png"];
	}
	UIImageView *backgroundImageView = [[[UIImageView alloc] initWithImage:backgroundImage] autorelease];
	[self.view addSubview:backgroundImageView];
	[super viewDidLoad];
	// [self customNavigationBar];



	self.detailView = [[[OnDemandDetailView alloc] initWithFrame:CGRectMake(0, 0, detailRightSize, detailHeight) config:nil] autorelease];
	self.listView = [[[DetailsListView alloc] initWithFrame:CGRectMake(0, 0, detailLeftSize, detailHeight)] autorelease];
	self.sdetailView = [[[SeasonDetailView alloc] initWithFrame:CGRectMake(0, 0, detailRightSize, detailHeight) config:nil] autorelease];
	self.sdetailView.detaildelegate = self;
	self.sdetailView.hidden = YES;
	self.detailView.hidden = YES;
	self.listView.listdelegate = self;
	self.detailView.backgroundColor = [UIColor clearColor];
	self.detailView.detaildelegate = self;

	UIView *leftConteiner = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, detailLeftSize, detailHeight)] autorelease];
	self.leftViewController = [[[UIViewController alloc] init] autorelease];
	self.leftViewController.view = leftConteiner;
	UINavigationController *leftnav = [[[UINavigationController alloc] initWithRootViewController:self.leftViewController] autorelease];

	UIView *rightConteiner = [[[UIView alloc] initWithFrame:CGRectMake(detailLeftSize, 0, detailRightSize, detailHeight)] autorelease];
	self.rightViewController = [[[UIViewController alloc] init] autorelease];
	self.rightViewController.view = rightConteiner;
	UINavigationController *rightNav = [[[UINavigationController alloc] initWithRootViewController:self.rightViewController] autorelease];

	[rightConteiner addSubview:self.detailView];
	[rightConteiner addSubview:self.sdetailView];
	[leftConteiner addSubview:self.listView];



	[self addChildViewController:leftnav];
	[self.view addSubview:leftnav.view];


	[self addChildViewController:rightNav];
	[self.view addSubview:rightNav.view];

	// @stef: Missed a separated tab "l" on top title bar between listing part and details content part
	CGRect sr = self.leftViewController.view.frame;
	sr.origin.x = sr.size.width - 2;
	sr.size.width = 2;
	sr.size.height = 21;

	UIView *sv = [[UIView alloc] initWithFrame:sr];
	UIImageView *iv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top_bar_separtor.png"]];
	iv.clipsToBounds = YES;
	iv.contentMode = UIViewContentModeScaleAspectFit;

	[sv addSubview:iv];

	[self.leftViewController.navigationController.view addSubview:sv];

	[sv release];
	[iv release];

	// bg image adjust
	CGRect bf = backgroundImageView.frame;
	bf.origin.y = self.leftViewController.navigationController.navigationBar.frame.size.height;
	backgroundImageView.frame = bf;

	[self updateOnDemand];
}

- (void)viewWillAppear:(BOOL)animated {
	[[self.view.subviews objectAtIndex:0] setFrame:self.view.bounds];

	self.navigationController.navigationBarHidden = YES;

	self.leftViewController.navigationController.view.frame = CGRectMake(0, 0, detailLeftSize, self.leftViewController.navigationController.view.frame.size.height);
	self.rightViewController.navigationController.view.frame = CGRectMake(detailLeftSize, 0, detailRightSize, self.rightViewController.navigationController.view.frame.size.height);

	UINavigationBar *navBar = [self.rightViewController.navigationController navigationBar];
	[navBar setTintColor:[UIColor blackColor]];

	if ([navBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
		[navBar setBackgroundImage:[UIImage imageNamed:@"top.png"] forBarMetrics:UIBarMetricsDefault];
	}
	else {
		UIImageView *imageView = (UIImageView *)[navBar viewWithTag:12000];
		if (imageView == nil) {
			imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top.png"]];
			[imageView setTag:12000];
			[navBar insertSubview:imageView atIndex:0];
			[imageView release];
		}
	}

	navBar = [self.leftViewController.navigationController navigationBar];
	[navBar setTintColor:[UIColor blackColor]];

	if ([navBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)]) {
		[navBar setBackgroundImage:[UIImage imageNamed:@"top.png"] forBarMetrics:UIBarMetricsDefault];
	}
	else {
		UIImageView *imageView = (UIImageView *)[navBar viewWithTag:12000];
		if (imageView == nil) {
			imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"top.png"]];
			[imageView setTag:12000];
			[navBar insertSubview:imageView atIndex:0];
			[imageView release];
		}
	}

	if (self.title) {
		self.leftViewController.navigationItem.title = self.title;
	}

	self.leftViewController.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(goback:)] autorelease];

	self.rightViewController.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage externalImageNamed:@"logo"]];

	[[self.view.subviews objectAtIndex:0] setFrame:CGRectMake(0, 0, 1024, 768)];


	[self customNavigationBar:self.rightViewController];
}

- (void)goback:(id)sender {
	[self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad1 {
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appLogin:)
												 name:@"APP_LOGIN"
											   object:nil];

	[super viewDidLoad];
	[self customNavigationBar];

	self.view.backgroundColor = [UIColor clearColor];
	UIImage *backgroundImage;
	if ([UIImage externalImageNamed:@"background"]) {
		backgroundImage = [UIImage externalImageNamed:@"background"];
	}
	else {
		backgroundImage = [UIImage imageNamed:@"background.png"];
	}
	UIImageView *backgroundImageView = [[[UIImageView alloc] initWithImage:backgroundImage] autorelease];
	// [backgroundImage release];

	[self.view addSubview:backgroundImageView];
	self.detailView = [[[OnDemandDetailView alloc] initWithFrame:CGRectMake(detailLeftSize, 0, detailRightSize, detailHeight) config:nil] autorelease];
	self.listView = [[[DetailsListView alloc] initWithFrame:CGRectMake(0, 0, detailLeftSize, detailHeight)] autorelease];
	self.sdetailView = [[[SeasonDetailView alloc] initWithFrame:CGRectMake(detailLeftSize, 0, detailRightSize, detailHeight) config:nil] autorelease];
	self.sdetailView.detaildelegate = self;
	self.sdetailView.hidden = YES;
	self.detailView.hidden = YES;
	self.listView.listdelegate = self;


	self.detailView.backgroundColor = [UIColor clearColor];
	self.detailView.detaildelegate = self;
	[self.view addSubview:self.listView];
	[self.view addSubview:self.detailView];
	[self.view addSubview:self.sdetailView];
	// [backgroundImageView release];
	[self updateOnDemand];
}

- (void)pinCorrect:(NSDictionary * )item {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   double now = [[NSDate date] timeIntervalSince1970];
					   NSNumber *rentalDays = [item objectForKey:@"pl1$rentalPeriod"];

					   double expiration = now + [rentalDays integerValue] * (24 * 3600);
	                   // seconds to milliseconds
					   expiration = expiration * 1000;

					   NSDictionary *rentalData = [NSDictionary dictionaryWithObjectsAndKeys:[item objectForKey:@"id"], @"id",
												   [NSNumber numberWithInt:0], @"playbackTime", [NSNumber numberWithDouble:expiration], @"rentalExpiry", nil];

	                   // NSLog(@"RENTAL DATA:%@", rentalData);
					   [[AppView sharedInstance] setRental:item rental:rentalData];
					   dispatch_async (dispatch_get_main_queue (), ^{
										   [self.listView reloadSelected];


										   UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"Rental Succesful" message:@"Your payment was confirmed. Enjoy your movie!" delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease ];
										   [alertView show];
									   });
				   });
}

- (void)itemSelected:(NSDictionary *)item {
	// NSLog(@"SELECTED: %@", item);

	// TVE thePlatform
	// fixed the incorrect redirect issue for the rental movie
	self.data = item;

	NSNumber *isSeries = [item objectForKey:@"pl1$isTVSeries"];

	if (isSeries && [isSeries boolValue]) {
		[self.sdetailView configureView:item];
		// [self.listView setData:self.collection index:0];
		self.sdetailView.hidden = NO;
		self.detailView.hidden = YES;
		NSString *seriesRef = [item objectForKey:@"pl1$seriesRef"];

		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
						   NSDictionary *rel = [[ThePlatform sharedInstance] getEpisodesBySeries:seriesRef];
						   NSArray *entries = [rel objectForKey:@"entries"];
						   if (entries && [entries count] > 0) {
							   dispatch_async (dispatch_get_main_queue (), ^{
												   [self.sdetailView setContent:entries type:YES];
											   });
						   }
					   });


		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
						   NSDictionary *rel = [[ThePlatform sharedInstance] getClipsBySeries:seriesRef];
		                   // NSDictionary * rel = [[ThePlatform sharedInstance] getEpisodesBySeries:seriesRef];
						   NSArray *entries = [rel objectForKey:@"entries"];
						   if (entries && [entries count] > 0) {
							   dispatch_async (dispatch_get_main_queue (), ^{
												   [self.sdetailView setContent:entries type:NO];
											   });
						   }
					   });
	}
	else {
		[self.detailView clearRelated];
		[self.detailView configureView:item];
		// [self.listView setData:self.collection index:0];
		self.detailView.hidden = NO;
		self.sdetailView.hidden = YES;

		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
						   NSString *keyWords = [item objectForKey:@"media$keywords"];


		                   // NSLog(@"KEYWORDS: %@", keyWords);
						   if (keyWords && [keyWords length] > 0) {
							   keyWords = [keyWords stringByReplacingOccurrencesOfString:@"," withString:@" "];
							   NSArray *fields = [keyWords componentsSeparatedByString:@" "];
							   NSMutableString *relatedSearch = [NSMutableString string];
							   for (int i = 0; i < [fields count];  i++) {
								   NSString *current = [fields objectAtIndex:i];
								   if ([current length] > 0) {
									   if ([relatedSearch length] == 0) {
										   [relatedSearch appendFormat:@"%@", current];
									   }
									   else {
										   [relatedSearch appendFormat:@"|%@", current];
									   }
								   }
							   }


							   ThePlatform *platform  = [ThePlatform sharedInstance];


							   NSDictionary *config = platform.config;
							   NSDictionary *cfg = [config objectForKey:@"config"];
							   NSString *cathegoryId = [cfg objectForKey:@"movieCatId"];
							   NSString *query =  [NSString stringWithFormat:@"&byCategoryIds=%@", [ThePlatform urlencode:[platform getAssetsByCategory:cathegoryId]]];




		                       // NSLog(@"%@",relatedSearch);
							   NSDictionary *rel = [[ThePlatform sharedInstance] search:[ThePlatform urlencode:relatedSearch] sort:[ThePlatform urlencode:query]];
		                       // [[ThePlatform sharedInstance]
							   NSArray *entries = [rel objectForKey:@"entries"];



							   NSMutableArray *entriesReadWrite = [NSMutableArray array];
							   for (NSDictionary *e in entries) {
								   if ([[e objectForKey:@"id"] isEqualToString:[item objectForKey:@"id"]]) {
									   NSLog (@"Same item removed %@", [e objectForKey:@"id"]);
								   }
								   else {
									   [entriesReadWrite addObject:e];
								   }
							   }




							   dispatch_async (dispatch_get_main_queue (), ^{
												   [self.detailView setRelatedContent:entriesReadWrite];
											   });
						   }
					   });
	}
}

- (void)updateOnDemand {
	int selected = 0;

	for (int i = 0; i < [self.collection count]; i++) {
		NSDictionary *item = [self.collection objectAtIndex:i];
		if ([item objectForKey:@"id"]) {
			NSString *is1 = [item objectForKey:@"id"];
			NSString *is2 = [self.data objectForKey:@"id"];
			if ([is1 isEqualToString:is2]) {
				selected = i;
				break;
			}
		}
		if ([item objectForKey:@"channel"]) {
			NSString *is1 = [item objectForKey:@"channel"];
			NSString *is2 = [self.data objectForKey:@"channel"];
			if ([is1 isEqualToString:is2]) {
				selected = i;
				break;
			}
		}
	}


	[self.listView setData:self.collection index:selected];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
