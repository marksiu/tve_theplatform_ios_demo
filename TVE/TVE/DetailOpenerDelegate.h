//
//  DetailOpenerDelegate.h
//  TVE
//
//  Created by Gabor Bottyan on 12/5/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#ifndef TVE_DetailOpenerDelegate_h
#define TVE_DetailOpenerDelegate_h

@protocol DetailOpenerDelegate <NSObject>
- (void)openDetail:(NSDictionary *)data withCollection:(NSArray *)collection title:(NSString *)title;
- (void)openDetailOndemand:(NSDictionary *)data withCollection:(NSArray *)collection title:(NSString *)title;
// -(void) openPin: (NSDictionary * )item;
@end


@protocol SearchViewControllerDelegate <NSObject>
- (void)doSearch:(NSString *)query;
@end

#endif
