//
//  TVEConfig.m
//  TVE
//
//  Created by Mark Siu on 15/8/13.
//  Copyright (c) 2013 Accedo Broadband. All rights reserved.
//

#import "TVEConfig.h"
#import "RentalMovie.h"
#import <Security/Security.h>
#import "KeychainItemWrapper.h"

@interface TVEConfig ()
@property (retain) NSMutableDictionary *rentalMovieDict;

// only for Live TV Tab pop up message fix
@property (atomic)  BOOL showSubscriptionMessage;

@end

@implementation TVEConfig
static TVEConfig *sharedInstance = nil;

+ (TVEConfig *)sharedConfig {
	static dispatch_once_t onceToken;

	dispatch_once(&onceToken, ^{
					  @synchronized ([TVEConfig class]) {
						  sharedInstance = [[TVEConfig alloc] init];
					  }
				  });
	return sharedInstance;
}

- (id)init {
	if (self = [super init]) {
		// init the rental movie dictionary
		self.rentalMovieDict = [[NSMutableDictionary alloc] init];
		self.showSubscriptionMessage = NO;
	}
	return self;
}

- (void)downloadRentalRecord {
	/*
	 * Retrieve rental record from product feed entitlement API
	 *
	 * URL: http://data.entitlement.theplatform.com/eds/data/Entitlement?
	 *
	 * Parameters:
	 * 1. schema=1.1
	 * 2. from=json
	 * 3. pretty=true
	 * 4. token(admin token)
	 * 5. byUserId
	 * 6. byInWindow(fliter for valid rental movie, no included expiry rental movie)
	 * 7. fields=ent$expirationDate,ent$scopeId (fliter, only retrieve media ID and expiry date)
	 *
	 */

	// retrieve require parameters
	NSString *adminToken = [self getAdminToken];
	NSString *endUserId = [self getEndUserID];
	NSString *nowStr = [self getCurrentTimeString];

	// set up URL connection
	NSURLRequest *url = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://data.entitlement.theplatform.com/eds/data/Entitlement?schema=1.1&form=json&pretty=true&token=%@&byUserId=%@&byInWindow=%@&fields=ent$expirationDate,ent$scopeId", adminToken, endUserId, nowStr]]];
	NSURLResponse *response = nil;
	NSError *error = nil;
	NSData *data = [NSURLConnection sendSynchronousRequest:url
										 returningResponse:&response
													 error:&error ];

	// handle response
	if (error == nil) {
		NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

		int numOfRentalMovie = [[jsonObject objectForKey:@"entryCount"] intValue];
		NSArray *entryArr = [jsonObject objectForKey:@"entries"];

		for (int i = 0; i < numOfRentalMovie; i++) {
			// retrieve move from json
			NSDictionary *record = [entryArr objectAtIndex:i];

			// put media ID and expiry date to RentalMovie object
			RentalMovie *rentalMovieRecord = [[RentalMovie alloc] init];
			rentalMovieRecord.mediaId = [record objectForKey:@"ent$scopeId"];
			rentalMovieRecord.expiryDateInSec = [[record objectForKey:@"ent$expirationDate"] longLongValue];

			// put RentalMovie object into instance dictionary
			[self.rentalMovieDict setObject:(id) rentalMovieRecord forKey:rentalMovieRecord.mediaId];
		}
	}
}

// Return Type: NSMutableDictionary
// Key: media ID
// Value: RentalMovie object
- (NSMutableDictionary *)getRentalMovieRecord {
	return self.rentalMovieDict;
}

- (void)clearRentalMovieRecord {
	[self.rentalMovieDict removeAllObjects];
}

- (void)renewRentalMovie {
	// clear existing rental movie record
	[self clearRentalMovieRecord];

	// retrieve rental movie record
	[self downloadRentalRecord];
}

// Retrieve data from media feed by media ID
- (NSDictionary *)getRentalMovieDataFromMediaFeed {
	NSMutableDictionary *rentalMovieDic = [self getRentalMovieRecord];

	if ([rentalMovieDic count] == 0) {
		return nil;
	}

	NSArray *rentalMovieArr = [rentalMovieDic allKeys];
	NSMutableArray *digitalOfMediaIdArr = [[NSMutableArray alloc] init];

	// retrieve the key from NSMutableDictionary
	// The key value of Dictionary = media ID
	// Format of media ID: http://data.media.theplatform.com/media/data/Media/12345678901
	for (int i = 0; i < [rentalMovieArr count]; i++) {
		NSString *fullMediaId = [rentalMovieArr objectAtIndex:i];
		NSArray *mediaIDArr = [fullMediaId componentsSeparatedByString:@"/Media/"];
		NSString *digitalOfMediaId = [mediaIDArr objectAtIndex:1];
		[digitalOfMediaIdArr insertObject:digitalOfMediaId atIndex:i];
	}

	NSString *mediaIdList = [digitalOfMediaIdArr componentsJoinedByString:@"|"];

	/*
	 * Retrieve movie record from media feed
	 *
	 * URL: http://feed.theplatform.com/f/xBrLJC/DwE21wX1pGqZ?form=json&schema=1.2&byId=%@&pretty=true
	 *
	 * Parameters:
	 * 1. schema=1.2
	 * 2. from=json
	 * 3. pretty=true
	 * 4. Id=<mediaIDwithNumberOnly>, separated by '|' symbol
	 *
	 */

	// set up URL connection
	NSString *urlStr = [NSString stringWithFormat:@"http://feed.theplatform.com/f/xBrLJC/DwE21wX1pGqZ?form=json&schema=1.2&byId=%@&pretty=true", mediaIdList];
	NSURLRequest *url = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
	NSURLResponse *response = nil;
	NSError *error = nil;
	NSData *data = [NSURLConnection sendSynchronousRequest:url
										 returningResponse:&response
													 error:&error ];

	// handle response
	if (error == nil) {
		NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];

		int mediaCount = [[jsonObject objectForKey:@"entryCount"] intValue];

		if (mediaCount != 0) {
			return jsonObject;
		}
	}

	return nil;
}

// only for Live TV Tab pop up message fix
- (BOOL)getShowSubscriptionMessage {
	return self.showSubscriptionMessage;
}

- (void)updateShowSubscriptionMessage:(BOOL)showMessage {
	self.showSubscriptionMessage = showMessage;
}

#pragma mark - privacy method

#pragma mark - time
- (NSString *)getCurrentTimeString {
	double now = [[NSDate date] timeIntervalSince1970] * 1000;
	NSString *nowStr = [NSString stringWithFormat:@"%.0f", now];

	return nowStr;
}

#pragma mark - keychain
- (NSArray *)getKeyChainArr {
	KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc] initWithIdentifier:@"login" accessGroup:nil];
	NSString *keyChainData = [keychainItem objectForKey:(id)kSecValueData];
	NSArray *keyChainArr = [keyChainData componentsSeparatedByString:@"#*#"];

	return keyChainArr;
}

- (NSString *)getAdminToken {
	NSArray *keyChainArr = [self getKeyChainArr];
	NSString *adminToken = [keyChainArr objectAtIndex:0];

	return adminToken;
}

- (NSString *)getEndUserID {
	NSArray *keyChainArr = [self getKeyChainArr];
	NSString *endUserId = [keyChainArr objectAtIndex:2];

	return endUserId;
}

@end
