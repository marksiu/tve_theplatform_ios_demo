//
//  SeriesViewController.h
//  TVE
//
//  Created by Gabor Bottyan on 12/3/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "BaseViewController.h"
#import "DetailViewController.h"
#import "DetailsListView.h"
@interface SeriesViewController : BaseViewController <DetailsListViewDelegate>{
}
- (id)initWithFeed:(NSDictionary *)desc;
- (id)initWithData:(NSDictionary *)__data withCollection:(NSArray *)col;
@end
