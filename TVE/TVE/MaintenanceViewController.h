//
//  MaintenanceViewController.h
//  TVE
//
//  Created by Zoltan Gobolos on 12/31/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaintenanceViewController : UIViewController

@property (retain) NSString *message;

@end
