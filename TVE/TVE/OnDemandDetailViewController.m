//
//  OnDemandDetailViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 12/4/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "OnDemandDetailViewController.h"
#import "OnDemandViewController.h"
#import "OnDemandDetailView.h"
#import "DetailsListView.h"
#import "Constants.h"
#import "SBJson.h"
#import "SeasonDetailView.h"
#import "AppView.h"
#import "UIImage+Extension.h"
@interface OnDemandDetailViewController ()

@property (retain) NSDictionary *feed;
@property (retain) OnDemandDetailView *detailView;
@property (retain) SeasonDetailView *sdetailView;
@property (retain) DetailsListView *listView;
@property (retain) NSDictionary *currentItem;

@end

@implementation OnDemandDetailViewController

- (id)initWithFeed:(NSDictionary *)desc {
	self = [super init];
	if (self) {
		self.feed = desc;
		// Custom initialization
	}
	return self;
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	self.feed = Nil;
	self.detailView = Nil;
	self.sdetailView = Nil;
	self.listView = Nil;
	self.currentItem = Nil;
	[super dealloc];
}

- (void)fetchData {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   NSString *url = Nil;
	                   // NSLog(@"%@", self.feed);
					   NSDictionary *f = [self.feed objectForKey:@"feed"];
					   if (f) {
						   url = [f objectForKey:@"url"];
					   }
					   else {
						   url = [self.feed objectForKey:@"url"];
					   }

					   NSString *u = [NSString stringWithFormat:@"%@?form=json&pretty=true", url];

					   NSString *content = [NSString stringWithContentsOfURL:[NSURL URLWithString:u] encoding:NSUTF8StringEncoding error:Nil];
					   NSDictionary *c = (NSDictionary *)[content JSONValue];
					   NSArray *entries = [c objectForKey:@"entries"];

					   NSLog (@"Calling %@ - > %d", url, [entries count]);
					   dispatch_async (dispatch_get_main_queue (), ^{
										   [self.listView setData:entries index:0];
										   [self.detailView setRelatedContent:entries];
									   });
				   });
}

- (void)itemSelected:(NSDictionary *)item {
	// NSLog(@"SELECTED: %@", item);
	self.currentItem = item;

	NSNumber *isSeries = [item objectForKey:@"pl1$isTVSeries"];
	if (isSeries && [isSeries boolValue]) {
		[self.sdetailView configureView:item];
		self.sdetailView.hidden = NO;
		self.detailView.hidden = YES;
	}
	else {
		[self.detailView configureView:item];
		self.detailView.hidden = NO;
		self.sdetailView.hidden = YES;
	}
}

- (void)appLogin:(id)sender {
	NSLog(@"OnDemandDetailViewController appLogin");
	[self itemSelected:self.currentItem];
}

- (void)viewDidLoad {
	[super viewDidLoad];
	[self customNavigationBar];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(appLogin:)
												 name:@"APP_LOGIN"
											   object:nil];


	self.view.backgroundColor = [UIColor redColor];
	UIImage *backgroundImage = [UIImage externalImageNamed:@"background" ofCollection:[AppView sharedInstance].images];
	UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
	[backgroundImage release];
	[self.view addSubview:backgroundImageView];
	self.detailView = [[[OnDemandDetailView alloc] initWithFrame:CGRectMake(detailLeftSize, 0, detailRightSize, detailHeight) config:nil] autorelease];
	self.sdetailView = [[[SeasonDetailView alloc] initWithFrame:CGRectMake(detailLeftSize, 0, detailRightSize, detailHeight) config:nil] autorelease];

	self.detailView.detaildelegate = self;

	self.listView = [[[DetailsListView alloc] initWithFrame:CGRectMake(0, 0, detailLeftSize, detailHeight)] autorelease];
	self.listView.listdelegate = self;
	self.detailView.backgroundColor = [UIColor clearColor];
	[self.view addSubview:self.listView];
	[self.view addSubview:self.detailView];
	[self.view addSubview:self.sdetailView];

	[backgroundImageView release];
	[self fetchData];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
