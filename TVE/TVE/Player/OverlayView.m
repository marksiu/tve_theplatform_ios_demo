//
//  OverlayView.m
//  CMore
//
//  Created by Gabor Bottyan on 8/6/12.
//  Copyright (c) 2012 Meganet-Soft Bt. All rights reserved.
//

#import "OverlayView.h"
#import <AccedoConnect/AccedoConnect.h>
#import <QuartzCore/QuartzCore.h>
#import "FontLabel.h"
#import "AppDelegate.h"

#import "CMOREPlayerViewController.h"

#define  minalpha 0.02f

@interface OverlayView () {
	BOOL animating;

	double mCurrentScale;
}

@property (retain, nonatomic) UIActivityIndicatorView *spinner;

@property (retain) UINavigationBar *navbar;
@property (retain) UIView *controls;
@property (retain) UIView *hideable;
@property (retain) UILabel *timeLabel;
@property (retain) UIButton *timeButton;

@property (retain) UILabel *playedLabel;
@property (retain) UISlider *progressSlider;
@property (retain) UISlider *volumeSlider;
@property (retain) MPVolumeView *systemVolumeSlider;

@property (retain) UIButton *prevButton;
@property (retain) UIButton *nextButton;
@property (assign) BOOL sliderLock;
@property (assign) BOOL volumeLock;
@property (retain) UIPopoverController *popup;
@property (assign) double lastSeen;
@property (retain) NSDateFormatter *timeFormatter;
@property (retain) UILabel *fontLabel;
// @property (retain) FontLabel * fontLabel;
@property (retain) NSTimer *timer;
@property (retain) UIButton *refreshButton;

@property (nonatomic, retain) UIButton *fullScreenButton;
@end

@implementation OverlayView

#define CONTROLS_BG_TAG 200

@synthesize navbar, controls, progressSlider;
@synthesize moviePlayer, smartTVButton, timeFormatter;
@synthesize fontLabel, timer, hideable;
@synthesize refreshButton;
@synthesize systemVolumeSlider;
@synthesize displayLink;
@synthesize finito;
@synthesize isLive;
@synthesize timeLabel, timeButton;
@synthesize playedLabel;

@synthesize spinner;

@synthesize fullScreenButton;

- (void)updateLive:(BOOL)_live {
	self.isLive = _live;

	if (_live) {
		[self updateControlsWithActiveButtons:[NSArray arrayWithObjects:self.smartTVButton, nil]];
		self.progressSlider.hidden = YES;
		self.playedLabel.hidden = YES;
		self.timeLabel.hidden = YES;
	}
	else {
		[self updateControlsWithActiveButtons:[NSArray arrayWithObjects:
											   self.refreshButton,
		                                       // self.prevButton,
											   self.pauseButton, self.playButton,
		                                       // self.nextButton,
											   self.smartTVButton,
											   nil]];
		self.progressSlider.hidden = NO;
		self.playedLabel.hidden = YES;
		self.timeLabel.hidden = NO;
	}

	// self.systemVolumeSlider.backgroundColor = [UIColor greenColor];
	// self.systemVolumeSlider.alpha = 0.9f;

	self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(update:)];
	[self.displayLink setFrameInterval:30];
	[self.displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];

	// self.pauseButton.backgroundColor = [UIColor clearColor];
	// [self.systemVolumeSlider sizeToFit];
}

- (void)dealloc {
	// self.finito  =YES;

	self.delegate = nil;

	if (timer) {
		[timer invalidate];
		self.timer = nil;
	}

	self.moviePlayer = nil;

	if (self.displayLink) {
		[self.displayLink invalidate];
		self.displayLink = nil;
	}

	self.systemVolumeSlider = Nil;
	self.refreshButton  = Nil;
	self.pauseButton = Nil;
	self.playButton = Nil;
	self.popup = Nil;
	self.fontLabel = Nil;
	self.timeFormatter  = Nil;
	self.navbar = Nil;
	self.timeLabel = Nil;
	self.timeButton = Nil;
	self.playedLabel = Nil;

	[super dealloc];
}

- (void)updateControlsWithActiveButtons:(NSArray *)buttons {
	for (UIView *view in self.controls.subviews) {
		view.hidden = YES;
	}
	[self.controls viewWithTag:CONTROLS_BG_TAG].hidden = NO;
	int i = 0;
	int btnWidth = 60;
	int marginLeft = 20;
	int marginRight = 35;
	int paddingBefore = 15;
	for (UIButton *button in buttons) {
		if (button.superview == self.controls) {
			CGRect btnFrame = CGRectOffset(CGRectOffset(CGRectMake(0, 0, btnWidth, 30),
														marginLeft + i * (btnWidth + paddingBefore), 12), paddingBefore, 0);
			button.frame = btnFrame;
			button.hidden = NO;
			if (button != self.pauseButton) {
				i++;
			}
		}
	}
	if (i == 0 || isLive) {
		self.controls.frame = CGRectMake(0, 1024 - (marginLeft + 3 * (btnWidth + paddingBefore) + marginRight) / 2, marginLeft + 3 * (btnWidth + paddingBefore) + marginRight, 88);
		CGPoint center = self.smartTVButton.center;
		center.x = self.controls.center.x;
		self.smartTVButton.center = center;
	}
	else {
		self.controls.frame = CGRectMake(0, 1024 - (marginLeft + i * (btnWidth + paddingBefore) + marginRight) / 2, marginLeft + i * (btnWidth + paddingBefore) + marginRight, 88);
	}
	[self.controls viewWithTag:CONTROLS_BG_TAG].frame = CGRectMake(0, 0, self.controls.frame.size.width, self.controls.frame.size.height);
	int top = i == 0 ? 35 : 60;
	self.systemVolumeSlider.frame = CGRectMake(20, top, self.controls.frame.size.width - 40, 40);
	self.systemVolumeSlider.hidden = NO;
}

- (void)createControls {
	self.controls = [[[UIView alloc] init] autorelease];
	self.controls.layer.masksToBounds = NO;
	UIImage *panelImage = [UIImage imageNamed:@"panel.png"];
	panelImage = [panelImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 4, 0, 4)];
	UIImageView *bg = [[[UIImageView alloc] initWithImage:panelImage] autorelease];
	bg.tag = CONTROLS_BG_TAG;
	[self.controls addSubview:bg];

	self.refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.refreshButton.showsTouchWhenHighlighted = YES;
	[self.refreshButton setImage:[UIImage imageNamed:@"30.png"] forState:UIControlStateNormal];
	[self.refreshButton addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventTouchUpInside];
	[self.controls addSubview:self.refreshButton];

	self.prevButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.prevButton.showsTouchWhenHighlighted = YES;
	[self.prevButton setImage:[UIImage imageNamed:@"prev.png"] forState:UIControlStateNormal];
	[self.controls addSubview:self.prevButton];
	[self.prevButton addTarget:self action:@selector(prev:) forControlEvents:UIControlEventTouchUpInside];

	self.pauseButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.pauseButton.showsTouchWhenHighlighted = YES;
	[self.pauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
	[self.pauseButton addTarget:self action:@selector(pause:) forControlEvents:UIControlEventTouchUpInside];
	[self.controls addSubview:self.pauseButton];

	self.playButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.playButton.showsTouchWhenHighlighted = YES;
	[self.playButton setImage:[UIImage imageNamed:@"player_play.png"] forState:UIControlStateNormal];
	[self.playButton addTarget:self action:@selector(play:) forControlEvents:UIControlEventTouchUpInside];
	self.playButton.hidden = YES;
	[self.controls addSubview:self.playButton];

	self.nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.nextButton.showsTouchWhenHighlighted = YES;
	[self.nextButton setImage:[UIImage imageNamed:@"next.png"] forState:UIControlStateNormal];
	[self.nextButton addTarget:self action:@selector(next:) forControlEvents:UIControlEventTouchUpInside];
	[self.controls addSubview:self.nextButton];

	// if ([ACManager sharedInstance].isPaired) {
	self.smartTVButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.smartTVButton.showsTouchWhenHighlighted = YES;
	[self.smartTVButton setImage:[UIImage imageNamed:@"smarttv_inactive.png"] forState:UIControlStateNormal];
	[self.smartTVButton addTarget:self action:@selector(smartTV:) forControlEvents:UIControlEventTouchUpInside];
	[self.controls addSubview:self.smartTVButton];
	/*} else {
	 *  self.smartTVButton = nil;
	 * }*/

	self.systemVolumeSlider = [[[MPVolumeView alloc] init] autorelease];
	[systemVolumeSlider sizeToFit];
	systemVolumeSlider.showsRouteButton = NO;
	[self.controls addSubview:self.systemVolumeSlider];
	[systemVolumeSlider release];

	self.fullScreenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.fullScreenButton.showsTouchWhenHighlighted = YES;
	[self.fullScreenButton setImage:[UIImage imageNamed:@"to_fullscreen_icon.png"] forState:UIControlStateNormal];
	[self.fullScreenButton addTarget:self action:@selector(fullscreen:) forControlEvents:UIControlEventTouchUpInside];
	[self.navbar addSubview:self.fullScreenButton];
}

- (id)initWithFrame:(CGRect)frame andDelegate:(id<OverlayViewDelegate>)delegate {
	self = [super initWithFrame:frame];

	if (self) {
		self.delegate = delegate;
		self.timeFormatter = [[[NSDateFormatter alloc] init] autorelease];
		[timeFormatter setDateFormat:@"HH:mm:ss"];
		[timeFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];

		self.spinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
		self.spinner.hidesWhenStopped = YES;

		[self addSubview:self.spinner];

		self.hideable = [[[UIView alloc] initWithFrame:frame] autorelease];
		self.hideable.alpha = minalpha;
		[self setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
		[self.hideable setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
		[self setBackgroundColor:[UIColor clearColor]];

		[self createControls];

		[self.hideable addSubview:self.controls];

		self.navbar = [[[UINavigationBar alloc] initWithFrame:CGRectMake(0, 10, self.frame.size.width, 42)] autorelease];
		self.navbar.barStyle = UIBarStyleBlack;

		// self.navbar.translucent = YES;
		// self.navbar.tintColor = [UIColor blackColor];
		[self.hideable addSubview:self.navbar];

		[self addSubview:self.hideable];

		// UIView * progressHolder = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width - 200, 45)];

		self.progressSlider =  [[[UISlider alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width - 200, 45)] autorelease];
		self.progressSlider.continuous = NO;

		// /[progressHolder addSubview:self.progressSlider];
		// progressHolder.backgroundColor = [UIColor yellowColor];
		[self.progressSlider addTarget:self action:@selector(sliderMoveStart) forControlEvents:UIControlEventTouchDown];
		// [self.progressSlider addTarget:self action:@selector(sliderMoved) forControlEvents:UIControlEventTouchDragEnter];
		// [self.progressSlider addTarget:self action:@selector(sliderMoved) forControlEvents:UIControlEventTouchDragInside];

		// [self.progressSlider addTarget:self action:@selector(sliderMoveEnded) forControlEvents:UIControlEventTouchUpInside];

		[self.progressSlider addTarget:self action:@selector(sliderMoveEnded) forControlEvents:UIControlEventValueChanged];

		UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@""];

		navItem.titleView = self.progressSlider;

		[navbar pushNavigationItem:navItem animated:NO];

		// navItem.leftBarButtonItem  =
		UIBarButtonItem *okbutton = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(ok:)] autorelease];

		navItem.leftBarButtonItems = [NSArray arrayWithObjects:okbutton, self.playedLabel, nil];
		/*
		 * NSString * okString = [[LocalizationSystem sharedLocalSystem]locStringForKey: @"lblDone"];
		 *
		 * navItem.leftBarButtonItem  = [[[UIBarButtonItem alloc] initWithTitle:@"faszom" style:UIBarButtonSystemItemDone target:self	 action:@selector(ok:)] autorelease];
		 */
		self.timeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 30)] autorelease];
		self.timeLabel.hidden = NO;
		self.timeButton = [[[UIButton alloc] initWithFrame:timeLabel.frame] autorelease];
		self.timeButton.backgroundColor = [UIColor clearColor];
		[self.timeButton addTarget:self action:@selector(didChangeCountMode:) forControlEvents:UIControlEventTouchUpInside];

		self.playedLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 30)] autorelease];
		self.playedLabel.hidden = YES;
		[self.timeLabel setFont:[UIFont systemFontOfSize:14]];
		[self.playedLabel setFont:[UIFont systemFontOfSize:14]];
		self.timeLabel.text = @"";
		self.playedLabel.text = @"";

		if (((CMOREPlayerViewController *)self.delegate).containerView) {
			UIView *rightView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 110, 30)] autorelease];

			[rightView addSubview:self.timeLabel];
			[rightView addSubview:self.playedLabel];
			[rightView addSubview:self.timeButton];

			self.fullScreenButton.frame = CGRectMake(76, 6, 18, 18);

			[rightView addSubview:self.fullScreenButton];
			navItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:rightView] autorelease];
		}
		else {
			UIView *rightView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 30)] autorelease];

			[rightView addSubview:self.timeLabel];
			[rightView addSubview:self.playedLabel];
			[rightView addSubview:self.timeButton];

			navItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:rightView] autorelease];
		}


		self.timeLabel.textColor = [UIColor whiteColor];
		self.timeLabel.backgroundColor = [UIColor clearColor];
		self.playedLabel.textColor = [UIColor whiteColor];
		self.playedLabel.backgroundColor = [UIColor clearColor];

		[navItem release];

		UISwipeGestureRecognizer *recognizer;
		recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(smartTV:)];
		[recognizer setDirection:UISwipeGestureRecognizerDirectionUp];
		[self.hideable addGestureRecognizer:recognizer];
		[recognizer release];

		UIPinchGestureRecognizer *recognizer2 = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)];
		[self.hideable addGestureRecognizer:recognizer2];
		[recognizer2 release];


		self.swipeindicationView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 364, 226)] autorelease];
		[self.swipeindicationView setImage:[UIImage imageNamed:@"swipe_message_bg.png"]];
		self.swipeindicationView.center = self.center;
		[self.swipeindicationView setBackgroundColor:[UIColor clearColor]];
		self.swipeindicationView.alpha = 1.0;
		self.swipeindicationView.hidden = YES;

		ZFont *zf = [[FontManager sharedManager] zFontWithName:@"ProximaNova-Bold" pointSize:25.665];

		FontLabel *noticeLabel = [[FontLabel alloc] initWithFrame:CGRectMake(22, 95, 228, 80)];
		[noticeLabel setZFont:zf];
		[noticeLabel setTextColor:[UIColor whiteColor]];
		[noticeLabel setBackgroundColor:[UIColor clearColor]];
		noticeLabel.numberOfLines = 2;
		noticeLabel.lineBreakMode = NSLineBreakByWordWrapping;
		[self.swipeindicationView addSubview:noticeLabel];
		[noticeLabel release];
		[noticeLabel setText:@"Swipe to TV"];

		[self addSubview:self.swipeindicationView];
	}

	return self;
}

- (void)startHidingSwipeIndication {
	self.swipeindicationView.center = self.center;
	self.swipeindicationView.alpha = 1.0;
	self.swipeindicationView.hidden = NO;
	[self bringSubviewToFront:self.swipeindicationView];
	[UIView animateWithDuration:4 delay:2 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
		 self.swipeindicationView.alpha = 0.0;
	 } completion:^(BOOL finished) {
		 self.swipeindicationView.hidden = YES;
	 }];
}

- (void)didChangeCountMode:(id)sender {
	if (self.playedLabel.hidden) {
		self.playedLabel.hidden = NO;
		self.timeLabel.hidden = YES;
	}
	else {
		self.playedLabel.hidden = YES;
		self.timeLabel.hidden = NO;
	}
}

- (void)sliderMoveStart {
	// NSLog(@"slider move start");
	self.sliderLock = YES;
	[self touch];
}

- (void)sliderMoved {
	// NSLog(@"slider move");
	[self touch];
}

- (void)sliderMoveEnded {
	// NSLog(@"slider move end");
	self.moviePlayer.currentPlaybackTime = MIN(self.moviePlayer.duration - 1, self.moviePlayer.duration * self.progressSlider.value);
	self.sliderLock = NO;
	[self touch];
}

- (void)update:(id)sender {
	if (finito) {
		return;
	}

	if (!self.isLive) {
		[self updateButtons];
	}

	double now = [NSDate timeIntervalSinceReferenceDate];

	if (now - self.lastSeen > 4.0f) {
		[self hideHud];
	}

	if (self.moviePlayer) {
		// self.moviePlayer.useApplicationAudioSession = YES;
		if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
			// NSLog (@"DURATION: %f CURENT %f",self.moviePlayer.duration,self.moviePlayer.currentPlaybackTime);
			double remaining = self.moviePlayer.duration - self.moviePlayer.currentPlaybackTime;

			NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:remaining];
			NSDate *playeddate = [NSDate dateWithTimeIntervalSinceReferenceDate:self.moviePlayer.currentPlaybackTime];
			self.timeLabel.text = [@"-" stringByAppendingString:[timeFormatter stringFromDate:date]];
			self.playedLabel.text = [@" " stringByAppendingString:[timeFormatter stringFromDate:playeddate]];
			// NSLog(@"%@",self.playedLabel.text);
			if (self.moviePlayer.duration != 0 && !self.sliderLock) {
				self.progressSlider.value = self.moviePlayer.currentPlaybackTime / self.moviePlayer.duration;
				// NSLog(@"%f  %f %f", self.moviePlayer.currentPlaybackTime, self.moviePlayer.initialPlaybackTime, self.moviePlayer.duration );
			}

			// self.volumeSlider.value = [MPMusicPlayerController applicationMusicPlayer].volume;
		}
	}
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
	// NSLog(@"dismissed");
}

- (void)volumeMoved {
	// NSLog(@"volume move");
	[self touch];
}

- (void)volumeMoveEnded {
	// NSLog(@"volume move end");
	// [MPMusicPlayerController applicationMusicPlayer].volume = self.volumeSlider.value;
	// self.moviePlayer.currentPlaybackTime = self.moviePlayer.duration * self.progressSlider.value;
	self.volumeLock = NO;
}

- (void)volumeMoveStart {
	// NSLog(@"volume move start");
	self.volumeLock = YES;
}

- (void)pause:(id)sender {
	[self.moviePlayer pause];
	[self layoutSubviews];
	[self touch];
}

- (void)play:(id)sender {
	[self.moviePlayer play];
	[self layoutSubviews];
	[self touch];
}

- (void)pinch:(UIPinchGestureRecognizer *)sender {
	if (sender.state == UIGestureRecognizerStateBegan) {
		mCurrentScale = sender.scale;
	}
	else if (sender.state == UIGestureRecognizerStateEnded) {
		if (mCurrentScale - sender.scale > 0) {
			[self fullscreen:nil];
		}
		mCurrentScale = 0;
	}
}

- (void)fullscreen:(id)sender {
	[((CMOREPlayerViewController *)self.delegate)switchToWindowed];
	[self touch];
}

- (void)prev:(id)sender {
	self.moviePlayer.currentPlaybackTime -= 2;
	[self touch];
}

- (void)next:(id)sender {
	self.moviePlayer.currentPlaybackTime += 2;
	[self touch];
}

- (void)smartTV:(id)sender {
//	if ([ACManager sharedInstance].isPaired && [ACManager sharedInstance].statusRecorder.playerOnline) {
	if (YES) {
		CMOREPlayerViewController *cmoreP = (CMOREPlayerViewController *)self.delegate;

		[ACManager sharedInstance].statusRecorder.currentPlaybackTime = self.moviePlayer.currentPlaybackTime;

		[cmoreP addPopupView:@"Sending video to TV..."];
		[cmoreP switchToRemotePlayback];
		AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
		// [[appDelegate currentViewController] hudForWaitingRemotePlayback:self.moviePlayer.view];
		[[appDelegate currentViewController] playMovie:cmoreP.playingAsset isStream:isLive isRemote:YES
									   initialPosition:cmoreP.moviePlayer.currentPlaybackTime containerView:nil];
	}
}

- (void)setSmartTVPlaying:(BOOL)playing {
	[self.smartTVButton setImage:[UIImage imageNamed:(playing ? @"smarttv_active.png":@"smarttv_inactive.png")] forState:UIControlStateNormal];
}

- (void)refresh:(id)sender {
	self.moviePlayer.currentPlaybackTime -= 30;
	[self touch];
}

- (void)ok:(id)sender {
	if (self.delegate) {
		[self.delegate close:self];
	}
}

- (void)updateButtons {
	if (self.moviePlayer && !self.isLive) {
		if (self.moviePlayer.playbackState != MPMoviePlaybackStatePlaying) {
			self.pauseButton.hidden = YES;
			self.playButton.hidden = NO;
		}
		else {
			self.pauseButton.hidden = NO;
			self.playButton.hidden = YES;
		}
	}
}

- (void)updateTimeCode:(NSTimeInterval)theElapsed {
	/*
	 * if (IS_DEBUG_MODE) {
	 *    int elapsed = (int)(theElapsed * 1000.0);
	 *    int h = elapsed / 3600000;
	 *    elapsed -= (h * 3600000);
	 *    int m = elapsed / 60000;
	 *    elapsed -= (m * 60000);
	 *    int s = elapsed / 1000;
	 *    elapsed -= (s * 1000);
	 *    int ms = elapsed;
	 *    NSString *timeCode = [NSString stringWithFormat:@"%02d:%02d:%02d.%03d", h, m, s, ms];
	 *    NSLog(@"%@", timeCode);
	 *    //self.timeCodeLabel.text = timeCode;
	 * }*/
}

- (void)layoutSubviews {
	[super layoutSubviews];

	if (self.width != self.frame.size.width) {
		self.width = self.frame.size.width;
		self.navbar.frame = CGRectMake(0, 19, self.frame.size.width, 42);
		self.controls.center = CGPointMake(self.center.x, self.center.y + self.frame.size.height / 3);
		self.progressSlider.frame = CGRectInset(CGRectMake(0, 0, self.frame.size.width - 100, 40), 80, 0);

		self.fontLabel.frame = CGRectInset(CGRectMake(0, self.frame.size.height * 3 / 4, self.frame.size.width, self.frame.size.height / 4), 20, 20);
		[self.popup dismissPopoverAnimated:YES];
		self.spinner.center = self.center;
		//        dispatch_time_t delay =  dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
		//        if (self.popup && self.subtitleVisible){
		//
		//            dispatch_after(delay, dispatch_get_main_queue(), ^{
		//                [self showPopup];
		//            });
		//        }
	}
}

- (void)touch {
	self.lastSeen = [NSDate timeIntervalSinceReferenceDate];
}

- (void)showHud {
	// NSLog(@"showhud");
	[self touch];
	if (!animating) {
		animating = YES;
		self.hideable.hidden = NO;
		[UIView animateWithDuration:0.5f delay:0.0 options:UIViewAnimationOptionAllowUserInteraction |                       UIViewAnimationOptionBeginFromCurrentState animations:^{
			 self.hideable.alpha = 1.0f;
		 } completion:^(BOOL finished) {
			 animating = NO;
		 }];
	}
}

- (void)hideHud {
	// NSLog(@"hidehud");
	if (!animating) {
		animating = YES;
		[UIView animateWithDuration:0.5f delay:0.0 options:UIViewAnimationOptionAllowUserInteraction |                       UIViewAnimationOptionBeginFromCurrentState animations:^{
			 self.hideable.alpha = minalpha;
			 [self.popup dismissPopoverAnimated:YES];
		 } completion:^(BOOL finished) {
			 self.hideable.hidden = YES;
			 animating = NO;
		 }];
	}
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	[self showHud];
	/*[UIView animateWithDuration:4.0 animations:^{
	 * self.swipeindicationView.alpha = 1.0;
	 * } completion:^(BOOL finished) {
	 *
	 * }];*/
}

- (void)setSpinnerAnimate:(BOOL)active {
	if (active) {
		self.spinner.hidden = NO;
		[self.spinner startAnimating];
	}
	else {
		[self.spinner stopAnimating];
		self.spinner.hidden = YES;
	}
}

@end
