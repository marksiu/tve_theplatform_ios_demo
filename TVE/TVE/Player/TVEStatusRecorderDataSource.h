//
//  TVEStatusRecorderDataSource.h
//  TVE
//
//  Created by Csaba Tűz on 2013.01.02..
//  Copyright (c) 2013 Accedo Broadband. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AccedoConnect/ACRemoteControlDataSource.h>
#import <AccedoConnect/AccedoConnect.h>

@interface TVEStatusRecorderDataSource : ACRemoteControlDataSource

@end
