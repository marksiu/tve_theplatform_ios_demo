//
//  RemotePlayerViewController.m
//  CMore
//
//  Created by Csaba Tűz on 2012.10.19..
//
//

#import "RemotePlayerViewController.h"

#import "AppDelegate.h"
#import "FontManager.h"
#import "FontLabel.h"
#import "FontLabelStringDrawing.h"
#import "UIHelper.h"

@interface RemotePlayerViewController () {
	BOOL playbackStarted;
	BOOL playbackRunning;
}

@end

@implementation RemotePlayerViewController

@synthesize overlayView;

@synthesize shouldShowCloseButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		[self customInit];
	}
	return self;
}

- (id)init {
	self = [super init];
	if (self) {
		[self customInit];
	}
	return self;
}

- (void)customInit {
	self.shouldShowCloseButton = YES;
	playbackRunning = NO;

	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(playbackStatusUpdated:)
												 name:@"PLAYBACK_STATUS_CHANGED"
											   object:[ACManager sharedInstance]];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(didClientDisconnected:)
												 name:@"AC_CLIENT_DISCONNECTED"
											   object:[ACManager sharedInstance]];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(stateUpdate:)
												 name:@"AC_UPDATE_OVERLAY"
											   object:[ACManager sharedInstance].statusRecorder];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(assetUpdated:)
												 name:@"AC_ASSET_UPDATED"
											   object:[ACManager sharedInstance].statusRecorder];
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];

	self.overlayView = nil;

	[super dealloc];
}

- (void)viewDidLoad {
	[super viewDidLoad];

	self.overlayView = [[[RemoteOverlayView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)] autorelease];

	// self.overlayView.bgImageDelegate = self;
	[self.view addSubview:self.overlayView];
	[self.overlayView setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];

	self.overlayView.moviePlayer = [ACManager sharedInstance].statusRecorder;
	// self.overlayView.backgroundImage.delegate = self;

#ifdef BUILD_CMORE
	NSString *fontName = @"nettoot.ttf";
	NSString *titleFont = @"nettootbold.ttf";
#endif
#ifdef BUILD_FILMNET
	NSString *fontName = @"ProximaNova-Reg.ttf";
	NSString *titleFont = @"ProximaNova-Bold.ttf";
#endif

	NSString *fontName = @"HelveticaNeue-Light.ttf";
	NSString *titleFont = @"HelveticaNeue-Bold.ttf";

	ZFont *fontTitleYear = [[FontManager sharedManager] zFontWithName:titleFont pointSize:22];
	ZFont *fontDescription = [[FontManager sharedManager] zFontWithName:fontName pointSize:16.5];

	[self.overlayView.titleLabel setZFont:fontTitleYear];
	[self.overlayView.descriptionLabel setZFont:fontDescription];

	self.view.alpha = 0.0;
	self.view.hidden = YES;

	if (!self.shouldShowCloseButton) {
		[self.overlayView.navbar topItem].leftBarButtonItem = nil;
		[self.overlayView.navbar topItem].hidesBackButton = YES;
	}

	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self assetUpdated:nil];
	[self.navigationController setNavigationBarHidden:!self.view.hidden animated:NO];
	[self.overlayView layoutSubviews];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	self.overlayView.width = 0;
	[self.overlayView layoutSubviews];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
	self.overlayView.width = 0;
	[self.overlayView layoutSubviews];
}

- (void)viewWillUnload {
	// self.overlayView.backgroundImage.delegate = nil;
	[self.overlayView.backgroundImage setImageUrl:nil];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)didClientDisconnected:(NSNotification *)notification {
	NSString *uuid = [notification.userInfo valueForKey:@"uuid"];

	if ([uuid isEqualToString:[ACManager sharedInstance].statusRecorder.player]) {
		[[NSNotificationCenter defaultCenter] postNotificationName:@"REMOTE_PLAYBACK_ENDED" object:[ACManager sharedInstance]];
		playbackRunning = NO;
	}
}

- (void)playbackStatusUpdated:(NSNotification *)notification {
	if (!notification) {
		return;
	}
	NSString *deviceType = [[notification userInfo] valueForKey:@"device_type"];
	if (![deviceType hasPrefix:@"TV"]) {
		return;
	}

	if (!playbackRunning && [ACManager sharedInstance].statusRecorder.playbackState != ACPlaybackStateTerminated) {
		AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
		if ([appDelegate currentViewController]) {
			[[appDelegate currentViewController].remoteTimeoutTimer invalidate];
			[appDelegate  currentViewController].remoteTimeoutTimer = nil;
		}
		[[NSNotificationCenter defaultCenter] postNotificationName:@"REMOTE_PLAYBACK_STARTED" object:[ACManager sharedInstance]];
		playbackRunning = YES;
	}
	if (self.view.hidden && !playbackRunning) {
		return;
	}

	[self.overlayView updateLive:[ACManager sharedInstance].statusRecorder.isStream];

	switch ([ACManager sharedInstance].statusRecorder.playbackState)
	{
		case ACPlaybackStateTerminated :
			[[NSNotificationCenter defaultCenter] postNotificationName:@"REMOTE_PLAYBACK_ENDED" object:[ACManager sharedInstance]];
			playbackRunning = NO;
			break;
		default :
			break;
	}
	[self.overlayView update:nil];
}

- (void)updateTitleYearLabel:(NSDictionary *)mdl {
	// NSLog(@"MODEL: %@", mdl);
	NSString *title = [mdl valueForKey:@"title"];
	NSDate *publishDate = [NSDate dateWithTimeIntervalSince1970:[[mdl valueForKey:@"pubDate"] longLongValue] / 1000];
	NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];

	formatter.dateFormat = @"yyyy";
	NSString *year = [formatter stringFromDate:publishDate];
	NSString *text = [NSString stringWithFormat:@"%@ (%@)", title, year];

	[self.overlayView.titleLabel setText:text];
}

- (CGSize)getProperSizeForLabel:(FontLabel *)label {
	CGSize size = [label.text sizeWithZFont:label.zFont
						  constrainedToSize:CGSizeMake(label.frame.size.width, 100000)
							  lineBreakMode:NSLineBreakByWordWrapping];

	size.width = label.frame.size.width;
	return size;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	[super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
	// [self rearrangeSubviewsForOrientation:toInterfaceOrientation];
}

- (void)rearrangeSubviewsForOrientation:(UIInterfaceOrientation)interfaceOrientation {
	if (!self.isViewLoaded) {
		return;
	}
	if (interfaceOrientation == UIInterfaceOrientationPortrait ||
		interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
	{
		self.overlayView.backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
	}
	else {
		self.overlayView.backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
	}

	if (self.overlayView.backgroundImage.image) {
		[self imageDidSet:self.overlayView.backgroundImage.image];
	}
}

- (void)imageDidSet:(UIImage *)image {
	// if (IS_DEBUG_MODE)
	//    NSLog(@"RemotePlayerViewController: imageDidSet called...");

	double w = image.size.width;
	double h = image.size.height;

	double ratio = 0;

	if (h != 0) {
		ratio = w / h;
	}

	CGRect frame = self.overlayView.backgroundImage.frame;
	UIInterfaceOrientation interfaceOrientation = self.interfaceOrientation;
	int height = 0;
	int width = 0;

	int screenH;
	int screenW;
	if (interfaceOrientation == UIInterfaceOrientationPortrait ||
		interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
	{
		screenH = 1024;
		screenW = 768;
		if ([ACManager sharedInstance].statusRecorder.isStream) {
			width = 315;
			height = 152;
		}
		else {
			height = 1024;
			width = 768;
		}
	}
	else {
		screenH = 768;
		screenW = 1024;
		if ([ACManager sharedInstance].statusRecorder.isStream) {
			width = 315;
			height = 152;
		}
		else {
			height = 768;
			width = 1024;
		}
	}
	int top_pad = 42;
	int bottom_pad = 0;
	if (!shouldShowCloseButton) {
		bottom_pad = 93;
	}


	frame.size.height = height - top_pad - bottom_pad;
	frame.origin.y    = top_pad;
	frame.size.width = frame.size.height * ratio;
	frame.origin.x = (width - frame.size.width) / 2;
	self.overlayView.backgroundImage.frame = frame;

	self.overlayView.staticBackgroundImage.frame = self.overlayView.frame;
}

- (void)assetUpdated:(NSNotification *)n {
	if ([ACManager sharedInstance].statusRecorder.asset == nil) {
		self.overlayView.backgroundImageUrl = nil;
		[self.overlayView.backgroundImage setImageUrl:self.overlayView.backgroundImageUrl];
		[self.overlayView.titleLabel setText:@""];
		[self.overlayView.descriptionLabel setText:@""];
		// [self.overlayView hideHud];
		return;
	}
	else {
		NSString *imageUrl = nil;
		if (![ACManager sharedInstance].statusRecorder.isStream) {
			self.overlayView.backgroundImage.hidden = NO;
			imageUrl = [UIHelper getImageUrl:[ACManager sharedInstance].statusRecorder.asset forSize:self.view.frame.size];
			if (!self.overlayView.backgroundImageUrl || (self.overlayView.backgroundImageUrl && ![self.overlayView.backgroundImageUrl isEqual:imageUrl]) || self.overlayView.backgroundImage.image == nil) {
				self.overlayView.backgroundImageUrl = imageUrl;
				[self.overlayView.backgroundImage setImageUrl:imageUrl completion:^(UIImage * image) {
					 [self imageDidSet:image];
				 }];
			}
		}
		else {
			self.overlayView.backgroundImage.hidden = YES;
		}

		[self updateTitleYearLabel:[ACManager sharedInstance].statusRecorder.asset];
		CGSize size = [self getProperSizeForLabel:self.overlayView.titleLabel];
		CGRect frame = self.overlayView.titleLabel.frame;
		frame.size = size;
		self.overlayView.titleLabel.frame = frame;
		[self.overlayView.descriptionLabel setText:[[ACManager sharedInstance].statusRecorder.asset valueForKey:@"description"]];
		size = [self getProperSizeForLabel:self.overlayView.descriptionLabel];
		frame = self.overlayView.descriptionLabel.frame;
		frame.origin.y = self.overlayView.titleLabel.frame.origin.y + self.overlayView.titleLabel.frame.size.height + 20;
		frame.size = size;
		self.overlayView.descriptionLabel.frame = frame;
		CGRect dFrame = self.overlayView.detailPanel.frame;
		[self.overlayView.detailPanel setContentSize:CGSizeMake (dFrame.size.width, frame.origin.y + frame.size.height + 20)];
	}
}

- (void)stateUpdate:(NSNotification *)n {
	[self.overlayView update:nil];
}

@end
