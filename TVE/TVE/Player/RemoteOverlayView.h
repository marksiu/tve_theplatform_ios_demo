//
//  OverlayView.h
//  CMore
//
//  Created by Gabor Bottyan on 8/6/12.
//  Copyright (c) 2012 Meganet-Soft Bt. All rights reserved.
//
#import <MediaPlayer/MediaPlayer.h>
#import <AccedoConnect/AccedoConnect.h>
#import <UIKit/UIKit.h>
#import "VSRemoteImageView.h"
#import "FontLabel.h"

@interface RemoteOverlayView : UIView<UIPopoverControllerDelegate, ACStatusRecorderOverlay>

- (void)updateLive:(BOOL)_live;

@property (nonatomic, assign) ACStatusRecorder *moviePlayer;
@property (nonatomic, assign) id <RemoteOverlayViewDelegate> delegate;

@property (nonatomic, assign) BOOL wasSwipedBack;

@property (retain) UIScrollView *detailPanel;
@property (nonatomic, retain) FontLabel *titleLabel;
@property (nonatomic, retain) FontLabel *descriptionLabel;

@property (retain, nonatomic) NSString *backgroundImageUrl;
@property (retain, nonatomic) IBOutlet VSRemoteImageView *backgroundImage;
@property (retain, nonatomic) IBOutlet UIImageView *staticBackgroundImage;

@property (retain) UINavigationBar *navbar;

@property (assign) float width;
@property (assign) float height;

- (void)showHud;
- (void)hideHud;

- (void)smartTV:(id)sender;

- (void)update:(id)sender;

@end
