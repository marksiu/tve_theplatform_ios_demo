//
//  EPGVideoOverlayView.m
//  CMore
//
//  Created by Csaba Tűz on 2012.11.14..
//
//

#import "EPGVideoOverlayView.h"

#import "AppDelegate.h"
#import "CMOREPlayerViewController.h"

#import <QuartzCore/QuartzCore.h>

#define  minalpha 0.02f

@interface EPGVideoOverlayView () {
	BOOL animating;

	double mCurrentScale;
	double mLastScale;
}


@property (retain, nonatomic) UIActivityIndicatorView *spinner;

@property (nonatomic, retain) UILabel *fontLabel;

@property (retain) UINavigationBar *navbar;
@property (retain) UIView *hideable;
@property (retain) UILabel *timeLabel;
@property (retain) UILabel *playedLabel;
@property (retain) UISlider *progressSlider;

@property (retain) NSDateFormatter *timeFormatter;

@property (assign) BOOL sliderLock;

@property (assign) double lastSeen;

@property (nonatomic, retain) UIButton *fullScreenButton;

@property (nonatomic, retain) NSTimer *timer;

@end

@implementation EPGVideoOverlayView

@synthesize navbar, progressSlider;
@synthesize moviePlayer, timeFormatter;
@synthesize hideable;
@synthesize subtitles;
@synthesize displayLink;
@synthesize finito;
@synthesize isLive;
@synthesize timeLabel;
@synthesize playedLabel;
@synthesize sliderLock, lastSeen;
@synthesize fullScreenButton;
@synthesize fontLabel, timer;
@synthesize spinner;

- (void)updateLive:(BOOL)_live {
	self.isLive = _live;

	if (_live) {
		self.pauseButton.hidden = YES;
		self.playButton.hidden = YES;
		self.progressSlider.hidden = YES;
		self.timeLabel.hidden = YES;
		self.playedLabel.hidden = YES;
	}
	else {
		self.pauseButton.hidden = NO;
		self.playButton.hidden = NO;
		self.progressSlider.hidden = NO;
		self.timeLabel.hidden = NO;
		self.playedLabel.hidden = NO;
	}

	// self.systemVolumeSlider.backgroundColor = [UIColor greenColor];
	// self.systemVolumeSlider.alpha = 0.9f;

	self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(update:)];
	[self.displayLink setFrameInterval:30];
	[self.displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];

	// self.pauseButton.backgroundColor = [UIColor clearColor];
	// [self.systemVolumeSlider sizeToFit];
}

- (void)dealloc {
	// self.finito  =YES;

	self.delegate = nil;

	if (timer) {
		[timer invalidate];
		self.timer = nil;
	}

	self.moviePlayer = Nil;

	if (self.displayLink) {
		[self.displayLink invalidate];
		self.displayLink = nil;
	}

	self.pauseButton = Nil;
	self.playButton = Nil;
	self.subtitles = Nil;

	self.fontLabel = Nil;

	self.timeFormatter  = Nil;
	self.navbar = Nil;
	self.timeLabel = Nil;
	self.playedLabel = Nil;
	self.fullScreenButton = nil;

	[super dealloc];
}

- (void)createControls {
	self.pauseButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.pauseButton.showsTouchWhenHighlighted = YES;
	[self.pauseButton setImage:[UIImage imageNamed:@"epg_overlay_pause.png"] forState:UIControlStateNormal];
	[self.pauseButton addTarget:self action:@selector(pause:) forControlEvents:UIControlEventTouchUpInside];
	[self.navbar addSubview:self.pauseButton];

	self.playButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.playButton.showsTouchWhenHighlighted = YES;
	[self.playButton setImage:[UIImage imageNamed:@"epg_overlay_play.png"] forState:UIControlStateNormal];
	[self.playButton addTarget:self action:@selector(play:) forControlEvents:UIControlEventTouchUpInside];
	self.playButton.hidden = YES;
	[self.navbar addSubview:self.playButton];

	self.fullScreenButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.fullScreenButton.showsTouchWhenHighlighted = YES;
	[self.fullScreenButton setImage:[UIImage imageNamed:@"to_fullscreen_icon.png"] forState:UIControlStateNormal];
	[self.fullScreenButton addTarget:self action:@selector(fullscreen:) forControlEvents:UIControlEventTouchUpInside];
	[self.navbar addSubview:self.fullScreenButton];
}

- (id)initWithFrame:(CGRect)frame andDelegate:(id<OverlayViewDelegate>)delegate {
	self = [super initWithFrame:frame];

	if (self) {
		self.delegate = delegate;
		self.timeFormatter = [[[NSDateFormatter alloc] init] autorelease];
		[timeFormatter setDateFormat:@"HH:mm:ss"];
		[timeFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];

		self.spinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
		self.spinner.hidesWhenStopped = YES;

		[self addSubview:self.spinner];

		self.hideable = [[[UIView alloc] initWithFrame:frame] autorelease];
		self.hideable.alpha = minalpha;
		[self setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
		[self.hideable setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
		[self setBackgroundColor:[UIColor clearColor]];

		[self createControls];


		self.navbar = [[[UINavigationBar alloc] initWithFrame:CGRectMake(0, 10, self.frame.size.width, 42)] autorelease];
		self.navbar.barStyle = UIBarStyleBlack;

		// self.navbar.translucent = YES;
		// self.navbar.tintColor = [UIColor blackColor];
		[self.hideable addSubview:self.navbar];

		[self addSubview:self.hideable];

		// UIView * progressHolder = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width - 200, 45)];

		self.progressSlider =  [[[UISlider alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width - 200, 45)] autorelease];
		self.progressSlider.continuous = NO;

		// /[progressHolder addSubview:self.progressSlider];
		// progressHolder.backgroundColor = [UIColor yellowColor];
		[self.progressSlider addTarget:self action:@selector(sliderMoveStart) forControlEvents:UIControlEventTouchDown];
		// [self.progressSlider addTarget:self action:@selector(sliderMoved) forControlEvents:UIControlEventTouchDragEnter];
		// [self.progressSlider addTarget:self action:@selector(sliderMoved) forControlEvents:UIControlEventTouchDragInside];

		// [self.progressSlider addTarget:self action:@selector(sliderMoveEnded) forControlEvents:UIControlEventTouchUpInside];

		[self.progressSlider addTarget:self action:@selector(sliderMoveEnded) forControlEvents:UIControlEventValueChanged];

		UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@""];

		navItem.titleView = self.progressSlider;

		[navbar pushNavigationItem:navItem animated:NO];


		UIView *playPauseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 30)];
		[playPauseView addSubview:self.playButton];
		[playPauseView addSubview:self.pauseButton];
		self.playButton.frame = CGRectMake(6, 6, 18, 18);
		self.pauseButton.frame = CGRectMake(6, 6, 18, 18);

		self.timeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 5, 80, 20)] autorelease];
		self.playedLabel = [[[UILabel alloc] initWithFrame:CGRectMake(40, 5, 80, 20)] autorelease];
		[playPauseView addSubview:self.playedLabel];
		[self.timeLabel setFont:[UIFont systemFontOfSize:14]];
		[self.playedLabel setFont:[UIFont systemFontOfSize:14]];
		self.timeLabel.text = @"";
		self.playedLabel.text = @"";

		self.timeLabel.textColor = [UIColor whiteColor];
		self.timeLabel.backgroundColor = [UIColor clearColor];
		self.playedLabel.textColor = [UIColor whiteColor];
		self.playedLabel.backgroundColor = [UIColor clearColor];

		UIView *remainingFullscreenButton = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
		[remainingFullscreenButton addSubview:self.timeLabel];
		self.fullScreenButton.frame = CGRectMake(76, 6, 18, 18);
		[remainingFullscreenButton addSubview:self.fullScreenButton];

		navItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:remainingFullscreenButton] autorelease];
		navItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:playPauseView] autorelease];
		[playPauseView release];
		[remainingFullscreenButton release];

		[navItem release];

		UIPinchGestureRecognizer *recognizer;
		recognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinch:)];
		[self.hideable addGestureRecognizer:recognizer];
		[recognizer release];

		self.swipeindicationView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 364, 226)] autorelease];
		[self.swipeindicationView setImage:[UIImage imageNamed:@"swipe_message_bg.png"]];
		self.swipeindicationView.center = self.center;
		[self.swipeindicationView setBackgroundColor:[UIColor clearColor]];
		self.swipeindicationView.alpha = 1.0;
		self.swipeindicationView.hidden = YES;

		ZFont *zf = [[FontManager sharedManager] zFontWithName:@"ProximaNova-Bold" pointSize:25.665];

		FontLabel *noticeLabel = [[FontLabel alloc] initWithFrame:CGRectMake(22, 95, 228, 80)];
		[noticeLabel setZFont:zf];
		[noticeLabel setTextColor:[UIColor whiteColor]];
		[noticeLabel setBackgroundColor:[UIColor clearColor]];
		noticeLabel.numberOfLines = 2;
		noticeLabel.lineBreakMode = NSLineBreakByWordWrapping;
		[self.swipeindicationView addSubview:noticeLabel];
		[noticeLabel release];
		[noticeLabel setText:@"Swipe out to TV"];

		[self addSubview:self.swipeindicationView];
	}

	return self;
}

- (void)startHidingSwipeIndication {
	[UIView animateWithDuration:4 animations:^{
		 self.swipeindicationView.alpha = 0.0;
	 } completion:^(BOOL finished) {
		 self.swipeindicationView.hidden = YES;
	 }];
}

- (void)sliderMoveStart {
	// NSLog(@"slider move start");
	self.sliderLock = YES;
	[self touch];
}

- (void)sliderMoved {
	// NSLog(@"slider move");
	[self touch];
}

- (void)sliderMoveEnded {
	// NSLog(@"slider move end");
	self.moviePlayer.currentPlaybackTime = MIN(self.moviePlayer.duration - 1, self.moviePlayer.duration * self.progressSlider.value);
	self.sliderLock = NO;
	[self touch];
}

- (void)update:(id)sender {
	if (finito) {
		return;
	}

	if (!self.isLive) {
		[self updateButtons];
	}

	double now = [NSDate timeIntervalSinceReferenceDate];

	if (now - self.lastSeen > 4.0f) {
		[self hideHud];
	}

	if (self.moviePlayer) {
		// self.moviePlayer.useApplicationAudioSession = YES;
		if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
			// NSLog (@"DURATION: %f CURENT %f",self.moviePlayer.duration,self.moviePlayer.currentPlaybackTime);
			double remaining = self.moviePlayer.duration - self.moviePlayer.currentPlaybackTime;

			NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:remaining];
			NSDate *playeddate = [NSDate dateWithTimeIntervalSinceReferenceDate:self.moviePlayer.currentPlaybackTime];
			self.timeLabel.text = [@"-" stringByAppendingString:[timeFormatter stringFromDate:date]];
			self.playedLabel.text = [@" " stringByAppendingString:[timeFormatter stringFromDate:playeddate]];
			// NSLog(@"%@",self.playedLabel.text);
			if (self.moviePlayer.duration != 0 && !self.sliderLock) {
				self.progressSlider.value = self.moviePlayer.currentPlaybackTime / self.moviePlayer.duration;
				// NSLog(@"%f  %f %f", self.moviePlayer.currentPlaybackTime, self.moviePlayer.initialPlaybackTime, self.moviePlayer.duration );
			}

			// self.volumeSlider.value = [MPMusicPlayerController applicationMusicPlayer].volume;
		}
	}
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
	// NSLog(@"dismissed");
}

- (void)pause:(id)sender {
	[self.moviePlayer pause];
	[self layoutSubviews];
	[self touch];
}

- (void)play:(id)sender {
	[self.moviePlayer play];
	[self layoutSubviews];
	[self touch];
}

- (void)pinch:(UIPinchGestureRecognizer *)sender {
	if (sender.state == UIGestureRecognizerStateBegan) {
		mCurrentScale = sender.scale;
	}
	else if (sender.state == UIGestureRecognizerStateEnded) {
		if (mCurrentScale - sender.scale < 0) {
			[self fullscreen:nil];
		}
		mCurrentScale = 0;
	}
}

- (void)fullscreen:(id)sender {
	[((CMOREPlayerViewController *)self.delegate)switchToFullscreen];
	[self touch];
}

- (void)prev:(id)sender {
	self.moviePlayer.currentPlaybackTime -= 2;
	[self touch];
}

- (void)next:(id)sender {
	self.moviePlayer.currentPlaybackTime += 2;
	[self touch];
}

- (void)refresh:(id)sender {
	self.moviePlayer.currentPlaybackTime -= 30;
	[self touch];
}

- (void)ok:(id)sender {
	self.finito  = YES;

	if (self.delegate) {
		[self.delegate close:self];
	}

	self.displayLink = nil;
	self.moviePlayer = Nil;
}

- (void)updateButtons {
	if (self.moviePlayer && !self.isLive) {
		if (self.moviePlayer.playbackState != MPMoviePlaybackStatePlaying) {
			self.pauseButton.hidden = YES;
			self.playButton.hidden = NO;
		}
		else {
			self.pauseButton.hidden = NO;
			self.playButton.hidden = YES;
		}
	}
}

- (void)updateTimeCode:(NSTimeInterval)theElapsed {
	/*
	 * if (IS_DEBUG_MODE) {
	 *  int elapsed = (int)(theElapsed * 1000.0);
	 *  int h = elapsed / 3600000;
	 *  elapsed -= (h * 3600000);
	 *  int m = elapsed / 60000;
	 *  elapsed -= (m * 60000);
	 *  int s = elapsed / 1000;
	 *  elapsed -= (s * 1000);
	 *  int ms = elapsed;
	 *  NSString *timeCode = [NSString stringWithFormat:@"%02d:%02d:%02d.%03d", h, m, s, ms];
	 *  NSLog(@"%@", timeCode);
	 *  //self.timeCodeLabel.text = timeCode;
	 * }*/
}

- (void)layoutSubviews {
	[super layoutSubviews];

	if (self.width != self.moviePlayer.view.frame.size.width) {
		self.width = self.frame.size.width;
		self.navbar.frame = CGRectMake(0, self.frame.size.height - 42, self.frame.size.width, 42);
		self.progressSlider.frame = CGRectInset(CGRectMake(0, 0, self.frame.size.width - 100, 40), 80, 0);

		self.fontLabel.frame = CGRectInset(CGRectMake(0, self.frame.size.height * 3 / 4, self.frame.size.width, self.frame.size.height / 4), 20, 20);

		if (self.width < 200 || isLive) {
			self.playedLabel.hidden = YES;
			self.timeLabel.hidden = YES;
		}
		else {
			self.playedLabel.hidden = NO;
			self.timeLabel.hidden = NO;
		}

		self.spinner.center = self.center;

		//        dispatch_time_t delay =  dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
		//        if (self.popup && self.subtitleVisible){
		//
		//            dispatch_after(delay, dispatch_get_main_queue(), ^{
		//                [self showPopup];
		//            });
		//        }
	}
}

- (void)touch {
	self.lastSeen = [NSDate timeIntervalSinceReferenceDate];
}

- (void)showHud {
	// NSLog(@"showhud");
	[self touch];
	if (!animating) {
		self.hideable.hidden = NO;
		animating = YES;
		[UIView animateWithDuration:0.5f delay:0.0 options:UIViewAnimationOptionAllowUserInteraction |                       UIViewAnimationOptionBeginFromCurrentState animations:^{
			 self.hideable.alpha = 1.0f;
		 } completion:^(BOOL finished) {
			 animating = NO;
		 }];
	}
}

- (void)hideHud {
	// NSLog(@"hidehud");
	if (!animating) {
		animating = YES;
		[UIView animateWithDuration:0.5f delay:0.0 options:UIViewAnimationOptionAllowUserInteraction |                       UIViewAnimationOptionBeginFromCurrentState animations:^{
			 self.hideable.alpha = minalpha;
		 } completion:^(BOOL finished) {
			 self.hideable.hidden = YES;
			 animating = NO;
		 }];
	}
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	[self showHud];
	/*[UIView animateWithDuration:4.0 animations:^{
	 * self.swipeindicationView.alpha = 1.0;
	 * } completion:^(BOOL finished) {
	 *
	 * }];*/
}

- (void)setSpinnerAnimate:(BOOL)active {
	if (active) {
		self.spinner.hidden = NO;
		[self.spinner startAnimating];
	}
	else {
		[self.spinner stopAnimating];
		self.spinner.hidden = YES;
	}
}

@end
