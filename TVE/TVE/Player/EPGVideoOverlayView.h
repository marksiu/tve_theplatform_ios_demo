//
//  EPGVideoOverlayView.h
//  CMore
//
//  Created by Csaba Tűz on 2012.11.14..
//
//

#import <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>
#import "OverlayView.h"
#import "FontManager.h"
#import "FontLabel.h"

@interface EPGVideoOverlayView : UIView<OverlayViewProtocol>

@property (nonatomic, assign) MPMoviePlayerController *moviePlayer;
@property (nonatomic, assign) id <OverlayViewDelegate> delegate;

@property (retain) UIButton *pauseButton;
@property (retain) UIButton *playButton;
@property (retain) UIButton *smartTVButton;


@property (retain) UIImageView *swipeindicationView;

@property (nonatomic, assign) BOOL subtitleVisible;

@property (nonatomic, retain) CADisplayLink *displayLink;

@property (assign) BOOL finito;

@property (assign) BOOL isLive;

@property (nonatomic, retain) NSArray *subtitles;

@property (assign) float width;

- (id)initWithFrame:(CGRect) frame andDelegate:(id<OverlayViewDelegate>)delegate;

- (void)setSpinnerAnimate:(BOOL)spinner;

@end
