//
//  RemoteControlCell.h
//  CMore
//
//  Created by Csaba Tűz on 2012.10.29..
//
//

#import <UIKit/UIKit.h>

#import <MediaPlayer/MediaPlayer.h>
@interface RemoteControlCell : UITableViewCell
@property (retain) IBOutlet UIButton *fwdBtn;
@property (retain) IBOutlet UIButton *rewBtn;
@property (retain) IBOutlet UIButton *playBtn;
@property (retain) IBOutlet UIButton *pauseBtn;

@property (retain) IBOutlet UIButton *backBtn;

@property (retain) IBOutlet UISlider *volumeSlider;

@property (assign) id delegate;

@end
