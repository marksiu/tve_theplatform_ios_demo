//
//  ACRemoteControlPopup.h
//  CMore
//
//  Created by Csaba Tűz on 2012.10.29..
//
//

#import <UIKit/UIKit.h>
#import "FontLabel.h"
#import "FontManager.h"
#import <AccedoConnect/ACStatusRecorder.h>
#import "RemoteControlCell.h"

@class BaseViewController;

@interface ACRemoteControlPopup : UITableViewController<UITableViewDataSource, UITableViewDelegate, UIPopoverControllerDelegate, ACStatusRecorderOverlay>

@property (assign) BaseViewController *delegate;

@property (nonatomic, retain) UIPopoverController *popup;
@property (nonatomic, retain) NSArray *subtitles;

@property (nonatomic, retain) IBOutlet RemoteControlCell *controlCell;

@property (nonatomic, assign) BOOL isLive;

@property (assign) BOOL volumeLock;

@end
