//
//  CMOREPlayerViewController.m
//  CMore
//
//  Created by Stephen King on 8/3/12.
//  Copyright (c) 2012 Meganet-Soft Bt. All rights reserved.
//

#import "CMOREPlayerViewController.h"
#import "ASIHTTPRequest.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>

@interface CMOREPlayerViewController () {
	BOOL localMode;

	BOOL started;

	BOOL terminated;
}

@property (retain) FontLabel *fontLabel;

@property (nonatomic, retain) UIView<OverlayViewProtocol> *fullscreenOverlayView;
@property (nonatomic, retain) UIView<OverlayViewProtocol> *windowedOverlayView;
// @property (retain) NSTimer * timer;

@end

@implementation CMOREPlayerViewController

// @synthesize timer;
@synthesize fontLabel, isLiveStream, isRemote;
@synthesize overlayView;
@synthesize buffering;

@synthesize storedUrl;

@synthesize playingAsset;
@synthesize takingRemoteUpdates;

@synthesize initial_playback_time;

@synthesize containerView;

@synthesize fullscreenOverlayView;
@synthesize windowedOverlayView;

@synthesize fullscreen;
@synthesize moviePlayer;
@synthesize startsFullscreen;

- (void)initCustomControls {
	// DLog(@"CMOREPlayerViewController: initCustomControls");

	// self.moviePlayer.useApplicationAudioSession = NO;

	self.fullscreenOverlayView = [[[OverlayView alloc] initWithFrame:self.moviePlayer.view.frame andDelegate:self] autorelease];
	self.windowedOverlayView = [[[EPGVideoOverlayView alloc] initWithFrame:self.moviePlayer.view.frame andDelegate:self] autorelease];

	[self.fullscreenOverlayView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth ];
	[self.windowedOverlayView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth ];
	[self.moviePlayer.view setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth ];

	self.fullscreenOverlayView.moviePlayer = self.moviePlayer;
	self.windowedOverlayView.moviePlayer = self.moviePlayer;

	[self.view addSubview:self.fullscreenOverlayView];
	[self.view addSubview:self.windowedOverlayView];

	[self updateLive:self.isLiveStream];

	self.moviePlayer.controlStyle = MPMovieControlStyleNone;
	self.moviePlayer.allowsAirPlay = NO;

	self.overlayView = self.fullscreenOverlayView;

	[self updateSmartTVButtonOnOverlay];
}

- (void)updateSmartTVButtonOnOverlay {
	if ([[ACManager sharedInstance] isPaired] && [ACManager sharedInstance].statusRecorder.playerOnline) {
		self.fullscreenOverlayView.smartTVButton.enabled = YES;
		self.fullscreenOverlayView.smartTVButton.alpha = 1.0;
	}
	else {
		self.fullscreenOverlayView.smartTVButton.enabled = NO;
		self.fullscreenOverlayView.smartTVButton.alpha = 0.5;
	}
}

- (void)switchToFullscreen {
	if (started && fullscreen) {
		return;
	}
	started = YES;
	self.windowedOverlayView.hidden = YES;
	self.fullscreenOverlayView.hidden = NO;

	self.overlayView = self.fullscreenOverlayView;

	self.overlayView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

	[self updateLive:self.isLiveStream];

	fullscreen = YES;

	AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
	[appDelegate.currentViewController presentViewController:self animated:NO completion:nil];

	[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
	AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[delegate.currentViewController.view setNeedsDisplay];

	self.overlayView.width = 0;
	[self.view layoutSubviews];
}

- (void)presentPlayer {
	if (startsFullscreen) {
		[self switchToFullscreen];
	}
	else {
		[self switchToWindowed];
	}
}

- (void)switchToWindowed {
	if (started && !fullscreen) {
		return;
	}
	if (!self.containerView) {
		return;
	}

	started = YES;

	AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
	[appDelegate.currentViewController dismissViewControllerAnimated:NO completion:nil];

	self.windowedOverlayView.hidden = NO;
	self.fullscreenOverlayView.hidden = YES;

	self.overlayView = self.windowedOverlayView;


	self.view.frame = self.containerView.bounds;
	self.overlayView.frame = self.view.bounds;

	[self.containerView addSubview:self.view];

	[self updateLive:self.isLiveStream];

	fullscreen = NO;

	self.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
	self.overlayView.frame = self.view.frame;
	[self.containerView bringSubviewToFront:self.view];
	[self.containerView.superview setNeedsDisplay];

	[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
	AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[delegate.currentViewController.view setNeedsDisplay];

	self.overlayView.width = 0;
	[self.view layoutSubviews];
}

- (void)sendKeepAlive:(id)n {
	/*if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying) {
	 * StreamService* service = (StreamService*)[[ServiceFactory sharedInstance] getService:@"StreamService"];
	 * AppDelegate* delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	 *
	 * [service syncSendKeepAlive:self.playingAsset onSuccess:[delegate.currentCMoreViewController streamManagementSuccessBlock] onFailure:nil];
	 * }*/
}

- (void)sendStartStream:(id)n {
	/* if (!self.playingAsset) return;
	 * StreamService* service = (StreamService*)[[ServiceFactory sharedInstance] getService:@"StreamService"];
	 * AppDelegate* delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	 *
	 * [service syncSendStartStream:self.playingAsset onSuccess:[delegate.currentCMoreViewController streamManagementSuccessBlock] onFailure:nil];*/
}

- (void)sendStopStream:(id)n {
	/*
	 * StreamService* service = (StreamService*)[[ServiceFactory sharedInstance] getService:@"StreamService"];
	 * AppDelegate* delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	 *
	 * [service syncSendStopStream:self.playingAsset onSuccess:[delegate.currentCMoreViewController streamManagementSuccessBlock] onFailure:nil];
	 */
}

- (void)handleMPMoviePlayerLoadStateDidChangeNotification:(NSNotification *)n {
	MPMoviePlayerController *p = (MPMoviePlayerController *)n.object;
	MPMovieLoadState state = p.loadState;

	if (state & MPMovieLoadStatePlayable) {
		if (self.buffering) {
			self.buffering = NO;
			if (!terminated) {
				if (self.storedUrl) {
					NSLog(@"########## PLAYABLE, BUT WHOOPS, THERE IS A NEW URL TO PLAY");
					[self replaceURL:self.storedUrl];
					self.storedUrl = nil;
				}
				else {
					NSLog(@"########## PLAYABLE, AND NO NEW URL, PHEW");
					[self.moviePlayer setInitialPlaybackTime:0];
					if (!self.fullscreen && self.containerView) {
						[self.containerView addSubview:self.view];
						self.view.frame = CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height);
						self.overlayView.frame = self.view.frame;
						[self.containerView bringSubviewToFront:self.view];
						[self.containerView.superview setNeedsDisplay];
					}
					[self.fullscreenOverlayView setSpinnerAnimate:NO];
					[self.windowedOverlayView setSpinnerAnimate:NO];
				}
			}
			else {
				[self closePlayer];
			}
		}

		// [self hideHUD];
	}

	NSLog(@"POC1 - BEGIN LOADSTATE");

	if (state & MPMovieLoadStatePlayable) {
		// [self hideHUD];

		NSLog(@"POC1 - Playable");
	}

	if (state & MPMovieLoadStatePlaythroughOK) {
		// [self hideHUD];

		NSLog(@"POC1 - PlaythroughOK");
	}

	if (state & MPMovieLoadStateStalled) {
		NSLog(@"POC1 - Stalled");
	}

	if (state & MPMovieLoadStateUnknown) {
		NSLog(@"POC1 - Unknown");
	}

	NSLog(@"POC1 - END LOADSTATE");
}

- (id)initAsRemote:(BOOL)remote fullscreen:(BOOL)fullScreen containerView:(UIView *)__containerView {
	self = [super init];
	if (self) {
		self.buffering = NO;
		self.moviePlayer = [[[MPMoviePlayerController alloc] init] autorelease];
		self.isRemote = remote;
		self.startsFullscreen = fullScreen;
		self.containerView = __containerView;
		terminated = NO;
		self.remotePlayerVC = [[[RemotePlayerViewController alloc] init] autorelease];
		[self addChildViewController:self.remotePlayerVC];

		if (!remote) {
			self.takingRemoteUpdates = NO;
		}
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(handleMPMoviePlayerLoadStateDidChangeNotification:)
													 name:MPMoviePlayerLoadStateDidChangeNotification
												   object:self.moviePlayer];
	}
	return self;
}

- (void)replaceURL:(NSURL *)url {
	if (terminated) {
		return;
	}
	[self.fullscreenOverlayView setSpinnerAnimate:YES];
	[self.windowedOverlayView setSpinnerAnimate:YES];
	if (self.buffering) {
		NSLog(@"########## BUFFERING, WILL NOT START PLAYBACK");
		self.storedUrl = url;
	}
	else {
		NSLog(@"########## NOT BUFFERING, WILL START PLAYBACK");
		self.buffering = YES;
		// [self.moviePlayer stop];
		[self.moviePlayer setContentURL:url];
		if (!isRemote) {
			[self.moviePlayer play];
		}
	}
}

// ////////////// Accedo Connect functions for state exchange ////////////////////

- (void)didRemotePlaybackStart:(NSNotification *)n {
	[self.overlayView.smartTVButton setImage:[UIImage imageNamed:@"smarttv_active.png"] forState:UIControlStateNormal];
}

- (void)didRemotePlaybackEnd:(NSNotification *)n {
	[self.overlayView.smartTVButton setImage:[UIImage imageNamed:@"smarttv_inactive.png"] forState:UIControlStateNormal];


	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:@"REMOTE_PLAYBACK_STARTED"
												  object:[ACManager sharedInstance]];
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:@"REMOTE_PLAYBACK_ENDED"
												  object:[ACManager sharedInstance]];

	if (self.remotePlayerVC.overlayView.wasSwipedBack) {
		[self switchToLocalPlaybackWithPosUpdate:YES];
	}
	else {
		[self close:nil];
	}
}

- (void)playbackStatusUpdated:(NSNotification *)notification {
	NSLog(@"CMOREPlayerVC: playbackStatusUpdated (shouldn't get called(!) do nothing...)");
}

// //////////////////////////////////////////////////////////////////////////////

/*
 * -(id) retain {
 * NSLog(@"@@@@@ player retained. retain count: %d", self.retainCount);
 * return [super retain];
 * }
 *
 * -(oneway void) release {
 * NSLog(@"@@@@@ player released. retain count: %d", self.retainCount);
 * [super release];
 * }
 */

- (void)dealloc {
	/*if (IS_DEBUG_MODE)
	 * NSLog(@"CMOREPlayerViewController dealloc");*/

	[[NSNotificationCenter defaultCenter] removeObserver:self];

	self.remote = nil;
	self.playingAsset = nil;
	self.currentSubtitles = nil;

	self.fullscreenOverlayView.moviePlayer = nil;
	[self.fullscreenOverlayView.displayLink invalidate];
	self.fullscreenOverlayView.displayLink = nil;
	self.fullscreenOverlayView = nil;

	self.windowedOverlayView.moviePlayer = nil;
	[self.windowedOverlayView.displayLink invalidate];
	self.windowedOverlayView.displayLink = nil;
	self.windowedOverlayView = nil;

	[self.remotePlayerVC removeFromParentViewController];
	self.remotePlayerVC = nil;

	[self.moviePlayer stop];
	self.moviePlayer = nil;

	[super dealloc];
}

- (void)close:(id)sender {
	terminated = YES;
	// [[NSNotificationCenter defaultCenter] removeObserver:self];

	[self.fullscreenOverlayView.displayLink invalidate];
	[self.windowedOverlayView.displayLink invalidate];
	self.fullscreenOverlayView.displayLink = nil;
	self.windowedOverlayView.displayLink = nil;
	self.fullscreenOverlayView.finito = YES;
	self.windowedOverlayView.finito = YES;
	self.fullscreenOverlayView.moviePlayer = nil;
	self.windowedOverlayView.moviePlayer = nil;

	[self.moviePlayer pause];
	self.moviePlayer.currentPlaybackTime = -1;

	if (self.modalPresentationStyle == UIModalPresentationFullScreen) {
		[self dismissViewControllerAnimated:YES completion:nil];
	}


	[self.moviePlayer.view removeFromSuperview];
	if (!(self.moviePlayer.playbackState & MPMovieLoadStatePlayable)) {
		[[NSNotificationCenter defaultCenter] postNotificationName:MPMoviePlayerPlaybackDidFinishNotification
															object:self.moviePlayer
														  userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@ (YES), @"interrupted", nil]];
	}
	[self.moviePlayer stop];
	self.moviePlayer = nil;
}

- (void)closeRemoteOverlayView:(id)sender {
	self.moviePlayer.currentPlaybackTime = [ACManager sharedInstance].statusRecorder.currentPlaybackTime;
	[self close:sender];
}

- (void)updateLive:(BOOL)_live {
	self.isLiveStream = _live;
	[self.fullscreenOverlayView updateLive:_live];
	[self.windowedOverlayView updateLive:_live];
}

- (void)viewDidLoad {
	[super viewDidLoad];

	[self.view setBackgroundColor:[UIColor blackColor]];
	self.moviePlayer.controlStyle = MPMovieControlStyleNone;
	[self.view addSubview:self.moviePlayer.view];
	self.moviePlayer.view.frame = self.view.bounds;

	self.navigationController.navigationBarHidden = NO;
	self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
	[self initCustomControls];

	[self.moviePlayer.view addSubview:self.remotePlayerVC.view];
	self.remotePlayerVC.view.frame = self.moviePlayer.view.frame;
	self.remotePlayerVC.overlayView.delegate = self;

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tvOnOfflineStateChanged:) name:@"AC_PLAYER_BECAME_ONLINE" object:[ACManager sharedInstance]];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tvOnOfflineStateChanged:) name:@"AC_PLAYER_BECAME_OFFLINE" object:[ACManager sharedInstance]];

	if (self.isRemote) {
		[self switchToRemotePlayback];
	}
	else {
		[self switchToLocalPlaybackWithPosUpdate:NO];
	}
}

- (void)addPopupView:(NSString *)popupString {
	UIImageView *swipePopupView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 364, 226)] autorelease];

	swipePopupView.center = CGPointMake(1024 / 2, 768 / 2);
	[swipePopupView setBackgroundColor:[UIColor colorWithRed:7 / 255.0 green:12 / 255.0 blue:15 / 255.0 alpha:1.0]];
	swipePopupView.alpha = 0.8;
	swipePopupView.layer.cornerRadius = 5;
	swipePopupView.layer.masksToBounds = YES;

	ZFont *zf = [[FontManager sharedManager] zFontWithName:@"ProximaNova-Bold" pointSize:25.665];

	FontLabel *noticeLabel = [[FontLabel alloc] initWithFrame:CGRectMake(0, 0, 228, 80)];
	noticeLabel.center = CGPointMake(364 / 2, 226 / 2);
	[noticeLabel setZFont:zf];
	[noticeLabel setTextColor:[UIColor whiteColor]];
	[noticeLabel setBackgroundColor:[UIColor clearColor]];
	noticeLabel.numberOfLines = 2;
	noticeLabel.lineBreakMode = NSLineBreakByWordWrapping;
	[swipePopupView addSubview:noticeLabel];
	[noticeLabel release];
	[noticeLabel setText:popupString];

	[self.view addSubview:swipePopupView];
//	[swipePopupView performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:3.0];
	[UIView animateWithDuration:2.0
					 animations:^{
		 swipePopupView.alpha = 0.7;
	 }
					 completion:^(BOOL finished) {
		 [UIView animateWithDuration:2.0
						  animations:^{
				  swipePopupView.alpha  = 0.0;
			  }
						  completion:^(BOOL finished) {
				  [swipePopupView removeFromSuperview];
			  }];
	 }];
}

- (void)switchToLocalPlaybackWithPosUpdate:(BOOL)posUpdate {
//	[self addPopupView:@"Sending video back to local..."];

	if (localMode) {
		return;
	}
	localMode = YES;

	[UIView animateWithDuration:1 animations:^{
		 self.remotePlayerVC.view.alpha = 0.0;
	 } completion:^(BOOL finished) {
		 self.remotePlayerVC.view.hidden = YES;
		 self.overlayView.hidden = NO;

		 [self.overlayView play:self.overlayView.playButton];
		 if ([ACManager sharedInstance].isPaired && [ACManager sharedInstance].statusRecorder.playerOnline) {
			 [self.overlayView startHidingSwipeIndication];
		 }
		 else {
			 self.overlayView.swipeindicationView.hidden = YES;
			 self.overlayView.swipeindicationView.alpha = 0.0;
		 }
	 }];

	if (posUpdate) {
		[self.moviePlayer pause];
		NSLog (@"Playback time on TV: %lf", [ACManager sharedInstance].statusRecorder.currentPlaybackTime);
		self.moviePlayer.currentPlaybackTime = 0;
		self.moviePlayer.initialPlaybackTime = [ACManager sharedInstance].statusRecorder.currentPlaybackTime;
		[self.moviePlayer play];
	}
}

- (void)tvOnOfflineStateChanged:(NSNotification *)n {
	if (!localMode) {
		if ([n.name isEqualToString:@"AC_PLAYER_BECAME_OFFLINE"]) {
			[self.remotePlayerVC.overlayView smartTV:nil];
		}
	}
	else {
		if ([n.name isEqualToString:@"AC_PLAYER_BECAME_ONLINE"]) {
			[self.overlayView startHidingSwipeIndication];
		}
	}
	[self updateSmartTVButtonOnOverlay];
}

- (void)switchToRemotePlayback {
//	[self addPopupView:@"Sending video to TV..."];

	[[NSNotificationCenter defaultCenter]
	 addObserver:self
		selector:@selector(didRemotePlaybackStart:)
			name:@"REMOTE_PLAYBACK_STARTED"
		  object:[ACManager sharedInstance]];

	[[NSNotificationCenter defaultCenter]
	 addObserver:self
		selector:@selector(didRemotePlaybackEnd:)
			name:@"REMOTE_PLAYBACK_ENDED"
		  object:[ACManager sharedInstance]];
	localMode = NO;

	[ACManager sharedInstance].statusRecorder.isStream = self.overlayView.isLive;
	[[ACManager sharedInstance].statusRecorder updateAsset:[[self.playingAsset valueForKey:@"id"] lastPathComponent]];

	if (self.moviePlayer) {
		[[ACManager sharedInstance].statusRecorder updateAsset:[[self.playingAsset valueForKey:@"id"] lastPathComponent]];
		[ACManager sharedInstance].statusRecorder.currentPlaybackTime = self.moviePlayer.currentPlaybackTime;
		[ACManager sharedInstance].statusRecorder.duration = self.moviePlayer.duration;

		[self.remotePlayerVC.overlayView updateLive:self.overlayView.isLive];
		[self.remotePlayerVC.overlayView update:nil];
	}

	self.remotePlayerVC.view.hidden = NO;
	[UIView animateWithDuration:1 animations:^{
		 self.remotePlayerVC.view.alpha = 1.0;
	 } completion:^(BOOL finished) {
		 NSLog (@"Remote Player View animated in");
	 }];
	self.overlayView.hidden = YES;
	self.takingRemoteUpdates = NO;
	self.sendingStatusUpdate = NO;


	self.remotePlayerVC.overlayView.wasSwipedBack = NO;
	[self.overlayView pause:self.overlayView.pauseButton];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	self.moviePlayer.view.frame = self.view.bounds;

	self.overlayView.swipeindicationView.center = self.overlayView.center;

	CGRect or = self.fullscreenOverlayView.frame;

	or.origin.y = -20;

	self.fullscreenOverlayView.frame = or;

	self.overlayView.width = 0;
	[self.overlayView layoutSubviews];

	[[UIApplication sharedApplication] setStatusBarHidden:fullscreen withAnimation:UIStatusBarAnimationSlide];
	[self.view setFrame:[self.view bounds]];
	// [[UIScreen mainScreen] applicationFrame];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	BOOL portrait = toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown;

	self.fullscreenOverlayView.swipeindicationView.center = CGPointMake(portrait ? 384 : 512, portrait ? 502 : 374);
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];

	[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
	AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[delegate.currentViewController.view setNeedsDisplay];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

@end
