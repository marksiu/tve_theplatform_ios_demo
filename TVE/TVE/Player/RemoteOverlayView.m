//
//  OverlayView.m
//  CMore
//
//  Created by Gabor Bottyan on 8/6/12.
//  Copyright (c) 2012 Meganet-Soft Bt. All rights reserved.
//

#import "RemoteOverlayView.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "UIImage+Extension.h"

@interface RemoteOverlayView ()
@property (retain) UIView *controls;
@property (retain) UIView *hideable;
@property (retain) UILabel *timeLabel;
@property (retain) UIButton *timeButton;
@property (retain) UILabel *playedLabel;
@property (retain) UISlider *progressSlider;
@property (retain) UISlider *volumeSlider;

@property (retain) UIButton *prevButton;
@property (retain) UIButton *pauseButton;
@property (retain) UIButton *playButton;
@property (retain) UIButton *nextButton;
@property (retain) UIButton *smartTVButton;
@property (assign) BOOL sliderLock;
@property (assign) BOOL volumeLock;
@property (retain) UIPopoverController *popup;
@property (retain) UIPopoverController *disconnectPopup;
@property (assign) double lastSeen;
@property (retain) NSDateFormatter *timeFormatter;
@property (assign) BOOL finito;
@property (retain) UILabel *fontLabel;
// @property (retain) FontLabel * fontLabel;
@property (retain) NSTimer *timer;
@property (assign) BOOL isLive;
@property (retain) UIButton *refreshButton;
@property (retain) UIViewController *disconnectController;
@end

@implementation RemoteOverlayView

#define CONTROLS_BG_TAG    200

#define DETAIL_PANEL_WIDTH 300

@synthesize navbar, detailPanel, controls, progressSlider;
@synthesize moviePlayer, smartTVButton, timeFormatter;
@synthesize fontLabel, timer, hideable;
@synthesize refreshButton;
@synthesize volumeSlider;
@synthesize finito;
@synthesize isLive;
@synthesize timeLabel;
@synthesize playedLabel;

@synthesize backgroundImage;
@synthesize staticBackgroundImage;
@synthesize backgroundImageUrl;

@synthesize wasSwipedBack;


- (void)updateLive:(BOOL)_live {
	self.isLive = _live;

	if (_live) {
		[self updateControlsWithActiveButtons:[NSArray arrayWithObjects:self.smartTVButton, nil]];
		self.progressSlider.hidden = YES;
		self.timeLabel.hidden = YES;
		self.detailPanel.hidden = YES;
		self.backgroundImage.hidden = YES;
		self.staticBackgroundImage.hidden = NO;
	}
	else {
		[self updateControlsWithActiveButtons:[NSArray arrayWithObjects:
											   self.refreshButton,
		                                       // self.prevButton,
											   self.pauseButton, self.playButton,
		                                       // self.nextButton,
											   self.smartTVButton,
											   nil]];
		self.progressSlider.hidden = NO;
		self.playedLabel.hidden = YES;
		self.timeLabel.hidden = NO;
		self.backgroundImage.hidden = NO;
		self.staticBackgroundImage.hidden = YES;
	}
}

- (void)dealloc {
	// self.finito  =YES;

	if (timer) {
		[timer invalidate];
		self.timer = nil;
	}

	self.moviePlayer = Nil;

	self.volumeSlider = Nil;
	self.refreshButton  = Nil;
	self.pauseButton = Nil;
	self.playButton = Nil;
	self.popup = Nil;
	self.fontLabel = Nil;
	self.timeFormatter  = Nil;
	self.navbar = Nil;
	self.timeLabel = Nil;
	self.timeButton = Nil;
	self.playedLabel = Nil;
	self.titleLabel = Nil;
	self.descriptionLabel = Nil;
	self.detailPanel = Nil;
	[super dealloc];
}

- (void)updateControlsWithActiveButtons:(NSArray *)buttons {
	for (UIView *view in self.controls.subviews) {
		view.hidden = YES;
	}
	[self.controls viewWithTag:CONTROLS_BG_TAG].hidden = NO;
	int i = 0;
	int btnWidth = 60;
	int marginLeft = 20;
	int marginRight = 35;
	int paddingBefore = 15;
	for (UIButton *button in buttons) {
		if (button.superview == self.controls) {
			CGRect btnFrame = CGRectOffset(CGRectOffset(CGRectMake(0, 0, btnWidth, 30),
														marginLeft + i * (btnWidth + paddingBefore), 12), paddingBefore, 0);
			button.frame = btnFrame;
			button.hidden = NO;
			if (button != self.pauseButton) {
				i++;
			}
		}
	}
	if (i == 0 || isLive) {
		self.controls.frame = CGRectMake(0, 1024 - (marginLeft + 3 * (btnWidth + paddingBefore) + marginRight) / 2, marginLeft + 3 * (btnWidth + paddingBefore) + marginRight, 88);
		CGPoint center = self.smartTVButton.center;
		center.x = self.controls.center.x;
		self.smartTVButton.center = center;
	}
	else {
		self.controls.frame = CGRectMake(0, 1024 - DETAIL_PANEL_WIDTH - (marginLeft + i * (btnWidth + paddingBefore) + marginRight) / 2, marginLeft + i * (btnWidth + paddingBefore) + marginRight, 88);
	}
	[self.controls viewWithTag:CONTROLS_BG_TAG].frame = CGRectMake(0, 0, self.controls.frame.size.width, self.controls.frame.size.height);
	self.controls.center = CGPointMake(self.center.x - (isLive ? 0 : DETAIL_PANEL_WIDTH) / 2, self.center.y + self.frame.size.height / 3);
	int top = (i == 0) ? 35 : 50;
	self.volumeSlider.frame = CGRectMake(20, top, self.controls.frame.size.width - 40, 40);
	self.volumeSlider.hidden = NO;

	[self.volumeSlider addTarget:self action:@selector(volumeMoveStart) forControlEvents:UIControlEventTouchDown];
	// [self.volumeSlider addTarget:self action:@selector(volumeMoved) forControlEvents:UIControlEventTouchDragEnter];
	// [self.volumeSlider addTarget:self action:@selector(volumeMoved) forControlEvents:UIControlEventTouchDragInside];

	// [self.volumeSlider addTarget:self action:@selector(volumeMoveEnded) forControlEvents:UIControlEventTouchUpInside];

	[self.volumeSlider addTarget:self action:@selector(volumeMoveEnded) forControlEvents:UIControlEventTouchUpOutside | UIControlEventTouchUpInside];
}

- (void)createControls {
	if ([UIImage externalImageNamed:@"background"]) {
		self.staticBackgroundImage.image = [UIImage externalImageNamed:@"background"];
	}
	else {
		self.staticBackgroundImage.image = [UIImage imageNamed:@"background.png"];
	}
	self.controls = [[[UIView alloc] init] autorelease];
	self.controls.layer.masksToBounds = NO;
	UIImage *panelImage = [UIImage imageNamed:@"panel.png"];
	panelImage = [panelImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 4, 0, 4)];
	UIImageView *bg = [[[UIImageView alloc] initWithImage:panelImage] autorelease];
	bg.tag = CONTROLS_BG_TAG;
	[self.controls addSubview:bg];

	self.refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.refreshButton.showsTouchWhenHighlighted = YES;
	[self.refreshButton setImage:[UIImage imageNamed:@"30.png"] forState:UIControlStateNormal];
	[self.refreshButton addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventTouchUpInside];
	[self.controls addSubview:self.refreshButton];

	self.prevButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.prevButton.showsTouchWhenHighlighted = YES;
	[self.prevButton setImage:[UIImage imageNamed:@"prev.png"] forState:UIControlStateNormal];
	[self.controls addSubview:self.prevButton];
	[self.prevButton addTarget:self action:@selector(prev:) forControlEvents:UIControlEventTouchUpInside];

	self.pauseButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.pauseButton.showsTouchWhenHighlighted = YES;
	[self.pauseButton setImage:[UIImage imageNamed:@"pause.png"] forState:UIControlStateNormal];
	[self.pauseButton addTarget:self action:@selector(pause:) forControlEvents:UIControlEventTouchUpInside];
	[self.controls addSubview:self.pauseButton];

	self.playButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.playButton.showsTouchWhenHighlighted = YES;
	[self.playButton setImage:[UIImage imageNamed:@"player_play.png"] forState:UIControlStateNormal];
	[self.playButton addTarget:self action:@selector(play:) forControlEvents:UIControlEventTouchUpInside];
	self.playButton.hidden = YES;
	[self.controls addSubview:self.playButton];

	self.nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.nextButton.showsTouchWhenHighlighted = YES;
	[self.nextButton setImage:[UIImage imageNamed:@"next.png"] forState:UIControlStateNormal];
	[self.nextButton addTarget:self action:@selector(next:) forControlEvents:UIControlEventTouchUpInside];
	[self.controls addSubview:self.nextButton];

	self.smartTVButton = [UIButton buttonWithType:UIButtonTypeCustom];
	self.smartTVButton.showsTouchWhenHighlighted = YES;
	[self.smartTVButton setImage:[UIImage imageNamed:@"smarttv_active.png"] forState:UIControlStateNormal];
	[self.smartTVButton addTarget:self action:@selector(smartTV:) forControlEvents:UIControlEventTouchUpInside];
	[self.controls addSubview:self.smartTVButton];


	self.volumeSlider = [[[UISlider alloc] init] autorelease];
	self.volumeSlider.maximumValue = 1.0;
	self.volumeSlider.minimumValue = 0.0;
	self.volumeSlider.value = self.moviePlayer.volume / 100.0;
	self.volumeSlider.enabled = YES;
	[self.volumeSlider sizeToFit];
	[self.controls addSubview:self.volumeSlider];
	[volumeSlider release];
}

- (void)awakeFromNib {
	self.timeFormatter = [[[NSDateFormatter alloc] init] autorelease];
	[timeFormatter setDateFormat:@"HH:mm:ss"];
	[timeFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];

	self.staticBackgroundImage = [[[UIImageView alloc] initWithFrame:self.frame] autorelease];
	[self.staticBackgroundImage setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth ];
	[self addSubview:self.staticBackgroundImage];

	self.backgroundImage = [[[VSRemoteImageView alloc] initWithFrame:self.frame] autorelease];
	[self.backgroundImage setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth ];
	[self addSubview:self.backgroundImage];

	self.hideable = [[[UIView alloc] initWithFrame:self.frame] autorelease];
	self.width = self.frame.size.width;
	self.height = self.frame.size.height;
	// self.hideable.alpha = minalpha;
	[self setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
	[self.hideable setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
	[self setBackgroundColor:[UIColor whiteColor]];

	[self createControls];

	[self.hideable addSubview:self.controls];

	self.navbar = [[[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 42)] autorelease];
	self.navbar.barStyle = UIBarStyleBlack;

	self.detailPanel = [[[UIScrollView alloc] initWithFrame:CGRectMake(self.frame.size.width - DETAIL_PANEL_WIDTH, 42, DETAIL_PANEL_WIDTH, self.frame.size.height - 42)] autorelease];
	[self.detailPanel setBackgroundColor:[UIColor blackColor]];
	self.detailPanel.alpha = 0.8;
	self.titleLabel = [[[FontLabel alloc] initWithFrame:CGRectMake(15, 15, DETAIL_PANEL_WIDTH - 30, 50)] autorelease];
	[self.titleLabel setTextColor:[UIColor whiteColor]];
	[self.titleLabel setBackgroundColor:[UIColor clearColor]];
	self.titleLabel.numberOfLines = 0;
	self.descriptionLabel = [[[FontLabel alloc] initWithFrame:CGRectMake(15, 55, DETAIL_PANEL_WIDTH - 30, 0)] autorelease];
	[self.descriptionLabel setTextColor:[UIColor whiteColor]];
	[self.descriptionLabel setBackgroundColor:[UIColor clearColor]];
	self.descriptionLabel.numberOfLines = 0;
	[self.detailPanel addSubview:self.titleLabel];
	[self.detailPanel addSubview:self.descriptionLabel];

	// self.navbar.translucent = YES;
	// self.navbar.tintColor = [UIColor blackColor];
	[self.hideable addSubview:self.navbar];
	[self.hideable addSubview:self.detailPanel];

	[self addSubview:self.hideable];

	// UIView * progressHolder = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width - 200, 45)];

	self.progressSlider =  [[[UISlider alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width - 200, 45)] autorelease];
	self.progressSlider.continuous = NO;

	// /[progressHolder addSubview:self.progressSlider];
	// progressHolder.backgroundColor = [UIColor yellowColor];
	[self.progressSlider addTarget:self action:@selector(sliderMoveStart) forControlEvents:UIControlEventTouchDown];
	// [self.progressSlider addTarget:self action:@selector(sliderMoved) forControlEvents:UIControlEventTouchDragEnter];
	// [self.progressSlider addTarget:self action:@selector(sliderMoved) forControlEvents:UIControlEventTouchDragInside];

	// [self.progressSlider addTarget:self action:@selector(sliderMoveEnded) forControlEvents:UIControlEventTouchUpInside];

	[self.progressSlider addTarget:self action:@selector(sliderMoveEnded) forControlEvents:UIControlEventValueChanged];

	UINavigationItem *navItem = [[UINavigationItem alloc] initWithTitle:@""];

	navItem.titleView = self.progressSlider;

	[navbar pushNavigationItem:navItem animated:NO];

	// navItem.leftBarButtonItem  =
	UIBarButtonItem *okbutton = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(ok:)] autorelease];

	navItem.leftBarButtonItems = [NSArray arrayWithObjects:okbutton, self.playedLabel, nil];
	/*
	 * NSString * okString = [[LocalizationSystem sharedLocalSystem]locStringForKey: @"lblDone"];
	 *
	 * navItem.leftBarButtonItem  = [[[UIBarButtonItem alloc] initWithTitle:@"faszom" style:UIBarButtonSystemItemDone target:self	 action:@selector(ok:)] autorelease];
	 */
	self.timeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 20)] autorelease];
	self.timeButton = [[[UIButton alloc] initWithFrame:timeLabel.frame] autorelease];
	self.timeButton.backgroundColor = [UIColor clearColor];
	[self.timeButton addTarget:self action:@selector(didChangeCountMode:) forControlEvents:UIControlEventTouchUpInside];

	self.playedLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 20)] autorelease];
	self.playedLabel.hidden = YES;
	[self.timeLabel setFont:[UIFont systemFontOfSize:14]];
	[self.playedLabel setFont:[UIFont systemFontOfSize:14]];
	self.timeLabel.text = @"";
	self.playedLabel.text = @"";

	UIView *rightView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 20)] autorelease];

	[rightView addSubview:self.timeLabel];
	[rightView addSubview:self.playedLabel];
	[rightView addSubview:self.timeButton];

	navItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:rightView] autorelease];
	self.timeLabel.textColor = [UIColor whiteColor];
	self.timeLabel.backgroundColor = [UIColor clearColor];
	self.playedLabel.textColor = [UIColor whiteColor];
	self.playedLabel.backgroundColor = [UIColor clearColor];

	[navItem release];

	UISwipeGestureRecognizer *recognizer;
	recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(smartTV:)];
	[recognizer setDirection:UISwipeGestureRecognizerDirectionDown];
	[self.hideable addGestureRecognizer:recognizer];
	[recognizer release];

	self.disconnectController = [[[UIViewController alloc] init] autorelease];
	self.disconnectController.view.frame = CGRectMake(0, 0, 320, 60);

	UIButton *okButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 303, 45)];
	[okButton setBackgroundImage:[UIImage imageNamed:@"disconnect_button_bg.png"] forState:UIControlStateNormal];
	[okButton.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:20]];
	okButton.titleLabel.textColor = [UIColor blackColor];
	[okButton setTitle:@"OK" forState:UIControlStateNormal];
	[okButton addTarget:self action:@selector(disconnectButtonOk:) forControlEvents:UIControlEventTouchUpInside];
	[self.disconnectController.view addSubview:okButton];
	[okButton release];
}

- (void)didChangeCountMode:(id)sender {
	if (self.playedLabel.hidden) {
		self.playedLabel.hidden = NO;
		self.timeLabel.hidden = YES;
	}
	else {
		self.playedLabel.hidden = YES;
		self.timeLabel.hidden = NO;
	}
}

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];

	if (self) {
		[self awakeFromNib];
	}

	return self;
}

- (void)sliderMoveStart {
	// NSLog(@"slider move start");
	self.sliderLock = YES;
	[self touch];
}

- (void)sliderMoved {
	// NSLog(@"slider move");
	[self touch];
}

- (void)sliderMoveEnded {
	// NSLog(@"slider move end");
	[self.moviePlayer seek:MIN(self.moviePlayer.duration - 1, self.moviePlayer.duration * self.progressSlider.value)];
	self.sliderLock = NO;
	[self touch];
}

- (void)update:(id)sender {
	if (finito) {
		return;
	}

	[self updateButtons];

	// double now = [NSDate timeIntervalSinceReferenceDate];

	/*if (now-self.lastSeen > 4.0f){
	 * [self hideHud];
	 * }*/

	if (self.moviePlayer) {
		// self.moviePlayer.useApplicationAudioSession = YES;
		if (self.moviePlayer.playbackState == ACPlaybackStatePlaying || self.volumeSlider.value == 0.0f) {
			// NSLog (@"DURATION: %f CURENT %f",self.moviePlayer.duration,self.moviePlayer.currentPlaybackTime);
			double remaining = self.moviePlayer.duration - self.moviePlayer.currentPlaybackTime;

			NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:remaining];
			NSDate *playeddate = [NSDate dateWithTimeIntervalSinceReferenceDate:self.moviePlayer.currentPlaybackTime];
			self.timeLabel.text = [@"-" stringByAppendingString:[timeFormatter stringFromDate:date]];
			self.playedLabel.text = [@" " stringByAppendingString:[timeFormatter stringFromDate:playeddate]];
			// NSLog(@"%@",self.playedLabel.text);
			if (self.moviePlayer.duration != 0 && !self.sliderLock) {
				self.progressSlider.value = self.moviePlayer.currentPlaybackTime / self.moviePlayer.duration;
				// NSLog(@"%f  %f %f", self.moviePlayer.currentPlaybackTime, self.moviePlayer.initialPlaybackTime, self.moviePlayer.duration );
			}

			// self.volumeSlider.value = [MPMusicPlayerController applicationMusicPlayer].volume;
		}
		self.volumeSlider.value = self.moviePlayer.volume / 100.0;
	}
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
	// NSLog(@"dismissed");
}

- (void)volumeMoved {
	// NSLog(@"volume move");
	[self touch];
}

- (void)volumeMoveEnded {
	// NSLog(@"volume move end");
	// [MPMusicPlayerController applicationMusicPlayer].volume = self.volumeSlider.value;
	// self.moviePlayer.currentPlaybackTime = self.moviePlayer.duration * self.progressSlider.value;
	self.volumeLock = NO;
	[self.moviePlayer volumeChange:self.volumeSlider.value];
	[self touch];
}

- (void)volumeMoveStart {
	// NSLog(@"volume move start");
	self.volumeLock = YES;
}

- (void)pause:(id)sender {
	[self.moviePlayer pause];
	[self layoutSubviews];
	[self touch];
}

- (void)play:(id)sender {
	[self.moviePlayer play];
	[self layoutSubviews];
	[self touch];
}

- (void)prev:(id)sender {
	self.moviePlayer.currentPlaybackTime -= 2;
	[self touch];
}

- (void)next:(id)sender {
	self.moviePlayer.currentPlaybackTime += 2;
	[self touch];
}

- (void)smartTVIcon:(id)sender {
	[self showDisconnectPopup];
}

- (void)disconnectButtonOk:(id)sender {
	[self ok:sender];
	[[ACManager sharedInstance] disconnect];
	[self.disconnectPopup dismissPopoverAnimated:YES];
}

- (void)smartTV:(id)sender {
	self.wasSwipedBack = YES;
	[self.delegate addPopupView:@"Sending video back to iPad..."];
	[self.delegate switchToLocalPlaybackWithPosUpdate:YES];

	[self.moviePlayer terminateControl];
}

- (void)refresh:(id)sender {
	[self.moviePlayer seek:self.moviePlayer.currentPlaybackTime - 30];
	self.moviePlayer.currentPlaybackTime -= 30;
	[self touch];
}

- (void)ok:(id)sender {
	if (self.delegate) {
		[self.delegate closeRemoteOverlayView:sender];
	}

	if (self.navbar.topItem.leftBarButtonItem) {
		self.finito  = YES;
		self.moviePlayer = Nil;
	}
}

- (void)updateButtons {
	if (self.moviePlayer && !self.isLive) {
		if (self.moviePlayer.playbackState != ACPlaybackStatePlaying) {
			self.pauseButton.hidden = YES;
			self.playButton.hidden = NO;
		}
		else {
			self.pauseButton.hidden = NO;
			self.playButton.hidden = YES;
		}
	}
}

- (void)showDisconnectPopup {
	if (!self.disconnectPopup) {
		UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:self.disconnectController];
		self.disconnectController.title = @"Bryt enhetskoppling";

		UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
		[button setTitle:@"Close" forState:UIControlStateNormal];
		[button setBackgroundImage:[UIImage imageNamed:@"bar_button_bg.png"] forState:UIControlStateNormal];
		[button.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0]];
		// [button addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
		UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:button];
		[button release];
		[rightBarButton setStyle:UIBarButtonItemStylePlain];
		self.disconnectController.navigationItem.rightBarButtonItem = rightBarButton;
		[rightBarButton release];

		self.disconnectPopup = [[[UIPopoverController alloc] initWithContentViewController:nav] autorelease];

		[nav release];
		[self.disconnectPopup setPopoverContentSize:CGSizeMake(320, 100)];
		self.disconnectPopup.delegate = self;
	}

	CGRect rect = CGRectMake(self.controls.frame.origin.x + self.smartTVButton.frame.origin.x + 30, self.controls.frame.origin.y + self.smartTVButton.frame.origin.y, 1, 1);
	[self.disconnectPopup presentPopoverFromRect:rect inView:self permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

- (void)updateTimeCode:(NSTimeInterval)theElapsed {
	/* if (IS_DEBUG_MODE) {
	 * int elapsed = (int)(theElapsed * 1000.0);
	 * int h = elapsed / 3600000;
	 * elapsed -= (h * 3600000);
	 * int m = elapsed / 60000;
	 * elapsed -= (m * 60000);
	 * int s = elapsed / 1000;
	 * elapsed -= (s * 1000);
	 * int ms = elapsed;
	 * NSString *timeCode = [NSString stringWithFormat:@"%02d:%02d:%02d.%03d", h, m, s, ms];
	 * NSLog(@"%@", timeCode);
	 * //self.timeCodeLabel.text = timeCode;
	 * }*/
}

- (void)repositionAndScaleBackground {
	UIImage *image = self.backgroundImage.image;
	double w = image.size.width;
	double h = image.size.height;

	double ratio = 0;

	if (h != 0) {
		ratio = w / h;
	}

	CGRect frame = self.backgroundImage.frame;
	int height = 0;
	int width = 0;

	int screenH;
	int screenW;

	screenH = 768;
	screenW = 1024;
	if ([ACManager sharedInstance].statusRecorder.isStream) {
		width = 315;
		height = 152;
	}
	else {
		height = 768;
		width = 1024;
	}
	int top_pad = 42;
	int bottom_pad = 0;

	/*if (!shouldShowCloseButton) {
	 * bottom_pad = 93;
	 * }*/

	frame.size.height = height - top_pad - bottom_pad;
	frame.origin.y    = top_pad;
	frame.size.width = frame.size.height * ratio;
	frame.origin.x = (width - frame.size.width) / 2;
	self.backgroundImage.frame = frame;

	self.staticBackgroundImage.frame = self.frame;
}

- (void)layoutSubviews {
	[super layoutSubviews];
	if (self.width != self.frame.size.width || self.height != self.frame.size.height) {
		self.width = self.frame.size.width;
		self.height = self.frame.size.height;
		self.hideable.frame = self.frame;
		self.navbar.frame = CGRectMake(0, 0, self.frame.size.width, 42);
		self.detailPanel.frame = CGRectMake(self.frame.size.width - DETAIL_PANEL_WIDTH, 42, DETAIL_PANEL_WIDTH, self.frame.size.height - 42);
		self.controls.center = CGPointMake(self.center.x - (isLive ? 0 : DETAIL_PANEL_WIDTH) / 2, self.center.y + self.frame.size.height / 3);
		self.progressSlider.frame = CGRectInset(CGRectMake(0, 0, self.frame.size.width - 100, 40), 80, 0);

		self.fontLabel.frame = CGRectInset(CGRectMake(0, self.frame.size.height * 3 / 4, self.frame.size.width, self.frame.size.height / 4), 20, 20);
		[self.popup dismissPopoverAnimated:YES];
		[self repositionAndScaleBackground];
	}
}

- (void)touch {
	self.lastSeen = [NSDate timeIntervalSinceReferenceDate];
}

- (void)showHud {
	// NSLog(@"showhud");
	[self touch];
	self.hideable.hidden = NO;
	[UIView animateWithDuration:0.5f delay:0.0 options:UIViewAnimationOptionAllowUserInteraction |                       UIViewAnimationOptionBeginFromCurrentState animations:^{
		 self.hideable.alpha = 1.0f;
	 } completion:^(BOOL finished) {
	 }];
}

- (void)hideHud {
	// NSLog(@"hidehud");
	[UIView animateWithDuration:0.5f delay:0.0 options:UIViewAnimationOptionAllowUserInteraction |                       UIViewAnimationOptionBeginFromCurrentState animations:^{
		 self.hideable.alpha = 0.02;
		 [self.popup dismissPopoverAnimated:YES];
	 } completion:^(BOOL finished) {
		 self.hideable.hidden = YES;
	 }];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	// [self showHud];
	/*self.swipeindicationView.hidden = NO;
	 * [UIView animateWithDuration:4.0 animations:^{
	 * self.swipeindicationView.alpha = 1.0;
	 * } completion:^(BOOL finished) {
	 *
	 * }];*/
}

@end
