//
//  UIViewController+TVERemoteControlPanel.h
//  TVE
//
//  Created by Csaba Tűz on 2013.01.03..
//  Copyright (c) 2013 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ACRemoteControlPopup.h"
#import <AccedoConnect/ACPairingViewController.h>
#import "BaseViewController.h"

@interface BaseViewController (TVERemoteControlPanel)

@property (retain) UIPopoverController *acRemotePopup;
@property (retain) ACRemoteControlPopup *acRemoteController;
@property (retain, nonatomic) ACPairingViewController *acPairingPopup;
@property (nonatomic, retain) UIButton *acRemoteNavButton;

- (UIButton *)acRemoteButton;

- (void)performTaskRequiringPairing:(void(^) (void)) action onFailure:(void(^) (void))failBlock;

@end
