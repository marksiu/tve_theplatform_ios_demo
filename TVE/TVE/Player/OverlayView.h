//
//  OverlayView.h
//  CMore
//
//  Created by Gabor Bottyan on 8/6/12.
//  Copyright (c) 2012 Meganet-Soft Bt. All rights reserved.
//
#import <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>

#pragma mark OverlayViewDelegate

@protocol OverlayViewDelegate <NSObject, UIPopoverControllerDelegate>

- (void)close:(id)sender;

- (void)sendKeepAlive:(id)n;

- (void)sendStartStream:(id)n;
- (void)sendStopStream:(id)n;
@end


#pragma mark OverlayViewProtocol

@protocol OverlayViewProtocol

@property (nonatomic, assign) MPMoviePlayerController *moviePlayer;
@property (nonatomic, assign) id <OverlayViewDelegate> delegate;

@property (assign) BOOL finito;

@property (retain) UIButton *pauseButton;
@property (retain) UIButton *playButton;
@property (retain) UIButton *smartTVButton;


@property (retain) UIImageView *swipeindicationView;

@property (nonatomic, retain) CADisplayLink *displayLink;

@property (assign) BOOL isLive;

@property (assign) float width;

- (void)pause:(id)sender;
- (void)play:(id)sender;

- (void)startHidingSwipeIndication;

- (void)setSpinnerAnimate:(BOOL)active;

- (void)updateLive:(BOOL)_live;

- (void)update:(id)sender;

@end


#pragma mark OverlayView

@interface OverlayView : UIView<UIPopoverControllerDelegate, OverlayViewProtocol>

@property (nonatomic, assign) MPMoviePlayerController *moviePlayer;
@property (nonatomic, assign) id <OverlayViewDelegate> delegate;

@property (retain) UIButton *pauseButton;
@property (retain) UIButton *playButton;
@property (retain) UIButton *smartTVButton;

- (id)initWithFrame:(CGRect) frame andDelegate:(id<OverlayViewDelegate>)delegate;

@property (retain) UIImageView *swipeindicationView;

@property (nonatomic, retain) CADisplayLink *displayLink;

@property (assign) BOOL isLive;

@property (assign) BOOL finito;

@property (assign) float width;


- (void)setSmartTVPlaying:(BOOL)playing;
- (void)setSpinnerAnimate:(BOOL)active;

@end
