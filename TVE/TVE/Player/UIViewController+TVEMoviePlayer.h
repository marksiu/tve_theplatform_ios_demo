//
//  UIViewController+TVEMoviePlayer.h
//  TVE
//
//  Created by Csaba Tűz on 2013.01.03..
//  Copyright (c) 2013 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACRemoteControlPopup.h"
#import "CMOREPlayerViewController.h"

@interface UIViewController (TVEMoviePlayer)

@property (retain, nonatomic) CMOREPlayerViewController *cmorePlayer;
@property (nonatomic, retain) NSTimer *remoteTimeoutTimer;

- (void)handleMPMoviePlayerPlaybackDidFinishNotification:(NSNotification *)n;

- (void) playMovie:(NSDictionary *)asset
		  isStream:(BOOL) isStream
		  isRemote:(BOOL) remote
   initialPosition:(double)initialPosition
	 containerView:(UIView *)containerView;

- (void)closePlayer;

@end
