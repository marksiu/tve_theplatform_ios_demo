//
//  ACRemoteControlPopup.m
//  CMore
//
//  Created by Csaba Tűz on 2012.10.29..
//
//

#import "ACRemoteControlPopup.h"

#import "AppDelegate.h"
#import "Constants.h"

@interface ACRemoteControlPopup () {
	BOOL playbackRunning;
	BOOL shouldShowControls;
	BOOL shouldTVBeEnabled;
}

@end

@implementation ACRemoteControlPopup
@synthesize delegate;
@synthesize controlCell;
@synthesize popup;
@synthesize isLive;
@synthesize volumeLock;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackStatusUpdated:) name:@"PLAYBACK_STATUS_CHANGED" object:[ACManager sharedInstance]];

		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didClientDisconnected:) name:@"AC_CLIENT_DISCONNECTED" object:[ACManager sharedInstance]];

		ACManager *acManager = [ACManager sharedInstance];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI) name:@"AC_PAIRED" object:acManager];

		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI) name:@"AC_CONNECTION_FAILED" object:acManager];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI) name:@"AC_UNPAIRED" object:acManager];



		shouldShowControls = NO;
		playbackRunning = [ACManager sharedInstance].statusRecorder.playbackState != ACPlaybackStateTerminated;

		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(update:) name:@"AC_UPDATE_OVERLAY" object:[ACManager sharedInstance].statusRecorder];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(assetUpdated:) name:@"AC_ASSET_UPDATED" object:[ACManager sharedInstance].statusRecorder];

		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI) name:@"AC_PLAYER_BECAME_ONLINE" object:[ACManager sharedInstance]];
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI) name:@"AC_PLAYER_BECAME_OFFLINE" object:[ACManager sharedInstance]];
	}
	return self;
}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];

	if (popup) {
		self.popup = nil;
	}

	if (controlCell) {
		self.controlCell = nil;
	}

	[super dealloc];
}

- (void)updateLive:(BOOL)_live {
	self.isLive = _live;
	[self updateUI];
}

- (void)viewDidLoad {
	[super viewDidLoad];

	[self updateUI];
	self.tableView.scrollEnabled = NO;
	// Do any additional setup after loading the view from its nib.

	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(handleQRButtonClicked:)
												 name:@"QRBUTTONCLICKED"
											   object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
	[self updateUI];
}

- (void)viewDidAppear:(BOOL)animated {
	[self updateUI];
}

- (void)updateUI {
	shouldShowControls = [[ACManager sharedInstance] isPaired] && [ACManager sharedInstance].statusRecorder.playerOnline;
	shouldTVBeEnabled = [ACManager sharedInstance].statusRecorder.playerOnline || ![[ACManager sharedInstance] isPaired];
	[self.tableView reloadData];

	if (self.controlCell) {
		if ([ACManager sharedInstance].statusRecorder.playbackState == ACPlaybackStatePlaying) {
			self.controlCell.playBtn.hidden = YES;
			self.controlCell.pauseBtn.hidden = NO;
		}
		else {
			self.controlCell.playBtn.hidden = NO;
			self.controlCell.pauseBtn.hidden = YES;
		}

		self.controlCell.playBtn.enabled = playbackRunning && !self.isLive;
		self.controlCell.pauseBtn.enabled = playbackRunning && !self.isLive;
		self.controlCell.backBtn.enabled = playbackRunning && !self.isLive;
		self.controlCell.fwdBtn.enabled = playbackRunning && !self.isLive;
		self.controlCell.rewBtn.enabled = playbackRunning && !self.isLive;
		self.controlCell.volumeSlider.enabled = playbackRunning;
		if (!volumeLock) {
			self.controlCell.volumeSlider.value = [ACManager sharedInstance].statusRecorder.volume / 100.0;
		}
	}

	if (shouldShowControls) {
		[self.delegate.acRemotePopup setPopoverContentSize:CGSizeMake(320, 290)];
	}
	else {
		[self.delegate.acRemotePopup setPopoverContentSize:CGSizeMake(320, 165)];
	}
}

- (void)didClientDisconnected:(NSNotification *)notification {
	NSString *uuid = [notification.userInfo valueForKey:@"uuid"];

	if ([uuid isEqualToString:[ACManager sharedInstance].statusRecorder.player]) {
		playbackRunning = NO;
		[self updateUI];
	}
}

- (void)playbackStatusUpdated:(NSNotification *)notification {
	if (!notification) {
		return;
	}
	NSString *deviceType = [[notification userInfo] valueForKey:@"device_type"];
	if (![deviceType hasPrefix:@"TV"]) {
		return;
	}

	if ([ACManager sharedInstance].statusRecorder.playbackState != ACPlaybackStateTerminated) {
		playbackRunning = YES;
	}
	else {
		playbackRunning = NO;
	}

	[self updateLive:[ACManager sharedInstance].statusRecorder.isStream];

	self.controlCell.playBtn.enabled = YES;
	self.controlCell.pauseBtn.enabled = YES;
	self.controlCell.backBtn.enabled = YES;
	self.controlCell.fwdBtn.enabled = YES;
	self.controlCell.rewBtn.enabled = YES;

	switch ([ACManager sharedInstance].statusRecorder.playbackState)
	{
		case ACPlaybackStatePlaying :
			self.controlCell.playBtn.hidden = YES;
			self.controlCell.pauseBtn.hidden = NO;
			break;
		case ACPlaybackStatePaused :
			self.controlCell.playBtn.hidden = NO;
			self.controlCell.pauseBtn.hidden = YES;
			break;
		case ACPlaybackStateTerminated :
			playbackRunning = NO;
			[self updateUI];
			break;
		default :
			break;
	}
}

- (void)resizeToFit {
	CGRect frame = self.tableView.frame;

	frame.size = self.tableView.contentSize;
	self.tableView.frame = frame;
	self.view.frame = frame;
	[self.delegate performSelector:@selector(resizeACRemotePopover)];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
	UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake((tableView.frame.size.width - 185) / 2, 10, 185, 30)];
	UILabel *footerLabel = [[UILabel alloc] initWithFrame:footerView.frame];

	[footerLabel setText:@"Select playback target"];
	[footerLabel setTextColor:[UIColor lightGrayColor]];
	[footerLabel setTextAlignment:NSTextAlignmentCenter];
	[footerLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:11.0]];
	footerLabel.adjustsFontSizeToFitWidth = NO;
	footerLabel.numberOfLines = 0;
	[footerView addSubview:footerLabel];
	[footerLabel release];
	[footerView setBackgroundColor:[UIColor whiteColor]];
	return [footerView autorelease];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
	return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;

	if (indexPath.row == 0) {
		if (shouldTVBeEnabled) {
			[self.delegate performTaskRequiringPairing:^{
				 shouldShowControls = YES;
				 appDelegate.playOnAccedoConnect = YES;
			 } onFailure:nil];
		}
	}
	else if ((indexPath.row == 1 && !shouldShowControls) || (shouldShowControls &&  indexPath.row == 2)) {
		if ([ACManager sharedInstance].statusRecorder.playbackState != ACPlaybackStateTerminated) {
			[self.delegate playMovie:[ACManager sharedInstance].statusRecorder.asset
							isStream:[ACManager sharedInstance].statusRecorder.isStream
							isRemote:NO
					 initialPosition:[ACManager sharedInstance].statusRecorder.currentPlaybackTime containerView:nil];
			[self.delegate.acRemotePopup dismissPopoverAnimated:NO];
			[[ACManager sharedInstance].statusRecorder terminateControl];
		}
		appDelegate.playOnAccedoConnect = NO;
	}
	[self updateUI];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	if (shouldShowControls && indexPath.row == 1) {
		return 120;
	}
	else {
		return 40;
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *cellIdentifier = @"DeviceCell";
	static NSString *rcCellIdentifier = @"RemoteControlCell";
	AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
	UITableViewCell *cell;

	if (shouldShowControls && indexPath.row == 1) {
		cell = [tableView dequeueReusableCellWithIdentifier:rcCellIdentifier];
		if (cell == nil) {
			[[NSBundle mainBundle] loadNibNamed:@"RemoteControlCell" owner:self options:nil];
			self.controlCell.delegate = self;
			[self.controlCell.volumeSlider addTarget:self action:@selector(volumeMoveStart) forControlEvents:UIControlEventTouchDown];
			// [self.volumeSlider addTarget:self action:@selector(volumeMoved) forControlEvents:UIControlEventTouchDragEnter];
			// [self.volumeSlider addTarget:self action:@selector(volumeMoved) forControlEvents:UIControlEventTouchDragInside];

			// [self.volumeSlider addTarget:self action:@selector(volumeMoveEnded) forControlEvents:UIControlEventTouchUpInside];

			[self.controlCell.volumeSlider addTarget:self action:@selector(volumeMoveEnded) forControlEvents:UIControlEventTouchUpOutside | UIControlEventTouchUpInside];
			cell = self.controlCell;
			[self updateUI];
		}
	}
	else {
		cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
		if (cell == nil) {
			UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 10, 20, 20)];
			imgView.tag = 400;
			switch (indexPath.row)
			{
				case 0 :
					cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
					break;
				case 1 :
				case 2 :
				case 3 :
					cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
					break;
			}
			[cell addSubview:imgView];
			[imgView release];
			[imgView setContentMode:UIViewContentModeScaleAspectFit];
		}
		cell.indentationLevel = 1;
		cell.indentationWidth = 24;
		UIImageView *imgView;
		switch (indexPath.row)
		{
			case 0 :
				imgView = (UIImageView *)[cell viewWithTag:400];
				[imgView setImage:[UIImage imageNamed:@"rc_tv_img.png"]];
				cell.selectionStyle = UITableViewCellSelectionStyleBlue;
				if (appDelegate.playOnAccedoConnect) {
					[cell.textLabel setTextColor:[UIColor colorWithRed:56 / 255.0 green:84 / 255.0 blue:135 / 255.0 alpha:1.0]];
					[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
				}
				else {
					[cell.textLabel setTextColor:[UIColor blackColor]];
					[cell setAccessoryType:UITableViewCellAccessoryNone];
				}
				if (!shouldTVBeEnabled) {
					[cell.textLabel setTextColor:[UIColor grayColor]];
					[cell setAccessoryType:UITableViewCellAccessoryNone];
					cell.selectionStyle = UITableViewCellSelectionStyleNone;
				}
				if ([ACManager sharedInstance].statusRecorder.pairedDeviceName == nil || (id)[ACManager sharedInstance].statusRecorder.pairedDeviceName == [NSNull null]) {
					[cell.textLabel setText:@"External device"];
				}
				else {
					[cell.textLabel setText:[ACManager sharedInstance].statusRecorder.pairedDeviceName];
				}
				break;
			case 1 :
			case 2 :
			case 3 :
				imgView = (UIImageView *)[cell viewWithTag:400];
				[imgView setImage:[UIImage imageNamed:@"rc_ipad_img.png"]];
				if (!appDelegate.playOnAccedoConnect) {
					[cell.textLabel setTextColor:[UIColor colorWithRed:56 / 255.0 green:84 / 255.0 blue:135 / 255.0 alpha:1.0]];
					[cell setAccessoryType:UITableViewCellAccessoryCheckmark];
				}
				else {
					[cell.textLabel setTextColor:[UIColor blackColor]];
					[cell setAccessoryType:UITableViewCellAccessoryNone];
				}
				[cell.textLabel setText:[[UIDevice currentDevice] name]];
				break;
		}
	}
	return cell;
}

- (int)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	int i = 2;

	if (shouldShowControls) {
		++i;
	}
	return i;
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
	// NSLog(@"dismissed");
}

- (IBAction)pause:(id)sender {
	[[ACManager sharedInstance].statusRecorder pause];
}

- (IBAction)play:(id)sender {
	[[ACManager sharedInstance].statusRecorder play];
}

- (IBAction)prev:(id)sender {
	[[ACManager sharedInstance].statusRecorder seek:[ACManager sharedInstance].statusRecorder.currentPlaybackTime - 2];
}

- (IBAction)next:(id)sender {
	[[ACManager sharedInstance].statusRecorder seek:[ACManager sharedInstance].statusRecorder.currentPlaybackTime + 2];
}

- (IBAction)refresh:(id)sender {
	[[ACManager sharedInstance].statusRecorder seek:[ACManager sharedInstance].statusRecorder.currentPlaybackTime - 30];
}

- (void)volumeMoved {
	// NSLog(@"volume move");
}

- (void)volumeMoveEnded {
	// NSLog(@"volume move end");
	// [MPMusicPlayerController applicationMusicPlayer].volume = self.volumeSlider.value;
	// self.moviePlayer.currentPlaybackTime = self.moviePlayer.duration * self.progressSlider.value;
	self.volumeLock = NO;
	[[ACManager sharedInstance].statusRecorder volumeChange:self.controlCell.volumeSlider.value];
}

- (void)volumeMoveStart {
	// NSLog(@"volume move start");
	self.volumeLock = YES;
}

- (void)update:(id)sender {
	[self updateUI];
}

- (void)assetUpdated:(NSNotification *)n {
}

- (void)handleQRButtonClicked:(NSNotification *)n {
	if ([[n.userInfo valueForKey:@"fullscreen"] boolValue]) {
		return;
	}
	NSLog(@"didReadQRButtonClicked called. Should show camera...");
	// QR Code reader implementation...
	ZBarReaderViewController *reader = [[[ZBarReaderViewController alloc] init] autorelease];
	reader.readerDelegate = self;
	/*[reader.scanner setSymbology: ZBAR_QRCODE
	 * config: ZBAR_CFG_ENABLE
	 * to: 0];*/
	reader.readerView.zoom = 1.0;

	[CURRENT_WINDOW.rootViewController presentViewController:reader animated:YES completion:nil];
}

#pragma mark - ZBarReaderDelegate

// - (void) readerControllerDidFailToRead: (ZBarReaderController*) reader withRetry: (BOOL) retry {
//    NSLog(@"readerControllerDidFailToRead...");
//
// }

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	NSLog(@"imagePickerControllerDidCancel...");

	[CURRENT_WINDOW.rootViewController dismissViewControllerAnimated:YES completion:^{
		 dispatch_async (dispatch_get_main_queue (), ^{
							 if (shouldTVBeEnabled) {
								 [self.delegate performTaskRequiringPairing:^{
										  shouldShowControls = YES;
										  ((AppDelegate *)[UIApplication sharedApplication].delegate).playOnAccedoConnect = YES;
									  } onFailure:nil];
							 }
						 });
	 }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	NSLog(@"imagePickerController didFinish...");

	id<NSFastEnumeration> results = [info objectForKey:ZBarReaderControllerResults];

	for (ZBarSymbol *zbs in results) {
		if (zbs.data) {
			// NSInteger i = [zbs.data rangeOfString:@"token="].location;
			// NSInteger i = 0;

			// if (i < [zbs.data length]) {
			//  NSString * code = [zbs.data substringFromIndex: (i + [@"token=" length])];
			NSString *code = zbs.data;


			/*
			 * [CURRENT_WINDOW dismissViewControllerAnimated:YES completion:^(void) {
			 *  txtInputCode.text = code;
			 *  [self didConnectButtonSelected:txtInputCode];
			 * }];
			 */

			((AppDelegate *)[UIApplication sharedApplication].delegate).acCode = code;

			[CURRENT_WINDOW.rootViewController dismissViewControllerAnimated:YES completion:^{
				 dispatch_async (dispatch_get_main_queue (), ^{
									 if (shouldTVBeEnabled) {
										 [self.delegate performTaskRequiringPairing:^{
														  shouldShowControls = YES;
														  ((AppDelegate *)[UIApplication sharedApplication].delegate).playOnAccedoConnect = YES;
													  } onFailure:nil];
									 }
								 });
			 }];

			// }
		}
	}
}

@end
