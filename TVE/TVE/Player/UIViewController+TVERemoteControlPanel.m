//
//  UIViewController+TVERemoteControlPanel.m
//  TVE
//
//  Created by Csaba Tűz on 2013.01.03..
//  Copyright (c) 2013 Accedo Broadband. All rights reserved.
//

#import "UIViewController+TVERemoteControlPanel.h"

#import "AppDelegate.h"
#import "BaseViewController.h"

@implementation BaseViewController (TVERemoteControlPanel)

@dynamic acPairingPopup;
@dynamic acRemoteController;
@dynamic acRemotePopup;
@dynamic acRemoteNavButton;

#pragma mark - ACRemoteControlPopup

- (void)pairDevice:(void (^)(void))sblock Failed:(void (^)(void))fblock {
	if ([ACManager sharedInstance].isPaired) {
		sblock();
		return;
	}

	if (!self.acPairingPopup) {
		NSString *filePath = [[NSBundle mainBundle] pathForResource:@"AccedoConnect" ofType:@"bundle"];
		NSBundle *bundle = [NSBundle bundleWithPath:filePath];
		self.acPairingPopup = [[[ACPairingViewController alloc] initWithNibName:@"ACPairingViewController" bundle:bundle] autorelease];
	}

	self.acPairingPopup.successBlock = sblock;
	self.acPairingPopup.failureBlock = fblock;

	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:self.acPairingPopup];

	nav.modalPresentationStyle = UIModalPresentationFormSheet;
	[self presentViewController:nav animated:YES completion:nil];

	nav.view.superview.frame = CGRectMake(0, 0, 700, 550);
	// nav.view.superview.frame = CGRectMake(0, 0, acPairingPopup.view.frame.size.width, acPairingPopup.view.frame.size.height + 50);

	CGPoint p = self.view.center;
	UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;

	if (UIInterfaceOrientationIsLandscape(orientation)) {
		nav.view.superview.center = p.x > p.y ? p : CGPointMake(p.y, p.x);
	}
	else {
		nav.view.superview.center = p;
	}

	[nav release];
}

- (void)performTaskRequiringPairing:(void (^)(void))action onFailure:(void (^)(void))failBlock {
	if ([[ACManager sharedInstance] isPaired]) {
		action();
	}
	else {
		dispatch_async(dispatch_get_main_queue(), ^{
						   [self.acRemotePopup dismissPopoverAnimated:YES];
					   });
		[self pairDevice:action Failed: ^(void) {
		     // [CMoreViewController error:[[LocalizationSystem sharedLocalSystem]locStringForKey: @"lblPairingFailed"]];
			 if (failBlock) {
				 failBlock ();
			 }
		 }];
	}
}

- (void)didClientDisconnected:(NSNotification *)notification {
	NSString *uuid = [notification.userInfo valueForKey:@"uuid"];
	AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;

	if ([uuid isEqualToString:[ACManager sharedInstance].statusRecorder.player]) {
		[self.acRemoteNavButton setImage:[UIImage imageNamed:@"navbar_tv_icon.png"] forState:UIControlStateNormal];
		appDelegate.playOnAccedoConnect = NO;
	}
}

- (void)didACDisconnected {
	AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;

	[ACManager sharedInstance].statusRecorder.playbackState = ACPlaybackStateTerminated;
	[self.acRemoteNavButton setImage:[UIImage imageNamed:@"navbar_tv_icon.png"] forState:UIControlStateNormal];
	appDelegate.playOnAccedoConnect = NO;
}

- (void)playbackStatusUpdated:(NSNotification *)notification {
	if (!notification) {
		return;
	}
	NSString *deviceType = [[notification userInfo] valueForKey:@"device_type"];
	if (![deviceType hasPrefix:@"TV"]) {
		return;
	}
	AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;

	if ([ACManager sharedInstance].statusRecorder.playbackState != ACPlaybackStateTerminated) {
		[self.acRemoteNavButton setImage:[UIImage imageNamed:@"navbar_tv_icon_active.png"] forState:UIControlStateNormal];
		appDelegate.playOnAccedoConnect = YES;
	}
	else {
		[self.acRemoteNavButton setImage:[UIImage imageNamed:@"navbar_tv_icon.png"] forState:UIControlStateNormal];
		appDelegate.playOnAccedoConnect = NO;
	}
}

- (void)resizeACRemotePopover {
	[self.acRemotePopup setPopoverContentSize:self.acRemoteController.tableView.contentSize];
}

- (void)showACRemotePopup {
	if (!self.acRemotePopup) {
		UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:self.acRemoteController];
		self.acRemoteController.title = @"Device";
		UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
		[button setTitle:@"Close" forState:UIControlStateNormal];
		[button setBackgroundImage:[UIImage imageNamed:@"bar_button_bg.png"] forState:UIControlStateNormal];
		[button.titleLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:12.0]];
		[button addTarget:self action:@selector(closeACRemotePopover:) forControlEvents:UIControlEventTouchUpInside];
		UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:button];
		[button release];
		[rightBarButton setStyle:UIBarButtonItemStylePlain ];
		self.acRemoteController.navigationItem.rightBarButtonItem = rightBarButton;
		[rightBarButton release];

		self.acRemotePopup = [[[UIPopoverController alloc]
							   initWithContentViewController:nav] autorelease];
		self.acRemotePopup.delegate = self;
		self.acRemoteController.delegate = self;

		[nav release];
		[self.acRemotePopup setPopoverContentSize:CGSizeMake(320, 165)];
		self.acRemotePopup.delegate = self;
	}

	[self.acRemoteController.tableView reloadData];
	[self.acRemotePopup presentPopoverFromBarButtonItem:self.navigationItem.leftBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void)acRemotePopover:(id)sender {
	[self showACRemotePopup];
}

- (void)closeACRemotePopover:(id)sender {
	[self.acRemotePopup dismissPopoverAnimated:YES];
}

- (UIButton *)acRemoteButton {
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];

	button.frame = CGRectMake(10, (self.navigationController.navigationBar.frame.size.height - 29) / 2, 42, 29);
	[button setImage:[UIImage imageNamed:@"navbar_tv_icon.png"] forState:UIControlStateNormal];
	[button setBackgroundColor:[UIColor clearColor]];
	[button addTarget:self action:@selector(acRemotePopover:) forControlEvents:UIControlEventTouchUpInside];
	return button;
}

@end
