//
//  TVEStatusRecorderDataSource.m
//  TVE
//
//  Created by Csaba Tűz on 2013.01.02..
//  Copyright (c) 2013 Accedo Broadband. All rights reserved.
//

#import "TVEStatusRecorderDataSource.h"
#import "ThePlatform.h"
#import "AppView.h"

@implementation TVEStatusRecorderDataSource

- (void)assetForId:(NSString *)assetId {
	NSDictionary *asset = [[ThePlatform sharedInstance] getDataForId:assetId];

	[self.statusRecorder didAssetReceived:[[asset valueForKey:@"entries"] lastObject]];
}

- (NSString *)idForAsset:(NSDictionary *)asset isStream:(BOOL)stream {
	if (stream) {
		return [asset valueForKey:@"channel"];
	}
	else {
		return [[asset valueForKey:@"id"] lastPathComponent];
	}
}

- (NSArray *)subtitlesForAsset:(NSDictionary *)asset {
	return [NSArray array];
}

- (NSDictionary *)channelAssetForAssetId:(NSString *)assetId {
	return [[AppView sharedInstance] channelByChannelId:assetId];
}

@end
