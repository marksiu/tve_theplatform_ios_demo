//
//  RemoteControlCell.m
//  CMore
//
//  Created by Csaba Tűz on 2012.10.29..
//
//

#import "RemoteControlCell.h"

@implementation RemoteControlCell

@synthesize playBtn, pauseBtn;
@synthesize backBtn;
@synthesize fwdBtn, rewBtn;
@synthesize volumeSlider;

@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	if (self) {
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];

	// Configure the view for the selected state
}

@end
