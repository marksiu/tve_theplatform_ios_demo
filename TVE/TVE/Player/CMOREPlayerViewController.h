//
//  CMOREPlayerViewController.h
//  CMore
//
//  Created by Stephen King on 8/3/12.
//  Copyright (c) 2012 Meganet-Soft Bt. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

#import "OverlayView.h"
#import "EPGVideoOverlayView.h"
#import <AccedoConnect/ACStatusRecorder.h>
#import "RemotePlayerViewController.h"

@interface CMOREPlayerViewController : UIViewController<OverlayViewDelegate, RemoteOverlayViewDelegate> {
}

@property (nonatomic, assign) BOOL isLiveStream;
@property (nonatomic, assign) BOOL isRemote;
@property (nonatomic, assign) BOOL buffering;

@property (nonatomic, retain) NSURL *storedUrl;

@property (nonatomic, retain) NSString *remote;

@property (nonatomic, retain) UIView<OverlayViewProtocol> *overlayView;
@property (nonatomic, retain) NSDictionary *playingAsset;
@property (nonatomic, retain) NSDictionary *currentSubtitles;
@property (nonatomic, retain) RemotePlayerViewController *remotePlayerVC;

@property (nonatomic, assign) BOOL takingRemoteUpdates;
@property (nonatomic, assign) BOOL sendingStatusUpdate;

@property (nonatomic, retain) MPMoviePlayerController *moviePlayer;

@property (assign) UIView *containerView;

@property (assign) BOOL fullscreen;
@property (assign) BOOL startsFullscreen;

@property (assign) int initial_playback_time;

- (id)initAsRemote:(BOOL) remote fullscreen:(BOOL) fullscreen containerView:(UIView *)containerView;

- (void)updateLive:(BOOL)_live;

- (void)replaceURL:(NSURL *)url;

- (void)switchToLocalPlaybackWithPosUpdate:(BOOL)posUpdate;
- (void)addPopupView:(NSString *)popupString;
- (void)switchToRemotePlayback;

- (void)switchToFullscreen;
- (void)switchToWindowed;

- (void)presentPlayer;

@end
