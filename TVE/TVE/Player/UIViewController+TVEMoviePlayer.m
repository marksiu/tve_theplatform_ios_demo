//
//  UIViewController+TVEMoviePlayer.m
//  TVE
//
//  Created by Csaba Tűz on 2013.01.03..
//  Copyright (c) 2013 Accedo Broadband. All rights reserved.
//

#import "UIViewController+TVEMoviePlayer.h"
#import "UIHelper.h"
#import "XMLReader.h"

#import <libkern/OSAtomic.h>

@implementation UIViewController (TVEMoviePlayer)

@dynamic remoteTimeoutTimer;
@dynamic cmorePlayer;

#pragma mark - MoviePlayer stuff

- (void) playMovie:(NSDictionary *)asset
		  isStream:(BOOL)isStream
		  isRemote:(BOOL)remote
   initialPosition:(double)initialPosition
	 containerView:(UIView *)containerView {
	NSString *mediaURL = [UIHelper getVideoUrl:asset];

	[self initializeCMorePlayer:isStream remote:remote initialPosition:initialPosition containerView:containerView];


	[self setUpMoviePlayerNotificationHandlers];

	[self processUrl:mediaURL block:^(NSString * url) {
		 self.cmorePlayer.playingAsset = asset;
		 if (remote) {
			 NSString *assetId = isStream ? [asset valueForKey:@"channel"]:[[asset valueForKey:@"id"] lastPathComponent];
			 [self handleRemotePlaybackStartForStream:url assetId:assetId isStream:isStream initialPosition:initialPosition];
		 }


		 if (self.cmorePlayer) {
			 [self.cmorePlayer replaceURL:[NSURL URLWithString:url]];
		 }
	 }];
}

- (void)initializeCMorePlayer:(BOOL)isStream remote:(BOOL)remote initialPosition:(double)initialPosition
				containerView:(UIView *)containerView {
	if (!self.cmorePlayer) {
		self.cmorePlayer = [[[CMOREPlayerViewController alloc] initAsRemote:remote
																 fullscreen:(containerView == nil)
															  containerView:containerView] autorelease];

		self.cmorePlayer.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

		[self.cmorePlayer setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
	}
	else {
		self.cmorePlayer.startsFullscreen = (containerView == nil);
		self.cmorePlayer.isRemote = remote;
		self.cmorePlayer.containerView = containerView;
	}
	if (!remote) {
		[self.cmorePlayer updateLive:isStream];
	}

	[self.cmorePlayer presentPlayer];
	self.cmorePlayer.initial_playback_time = initialPosition;
	[self.cmorePlayer.moviePlayer setInitialPlaybackTime:initialPosition];
}

static int requestVersion = 0;

- (void)processUrl:(NSString *)url block:(void (^)(NSString * url))block {
	int currentVersion;

	@synchronized(self)
	{
		currentVersion = ++requestVersion;
	}
	if (0 < [url length]) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
						   NSData *smilData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
						   NSDictionary *smil = [XMLReader dictionaryForXMLData:smilData error:nil];
						   NSString *_url = [smil valueForKeyPath:@"smil.body.seq.video.src"];
						   dispatch_sync (dispatch_get_main_queue (), ^{
											  @synchronized (self) {
												  if (requestVersion == currentVersion) {
													  block (_url);
												  }
											  }
										  });
					   });
	}
	else {
		@synchronized(self)
		{
			if (requestVersion == currentVersion) {
				block(LIVE_VIDEO_URL);
			}
		}
	}
}

- (void)setUpMoviePlayerNotificationHandlers {
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(handleMPMoviePlayerPlaybackDidFinishNotification:)
												 name:MPMoviePlayerPlaybackDidFinishNotification
											   object:self.cmorePlayer.moviePlayer];
}

- (void)handleRemotePlaybackStartForStream:(NSString *)mediaURL
								   assetId:(NSString *)assetId
								  isStream:(BOOL)isStream
						   initialPosition:(double)initialPosition {
	self.cmorePlayer.moviePlayer.shouldAutoplay = NO;
	[[ACManager sharedInstance].commandEngine sendPlayAsset:mediaURL
												   isStream:isStream
													  asset:assetId
											initialPosition:initialPosition
										   selectedSubtitle:@""
													 target:nil];
	if (self.remoteTimeoutTimer) {
		[self.remoteTimeoutTimer invalidate];
	}

	self.remoteTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(remotePbFail:) userInfo:nil repeats:NO];
}

- (void)remotePbFail:(id)t {
	[self.remoteTimeoutTimer invalidate];
	self.remoteTimeoutTimer = nil;

	/*[CMoreViewController error: [[LocalizationSystem sharedLocalSystem]locStringForKey: @"lblACErrorPairing"]];*/

	if (self.cmorePlayer) {
		[self.cmorePlayer switchToLocalPlaybackWithPosUpdate:NO];
	}
}

- (void)handleMPMoviePlayerPlaybackDidFinishNotification:(NSNotification *)n {
	MPMoviePlayerController *p = (MPMoviePlayerController *)n.object;

	/* [[NSNotificationCenter defaultCenter] removeObserver:self
	 * name:MPMoviePlayerLoadStateDidChangeNotification
	 * object:p];*/
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:MPMoviePlayerPlaybackDidFinishNotification

												  object:p];

	if (self.cmorePlayer) {
		if (self.cmorePlayer.modalPresentationStyle == UIModalPresentationFullScreen) {
			[self dismissViewControllerAnimated:YES completion:nil];
		}
		[self.cmorePlayer.view removeFromSuperview];
		self.cmorePlayer = nil;
	}

	/*StreamService * streamService = (StreamService *)
	 * [[ServiceFactory sharedInstance] getService:@"StreamService"];
	 * [streamService syncSendStopStream:self.playingAsset onSuccess:self.streamManagementSuccessBlock onFailure:nil];*/

	// self.playingAsset = nil;

	// [self hideHUD];
}

#pragma mark - UINavigationControllerDelegate
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
	if ([navigationController.viewControllers count] > 1) {
		[self closePlayer];
	}
}

- (void)closePlayer {
	if (self.cmorePlayer) {
		CMOREPlayerViewController *player = self.cmorePlayer;
		[player close:self];
		self.cmorePlayer = nil;
	}
}

@end
