//
//  HomeViewController.h
//  TVE
//
//  Created by Gabor Bottyan on 11/26/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomePanelView.h"
#import "BaseViewController.h"
#import "LoginView.h"

@interface HomeViewController : BaseViewController <HomePanelViewDelegate, LoginViewDelegate>{
}
- (id)initWithDescription:(NSDictionary *)desc;
@end
