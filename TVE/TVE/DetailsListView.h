//
//  DetailsListView.h
//  TVE
//
//  Created by Gabor Bottyan on 11/30/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetView.h"
@protocol DetailsListViewDelegate
- (void)itemSelected:(NSDictionary *)item;
@end


@interface DetailsListView : UIView <AsetViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>{
}
- (void)reloadSelected;
- (void)jumpToChannel:(NSString *)channelId;
- (void)reloadItems;
@property (assign) id <DetailsListViewDelegate> listdelegate;
- (void)setData:(NSArray * )entries index:(int)index;
@end
