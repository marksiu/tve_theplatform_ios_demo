//
//  SortViewController.h
//  TVE
//
//  Created by Gabor Bottyan on 1/1/13.
//  Copyright (c) 2013 Accedo Broadband. All rights reserved.
//

#import "FilterViewController.h"

@interface SortViewController : FilterViewController

@end
