//
//  SettingsViewController.h
//  TVE
//
//  Created by Mate Beres on 12/5/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingsViewController : UIViewController {
}

- (IBAction)clearSubscriptions:(id)sender;
- (IBAction)clearRental:(id)sender;
- (IBAction)logout:(id)sender;
@end
