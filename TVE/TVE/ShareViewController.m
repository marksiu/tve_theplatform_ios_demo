//
//  ShareViewController.m
//  TVE
//
//  Created by Mate Beres on 12/11/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "ShareViewController.h"

@interface ShareViewController ()
@property (retain) NSString *assetTitle;
@end

@implementation ShareViewController


- (id)initWithAssetTitle:(NSString *)___title {
	self = [super init];
	self.assetTitle = ___title;
	return self;
}

- (void)dealloc {
	self.assetTitle = Nil;
	[super dealloc];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view from its nib.
}

- (CGSize)contentSizeForViewInPopover {
	return CGSizeMake(140, 45);
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)twitterShare:(id)sender {
	[self shareTwitterPopup:self.assetTitle];
}

- (IBAction)facebookShare:(id)sender {
	[self shareFacebookPopup:self.assetTitle];
}

@end
