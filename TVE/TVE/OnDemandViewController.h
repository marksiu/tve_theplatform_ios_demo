//
//  OnDemandViewController.h
//  TVE
//
//  Created by Gabor Bottyan on 11/26/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "AssetView.h"
#import "DetailsListView.h"
@interface OnDemandViewController : BaseViewController <AsetViewDelegate>{
}
@property (assign) BOOL movies;
@property (assign) float top;
@property (retain) UIScrollView *scrollView;
@property (retain) UIButton *cachupButton;
@property (retain) UIButton *filterButton;
@property (retain) UIButton *sortButton;
@property (retain) UIButton *moviesButton;
@property (retain) UIImageView *textBackgroundView;
@property (retain) UILabel *separationLabel;
@property (retain) NSString *sortData;
- (void)display:(NSArray * )entries;
- (void)displayMyRental;
- (void)displayMyRentalFromLocal;
- (void)setupFilters;
@property (retain) NSDictionary *data;
- (id)initWithFeed:(NSDictionary *)desc;
@end
