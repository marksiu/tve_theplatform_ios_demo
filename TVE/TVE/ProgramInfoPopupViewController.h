//
//  ProgramInfoPopupViewController.h
//  TVE
//
//  Created by Mate Beres on 12/13/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "BaseViewController.h"
#import "VSRemoteImageView.h"

@interface ProgramInfoPopupViewController : BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil mode:(int)mode meta:(NSDictionary *)meta;

@property (retain, nonatomic) IBOutlet UIButton *button1;
@property (retain, nonatomic) IBOutlet UIButton *button2;
@property (retain, nonatomic) IBOutlet UIButton *button3;
@property (retain, nonatomic) IBOutlet UIButton *button4;
@property (retain, nonatomic) IBOutlet UIImageView *imageView;
@property (retain, nonatomic) IBOutlet UILabel *demandLabel;
@property (retain, nonatomic) IBOutlet UILabel *expiresLabel;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *seasonLabel;
@property (retain, nonatomic) IBOutlet UILabel *thirdLabel;
@property (retain, nonatomic) IBOutlet VSRemoteImageView *contentProviderImageView;
@property (retain, nonatomic) IBOutlet UILabel *dateLabel;
@property (retain, nonatomic) IBOutlet UITextView *textView;
@property (retain, nonatomic) IBOutlet UIImageView *expiresImageView;

@property (retain, nonatomic) NSDictionary *metaData;

@property (retain, nonatomic) IBOutlet UIButton *tvWatchChannelButton;
@property (retain, nonatomic) IBOutlet UILabel *tvTitleLabel;
@property (retain, nonatomic) IBOutlet UITextView *tvTextView;

@end
