//
//  SearchViewController.h
//  TVE
//
//  Created by Mate Beres on 12/19/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "BaseViewController.h"



@interface SearchViewController : UIViewController
@property (assign) id <SearchViewControllerDelegate> delegate;
@property (assign) UIPopoverController *popover;
@end
