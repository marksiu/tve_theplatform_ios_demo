//
//  SearchDetailViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 12/21/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "SearchDetailViewController.h"
#import "ThePlatform.h"
@interface SearchDetailViewController ()

@end

@implementation SearchDetailViewController

- (id)initWithQuery:(NSString *)__query {
	self = [super initWithFeed:Nil];
	if (self) {
		self.query = __query;
	}
	return self;
}

- (void)showFilters {
	[self.scrollView addSubview:self.textBackgroundView];
	[self.scrollView addSubview:self.separationLabel];
	[self.scrollView addSubview:self.textBackgroundView];
	[self.scrollView addSubview:self.separationLabel];
	[self.scrollView addSubview:self.moviesButton];
	[self.scrollView addSubview:self.cachupButton];
	// [self.scrollView addSubview:self.filterButton];
	// [self.scrollView addSubview:self.sortButton];
}

- (void)display:(NSArray * )entries {
	[super display:entries];
	if ([entries count] == 0) {
		NSLog(@"Adding no result label");
		UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 200, 300, 50)];
		l.text = @"No Result Found.";
		l.textAlignment = NSTextAlignmentCenter;
		l.font  = [UIFont fontWithName:@"HelveticaNeue-Medium" size:24];
		l.textColor = [UIColor whiteColor];
		l.backgroundColor = [UIColor clearColor];
		[self.scrollView addSubview:l];
		l.center = CGPointMake(self.scrollView.center.x, l.center.y);
		[l release];
	}
}

- (void)fetchData {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   NSDictionary *c = Nil;

					   dispatch_async (dispatch_get_main_queue (), ^{
										   [self startSpinner];
									   });


					   NSString *sortQuery = Nil;
					   if (self.sortData) {
						   if ([self.sortData isEqualToString:@"A-Z"]) {
							   sortQuery = @"&sort=title";
						   }
						   if ([self.sortData isEqualToString:@"Date"]) {
							   sortQuery = @"&sort=added,title|desc";
						   }
						   if ([self.sortData isEqualToString:@"Rating"]) {
							   sortQuery = @"&sort=:starRating|desc";
						   }
					   }
					   NSString *catQuery = Nil;

	                   // NSArray * filters = [[ThePlatform sharedInstance] getCategories];
	                   // NSMutableArray * relatedFilters = [NSMutableArray array];
	                   // NSMutableArray * finalrelatedFilters = [NSMutableArray array];
					   ThePlatform *platform = [ThePlatform sharedInstance];
					   NSDictionary *config = platform.config;
					   NSDictionary *cfg = [config objectForKey:@"config"];
	                   // NSString * cathegoryId = Nil;
					   NSString *catBaseUrl = [cfg objectForKey:@"categoryBaseUrl"];


					   if (self.movies) {
						   catQuery = [catBaseUrl stringByAppendingString:[cfg objectForKey:@"movieCatId"]];
					   }
					   else {
						   catQuery = [catBaseUrl stringByAppendingString:[cfg objectForKey:@"tvSeriesCatId"]];
					   }
					   catQuery = [NSString stringWithFormat:@"&byCategoryIds=%@", catQuery];

					   c =  [[ThePlatform sharedInstance] search:self.query sort:catQuery];
					   NSArray *entries = [c objectForKey:@"entries"];




					   dispatch_async (dispatch_get_main_queue (), ^{
										   [self display:entries];
										   [self stopSpinner];
									   });

					   self.data = c;
				   });
}

- (void)setupFilters {
	float topY = 20;

	[super setupFilters];

	// self.textBackgroundView.frame = CGRectMake(170, topY, 267, 30);
	self.textBackgroundView.center = CGPointMake(self.scrollView.center.x, self.textBackgroundView.center.y);

	self.separationLabel.frame = CGRectMake(self.textBackgroundView.frame.origin.x + 141, topY + 1, 40, 30);
	self.cachupButton.frame = CGRectMake(self.textBackgroundView.frame.origin.x + 10, topY, 131, 31);
	self.moviesButton.frame = CGRectMake(self.textBackgroundView.frame.origin.x + 141, topY, 131, 31);



	// self.sortButton.frame = CGRectMake(501, 0, 175, 30);
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
