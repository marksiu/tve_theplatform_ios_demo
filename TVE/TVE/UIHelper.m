//
//  UIHelper.m
//  TVE
//
//  Created by Gabor Bottyan on 12/4/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "UIHelper.h"
#import "XMLReader.h"

@implementation UIHelper



+ (NSString *)getEpisode:(NSDictionary *)item {
	NSString *title = [item objectForKey:@"title"];
	NSArray *arr = [title componentsSeparatedByString:@" "];

	for (int i = 0; i < [arr count]; i++) {
		NSString *s = [arr objectAtIndex:i];
		if ([[s lowercaseString] isEqualToString:@"episode"]) {
			if ([arr count] > i + 1) {
				return [arr objectAtIndex:i + 1];
			}
		}
	}
	return Nil;
}

+ (NSString *)getSeason:(NSDictionary *)item {
	NSString *title = [item objectForKey:@"title"];
	NSArray *arr = [title componentsSeparatedByString:@" "];

	for (int i = 0; i < [arr count]; i++) {
		NSString *s = [arr objectAtIndex:i];
		if ([[s lowercaseString] isEqualToString:@"season"]) {
			if ([arr count] > i + 1) {
				return [arr objectAtIndex:i + 1];
			}
		}
	}
	return Nil;
}

+ (NSString *)getImageUrl:(NSDictionary *)entry forSize:(CGSize)size {
	NSArray *thumbs = [entry objectForKey:@"media$thumbnails"];

	CGSize correctSize = size;

	if ([UIScreen mainScreen].scale == 2) {
		correctSize = CGSizeMake(size.width * 2, size.height * 2);
	}

	// int mindeltax = correctSize.width;
	// int mindeltay = correctSize.height;

	// NSLog(@"%@\n\n\n\n", [entry objectForKey:@"title"]);

	NSDictionary *goodImage = Nil;
	for (NSDictionary *image in thumbs) {
		NSNumber *def = [image objectForKey:@"plfile$isDefault"];

		if (def && [def boolValue]) {
			goodImage = image;
			break;
		}

		// NSLog(@"IMAGE URL: %@   %@", [image objectForKey:@"plfile$url"], image);

//        int  width = [[image objectForKey:@"plfile$width"] intValue];
//        int  height = [[image objectForKey:@"plfile$height"] intValue];
//
//        int deltax = width - correctSize.width;
//        int deltay = height - correctSize.height;
//
//        if ((width>=correctSize.width ||  height>=correctSize.height )&&  (deltax< mindeltax  || deltay < mindeltay)){
//            mindeltax = deltax;
//            mindeltay = deltay;
//
//            goodImage = image;
//
//        }
	}
	if (goodImage) {
		// int  width = [[goodImage objectForKey:@"plfile$width"] intValue];
		// int  height = [[goodImage objectForKey:@"plfile$height"] intValue];

		// NSLog(@"SIZE FOR: {%f %f} is {%d, %d}", size.width, size.height, width,height);

		return [goodImage objectForKey:@"plfile$url"];
	}
	else {
		return [[thumbs objectAtIndex:0] objectForKey:@"plfile$url"];
	}
}

+ (NSString *)getTVShowImageUrl:(NSDictionary *)entry {
	NSArray *thumbs = [entry objectForKey:@"media$thumbnails"];

	NSString *pattern = @"tvSeriesThumbnail";

	for (NSDictionary *image in thumbs) {
		if ([[image valueForKey:@"plfile$assetTypes"] containsObject:pattern]) {
			return [image objectForKey:@"plfile$url"];
		}
	}
	return nil;
}

+ (NSString *)getVideoUrl:(NSDictionary *)entry {
	if (0 < [[entry valueForKey:@"channel"] length]) {
		return nil;
	}

	NSArray *videos = [entry objectForKey:@"media$content"];

	NSDictionary *pickedVideo = nil;
	NSDictionary *video = nil;

	NSString *originalVideo = @"Original Video";

	for (video in videos) {
		if ([[video valueForKey:@"plfile$format"] isEqualToString:@"MPEG4"] && 0 < [[video valueForKey:@"plfile$url"] length]) {
			if ([[video valueForKey:@"plfile$assetTypes"] containsObject:originalVideo]) {
				pickedVideo = video;
				break;
			}
			if (!pickedVideo) {
				pickedVideo = video;
				continue;
			}
			int width = [[video valueForKey:@"plfile$width"] intValue];
			int pickedWidth = [[pickedVideo valueForKey:@"plfile$width"] intValue];

			if (pickedWidth < width) {
				pickedVideo = video;
			}
		}
	}

	NSString *url = nil;

	url = [pickedVideo valueForKey:@"plfile$url"];

	return url;
}

+ (NSDictionary *)getVideo:(NSDictionary *)entry {
	if (0 < [[entry valueForKey:@"channel"] length]) {
		return nil;
	}

	NSArray *videos = [entry objectForKey:@"media$content"];

	NSDictionary *pickedVideo = nil;
	NSDictionary *video = nil;

	NSString *originalVideo = @"Original Video";

	for (video in videos) {
		if ([[video valueForKey:@"plfile$format"] isEqualToString:@"MPEG4"] && 0 < [[video valueForKey:@"plfile$url"] length]) {
			if ([[video valueForKey:@"plfile$assetTypes"] containsObject:originalVideo]) {
				return video;
			}
			if (!pickedVideo) {
				pickedVideo = video;
				continue;
			}
			int width = [[video valueForKey:@"plfile$width"] intValue];
			int pickedWidth = [[pickedVideo valueForKey:@"plfile$width"] intValue];

			if (pickedWidth < width) {
				pickedVideo = video;
			}
		}
	}

	return pickedVideo;
}

@end
