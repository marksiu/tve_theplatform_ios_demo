//
//  TVGuideViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 11/26/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "TVGuideViewController.h"
#import "EPGConstants.h"
#import "VSRemoteImageView.h"
#import "AppDelegate.h"
#import "NSDate+Extras.h"
#import "Constants.h"
#import "LiveTVView.h"
#import "AppView.h"
#import "UIImage+Extension.h"
#import "ProgramInfoPopupViewController.h"
#import  <QuartzCore/QuartzCore.h>
#import "PopupHelper.h"

@interface TVGuideViewController () <VSEPGGridViewDataSource, VSEPGGridViewDelegate> {
	NSObject *tileJobMutex;
	NSObject *epgCacheMutex;
	int selectedDay;
	BOOL isToday;
}
@property (assign) int daysForward;
@property (retain) NSMutableDictionary *epgCache;
@property (retain) NSOperationQueue *operationQueue;
@property (retain) NSMutableArray *tileJobs;
@property (retain) NSMutableDictionary *tileJobsByTileKey;

@property (retain) UIImage *gridBackground;
@property (retain) UIImage *cellBackground;
@property (retain) UIImage *titleBackground;
@property (retain) UIImage *hairlineBackground;
@property (retain) UIImage *rulerBackground;

@property (retain) UIColor *gridBackgroundColor;
@property (retain) UIColor *cellBackgroundColor;
@property (retain) UIColor *titleBackgroundColor;
@property (retain) UIColor *hairlineBackgroundColor;
@property (retain) UIColor *rulerBackgroundColor;

@property (retain) UILabel *dateLabel;
@property (retain) UIBarButtonItem *leftButtom;

@property (retain) LiveTVView *liveTvView;

@property (retain) UIPopoverController *popup;

@property (retain) NSMutableArray *dayButtons;

@property (retain) NSString *selectedChannelId;

- (NSNumber *)chunkKeyFromHoursSince1970:(NSTimeInterval)hoursSince1970 row:(NSUInteger)row;

+ (VSEPGGridViewTitleCell *)defaultTitleCell;
+ (VSEPGGridViewContentCell *)defaultContentCell;

@end

#define kVSEPGGridViewChunkKeyMultiplier 10000

typedef enum
{
	TileStatusDone = 0,
	TileStatusPostProcessing = 1,
	TileStatusDownloading = 2,
	TileStatusPreProcessing = 3,
	TileStatusWaiting = 4
} TileStatus;

@implementation TVGuideViewController
@synthesize gridView = _gridView;
@synthesize channels;
@synthesize epgCache;
@synthesize operationQueue;
@synthesize tileJobs;
@synthesize tileJobsByTileKey;
@synthesize daysForward, dateLabel, leftButtom;
@synthesize gridBackground;
@synthesize cellBackground;
@synthesize titleBackground;
@synthesize hairlineBackground;
@synthesize rulerBackground;

@synthesize gridBackgroundColor;
@synthesize cellBackgroundColor;
@synthesize titleBackgroundColor;
@synthesize hairlineBackgroundColor;
@synthesize rulerBackgroundColor;
@synthesize popup;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
		self.daysForward = 0;
		boxTimeDelta = self.daysForward * 86400;
	}

	return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	return YES;
}

- (BOOL)needsBackButton {
	return YES;
}

- (int)getSelectedDay {
	return self.daysForward;
}

- (void)daySelected:(int)selected withLabel:(NSString *)text {
	[self.popup dismissPopoverAnimated:YES];
	self.daysForward = selected;
	boxTimeDelta = self.daysForward * 86400;
	[self.gridView reload];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"EEEE LL/dd YYYY"];
	NSString *stringFromDate = [[formatter stringFromDate:[NSDate localDate]] capitalizedString];
	[formatter release];
	self.dateLabel.text  = stringFromDate;
	self.leftButtom.title = text;
}

- (void)daySelect:(id)sender {
//    if (!self.popup) {
//        DaySelectController * daySelect = [[DaySelectController alloc] initWithNibName:@"DaySelectController" bundle:Nil];
//        daySelect.delegate = self;
//        daySelect.tableView.scrollEnabled = NO;
//        self.popup = [[[UIPopoverController alloc] initWithContentViewController: daySelect] autorelease];
//        [daySelect release];
//        [self.popup setPopoverContentSize:CGSizeMake(320, 304)];
//    } else {
//        [((DaySelectController *)(self.popup.contentViewController)).tableView reloadData];
//    }

//    [self.popup presentPopoverFromBarButtonItem:self.leftButtom permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void)dealloc {
	[self.operationQueue cancelAllOperations];

	[_gridView reset];
	[_gridView release];
	self.channels = nil;
	self.epgCache = nil;
	self.operationQueue = nil;
	self.tileJobs = nil;
	self.tileJobsByTileKey = nil;

	self.gridBackground = nil;
	self.cellBackground = nil;
	self.titleBackground = nil;
	self.hairlineBackground = nil;
	self.rulerBackground = nil;

	self.gridBackgroundColor = nil;
	self.cellBackgroundColor = nil;
	self.titleBackgroundColor = nil;
	self.hairlineBackgroundColor = nil;
	self.rulerBackgroundColor = nil;

	self.liveTvView = nil;

	[tileJobMutex release];
	[epgCacheMutex release];

	[super dealloc];
}

- (void)reload {
	[self.gridView reload];
}

- (void)viewDidLoad {
	[super viewDidLoad];

	self.view.backgroundColor = [UIColor clearColor];
	UIImage *backgroundImage;
	if ([UIImage externalImageNamed:@"background"]) {
		backgroundImage = [UIImage externalImageNamed:@"background"];
	}
	else {
		backgroundImage = [UIImage imageNamed:@"background.png"];
	}
	UIImageView *backgroundImageView = [[[UIImageView alloc] initWithImage:backgroundImage] autorelease];
	[self.view addSubview:backgroundImageView];

	[self customNavigationBar];
#if USETESTFLIGHT
	[TestFlight passCheckpoint:@"Screen:EPG Action:viewDidLoad"];
#endif

	NSDictionary *ch1 = [NSDictionary dictionaryWithObjectsAndKeys:@"1", @"id", nil];
	NSDictionary *ch2 = [NSDictionary dictionaryWithObjectsAndKeys:@"2", @"id", nil];
	self.channels = [NSArray arrayWithObjects:ch1, ch2, nil];
	NSString *td = @"TODAY";
	// [[LocalizationSystem sharedLocalSystem] locStringForKey:@"lblToday"];
	self.leftButtom = [[[UIBarButtonItem alloc] initWithTitle:td style:UIBarButtonSystemItemDone target:nil action:nil] autorelease];


	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"EEEE LL/dd YYYY"];
	NSString *stringFromDate = [[formatter stringFromDate:[NSDate localDate]] capitalizedString];
	[formatter release];

	[self.leftButtom setAction:@selector(daySelect:)];

	self.dateLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 30)] autorelease];
	self.dateLabel.backgroundColor = [UIColor clearColor];
	self.dateLabel.textAlignment = NSTextAlignmentRight;
	self.dateLabel.text  = stringFromDate;

	UIBarButtonItem *rightLabel = [[UIBarButtonItem alloc] initWithCustomView:self.dateLabel];

	UINavigationItem *item = [[UINavigationItem alloc] initWithTitle:@""];

	item.leftBarButtonItem = self.leftButtom;
	item.rightBarButtonItem = rightLabel;
	item.hidesBackButton = YES;

	[self.navBar pushNavigationItem:item animated:NO];

	[item release];
	[rightLabel release];

	/*
	 * UINavigationItem * item = [[UINavigationItem alloc] initWithTitle:@""];
	 * UIButton * dateButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	 * UILabel * dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 30)];
	 * dateLabel.text = @"Thursday";
	 * [dateButton setTitle:@"Today" forState:UIControlStateNormal];
	 *
	 * item.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:dateButton];
	 * item.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:dateLabel];
	 *
	 * [self.navBar pushNavigationItem:item animated:NO];
	 */

	self.liveTvView = [[[LiveTVView alloc] initWithFrame:CGRectMake(screenWidth - liveTVWidth + 2, 0, liveTVWidth, 768) data:Nil] autorelease];
	[self.view addSubview:self.liveTvView];



	isToday = YES;

	tileJobMutex = [[NSObject alloc] init];
	epgCacheMutex = [[NSObject alloc] init];

	if ([UIImage externalImageNamed:@"epgTitle"]) {
		self.titleBackground = [UIImage externalImageNamed:@"epgTitle"];
	}
	else {
		self.titleBackground = [UIImage imageNamed:@"epgTitle.png"];
	}
	self.hairlineBackground = nil;
	if ([UIImage externalImageNamed:@"epgRuler"]) {
		self.rulerBackground = [UIImage externalImageNamed:@"epgRuler"];
	}
	else {
		self.rulerBackground = [UIImage imageNamed:@"epgRuler.png"];
	}

	self.titleBackgroundColor = [UIColor colorWithPatternImage:self.titleBackground];
	self.hairlineBackgroundColor = [UIColor colorWithRed:6.0 / 255.0 green:135.0 / 255.0 blue:1 alpha:1];
	self.rulerBackgroundColor = [UIColor colorWithPatternImage:self.rulerBackground];

	self.operationQueue = [[[NSOperationQueue alloc] init] autorelease];
	self.operationQueue.maxConcurrentOperationCount = 1;

	CGRect gridFrame = CGRectMake(0, 61, epgWidth, 595);
	_gridView = [[VSEPGGridView alloc] initWithFrame:gridFrame];
	// self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.gridView.gridViewDataSource = self;
	self.gridView.gridViewDelegate = self;
	self.gridView.showsHorizontalScrollIndicator = NO;
	self.gridView.showsVerticalScrollIndicator = NO;

	self.gridView.rulerBackgroundColor = self.rulerBackgroundColor;
	self.gridView.titleBackgroundColor = self.titleBackgroundColor;
	self.gridView.contentBackgroundColor = [UIColor clearColor];
	self.gridView.hairlineBackgroundColor = self.hairlineBackgroundColor;

	[self.view addSubview:self.gridView];


	UIImageView *biv = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, epgWidth, 61)];
	biv.userInteractionEnabled = YES;
	biv.image = [UIImage imageNamed:@"epg_date_selector"];
	[self.view addSubview:biv];

	NSDateFormatter *ft = [[[NSDateFormatter alloc] init] autorelease];
	ft.timeZone = [NSTimeZone localTimeZone];
	ft.dateFormat = @"d/M";

	NSDateFormatter *fb = [[[NSDateFormatter alloc] init] autorelease];
	fb.timeZone = [NSTimeZone localTimeZone];
	fb.dateFormat = @"EEEE";

	NSDate *now = [NSDate date];

	CGFloat left = 50;
	self.dayButtons = [NSMutableArray array];
	for (int i = -1; i < 4; i++) {
		UIView *v = [[[UIView alloc] initWithFrame:CGRectMake(left, 5, 103, 49)] autorelease];
		left += (42 + 103);
		v.tag = i + 2;

		UIImageView *iv = [[UIImageView alloc] initWithFrame:v.bounds];
		iv.tag = v.tag + 100;
		iv.backgroundColor = [UIColor clearColor];
		iv.image = (i == 0) ? [UIImage imageNamed:@"epg_date_selector_selected"] : nil;
		[v addSubview:iv];

		UILabel *l;

		l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 103, 25)];
		l.tag = v.tag + 200;
		l.backgroundColor = [UIColor clearColor];
		l.textColor = [UIColor colorWithWhite:198.0 / 255.0 alpha:1];
		l.textAlignment = NSTextAlignmentCenter;
		l.font = [UIFont systemFontOfSize:16];
		l.text = [ft stringFromDate:[now dateByAddingTimeInterval:i * 86400]];
		[v addSubview:l];

		l = [[UILabel alloc] initWithFrame:CGRectMake(0, 25, 103, 24)];
		l.tag = v.tag + 300;
		l.backgroundColor = [UIColor clearColor];
		l.textColor = (i == 0) ? [UIColor colorWithRed:6.0 / 255.0
												 green:135.0 / 255.0
												  blue:1
												 alpha:1] : [UIColor whiteColor];
		l.textAlignment = NSTextAlignmentCenter;
		l.font = [UIFont systemFontOfSize:14];
		l.layer.shadowColor = (i == 0) ? [UIColor colorWithRed:6.0 / 255.0
														 green:135.0 / 255.0
														  blue:1
														 alpha:1].CGColor : [UIColor whiteColor].CGColor;
		l.layer.shadowOffset = CGSizeMake(0, 0);
		l.layer.shadowOpacity = 1;
		l.layer.shadowRadius = 0;
		NSString *day = nil;
		switch (i)
		{
			case -1 :
				day = @"yesterday";
				break;
			case 0 :
				day = @"today";
				break;
			case 1 :
				day = @"tomorrow";
				break;
			default :
				day = [fb stringFromDate:[now dateByAddingTimeInterval:i * 86400]];
				break;
		}
		l.text = [day uppercaseString];

		[v addSubview:l];

		UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
		b.frame = v.bounds;
		b.tag = v.tag + 400;
		[b addTarget:self action:@selector(dayButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
		[v addSubview:b];
		[biv addSubview:v];

		[self.dayButtons addObject:v];
	}


	self.channels = [[AppView sharedInstance] epgByCategories:nil];

	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(cancelProgress)
												 name:kVSNotificationStopping
											   object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(handleTilesRendered)
												 name:kVSTilesRendered
											   object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(handleAppLogin)
												 name:@"APP_LOGIN"
											   object:nil];

	[self startSpinner];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
	[super didReceiveMemoryWarning];

	// Release any cached data, images, etc that aren't in use.
}

#pragma mark - Public methods

- (void)dayButtonPressed:(id)source {
	int baseTag = ((UIButton *)source).tag - 400;
	int day = baseTag - 2;

	selectedDay = day;

	for (UIView *v in self.dayButtons) {
		if (day == v.tag - 2) {
			// selected
			((UIImageView *)[v viewWithTag:v.tag + 100]).image = [UIImage imageNamed:@"epg_date_selector_selected"];
			UILabel *l = ((UILabel *)[v viewWithTag:v.tag + 300]);
			l.textColor = [UIColor colorWithRed:6.0 / 255.0
										  green:135.0 / 255.0
										   blue:1
										  alpha:1];
			l.layer.shadowColor = [UIColor colorWithRed:6.0 / 255.0
												  green:135.0 / 255.0
												   blue:1
												  alpha:1].CGColor;
		}
		else {
			// not selected
			((UIImageView *)[v viewWithTag:v.tag + 100]).image = nil;
			UILabel *l = ((UILabel *)[v viewWithTag:v.tag + 300]);
			l.textColor = [UIColor whiteColor];
			l.layer.shadowColor = [UIColor whiteColor].CGColor;
		}
	}

	if (0 == day) {
		isToday = YES;
	}
	else {
		isToday = NO;
	}

	[_gridView reset];
	[_gridView release];
	[_gridView removeFromSuperview];

	CGRect gridFrame = CGRectMake(0, 61, epgWidth, 595);
	_gridView = [[VSEPGGridView alloc] initWithFrame:gridFrame];
	// self.gridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	self.gridView.gridViewDataSource = self;
	self.gridView.gridViewDelegate = self;
	self.gridView.showsHorizontalScrollIndicator = NO;
	self.gridView.showsVerticalScrollIndicator = NO;

	self.gridView.rulerBackgroundColor = self.rulerBackgroundColor;
	self.gridView.titleBackgroundColor = self.titleBackgroundColor;
	self.gridView.contentBackgroundColor = [UIColor clearColor];
	self.gridView.hairlineBackgroundColor = self.hairlineBackgroundColor;

	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(handleTilesRendered)
												 name:kVSTilesRendered
											   object:nil];

	[self.view addSubview:self.gridView];

	[self startSpinner];
}

- (void)cancelProgress {
	[self.operationQueue cancelAllOperations];
}

#pragma mark - VSEPGGridViewDataSource

- (NSUInteger)numberOfRowsInGridView:(VSEPGGridView *)aGridView {
	return [self.channels count];
}

- (CGFloat)heightOfRowsInGridView:(VSEPGGridView *)aGridView {
	return 52;
}

- (CGFloat)widthOfTitleInGridView:(VSEPGGridView *)aGridView {
	return 181;
}

- (CGFloat)heightOfRulerInGridView:(VSEPGGridView *)gridView {
	return 37;
}

- (VSEPGGridViewTitleCell *)titleCellForGridView:(VSEPGGridView *)aGridView {
	return [TVGuideViewController defaultTitleCell];
}

- (void)gridView:(VSEPGGridView *)aGridView configureTitleCell:(VSEPGGridViewTitleCell *)cell forRow:(NSUInteger)row {
	NSDictionary *dict = [self.channels objectAtIndex:row];

	dict = [[AppView sharedInstance] channelByChannelId:[dict valueForKey:@"channel"]];
	cell.channel = dict;
	if ([[dict valueForKey:@"pay"] boolValue]) {
		NSString *channelId = [dict valueForKey:@"channel"];
		NSNumber *chan = [NSNumber numberWithInt:[channelId intValue]];
		if (![[[AppView sharedInstance] getPaidChannels] containsObject:chan]) {
			[cell showLock];
		}
	}
}

- (VSEPGGridViewContentCell *)contentCellForGridView:(VSEPGGridView *)gridView {
	return [TVGuideViewController defaultContentCell];
}

- (BOOL)gridView:(VSEPGGridView *)gridView shouldHighlightContentCell:(VSEPGGridViewContentCell *)cell byScheduleInstance:(VSEPGScheduleInstance *)scheduleInstance {
	return [scheduleInstance.channelId isEqualToString:self.selectedChannelId];
}

- (BOOL)gridView:(VSEPGGridView *)gridView shouldHighlightTitleCell:(VSEPGGridViewTitleCell *)cell forRow:(NSUInteger)row {
	return self.selectedChannelId && [self.channels count] && [self.selectedChannelId isEqualToString:[[self.channels objectAtIndex:row] valueForKey:@"channel"]];
}

// This method does the downloading part. This is a customisation point:
// The general idea is the highest the index of a tile in the job queue, the highest
// the priority of the given job is. The EPG grid is the only component that can
// determine the right priority order.
//
// EPG grid will ask as whether it "shouldWaitForTileKey". In the respoctive method we'll
// reorder the job queue by placing the given tile key to the top of the queue this way
// giving the tile the highest prority.
- (void)prepareForTiles:(NSArray *)tileKeys forGridView:(VSEPGGridView *)gridView {
#warning stop potential curent progress

#if VS_DEBUG_LOG_EPG_PREPARE
	if ([tileKeys count]) {
		EPGLog(@"\n\n--- BEGIN PREPARING %d TILES ---\n\n", [tileKeys count]);
	}
	else {
		EPGLog(@"\n\n--- WILL NOT PREPARE FOR 0 TILES ---\n\n");
	}
#endif

	// STEP0: Define the block for the operation queue.
	void (^DPPBlock)(void) = ^(void) {
		// Just in case
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

		// Take the last - highest priority - tile and remove it from the top of the queue
		// The job is still available by tileKey from self.tileJobsByTileKey
		NSMutableDictionary *tileJob = nil;

		int tilesLeft = INT_MAX;

		@synchronized (tileJobMutex) {
			tileJob = [[[self.tileJobs lastObject] retain] autorelease];

			if (!tileJob) {
#if VS_DEBUG_LOG_EPG_PREPARE
				EPGLog (@"# No tileJob found. Maybe we've been just asked to prepare.");
#endif
				[pool release];
				return;
			}

			[self.tileJobs removeLastObject];

			tilesLeft = [self.tileJobs count];
#if VS_DEVELOPMENT_X

			if ([[NSUserDefaults standardUserDefaults] boolForKey:kVSSettingsShowTilesToGo]) {
				dispatch_sync (dispatch_get_main_queue (), ^(void) {
								   [[NSNotificationCenter defaultCenter] postNotificationName:kVSNotificationTilesToGo
																					   object:self
																					 userInfo:[NSDictionary dictionaryWithObjectsAndKeys:NUMINT (tilesLeft), kVSKeyTilesToGo, nil]];
							   });
			}
#endif

			EPGLog (@"# OF TILES LEFT: %d", tilesLeft);
			if (0 == tilesLeft) {
				EPGLog (@"\n\n--- PREPARE TILES FINISHED. PROCESSING LAST TILE ... ---\n\n");
			}
		}

		TileStatus currentStatus = [[tileJob objectForKey:@"tileStatus"] intValue];
		if (currentStatus < TileStatusPreProcessing) {
			// skip tile, was picked up by another operation
			[pool release];
			return;
		}

		// Change tile's status
		[tileJob setObject:NUMINT (TileStatusDownloading) forKey:@"tileStatus"];
#if VS_DEBUG_LOG_EPG
		// EPGLog(@"T%@, S%d", tileKey, TileStatusDownloading);
#endif

		NSArray *channelIds             = [tileJob valueForKey:@"channelIds"];
		NSDictionary *rowChannelIdMap   = [tileJob valueForKey:@"rowChannelIdMap"];
		NSTimeInterval hoursSince1970   = [[tileJob valueForKey:@"hoursSince1970"] doubleValue];
		NSUInteger row                  = [[tileJob valueForKey:@"row"] intValue];
		NSNumber *tileKey               = [tileJob valueForKey:@"tileKey"];

		if (![channelIds count]) {
			NSLog (@"ZZZZ");
		}

		// NSError *error = nil;

#warning DATA not loaded
		NSArray *content = [[AppView sharedInstance] scheduleForChannelIds:channelIds
																 startTime:hoursSince1970 * 3600.0
														   durationSeconds:kVSEPGChunkInterval * 3600.0];



		// NSLog(@"%@", content);

		if (!content || ![content count]) {
			// At this point we could requeue the job if we want to do this. Do we? We don't actually.
			[pool release];
			return;
		}

		// Change tile's status
		[tileJob setObject:[NSNumber numberWithInt:TileStatusPostProcessing] forKey:@"tileStatus"];
#if VS_DEBUG_LOG_EPG
		EPGLog (@"T%@, S%d", tileKey, TileStatusPostProcessing);
#endif
		NSMutableDictionary *channelsPrograms = [NSMutableDictionary dictionary];

		for (NSDictionary *program in content) {
			NSString *channelId = [program valueForKey:@"channel"];
			NSMutableArray *channelArray = [channelsPrograms valueForKey:channelId];
			if (!channelArray) {
				channelArray = [NSMutableArray array];
				[channelsPrograms setValue:channelArray forKey:channelId];
			}
			[channelArray addObject:[[[VSEPGScheduleInstance alloc] initWithSDPDictionary:program] autorelease]];
		}

		NSUInteger currentRow = row;

		@synchronized (epgCacheMutex) {
			for (int i = 0; i < kVSEPGTileRows; i++, currentRow++) {
				NSNumber *rowChunkKey = [self chunkKeyFromHoursSince1970:hoursSince1970 row:currentRow];
				NSString *channelId = [rowChannelIdMap objectForKey:NUMINT (currentRow)];
				NSMutableArray *channelPrograms = [channelsPrograms valueForKey:channelId];

				/*
				 * // This is an optimization: removing duplicate channels that are part of both.
				 * // This move has a positive side effect: long programs are preserved in 010101... fashion
				 * NSNumber *prevRowChunkKey = [self chunkKeyFromHoursSince1970:hoursSince1970 - kVSEPGChunkIntervalD row:currentRow];
				 * NSMutableArray *prevChannelPrograms = [self.epgCache objectForKey:prevRowChunkKey];
				 * if (prevChannelPrograms) {
				 * #if VS_DEBUG_OVERLAPPING_SCHEDULE
				 * NSMutableArray *oldChannelPrograms = [NSMutableArray arrayWithArray:channelPrograms];
				 * #endif
				 * [channelPrograms removeObjectsInArray:prevChannelPrograms];
				 * #if VS_DEBUG_OVERLAPPING_SCHEDULE
				 * NSMutableArray *newChannelPrograms = [NSMutableArray arrayWithArray:channelPrograms];
				 * [oldChannelPrograms removeObjectsInArray:newChannelPrograms];
				 * if (0 == [oldChannelPrograms count] && 0 < [newChannelPrograms count]) {
				 * // removed everything, the two are identical
				 * #if VS_DEBUG_LOG_EPG
				 * EPGLog(@"Nothing to eliminate by PREV (%.0f / %d)", hoursSince1970, row);
				 * #endif
				 * } else {
				 * #if VS_DEBUG_LOG_EPG
				 * EPGLog(@"Eliminated by PREV:  (%.0f / %d) %@", hoursSince1970, row, oldChannelPrograms);
				 * #endif
				 * }
				 * #endif
				 * } else {
				 * // Though we have to cover the case when there's no prev chunk loaded. In this case let's do
				 * // the same on the next chunk's behalf.
				 * NSNumber *nextRowChunkKey = [self chunkKeyFromHoursSince1970:hoursSince1970 + kVSEPGChunkIntervalD row:currentRow];
				 * NSMutableArray *nextChannelPrograms = [self.epgCache objectForKey:nextRowChunkKey];
				 * if (nextChannelPrograms) {
				 * #if VS_DEBUG_OVERLAPPING_SCHEDULE
				 * NSMutableArray *oldNextChannelPrograms = [NSMutableArray arrayWithArray:nextChannelPrograms];
				 * #endif
				 * [nextChannelPrograms removeObjectsInArray:channelPrograms];
				 * #if VS_DEBUG_OVERLAPPING_SCHEDULE
				 * NSMutableArray *newNextChannelPrograms = [NSMutableArray arrayWithArray:nextChannelPrograms];
				 * [oldNextChannelPrograms removeObjectsInArray:newNextChannelPrograms];
				 * if (0 == [oldNextChannelPrograms count] && 0 < [newNextChannelPrograms count]) {
				 * // removed everything, the two are identical
				 * #if VS_DEBUG_LOG_EPG
				 * EPGLog(@"Nothing to   eliminate anything by NEXT  (%.0f / %d)", hoursSince1970, row);
				 * #endif
				 * } else {
				 * #if VS_DEBUG_LOG_EPG
				 * EPGLog(@"Eliminated overlapping schedules by NEXT:  (%.0f / %d) %@", hoursSince1970, row, oldNextChannelPrograms);
				 * #endif
				 * }
				 * #endif
				 * // Then reset the next chunk
				 * [self.epgCache setObject:nextChannelPrograms forKey:self.epgCache];
				 * }
				 * }
				 */


				if (channelPrograms) {
					[self.epgCache setObject:channelPrograms forKey:rowChunkKey];
				}
			}
		}

		[tileJob setObject:NUMINT (TileStatusDone) forKey:@"tileStatus"];
		if ([self.gridView tileVisible:tileKey]) {
#if VS_DEBUG_LOG_EPG
			EPGLog (@"T%@, S%d visible", tileKey, TileStatusDone);
#endif
			dispatch_async (dispatch_get_main_queue (), ^(void) {
#if VS_DEBUG_LOG_EPG_FORCELAYOUT
								EPGLog (@"\n\n--- FORCING LAYOUT DUE TO TILE #%@ IS VISIBLE ---\n\n", tileKey);
#endif
								NSAutoreleasePool *poolM = [[NSAutoreleasePool alloc] init];
								self.gridView.forceNewLayout = YES;
								[self.gridView setNeedsLayout];
								[poolM release];
							});
		}
		else {
#if VS_DEBUG_LOG_EPG
			EPGLog (@"T%@, S%d non-visible", tileKey, TileStatusDone);
#endif
		}

		dispatch_async (dispatch_get_main_queue (), ^{
							[[NSNotificationCenter defaultCenter] postNotificationName:kVSAllTilesProcessed object:self];
						});

		[pool release];
	};

	// STEP1: Clear and build the new job queue. At the end, all the items are in waiting status.
	@synchronized (tileJobMutex) {
		[self.operationQueue cancelAllOperations];
		self.tileJobs = [NSMutableArray array];
		self.tileJobsByTileKey = [NSMutableDictionary dictionary];
		self.epgCache = [NSMutableDictionary dictionary];

		for (NSNumber *tileKey in tileKeys) {
			NSMutableDictionary *tileJob = [NSMutableDictionary dictionaryWithObjectsAndKeys:
											tileKey, @"tileKey",
											NUMINT (TileStatusWaiting), @"tileStatus",
											nil];
			[self.tileJobs addObject:tileJob];
			[self.tileJobsByTileKey setObject:tileJob forKey:tileKey];
#if VS_DEBUG_LOG_EPG
			EPGLog (@"T%@, S%d", tileKey, TileStatusWaiting);
#endif
		}
	}

	dispatch_async (dispatch_get_global_queue (DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
	                    // STEP2: Determine the channel ids per chunk assuming no tiles data available atm.
	                    // At the end, all the items are in  preprocessing status.
						@synchronized (tileJobMutex) {
							for (NSMutableDictionary *tileJob in [self.tileJobsByTileKey allValues]) {
								NSNumber *tileKey = [tileJob valueForKey:@"tileKey"];

	                    // Change tile's status
								[tileJob setObject:NUMINT (TileStatusPreProcessing) forKey:@"tileStatus"];
#if VS_DEBUG_LOG_EPG
								EPGLog (@"T%lld, S%d", [tileKey longLongValue], TileStatusPreProcessing);
#endif

	                    // Resolve the tileKey
								int chunkHoursSince1970;
								int row;

								[VSEPGGridView resolveTileKey:tileKey intoChunkHoursSince1970:&chunkHoursSince1970 andRow:&row];

	                    // Compile comma separated channel id list and
	                    // rowid -> channelid mapping table
								NSMutableArray *channelIds = [NSMutableArray array];
								NSMutableDictionary *rowChannelIdMap = [NSMutableDictionary dictionaryWithCapacity:kVSEPGTileRows];

								NSUInteger currentRow = row;
								int channelsCount = [self.channels count];
								for (int i = 0; i < kVSEPGTileRows && currentRow < channelsCount; i++, currentRow++) {
									NSString *channelId = [[self.channels objectAtIndex:currentRow] valueForKey:@"channel"];
									[channelIds addObject:channelId];

									[rowChannelIdMap setObject:channelId forKey:NUMINT (currentRow)];
								}

								if (![channelIds count]) {
									NSLog (@"ZZZZ");
								}
								[tileJob setObject:channelIds forKey:@"channelIds"];
								[tileJob setObject:rowChannelIdMap forKey:@"rowChannelIdMap"];
								[tileJob setObject:NUMDOUBLE (chunkHoursSince1970)  forKey:@"hoursSince1970"];
								[tileJob setObject:NUMINT (row)                     forKey:@"row"];
							}

	                        // STEP3: Add as many operation blocks to the queue as many tile jobs we have.
							int jobsCount = [self.tileJobsByTileKey count];
							for (int i = 0; i < jobsCount; i++) {
								[self.operationQueue addOperationWithBlock:DPPBlock];
							}
						}
					});
}
// This method does the queue ordering
- (BOOL)gridView:(VSEPGGridView *)gridView shouldWaitForTileKey:(NSNumber *)tileKey {
	NSMutableDictionary *tileJob = nil;

	// @synchronized(tileJobMutex) {
	tileJob = [self.tileJobsByTileKey objectForKey:tileKey];
	// }

	TileStatus tileStatus = [[tileJob valueForKey:@"tileStatus"] intValue];

	if (TileStatusDone == tileStatus) {
		// Already down, no need to keep gridView waiting
		return NO;
	}

	switch (tileStatus)
	{
		case TileStatusPreProcessing :
		case TileStatusWaiting : {
#if !VS_EPG_IGNORE_REQUEUING_ON_VIEWPORT_CHANGE
			@synchronized(tileJobMutex)
			{
#if kVSSettingsVisualizeEPGTiles
				EPGLog(@"Rescheduling tile #%@ to the highest priority", tileKey);
#endif

				[self.tileJobs removeObject:tileJob];
				[self.tileJobs addObject:tileJob];
			}
#endif
		}
		default :
			break;
	}

	return YES;
}

- (NSArray *)gridView:(VSEPGGridView *)gridView dataForChunkHoursSince1970:(NSTimeInterval)hoursSince1970 row:(NSUInteger)row {
	NSNumber *chunkKey = [self chunkKeyFromHoursSince1970:hoursSince1970 row:row];
	id chunk = nil;

	// @synchronized(epgCacheMutex) {
	chunk = [self.epgCache objectForKey:chunkKey];
	// }
	if ([chunk isKindOfClass:[NSArray class]]) {
		return [NSArray arrayWithArray:chunk];
	}
	return nil;
}

#pragma mark - Static methods

+ (VSEPGGridViewTitleCell *)defaultTitleCell {
	return [[[VSEPGGridViewTitleCell alloc] initWithFrame:CGRectMake(0, 0, 181, 52)] autorelease];
}

+ (VSEPGGridViewContentCell *)defaultContentCell {
	return [[[VSEPGGridViewContentCell alloc] initWithFrame:CGRectMake(0, 0, 100, 100)] autorelease];
}

- (NSNumber *)chunkKeyFromHoursSince1970:(NSTimeInterval)hoursSince1970 row:(NSUInteger)row {
	long long multiplier = (long long)kVSEPGGridViewChunkKeyMultiplier;
	long long hoursSince1970L = (long long)hoursSince1970;
	long long hrsPart = hoursSince1970L * multiplier;
	long long full = hrsPart + (long long)row;

	return NUMLONGLONG(full);
}

#pragma mark - VSEPGGridViewDelegate


- (void)gridView:(VSEPGGridView *)gridView titleCellSelected:(VSEPGGridViewTitleCell *)cell
			 row:(NSUInteger)row previouslySelectedCell:(VSEPGGridViewTitleCell *)previouslySelectedCell {
	// nop


	[self gridViewtitleCellSelectedWithChannelId:[cell.channel valueForKey:@"channel"] uponTap:YES];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 1) {
		NSDictionary *item = [[AppView sharedInstance] channelByChannelId:self.selectedChannelId];
		[PopupHelper showSubscribePinPopupForItem:item delegate:self];
	}
}

- (void)pinCorrect:(NSDictionary *)item {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
					   [[AppView sharedInstance] addChannelToPaidChannels:item];
					   dispatch_async (dispatch_get_main_queue (), ^{
										   UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"Subscription Succesful" message:[NSString stringWithFormat:@"Your subscription was confirmed. You can now watch %@", [item objectForKey:@"name"]] delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease ];
										   [alertView show];
									   });
				   });
}

- (void)startVideoIfPossible {
	NSLog(@"START LIVE TV VIDEO");
	if (self.selectedChannelId) {
		[self gridViewtitleCellSelectedWithChannelId:self.selectedChannelId uponTap:NO];
	}
}

- (void)gridViewtitleCellSelectedWithChannelId:(NSString *)channelId uponTap:(BOOL)uponTap {
	// nop
	self.selectedChannelId = channelId;
	NSDictionary *item = [[AppView sharedInstance] channelByChannelId:channelId];

	NSLog(@"ITEM SELECTED");
	[self closePlayer];

	NSNumber *pay = [item objectForKey:@"pay"];

	if (pay && [pay boolValue]) {
		// self.subscribeItem = item;


		if ([AppView sharedInstance].fingerprint) {
			NSArray *paidChannels = [[AppView sharedInstance] getPaidChannels];


			NSNumber *chan = [NSNumber numberWithInt:[[item objectForKey:@"channel"] intValue]];

			// NSLog(@"CHANNEL IS PAYD: %@ %@", chan, paidChannels);

			if (![paidChannels containsObject:chan]) {
				[PopupHelper showSubscribeAlert:self];
			}
			else {
				[self.liveTvView configureView:item];
				if (self.navigationController == ((UITabBarController *)self.view.window.rootViewController).selectedViewController && self.navigationController.topViewController == self) {
					[self playEmbedded:item inView:self.liveTvView.movieConteiner];
				}
			}
		}
		else {
			[self openLogin:Nil];
		}
	}
	else {
		[self.liveTvView configureView:item];
		NSLog(@" %@", ((UITabBarController *)[UIApplication sharedApplication].delegate.window.rootViewController).selectedViewController);
		if (self.navigationController == ((UITabBarController *)[UIApplication sharedApplication].delegate.window.rootViewController).selectedViewController) {
			if (self.navigationController.topViewController == self) {
				[self playEmbedded:item inView:self.liveTvView.movieConteiner];
			}
			else {
				NSLog(@"NOT ON TOP");
			}
		}
		else {
			NSLog(@"NOT SELECTED");
		}
	}






	if (NO && uponTap) {
		[UIView animateWithDuration:.3 animations:^{
			 [[NSNotificationCenter defaultCenter] postNotificationName:@"kVSEPGChannelSelectionChanged"
																 object:self
															   userInfo:[NSDictionary dictionaryWithObject:self.selectedChannelId
																									forKey:@"cid"]];
		 }];
	}
	else {
		[[NSNotificationCenter defaultCenter] postNotificationName:@"kVSEPGChannelSelectionChanged"
															object:self
														  userInfo:[NSDictionary dictionaryWithObject:self.selectedChannelId
																							   forKey:@"cid"]];
	}

	// [self.liveTvView configureView:[[AppView sharedInstance] channelByChannelId:self.selectedChannelId]];

	// [self playEmbedded:[[AppView sharedInstance] channelByChannelId: self.selectedChannelId ] inView:self.liveTvView.movieConteiner];
}

- (void)gridView:(VSEPGGridView *)gridView dayChanged:(NSDate *)newDate {
	// nop
}

- (BOOL)shouldShowHairlineInGridView:(VSEPGGridView *)gridView {
	return isToday;
}

- (BOOL)shouldConsiderHairlineInGridView:(VSEPGGridView *)gridView {
	return isToday;
}

- (void)gridView:(VSEPGGridView *)gridView contentCellSelected:(VSEPGGridViewContentCell *)cell {
	// [PopupHelper showProgramInfo:[NSDictionary dictionary] viewController:self];

	NSDictionary *metaData = cell.scheduleInstance.original;

	int mode = 0;

	if (selectedDay < 0) {
		NSLog(@"past");
		mode = 0;
	}
	else if (0 < selectedDay) {
		NSLog(@"future");
		mode = 1;
	}
	else {
		NSTimeInterval start = [[metaData valueForKey:@"startTimeSeconds"] doubleValue];
		NSTimeInterval duration = [[metaData valueForKey:@"durationSeconds"] doubleValue];

		// NSDate * localNow = [[NSDate date] dateByAddingTimeInterval:[[NSTimeZone localTimeZone] secondsFromGMT]];
		NSTimeInterval now = [[NSDate dateWithTimeInterval:[[NSTimeZone localTimeZone] secondsFromGMT] sinceDate:[NSDate date]] timeIntervalSince1970];

		if (now < start) {
			NSLog(@"future");
			mode = 1;
		}
		else if (start <= now && now <= start + duration) {
			NSLog(@"current");
			mode = 2;
		}
		else {
			NSLog(@"past");
			mode = 0;
		}
	}

	ProgramInfoPopupViewController *vc = [[ProgramInfoPopupViewController alloc] initWithNibName:@"ProgramInfoPopupViewController" bundle:Nil mode:mode meta:metaData];

	UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
	nav.modalPresentationStyle = UIModalPresentationFormSheet;
	[self.view.window.rootViewController presentViewController:nav animated:YES completion:^{}];

	nav.view.superview.frame = CGRectMake (0, 0, 700, 342 + nav.navigationBar.frame.size.height);
	CGPoint p = self.view.center;
	nav.view.superview.center = CGPointMake (p.x, p.y + 50);

	[nav release];
	[vc release];
}

#pragma mark - Actions

- (void)openProgramInfo {
}

- (void)handleTilesRendered {
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:kVSTilesRendered
												  object:nil];
	[self stopSpinner];
	if ([self.channels count] && ![self.selectedChannelId length]) {
		[self gridViewtitleCellSelectedWithChannelId:[[self.channels objectAtIndex:0] valueForKey:@"channel"] uponTap:NO];
	}
}

- (void)handleAppLogin {
	[self gridViewtitleCellSelectedWithChannelId:self.selectedChannelId uponTap:NO];
	// [self.gridView reload];
}

@end
