//
//  AppView.h
//  TVE
//
//  Created by Gabor Bottyan on 12/19/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppView : NSObject

// Singleton
+ (AppView *)sharedInstance;

- (BOOL)isActive;
- (NSString *)getMaintainanceMessage;

- (NSString *)getSession;
// @property (retain) NSArray * cachedPaidChannels;
- (void)clearSubscription;
- (NSDictionary *)loadMetadata;
- (NSDictionary *)loadConfig;
- (NSDictionary *)createConfig;
- (void)loadAssets;
- (NSData *)assetByKey:(NSString *)key;

// TVE thePlatform
- (void)updateLoginHome:(NSString *)username;

// -(void)doCachePaidChannels;
@property (retain) NSString *fingerprint;
- (BOOL)login:(NSString *)userName password:(NSString * )password;
- (int)getRating:(NSDictionary *)asset;
- (void)setRating:(NSDictionary *)asset rating:(int)rating;
- (void)clearRentalData;

- (void)logout;
- (NSArray *)getRentedMovies;
- (NSArray *)getFavoriteChannels;
- (NSArray *)getFavoriteShows;
- (NSArray *)getPaidChannels;
- (NSArray *)searchChannels:(NSString *)query;
- (void)addChannelToFavorites:(NSDictionary *)channel;
- (BOOL)isFavoriteShow:(NSDictionary *)show;
- (BOOL)isFavoriteChannel:(NSDictionary *)channel;
- (void)addAssetToFavorites:(NSDictionary *)asset;
- (void)addChannelToPaidChannels:(NSDictionary *)channel;
- (NSString *)getHomeChannel;
- (void)setHomeChannel:(NSString *)channeld;
- (NSDictionary *)getRentalData:(NSDictionary *)asset;
- (void)setRental:(NSDictionary *)asset rental:(NSDictionary * )rental;
#pragma mark - JJ's methods above, Z's methods below

- (void)loadChannelsAndEPG;

- (NSArray *)allChannels;

- (NSArray *)allCategories;

- (NSDictionary *)channelByChannelId:(NSString *)channelId;

- (NSArray *)channelsOfCategory:(NSString *)category;

- (NSArray *)epgByCategories:(NSArray *)categories;


- (NSDictionary *)currentEPGProgramOfChannel:(NSDictionary *)channel programsOffset:(int)offset;
- (NSDictionary *)currentEPGProgramOfChannelId:(NSString *)channelId programsOffset:(int)offset;
- (NSString *)getEPGPeriod:(NSDictionary *)program;
- (NSArray *)scheduleForChannelIds:(NSArray *)channelIds startTime:(NSTimeInterval) startTime durationSeconds:(NSTimeInterval)durationSeconds;

- (NSString *)getAppViewUrl;
- (void)setAppViewUrl:(NSString *)newAppViewUrl;
- (NSString *)getApiKey;
- (void)setApiKey:(NSString *)newApiKey;

@end
