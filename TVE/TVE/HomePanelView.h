//
//  HomePanelView.h
//  TVE
//
//  Created by Gabor Bottyan on 11/28/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailOpenerDelegate.h"
#import "AssetView.h"
@protocol HomePanelViewDelegate
- (void)panelWillOpen:(id)sender;
- (void)panelWillClose:(id)sender;

@end;


@interface HomePanelView : UIView <AsetViewDelegate>

@property (assign) id <HomePanelViewDelegate> delegate;
@property (assign) id <DetailOpenerDelegate> detaildelegate;
@property (assign) UIView *contentView;
@property (assign) float contentHeight;
@property (assign) int itemCount;
@property (assign) BOOL open;
@property (retain) UIImageView *arrowView;

- (id)initWithFrame:(CGRect) frame title:(NSString * )title data:(NSDictionary * )_data;
@end
