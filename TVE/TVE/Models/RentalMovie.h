//
//  RentalMovie.h
//  TVE
//
//  Created by Mark Siu on 15/8/13.
//  Copyright (c) 2013 Accedo Broadband. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RentalMovie : NSObject

@property (nonatomic) NSString *mediaId;
@property (nonatomic) long long expiryDateInSec;

- (RentalMovie *)init;

@end
