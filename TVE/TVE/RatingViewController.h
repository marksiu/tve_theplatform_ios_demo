//
//  RatingViewController.h
//  TVE
//
//  Created by Mate Beres on 12/11/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "BaseViewController.h"

@protocol RatingViewControllerDelegate <NSObject>

- (int)getCurrentRating;
- (void)setCurrentRating:(int)rating;

@end

@interface RatingViewController : BaseViewController
@property (assign) id <RatingViewControllerDelegate> delegate;
@property (retain) NSMutableArray *ratingStarArray;
@end
