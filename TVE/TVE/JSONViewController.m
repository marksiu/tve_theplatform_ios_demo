//
//  JSONViewController.m
//  TVE
//
//  Created by Gabor Bottyan on 12/13/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import "JSONViewController.h"
#import "SBJson.h"
#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>

typedef struct
{
	uint8_t red;
	uint8_t green;
	uint8_t blue;
	uint8_t alpha;
} MyPixel_T;

static int tagCounter = 1;
@interface JSONViewController ()
@property (retain) NSBundle *bundle;
@property (assign) BOOL constructed;
@property (retain) NSMutableDictionary *controlOptions;
@property (retain) NSMutableDictionary *actions;
@end

@implementation JSONViewController

- (id)initWithBundle:(NSBundle *)__bundle {
	self = [super init];
	if (self) {
		self.bundle = __bundle;

		self.controls = [NSMutableDictionary dictionary];
		self.controlOptions = [NSMutableDictionary dictionary];
		self.actions = [NSMutableDictionary dictionary];



//        dispatch_async(dispatch_get_main_queue(), ^{
//
//
//            [self viewDidLoad];
//        });
	}
	return self;
}

- (void)on:(NSString * )key run:(EventCallback)callback {
	[self.actions setObject:callback forKey:key ];

	// NSLog(@"%@",self.actions);
}

- (BOOL)isTransparent:(UIImage *)myImage {
// The pixel format depends on what sort of image you're expecting. If it's RGBA, this should work

	BOOL result = YES;

	CGImageRef myCGImage = [myImage CGImage];

	// Get a bitmap context for the image
	CGContextRef context = CGBitmapContextCreate(NULL, CGImageGetWidth(myCGImage), CGImageGetHeight(myCGImage), CGImageGetBitsPerComponent(myCGImage), CGImageGetBytesPerRow(myCGImage), CGImageGetColorSpace(myCGImage), CGImageGetBitmapInfo(myCGImage));

	// Draw the image into the context
	CGContextDrawImage(context, CGRectMake(0, 0, CGImageGetWidth(myCGImage), CGImageGetHeight(myCGImage)), myCGImage);

// Get pixel data for the image
	MyPixel_T *pixels = CGBitmapContextGetData(context);
	size_t pixelCount = CGImageGetWidth(myCGImage) * CGImageGetHeight(myCGImage);

	for (size_t i = 0; i < pixelCount; i++) {
		MyPixel_T p = pixels[i];
		// Your definition of what's blank may differ from mine
		if (p.red > 0 && p.green > 0 && p.blue > 0 && p.alpha > 0) {
			result =  NO;
		}
	}

	CGContextRelease(context);
	return result;
}

- (void)viewDidLoad {
	// NSLog(@"viewDidLoad");
	[super viewDidLoad];
	self.navigationController.navigationBarHidden = YES;

	if (!self.constructed) {
		NSString *str = [NSString stringWithContentsOfFile:[self.bundle pathForResource:@"view" ofType:@"json"] encoding:NSUTF8StringEncoding error:Nil];
		NSDictionary *views = [str JSONValue];
		UIView *content = [self viewFromDictionary:views withNumbers:NO];
		[self.view addSubview:content];
		// [content release];
		self.constructed = YES;
	}
	UIImageView *iv =  [self.controls objectForKey:@"backgroundimage"];
	iv.image = [UIImage imageNamed:@"movie.jpg"];

//    self.navigationController.navigationBarHidden = YES;
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
	self.navigationController.navigationBarHidden = YES;
}

- (void)clicked:(id)sender {
	UIButton *b = (UIButton *)sender;

	b.superview.layer.borderColor = [[UIColor clearColor] CGColor];
	b.superview.layer.borderWidth = 2;
	b.superview.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
	[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(unclicked:) userInfo:b.superview repeats:NO];
}

- (void)unclicked:(id)sender {
	NSNotification *not = (NSNotification *)sender;
	UIView *v = (UIView *)[not userInfo];

	v.backgroundColor = [UIColor clearColor];
	v.layer.borderWidth = 1.0f;
	v.layer.borderColor = [[UIColor whiteColor] CGColor];
}

- (void)buttonClicked:(id)sender {
	UIButton *clickedButton  = (UIButton * )sender;
	NSNumber *optionKey = [NSNumber numberWithInt:clickedButton.tag];
	NSDictionary *options = [self.controlOptions objectForKey:optionKey];

	NSString *target = [options valueForKey:@"target"];
	// NSLog(@"%@",self.actions);
	EventCallback callback = [self.actions objectForKey:target];

	if (callback) {
		callback(clickedButton);
		// EXC_BAD_ACCESS
	}


	/*
	 * if (target){
	 *  SEL sel = NSSelectorFromString(target);
	 *  //if ([self respondsToSelector:sel]){
	 *  if (self.delegate){
	 *      [self.delegate performSelectorOnMainThread:sel withObject:clickedButton waitUntilDone:YES];
	 *  }
	 *  //}else{
	 *    //  NSLog(@"Selector not found: %@", target);
	 *  //}
	 *
	 * }
	 */
}

- (UIView * )viewFromDictionary:(NSDictionary *)d withNumbers:(BOOL)numbers {
	NSNumber *optionKey = [NSNumber numberWithInt:tagCounter];

	[self.controlOptions setObject:[NSMutableDictionary dictionary] forKey:optionKey];

	float top     = [[d objectForKey:@"top"] floatValue];
	float left    = [[d objectForKey:@"left"] floatValue];
	float width   = [[d objectForKey:@"width"] floatValue];
	float height  = [[d objectForKey:@"height"] floatValue];



	NSNumber *disabled = [d objectForKey:@"disabled"];
	if (disabled && [disabled boolValue]) {
		return Nil;
	}

	NSString *number = [d objectForKey:@"id"];
	UIView *v = Nil;
	BOOL skipImage = NO;
	NSString *class = [d objectForKey:@"class"];

	if (class) {
		if ([class isEqualToString:@"mpmovieplayer"]) {
			MPMoviePlayerController *mp = [[[MPMoviePlayerController alloc] init] autorelease];
			v = mp.view;
			mp.controlStyle = MPMovieControlStyleNone;
			[self.controls setObject:mp forKey:number];
		}


		if ([class isEqualToString:@"label"]) {
			skipImage = YES;
			UILabel *l = [[[UILabel alloc] init] autorelease];

			NSArray *textColor = [d objectForKey:@"text-color"];
			if (textColor) {
				float r = [[textColor objectAtIndex:0] floatValue];
				float g = [[textColor objectAtIndex:1] floatValue];
				float b = [[textColor objectAtIndex:2] floatValue];
				float a = [[textColor objectAtIndex:3] floatValue];
				l.textColor = [UIColor colorWithRed:r green:g blue:b alpha:a];
			}

			NSNumber *fontSize = [d objectForKey:@"font-size"];
			if (fontSize) {
				l.font = [UIFont systemFontOfSize:[fontSize floatValue]];
			}
			else {
				l.font = [UIFont systemFontOfSize:10];
			}



			NSString *text = [d objectForKey:@"text"];
			if (text) {
				l.text = text;
			}

			NSNumber *autosize = [d objectForKey:@"autosize"];
			if (autosize && [autosize boolValue]) {
				[l setAdjustsFontSizeToFitWidth:NO];
				l.numberOfLines = 0;
				l.lineBreakMode = NSLineBreakByWordWrapping;
				// [l sizeToFit];
				CGSize s = [l.text sizeWithFont:l.font constrainedToSize:CGSizeMake(width, height)];
				width = s.width;
				height = s.height;
			}

			skipImage = YES;
			v = l;

			[self.controls setObject:l forKey:number];
		}



		if ([class isEqualToString:@"button"]) {
			UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];

			NSString *target = [d objectForKey:@"target"];
			if (target) {
				NSMutableDictionary *options = [self.controlOptions objectForKey:[NSNumber numberWithInt:tagCounter]];
				[options setObject:target forKey:@"target"];
				[b addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
			}

			NSString *image = [d objectForKey:@"image"];

			if (image) {
				NSString *str = [self.bundle pathForResource:image ofType:@""];
				UIImage *img = [[[UIImage alloc] initWithContentsOfFile:str] autorelease];
				[b setBackgroundImage:img forState:UIControlStateNormal];
			}

			skipImage = YES;
			v = b;
			[self.controls setObject:b forKey:number];
		}

		if ([class isEqualToString:@"imageview"]) {
			UIImageView *b = [[[UIImageView alloc] init] autorelease];
			v = b;
			[self.controls setObject:b forKey:number];
		}
	}


	if (!v) {
		v = [[[UIView alloc] init] autorelease];
		[self.controls setObject:v forKey:number];
	}


	NSNumber *notouch = [d objectForKey:@"notouch"];
	if (notouch && [notouch boolValue]) {
		v.userInteractionEnabled = NO;
	}
	else {
		v.userInteractionEnabled = YES;
	}

	v.tag = tagCounter;
	tagCounter++;


	// v.userInteractionEnabled = NO;


	v.frame = CGRectMake(left, top, width, height);



	NSNumber *radius  = [d objectForKey:@"border-radius"];
	if (radius) {
		v.layer.cornerRadius = [radius floatValue];
	}

	// v.layer.borderWidth = 2.0f;
	// v.layer.borderColor = [[UIColor grayColor] CGColor];



	NSNumber *borderwidth  = [d objectForKey:@"border-width"];
	if (borderwidth) {
		v.layer.borderWidth = [borderwidth floatValue];
	}


	NSArray *borderColor = [d objectForKey:@"border-color"];
	if (borderColor) {
		float r = [[borderColor objectAtIndex:0] floatValue];
		float g = [[borderColor objectAtIndex:1] floatValue];
		float b = [[borderColor objectAtIndex:2] floatValue];
		float a = [[borderColor objectAtIndex:3] floatValue];
		v.layer.borderColor = [[UIColor colorWithRed:r green:g blue:b alpha:a] CGColor];
	}




	NSNumber *hidden = [d objectForKey:@"hidden"];
	if (hidden && [hidden boolValue]) {
		v.hidden = YES;
	}



	NSArray *bgColor = [d objectForKey:@"background-color"];
	if (!bgColor) {
		v.backgroundColor = [UIColor clearColor];
	}
	else {
		float r = [[bgColor objectAtIndex:0] floatValue];
		float g = [[bgColor objectAtIndex:1] floatValue];
		float b = [[bgColor objectAtIndex:2] floatValue];
		float a = [[bgColor objectAtIndex:3] floatValue];
		v.backgroundColor = [UIColor colorWithRed:r green:g blue:b alpha:a];
	}


	NSArray *subViews = [d objectForKey:@"subviews"];
	if (subViews) {
		for (NSDictionary *svd in subViews) {
			UIView *sv = [self viewFromDictionary:svd withNumbers:numbers];
			if (sv) {
				[v addSubview:sv];
			}
		}
	}





	NSString *image = [d objectForKey:@"image"];

	if (image && !skipImage) {
		NSString *str = [self.bundle pathForResource:image ofType:@""];
		UIImage *img = [[UIImage alloc] initWithContentsOfFile:str];

		if (img.size.width > 0 && img.size.height > 0 && ![self isTransparent:img]) {
			CALayer *l = [CALayer layer];
			l.frame = CGRectMake(0, 0, width, height);
			l.contents = (id)img.CGImage;
			[v.layer addSublayer:l];
		}
		else {
			// NSLog(@"EMPTY IMAGE: %@", str);
		}
		[img release];
	}




	if (numbers) {
		v.layer.borderWidth = 1.0f;
		v.layer.borderColor = [[UIColor whiteColor] CGColor];



		UIButton *nr = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
		// [nr setTitle:[NSString stringWithFormat:@"%@ (%.0fx%f)",number, width,height] forState:UIControlStateNormal];
		[nr setTitle:[NSString stringWithFormat:@"%@", number] forState:UIControlStateNormal];
		nr.titleLabel.font = [UIFont systemFontOfSize:12];

		CGSize s = [nr.titleLabel.text sizeWithFont:nr.titleLabel.font];
		nr.frame = CGRectMake(0, 0, s.width, s.height);
		// NSLog([NSString stringWithFormat:@"%@ (%.0fx%.0f)",number, width,height]);

		[nr addTarget:self action:@selector(clicked:) forControlEvents:UIControlEventTouchUpInside];
		/*
		 * nr.font = [UIFont systemFontOfSize:12];
		 * nr.textAlignment = NSTextAlignmentCenter;
		 * nr.text = [NSString stringWithFormat:@"%d", number];
		 *
		 */

		int pos = random() % 4;
		switch (pos)
		{
			case 0 :
				nr.center = v.center;
				break;
			case 1 :
				nr.center = CGPointMake(0, 0);
				break;
			case 2 :
				nr.center = CGPointMake(0, height);
				break;
			case 3 :
				nr.center = CGPointMake(width, height);
				break;
			case 4 :
				nr.center = CGPointMake(width, 0);
				break;

			default :
				break;
		}


		// nr.center = v.center;
		nr.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5f];
		nr.layer.cornerRadius = 3.0f;
		[v addSubview:nr];
		[nr release];
	}

	return v;
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
