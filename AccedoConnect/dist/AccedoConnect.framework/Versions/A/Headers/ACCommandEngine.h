//
//  ACCommandEngine.h
//  CMore
//
//  Created by Csaba Tűz on 2012.10.04..
//
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

typedef enum {
    KEY_UP,     //"up";
    KEY_DOWN,   //"down";
    KEY_LEFT,   //"left";
    KEY_RIGHT,  //"right";
    KEY_ENTER,  //"ok";
    KEY_RED,    //"red";
    KEY_GREEN,  //"green";
    KEY_YELLOW, //"yellow";
    KEY_BLUE,   //"blue";
    KEY_BACK,   //"back";
    KEY_INFO,   //"info";
    KEY_EXIT,   //"exit";
    KEY_TOOLS,  //"tools";
    KEY_PRE_CH, //"pre-ch";
    KEY_PLAY,   //"play";
    KEY_PAUSE,  //"pause";
    KEY_STOP,   //"stop";
    KEY_FF,     //"ff";
    KEY_RW      //"rw";
} ACKeyCode;

@class ACManager;

@interface ACCommandEngine : NSObject

@property (assign) ACManager* manager;

- (void) processCommand:(NSString*)command payload:(id)payload sender:(NSString*)sender deviceType:(NSString*)deviceType deviceName:(NSString *)deviceName;

- (void) sendPlayWvAsset:(NSDictionary* ) wvPlaybackDict isStream:(BOOL)isStream
                 asset:(NSString*)assetId licenceBaseUri:(NSString*)licenceBaseUri
       initialPosition:(double)initialPosition
      selectedSubtitle:(NSString*)selectedSubtitle
                target:(NSString*)target;

- (void) sendPlayAsset:(NSString* )mediaURL
              isStream:(BOOL)isStream
                 asset:(NSString*)assetId
       initialPosition:(double)initialPosition
      selectedSubtitle:(NSString*)selectedSubtitle
                target:(NSString*)target;

- (void) sendPlaybackStatusUpdates:(double)currentPlaybackTime duration:(double)duration
                     playbackState:(MPMoviePlaybackState)state asset:(NSString*)assetId
                  selectedSubtitle:(NSString*)selectedSubtitle volume:(int)volume
                            target:(NSString*)target
                          isStream:(BOOL)isStream
                        deviceType:(NSString*)deviceType;
//- (void) sendPlaybackStartedToTarget:(NSString*)target;
- (void) sendPlaybackTerminatedToTarget:(NSString*)target deviceType:(NSString*)deviceType;

- (void) sendIdentPairingPersistent:(BOOL)persistent operatorName:(NSString*)operatorName userId:(NSString*)userId country:(NSString*)country;

- (void) sendKeyEvent:(ACKeyCode)key toTarget:(NSString*)target;
@end
