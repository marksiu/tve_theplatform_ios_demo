//
//  ACPairingPopupViewController.h
//  CMore
//
//  Created by Csaba Tűz on 2012.10.19..
//
//

#import <UIKit/UIKit.h>
#import "ACManager.h"
//#import "FontLabel.h"
#import "ZBarSDK.h"

@interface ACPairingViewController : UIViewController<ZBarReaderDelegate> {
    IBOutlet UIButton* connectButton;
    IBOutlet UIButton* disconnectButton;
    
    IBOutlet UIImageView* connectImage;
    IBOutlet UILabel* connectedLabel;
    IBOutlet UIActivityIndicatorView* connectIndicator;
    IBOutlet UIImageView* connectedImageOffline;
    IBOutlet UIImageView* connectedImageOnline;
    
    IBOutlet UIView* inputCodeView;
    IBOutlet UILabel *lblInputHeader;
    IBOutlet UILabel *lblInputKode;
    
    IBOutlet UIButton* readQRButton;

    IBOutlet UILabel *lblHowToStep1;
    IBOutlet UILabel *lblHowToStep2;
    IBOutlet UILabel *lblHowToStep3;
    IBOutlet UILabel *lblHowToStep4;
    IBOutlet UILabel *lblHowToStep5;
    
    IBOutlet UILabel * lblOrLabel;
    
    IBOutlet UIView* viewNotPaired;
    IBOutlet UIView* viewNotPaired_Portrait;
    IBOutlet UIView* infoView;
}

@property (retain, nonatomic) IBOutlet UITextField *txtInputCode;

@property (assign) BOOL connectedImageHidden;
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImage;

@property (nonatomic, copy) void (^successBlock)(void);
@property (nonatomic, copy) void (^failureBlock)(void);

@end
