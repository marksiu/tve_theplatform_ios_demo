//
//  ConnectChannel.h
//  ConnectSDK
//
//  Created by Alexej Kubarev on 9/18/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChannelDelegate.h"

@interface Channel : NSObject {
    NSString* name;
    NSString* pairingCode;
    id<ChannelDelegate> delegate;
    BOOL acceptOwnMessages;
}

/**
 Channel Name.
 */
@property (readonly, atomic) NSString* name;


/**
Pairing code for client
*/
@property (readonly, atomic) NSString* pairingCode;

/**
 Connect Channel Delegate.
 */
@property (readonly, atomic) id<ChannelDelegate> delegate;
/**
 4 letter channel connection code.
 */
@property (readonly, atomic) NSString* channelCode;
/**
 Should  this channel accept messages broadcasted by itself?
 Default is NO.
 */
@property (assign, atomic) BOOL acceptOwnMessages;

/**
 Initialize channel. (Internal use)
 @private
 @param name Channel Name
 @param delegate Delegate object implementing ConnectChannelDelegate protocol
 */
- (id) initWithName: (NSString*) name delegate: (id<ChannelDelegate>)delegate;


/**
 Initialize channel with pairing code (Internal use)
 @private
 @param name Channel Name
 @param code Pairing code
 @param delegate Delegate object implementing ConnectChannelDelegate protocol
 */
- (id) initWithName:(NSString *) channelName pairingCode: (NSString*) code delegate:(id<ChannelDelegate>)channelDelegate;

/**
 Send a message to channel
 @param Message to send
 */
- (void) sendMessage:(id) message;

/**
 Disconnect from channel.
 This ConnectChannel is no longer capable of communication and can be discarded.
 */
- (void) leave;

@end
