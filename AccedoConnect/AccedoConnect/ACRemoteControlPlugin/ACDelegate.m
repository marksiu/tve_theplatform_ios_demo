//
//  ACDelegate.m
//  CMore
//
//  Created by Csaba Tűz on 2012.10.05..
//
//

#import "ACDelegate.h"
//#import "AppDelegate.h"

@interface ACDelegate() {
    NSDictionary* playAssetUserInfo;
}
@end

@implementation ACDelegate

@synthesize acManager;


-(void) pairOnChannel:(NSString*)channelName {
    if (self.acManager) {
        [self.acManager disconnect];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    self.acManager = nil;
    self.acManager = [[[ACManager alloc] initWithChannelName:channelName] autorelease];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPlayAssetReceived:) name:@"AC_PLAY_ASSET" object:[UIApplication sharedApplication].delegate];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didHearthbeatReceived:) name:@"AC_HEARTBEAT" object:[UIApplication sharedApplication].delegate];
}

-(void) disconnectAC {
    if (self.acManager) {
        [self.acManager disconnect];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    self.acManager = nil;
}

- (void) dealloc {
    if (self.acManager) {
        [self.acManager disconnect];
    }
    self.acManager = nil;
    [super dealloc];
}

-(void) didHearthbeatReceived:(NSNotification*)notification {
  /*  AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    CMoreViewController * cmoreVC = [appDelegate currentCMoreViewController];
    
    if (cmoreVC.isPlaying) {
        [cmoreVC sendPlaybackStatusUpdate];
    }*/
    
}

-(NSDictionary*) assemblePlaybackDict:(NSDictionary*)sparse {
    NSMutableDictionary* result = [NSMutableDictionary dictionary];
    NSString* licenceURI = [sparse valueForKey:@"licenceURI"];
    NSString* movieURL = [sparse valueForKey:@"movieURL"];
    NSString* playbackStatus = [sparse valueForKey:@"playbackStatus"];
    NSDictionary* itemsDict = [NSDictionary dictionaryWithObjectsAndKeys:
                               [NSArray arrayWithObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSDictionary dictionaryWithObjectsAndKeys:licenceURI, @"@uri", nil], @"license",
                                   @"wvm", @"mediaFormat",
                                   movieURL,@"url", nil]], @"item", nil];
    [result setValue:[NSDictionary dictionaryWithObjectsAndKeys:
                      playbackStatus, @"playbackStatus",
                      itemsDict, @"items", nil] forKey:@"playback"];
    return result;
}

-(void) didPlayAssetReceived:(NSNotification*)notification {
    /*playAssetUserInfo = [[notification userInfo] retain];
    NSDictionary* payload = [[notification userInfo] valueForKey:@"payload"];
    CMOREService* service = (CMOREService*)[[ServiceFactory sharedInstance] getService:@"CMOREService"];
    service.delegate = self;
    [service asyncFetchAssetsByIds:[NSArray arrayWithObject:[payload valueForKey:@"asset_id"]]];*/
}

#pragma mark - CMOREServiceDelegate

-(void) finishedWithError:(NSError *)error withActionId:(NSString *)actionId {
    
}

-(void) finishedWithResult:(NSDictionary *)model withActionId:(NSString *)actionId {
    if ([@"fetchAssets" isEqual:actionId]) {
        NSArray* assets = [model valueForKey:@"result"];
        if (IsEmpty(assets) || [assets count] == 0) {
            return;
        }
//        NSDictionary* asset = [assets objectAtIndex:0];
//        AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
//        CMoreViewController * cmoreVC = [appDelegate currentCMoreViewController];
//        NSDictionary* payload = [playAssetUserInfo valueForKey:@"payload"];
//        cmoreVC.playingAsset = asset;
//        cmoreVC.isPlaying = YES;
//        [cmoreVC playMovie:[self assemblePlaybackDict:[payload valueForKey:@"wvDict" ]]
//                 subtitles:[asset valueForKeyPath:@"metadata.subtitles"]
//                  isStream:[[payload valueForKey:@"stream"] boolValue] isRemote:NO
//           initialPosition:[[payload valueForKey:@"initial_position"] doubleValue]];
//        cmoreVC.cmorePlayer.remote = [playAssetUserInfo valueForKey:@"sender"];
//        cmoreVC.cmorePlayer.sendingStatusUpdate = YES;
        [playAssetUserInfo release];
        playAssetUserInfo = nil;
//        [self.acManager.commandEngine sendPlaybackStartedToTarget:cmoreVC.cmorePlayer.remote];
    }
}


@end
