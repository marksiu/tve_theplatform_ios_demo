//
//  ACRemoteControlDataSource.m
//  CMore
//
//  Created by Csaba Tűz on 2012.11.27..
//
//

#import "ACRemoteControlDataSource.h"
#import "ACStatusRecorder.h"

@implementation ACRemoteControlDataSource

@synthesize statusRecorder;

-(id)initForStatusRecorder:(ACStatusRecorder*)_statusRecorder {
    self = [super init];
    if (self) {
        self.statusRecorder = _statusRecorder;
        _statusRecorder.dataSource = self;
    }
    return self;
}

-(void)networkServicesReady {
}

-(void)assetForId:(NSString*)assetId {
}

- (NSString *)idForAsset:(NSDictionary *)asset isStream:(BOOL)stream {
    return nil;
}

-(NSDictionary*)channelAssetForAssetId:(NSString*)assetId {
    return nil;
}

-(NSArray*)subtitlesForAsset:(NSDictionary*)asset {
    return nil;
}

@end
