//
//  ACStatusRecorder.h
//  CMore
//
//  Created by Csaba Tűz on 2012.10.04..
//
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

#import "ACManager.h"

@class ACRemoteControlDataSource;

@protocol ACStatusRecorderOverlay

-(void) update:(id)sender;

@end

typedef enum {
    ACPlaybackStatePlaying,
    ACPlaybackStatePaused,
    ACPlaybackStateSeek,
    ACPlaybackStateTerminated
} ACPlaybackState;

@interface ACStatusRecorder : NSObject

@property (nonatomic, retain) NSString * operatorName;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * country;

@property ACPlaybackState playbackState;
@property double currentPlaybackTime;
@property double duration;
@property (nonatomic, retain) NSTimer* timer;
@property (nonatomic, retain) NSString* selectedSubtitle;
@property (retain, nonatomic) NSString* player;
@property (assign) BOOL playerOnline;
@property (retain, nonatomic) NSString* pairedDeviceName;

@property (nonatomic, retain) NSArray* subtitles;
@property (nonatomic, retain) ACRemoteControlDataSource* dataSource;

@property (nonatomic, retain) NSDictionary* asset;

@property (assign) BOOL isStream;
@property (assign) int volume;

@property (retain) NSNotification *lastPlaybackStatus;

- (id) initWithManager:(ACManager*)acManager;

- (id) initWithDataSource:(ACRemoteControlDataSource*)dataSource andManager:(ACManager*)acManager;

- (void) pause;
- (void) play;
- (void) terminateControl;
- (void) seek:(double)position;
- (void) selectSubtitle:(NSString*)locale isIncoming:(BOOL)incoming;
- (void) volumeChange:(float)volume;
-(void) updateAsset:(NSString*)assetId;

-(void) networkServicesReady;

-(void) reprocessLastPlaybackStatus;

- (void) didAssetReceived:(NSDictionary*)lAsset;
- (void) didAssetChannelsReceived:(NSArray*)channels;

@end
