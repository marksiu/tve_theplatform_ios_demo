//
//  ACRemoteControlDataSource.h
//  CMore
//
//  Created by Csaba Tűz on 2012.11.27..
//
//

#import <Foundation/Foundation.h>

@class ACStatusRecorder;

@interface ACRemoteControlDataSource : NSObject

@property (assign) ACStatusRecorder* statusRecorder;

-(id)initForStatusRecorder:(ACStatusRecorder*)statusRecorder;

-(void)assetForId:(NSString*)assetId;

-(NSString *)idForAsset:(NSDictionary*)asset isStream:(BOOL)stream;

-(void)networkServicesReady;

-(NSDictionary*)channelAssetForAssetId:(NSString*)assetId;

-(NSArray*)subtitlesForAsset:(NSDictionary*)asset;

@end
