//
//  ACStatusRecorder.m
//  CMore
//
//  Created by Csaba Tűz on 2012.10.04..
//
//

#import "ACStatusRecorder.h"
#import "ACManager.h"

#import "ACRemoteControlDataSource.h"

@interface ACStatusRecorder() {
    BOOL playerOnline;
    NSString *player;
}

@property (nonatomic, retain) ACManager* acManager;

@end

@implementation ACStatusRecorder

@synthesize playbackState;
@synthesize currentPlaybackTime;
@synthesize duration;
@synthesize selectedSubtitle;
@synthesize pairedDeviceName;
@synthesize isStream;

@dynamic player, playerOnline;

@synthesize asset,subtitles;

@synthesize volume;

@synthesize lastPlaybackStatus;

- (id) initWithManager:(ACManager*)acManager {
    self = [self initWithDataSource:nil andManager:acManager];
    if (self) {
    }
    return self;
}

- (id) initWithDataSource:(ACRemoteControlDataSource*)dataSource
               andManager:(ACManager*)acManager {
    self = [super init];
    if (self) {
        self.dataSource = dataSource;
        if (self.dataSource) {
            self.dataSource.statusRecorder = self;
        }
        self.playbackState = ACPlaybackStateTerminated;
        self.currentPlaybackTime = 0;
        self.acManager = acManager;
        self.duration = 1;
        self.selectedSubtitle = @"";
        self.isStream = NO;
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(secondsStepFw:) userInfo:nil repeats:YES];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPlaybackStatusUpdate:) name:@"AC_PLAYBACK_STATUS" object:[ACManager sharedInstance]];
        
        NSString * _pairedDeviceName = [[NSUserDefaults standardUserDefaults] valueForKey:@"paired_device"];
        self.pairedDeviceName = _pairedDeviceName;
    }
    return self;
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.timer invalidate];
    self.timer = nil;
    
    self.acManager = nil;
    self.dataSource = nil;
    [super dealloc];
}

- (void) volumeChange:(float)newVolume {
    int vol = (int)floor(newVolume * 100);
    self.volume = vol;
    [self sendStatusUpdate];
}

- (void) secondsStepFw:(id)userInfo {
    if (self.playbackState == ACPlaybackStatePlaying) {
        self.currentPlaybackTime = MIN(self.currentPlaybackTime + 1, self.duration);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_UPDATE_OVERLAY" object:self];
    }
}

- (void) seek:(double)position {
    self.playbackState = ACPlaybackStateSeek;
    self.currentPlaybackTime = MAX(0, MIN(position, self.duration));
    [self sendStatusUpdate];
}

- (void) sendStatusUpdate {
    if (self.acManager) {
        // seconds to milliseconds
        double currentPlaybackTimeMilliseconds = self.currentPlaybackTime * 1000;
        [self.acManager.commandEngine sendPlaybackStatusUpdates:currentPlaybackTimeMilliseconds
                                                       duration:self.duration
                                                  playbackState:self.playbackState
                                                          asset:[[self.asset valueForKey:@"id"] lastPathComponent]
                                               selectedSubtitle:self.selectedSubtitle
                                                         volume:self.volume
                                                         target:self.player
                                                       isStream:self.isStream
                                                     deviceType:@"IOS"];
    }
}

- (void) play {
    self.playbackState = ACPlaybackStatePlaying;
    [self sendStatusUpdate];
}

- (void) pause {
    self.playbackState = ACPlaybackStatePaused;
    [self sendStatusUpdate];
}

- (void) setPlayer:(NSString *)newVal {
    if (player) {
        [player release];
        player = nil;
    }
    player = [newVal retain];
    if (player == nil) {
        self.playerOnline = NO;
    }
}

- (NSString *) player {
    return player;
}

- (void)didPlaybackStatusUpdate:(NSNotification *)n {
    self.lastPlaybackStatus = n;
    self.player = [[n userInfo] valueForKey:@"sender"];
    
    NSDictionary *value = [[n userInfo] valueForKey:@"payload"];
    
    self.playbackState = [[value valueForKey:@"playback_state"] intValue];
    self.isStream = [[value valueForKey:@"stream"] boolValue];
    
    self.currentPlaybackTime = [[value valueForKey:@"current_playback_time"] doubleValue] / 1000;
    self.duration = [[value valueForKey:@"duration"] doubleValue] / 1000;
    
    if ([value valueForKey:@"current_playback_time"] != nil && (id)[value valueForKey:@"current_playback_time"] != [NSNull null]) {
        
    }
    if ([value valueForKey:@"duration"] != nil && (id)[value valueForKey:@"duration"] != [NSNull null]) {
        
    }
    NSString* assetId = [value valueForKey:@"asset_id"];
    
    [self updateAsset:assetId];
    
    NSString *deviceName = [[n userInfo] valueForKey:@"device_name"];
    if (deviceName == nil || (id)deviceName == [NSNull null]) {
        deviceName = @"Unknown TV";
    }
    self.pairedDeviceName = deviceName;
    [[NSUserDefaults standardUserDefaults] setValue:self.pairedDeviceName forKey:@"paired_device"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.playerOnline = YES;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PLAYBACK_STATUS_CHANGED" object:self.acManager userInfo:n.userInfo];
}

- (void) setPlayerOnline:(BOOL)newVal {
    BOOL changed = newVal != playerOnline;
    playerOnline = newVal;
    if (changed) {
        switch (newVal) {
            case YES:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_PLAYER_BECAME_ONLINE" object:[ACManager sharedInstance]];
                break;
            case NO:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_PLAYER_BECAME_OFFLINE" object:[ACManager sharedInstance]];
                break;
        }
    }
}

- (BOOL) playerOnline {
    return playerOnline;
}

- (void) terminateControl {
    self.playbackState = ACPlaybackStateTerminated;
    [self sendStatusUpdate];
}

- (void) selectSubtitle:(NSString*)locale isIncoming:(BOOL)incoming {
    if (![self.selectedSubtitle isEqualToString:locale] && !incoming) {
        self.selectedSubtitle = locale;
        [self sendStatusUpdate];
    }
}

-(void) networkServicesReady {
    [self.dataSource networkServicesReady];
}

-(void) reprocessLastPlaybackStatus {
    if (self.lastPlaybackStatus) {
        [self didPlaybackStatusUpdate:self.lastPlaybackStatus];
    }
}

-(void) updateAsset:(NSString*)assetId {
    if (assetId == (id)[NSNull null]) {
        assetId = nil;
    }
    if (!assetId) {
        self.asset = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_ASSET_UPDATED" object:self];
        return;
    }
    if (assetId && self.isStream) {
        self.asset = [self.dataSource channelAssetForAssetId:assetId];
    }
    if (assetId && self.asset && [[self.dataSource idForAsset:self.asset isStream:self.isStream] isEqual:assetId]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_ASSET_UPDATED" object:self];
    } else {
        [self.dataSource assetForId:assetId];
    }
}

- (void) didAssetReceived:(NSDictionary*)lAsset {
    self.asset = lAsset;
    
    self.subtitles = [self.dataSource subtitlesForAsset:self.asset];
    [self updateAsset:[self.dataSource idForAsset:self.asset isStream:self.isStream]];
}

- (void) didAssetChannelsReceived:(NSArray*)channels {
    if (self.isStream && self.asset) {
        self.asset = [self.dataSource channelAssetForAssetId:[self.dataSource idForAsset:self.asset isStream:self.isStream]];
    }
}

@end
