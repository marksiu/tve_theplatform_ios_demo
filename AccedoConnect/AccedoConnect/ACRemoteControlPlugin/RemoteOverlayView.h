//
//  OverlayView.h
//  CMore
//
//  Created by Gabor Bottyan on 8/6/12.
//  Copyright (c) 2012 Meganet-Soft Bt. All rights reserved.
//
#import <MediaPlayer/MediaPlayer.h>
#import "ACStatusRecorder.h"
#import <UIKit/UIKit.h>
//#import "TV4PSubtitleParser.h"
//#import "SubtitleController.h"
//#import "VSRemoteImageView.h"
//#import "SimpleMediaItem.h"
//#import "FilmService.h"
//#import "FontLabel.h"

@protocol RemoteOverlayViewDelegate <NSObject, UIPopoverControllerDelegate>
-(void) closeRemoteOverlayView: (id) sender;
-(void) switchToLocalPlaybackWithPosUpdate:(BOOL)posUpdate;
@end

@interface RemoteOverlayView : UIView<UIPopoverControllerDelegate, ACStatusRecorderOverlay>

-(void) updateLive: (BOOL) _live;

@property (nonatomic, assign) ACStatusRecorder* moviePlayer;
@property (nonatomic, assign) id <RemoteOverlayViewDelegate> delegate;

@property (nonatomic, retain) NSArray* subtitles;

@property (nonatomic, assign) BOOL wasSwipedBack;

@property (retain) UIScrollView* detailPanel;
@property (nonatomic, retain) UILabel* titleLabel;
@property (nonatomic, retain) UILabel* descriptionLabel;

@property (retain, nonatomic) NSString * backgroundImageUrl;
//@property (retain, nonatomic) IBOutlet VSRemoteImageView * backgroundImage;

@property (retain, nonatomic) id bgImageDelegate;

@property (retain) UINavigationBar * navbar;

-(void) resetSubtitleModel;
-(void) subtitleChangedOnPlayer: (NSString*) locale;
-(void) showHud;
-(void) hideHud;

-(void) update: (id) sender;

@end
