//
//  ACDelegate.h
//  CMore
//
//  Created by Csaba Tűz on 2012.10.05..
//
//

#import <Foundation/Foundation.h>
#import "ACManager.h"
//#import "CMOREService.h"

@protocol ACPairingDelegate

-(void) pairOnChannel:(NSString*)channel;
-(void) disconnectAC;

@end

@interface ACDelegate : NSObject<ACPairingDelegate>

@property (retain, nonatomic) ACManager* acManager;

@end
