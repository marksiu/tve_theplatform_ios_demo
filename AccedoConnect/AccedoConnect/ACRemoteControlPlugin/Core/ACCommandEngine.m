//
//  ACCommandEngine.m
//  CMore
//
//  Created by Csaba Tűz on 2012.10.04..
//
//

#import "ACCommandEngine.h"
#import "ACManager.h"
#import "ChannelDelegate.h"

@implementation ACCommandEngine

@synthesize manager;

- (void) processCommand:(id)command payload:(NSDictionary *)value sender:(NSString*)sender deviceType:(NSString*)deviceType deviceName:(NSString *)deviceName {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"AC_%@", command]
                                                        object:self.manager
                                                      userInfo:[NSDictionary dictionaryWithObjectsAndKeys:
                                                                value, @"payload",
                                                                sender, @"sender",
                                                                deviceType, @"device_type",
                                                                deviceName,
                                                      @"device_name", nil] ];
}

- (void) sendPlayWvAsset:(NSDictionary *)wvPlaybackDict
              isStream:(BOOL)isStream
                 asset:(NSString*)assetId
        licenceBaseUri:(NSString*)licenceBaseUri
       initialPosition:(double)initialPosition
      selectedSubtitle:(NSString*)selectedSubtitle
                target:(NSString*)target {
    NSDictionary* d = wvPlaybackDict;
    NSDictionary *item = nil;
    id items = [d valueForKeyPath:@"playback.items.item"];
    if ([items isKindOfClass:[NSArray class]]) {
        NSPredicate *p = [NSPredicate predicateWithFormat:@"mediaFormat = %@", @"wvm"];
        item = [[items filteredArrayUsingPredicate:p] lastObject];
    } else if ([items isKindOfClass:[NSDictionary class]]) {
        item = (NSDictionary *)items;
    }
    NSString* licenceURI = @"";
    NSString* movieURL = @"";
    if (item && [item valueForKeyPath:@"url"] && [item valueForKeyPath:@"license"]) {
        licenceURI = [[item valueForKeyPath:@"license"] objectForKey:@"@uri" ];
        movieURL = [item valueForKeyPath:@"url"];
    }
    
    NSDictionary* toSend = [NSDictionary dictionaryWithObjectsAndKeys:
                            [wvPlaybackDict valueForKeyPath:@"playback.playbackStatus"], @"playbackStatus",
                            movieURL, @"movieURL",
                            licenceURI, @"licenceURI",
                            licenceBaseUri, @"licenceBaseUri",
                            nil];
    if (isStream) {
        initialPosition = 0;
    }
    else {
        //convert to millisecond
        initialPosition *= 1000;
    }
    [self.manager sendMessage:@"PLAY_ASSET" payload:[NSDictionary dictionaryWithObjectsAndKeys:
                                                     toSend, @"wvDict",
                                                     assetId, @"asset_id",
                                                     @(initialPosition), @"initial_position",
                                                     selectedSubtitle, @"selected_subtitle",
                                                     @(isStream), @"stream",
                                                     nil] target:target deviceType:@"IOS" deviceName:[[UIDevice currentDevice] name]];
}

- (void) sendPlayAsset:(NSString* )mediaURL isStream:(BOOL)isStream
                 asset:(NSString*)assetId
       initialPosition:(double)initialPosition
      selectedSubtitle:(NSString*)selectedSubtitle
                target:(NSString*)target {
    NSString* movieURL = mediaURL;
    
    NSDictionary* toSend = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"OK", @"playbackStatus",
                            movieURL, @"movieURL",
                            @"", @"licenceURI",
                            @"http://jboss.cnow.se", @"licenceBaseUri",
                            nil];
    if (isStream) {
        initialPosition = 0;
    }
    else {
        //convert to millisecond
        initialPosition *= 1000;
    }
    [self.manager sendMessage:@"PLAY_ASSET" payload:[NSDictionary dictionaryWithObjectsAndKeys:
                                                     toSend, @"wvDict",
                                                     assetId, @"asset_id",
                                                     @(initialPosition), @"initial_position",
                                                     selectedSubtitle, @"selected_subtitle",
                                                     @(isStream), @"stream",
                                                     nil] target:target deviceType:@"IOS" deviceName:[[UIDevice currentDevice] name]];
}

/*- (void) sendPlaybackStartedToTarget:(NSString*)target {
    [self.manager sendMessage:@"PLAYBACK_STARTED" payload:nil target:target];
}*/

- (void) sendPlaybackTerminatedToTarget:(NSString*)target deviceType:(NSString*)deviceType {
    [self.manager sendMessage:@"PLAYBACK_ENDED" payload:nil target:target deviceType:deviceType deviceName:[[UIDevice currentDevice] name]];
}

- (void) sendKeyEvent:(ACKeyCode)key toTarget:(NSString*)target {
    [self.manager sendMessage:@"KEY_EVENT" payload:[NSDictionary dictionaryWithObjectsAndKeys:@(key), @"key", nil] target:target deviceType:@"IOS" deviceName:[[UIDevice currentDevice] name]];
}

- (void) sendIdentPairingPersistent:(BOOL)persistent operatorName:(NSString*)operatorName userId:(NSString*)userId country:(NSString*)country{
    
    [self.manager sendMessage:@"IDENT"
                      payload:[NSDictionary dictionaryWithObjectsAndKeys:       persistent ? @"PERSISTENT" : @"WITH_CODE", @"paired",
                               operatorName, @"operator_name",
                               userId, @"userId",
                               country, @"country",
                               nil] target:nil
                   deviceType:@"IOS"
                   deviceName:[[UIDevice currentDevice] name]];
}

- (void) sendPlaybackStatusUpdates:(double)currentPlaybackTime duration:(double)duration
                     playbackState:(MPMoviePlaybackState)state asset:(NSString*)assetId
                  selectedSubtitle:(NSString*)selectedSubtitle volume:(int)volume
                            target:target
                          isStream:(BOOL)isStream
                        deviceType:(NSString*)deviceType {
    if (isnan(currentPlaybackTime)) {
        currentPlaybackTime = 0;
    }
    if (isnan(duration)) {
        duration = 0;
    }
    if (isStream) {
        currentPlaybackTime = 0;
        duration = 0;
        selectedSubtitle = @"";
    }
    [self.manager sendMessage:@"PLAYBACK_STATUS" payload:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          @(currentPlaybackTime), @"current_playback_time",
                                                          @(duration), @"duration",
                                                          selectedSubtitle, @"selected_subtitle",
                                                          @(volume), @"volume",
                                                          @(state), @"playback_state",
                                                          assetId, @"asset_id",
                                                          @(isStream),@"stream",
                                                          nil] target:target deviceType:deviceType deviceName:[[UIDevice currentDevice] name]];
}

@end
