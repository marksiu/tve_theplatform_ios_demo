//
//  ACManager.m
//  CMore
//
//  Created by Csaba Tűz on 2012.09.28..
//
//
#import "ACManager.h"
#import "ACCommandEngine.h"
#import "ACStatusRecorder.h"
#import "Connect.h"

#import "zlib.h"
// don't forget to add libz.1.2.x.dylib into your project

#define DISPATCH_SECONDS_FROM_NOW(s) dispatch_time(DISPATCH_TIME_NOW, (s) * NSEC_PER_SEC)

@interface  ACManager() {
    BOOL persistent;
    
}

@property (retain, nonatomic) Connect* connectObj;
@property (retain, nonatomic) Channel* channel;

@end

@implementation ACManager


@synthesize appKey;
@synthesize server;

@synthesize isPaired;

+ (ACManager *)sharedInstance {
    static dispatch_once_t pred;
    static ACManager* _sharedInstance = nil;
    dispatch_once(&pred, ^{
        _sharedInstance = [[ACManager alloc] init];
    });
    
    return _sharedInstance;
}

#define CHUNK_SIZE 1024

@synthesize commandEngine, statusRecorder;

@synthesize connectObj, channel;
@synthesize targets;

@synthesize messageBuffer;

- (id) init {
    self = [super init];
    if (self) {
        persistent = YES;
        self.commandEngine = [[[ACCommandEngine alloc] init] autorelease];
        self.commandEngine.manager = self;
        
        self.messageBuffer = [[[NSMutableDictionary alloc] init] autorelease];
    }
    return self;
}

- (void)connectWithAPIKey:(NSString *)_apiKey server:(NSString *)_server  {
    self.appKey = _apiKey;
    self.server = _server;
    
    if (![self isConnected]) {
        self.connectObj = [[[Connect alloc] initWithDelegate:self andChannelDelegate:self] autorelease];
        
        self.statusRecorder = [[[ACStatusRecorder alloc] initWithManager:self] autorelease];
        
        [self.connectObj connect:_apiKey server:_server];
    }
}

- (void)disconnect {
    [self.messageBuffer removeAllObjects];
    
    if (self.channel) {
        [self.channel leave];
    }
    
    self.connectObj= nil;
    
    self.statusRecorder = nil;
    
    self.channel = nil;
}

- (void) unpair {
    [self.connectObj unpair];
}

- (BOOL) isConnected {
    return self.connectObj != nil;
}

- (void) dealloc {
    [self disconnect];
    
    self.connectObj = nil;
    self.commandEngine.manager = nil;
    self.commandEngine = nil;
    
    [super dealloc];
}

// Equally, we don't want to generate multiple copies of the singleton.
- (id)copyWithZone:(NSZone *)zone {
    return self;
}

// Once again - do nothing, as we don't have a retain counter for this object.
- (id)retain {
    return self;
}

// Replace the retain counter so we can never release this object.
- (NSUInteger)retainCount {
    return NSUIntegerMax;
}

// This function is empty, as we don't want to let the user release this object.
- (oneway void)release {
    
}

//Do nothing, other than return the shared instance - as this is expected from autorelease.
- (id)autorelease {
    return self;
}

- (void) processMessageChunk:(NSString*)uuid data:(NSDictionary *)data sender:(NSString*)sender deviceType:(NSString*)deviceType deviceName:(NSString *)deviceName {
    NSString* command = [[uuid componentsSeparatedByString:@"|"] objectAtIndex:1];
    
    if (data == nil) {
        data = [NSDictionary dictionary];
    }
    if (data) {
        [self.commandEngine processCommand:command payload:data sender:sender deviceType:deviceType deviceName:deviceName];
    }
}

- (void) sendRawMessage:(NSDictionary*)rawMessage {
    [self.channel sendMessage:rawMessage];
}

- (void) sendChunk:(NSString*)command payload:(NSDictionary *)payload target:(NSString*)target
        deviceType:(NSString*)deviceType
        deviceName:(NSString*)deviceName {
    NSDictionary* rawMessage = [NSDictionary dictionaryWithObjectsAndKeys:
                                command, @"command",
                                payload ? payload : @"", @"payload",
                                deviceType, @"device_type",
                                deviceName, @"device_name",
                                [Connect getUUID], @"sender",
                                target ? target : @"", @"target",
                                nil];
    [self sendRawMessage:rawMessage];
}

- (void) sendMessage:(NSString*)command payload:(id)object target:(NSString*)target deviceType:(NSString*)deviceType deviceName:(NSString*)deviceName {
    NSError* error;
    NSData* data = nil;
    
    if (object) {
        data = [NSJSONSerialization dataWithJSONObject:object options:0 error:&error];
    }
    
    NSDictionary* payload = nil;
    if (data) {
        payload = object;
    }
    [self sendChunk:command payload:payload target:target deviceType:deviceType deviceName:[[UIDevice currentDevice] name]];
}

-(void) pairDevice:(NSString*)pairingCode {
    if (!self.isConnected) {
        return;
    }
    persistent = NO;
    [self.connectObj pair:pairingCode];
}

#pragma mark - ConnectDelegate methods

-(void) didConnect:(Channel *)_channel code:(ResponseCode) code {
    Channel * oldChannel = self.channel;
    self.channel = _channel;
    if (self.channel == nil && isPaired) {
        if (oldChannel) {
            [oldChannel leave];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_HOST_UNPAIRED"
                                                            object:self];
    }
    isPaired = NO;
    switch(code) {
        case ResponseCodeOk:
            if (channel) {
                isPaired = YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_PAIRED"
                                                                    object:self];
                
            }
            break;
        case ResponseCodeAuthorizationError:
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"paired_device"];
            self.statusRecorder.pairedDeviceName = nil;
            self.statusRecorder.player = nil;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_CONNECTED"
                                                                object:self];
            break;
        case ResponseCodeGenericError:
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"paired_device"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_CONNECTION_FAILED"
                                                                object:self];
            break;
    }
}

-(void) didPair:(Channel*)_channel code:(ResponseCode) code {
    self.channel = _channel;
    if (self.channel == nil && isPaired) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_HOST_UNPAIRED"
                                                            object:self];
    }
    isPaired = NO;
    switch(code) {
        case ResponseCodeOk:
            if (channel) {
                isPaired = YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_PAIRED"
                                                                    object:self];
                
            }
            break;
        case ResponseCodeAuthorizationError:
            NSLog(@"Invalid AC COnnect auth data is provided.");
            break;
        case ResponseCodeGenericError:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_PAIRING_FAILED"
                                                                object:self];
            break;
    }
}

-(void) didCheckPairing:(Channel *)_channel code:(ResponseCode) code {
    Channel *oldChannel = self.channel;
    self.channel = _channel;
    if (isPaired && self.channel == nil) {
        if (oldChannel) {
            [oldChannel leave];
        }
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"paired_device"];
        self.statusRecorder.pairedDeviceName = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_HOST_UNPAIRED"
                                                            object:self];
        self.statusRecorder.player = nil;
        isPaired = NO;
    }
}

-(void) didUnPair:(Channel*) channel code:(ResponseCode) code {
    if (self.channel) {
        [self.channel leave];
        self.channel = nil;
    }
    isPaired = NO;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"paired_device"];
    self.statusRecorder.pairedDeviceName = nil;
    self.statusRecorder.player = nil;
    switch(code) {
        case ResponseCodeOk:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_UNPAIRED"
                                                                object:self];
            break;
        case ResponseCodeAuthorizationError:
            NSLog(@"Invalid AC COnnect auth data is provided.");
            break;
        case ResponseCodeGenericError:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_UNPAIRED"
                                                                object:self];
            break;
    }
}

#pragma mark - ChannelDelegate method

- (void) didRecieveMessage:(id) message {
    if (![message isKindOfClass:[NSDictionary class]]) return;
    NSString* command = [message valueForKey:@"command"];
    NSString* target = [message valueForKey:@"target"];
    
    if ([target length] > 0 && ![target isEqual:[Connect getUUID]]) {
        return;
    }
    
    NSString* sender = [message valueForKey:@"sender"];
    NSString* uuid = [sender stringByAppendingFormat:@"|%@", command];
    NSDictionary* data = [message valueForKey:@"payload"];
    NSString* deviceType = [message valueForKey:@"device_type"];
    NSString* deviceName = [message valueForKey:@"device_name"];
    
    [self processMessageChunk:uuid data:data sender:sender deviceType:deviceType
                   deviceName:deviceName];
};

- (void) didFailSendingMessageWithError: (NSError*) error {
    NSLog(@"ERROR: %@", error);
}

- (void) didConnect {
    NSLog(@"Channel connected");
    [self.commandEngine sendIdentPairingPersistent:persistent operatorName:self.statusRecorder.operatorName userId:self.statusRecorder.userId country:self.statusRecorder.country];
}

- (void) didReconnect {
    NSLog(@"Channel reconnecting");
}

- (void) didDisconnect {
    NSLog(@"Channel disconnected");
}

- (void) didFailConnectingWithError: (NSError*) error {
    NSLog(@"Channel connecting FAILED");
}

- (void) presenceChanged: (NSDictionary*) presence {
    NSLog(@"Presence change: %@", presence);
    NSLog(@"%@", presence);
    NSString* otherUUID = [presence valueForKey:@"uuid"];
    NSString* action = [presence valueForKey:@"action"];
    if ([otherUUID isEqualToString:[Connect getUUID]]) return;
    if ([action isEqualToString:@"join"]) {
        NSDictionary* userInfo = [NSDictionary dictionaryWithObjectsAndKeys:otherUUID, @"uuid", nil];
        if ([otherUUID isEqual:self.statusRecorder.player]) {
            self.statusRecorder.playerOnline = YES;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_CLIENT_CONNECTED" object:self userInfo:userInfo];
        [self.commandEngine sendIdentPairingPersistent:persistent operatorName:self.statusRecorder.operatorName userId:self.statusRecorder.userId country:self.statusRecorder.country];
    } else if ([action isEqualToString:@"leave"] || [action isEqualToString:@"timeout"]) {
        if ([otherUUID isEqual:self.statusRecorder.player]) {
            self.statusRecorder.playerOnline = NO;
        }
        NSDictionary* userInfo = [NSDictionary dictionaryWithObjectsAndKeys:otherUUID, @"uuid", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AC_CLIENT_DISCONNECTED" object:self userInfo:userInfo];
        [self.connectObj checkPairing];
    }
}

- (void)didRecieveNotification {
    NSLog(@"Notification received");
}

@end
