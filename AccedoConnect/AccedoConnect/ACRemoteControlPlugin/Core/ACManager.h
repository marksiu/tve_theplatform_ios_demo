//
//  ACManager.h
//  ;
//
//  Created by Csaba Tűz on 2012.09.28..
//
//

#import <Foundation/Foundation.h>
#import "ACCommandEngine.h"

#import "Connect.h"


@class ACStatusRecorder;

@interface ACManager : NSObject<ConnectDelegate, ChannelDelegate>

@property (retain, nonatomic) NSString *appKey;
@property (retain, nonatomic) NSString *server;

+ (ACManager*)sharedInstance;

@property (nonatomic, retain) ACCommandEngine* commandEngine;

@property (retain, nonatomic) NSDictionary* targets;

@property (retain, nonatomic) NSMutableDictionary* messageBuffer;

@property (retain, nonatomic) ACStatusRecorder* statusRecorder;

@property (assign) BOOL isPaired;

- (id) init;

- (void)pairDevice:(NSString*)pairingCode;

- (void)connectWithAPIKey:(NSString *)apiKey server:(NSString *)server;
- (void)disconnect;
- (void)unpair;

- (BOOL)isConnected;

- (void)sendMessage:(NSString*)command payload:(id)object target:(NSString*)target deviceType:(NSString*)deviceType deviceName:(NSString*)deviceName;

@end
