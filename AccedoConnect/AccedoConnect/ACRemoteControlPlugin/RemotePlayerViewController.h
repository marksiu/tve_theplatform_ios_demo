//
//  RemotePlayerViewController.h
//  CMore
//
//  Created by Csaba Tűz on 2012.10.19..
//
//

#import <UIKit/UIKit.h>
#import "ACStatusRecorder.h"
#import "RemoteOverlayView.h"

@interface RemotePlayerViewController : UIViewController

@property (assign) ACStatusRecorder* statusRecorder;
@property (retain, nonatomic) RemoteOverlayView * overlayView;

@property (assign) BOOL shouldShowCloseButton;

@end
