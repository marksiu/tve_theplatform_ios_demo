//
//  SmartTVViewController.m
//  CMore
//
//  Created by Csaba Tűz on 2012.10.04..
//
//

#import "SmartTVViewController.h"
#import "ACManager.h"
#import "ACStatusRecorder.h"

//#import "AppDelegate.h"

//#import "Common.h"

@interface SmartTVViewController () {
    NSTimer* failTimer;
}

@end

@implementation SmartTVViewController

@synthesize remotePlayerVC, pairingViewController;

BOOL animating;

- (id)initWithPlayerController: (UIViewController * ) vc {

    self = [super init];
    if (self) {
        
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(didRemotePlaybackStart:)
         name:@"REMOTE_PLAYBACK_STARTED"
         object:[ACManager sharedInstance]];
        
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(didRemotePlaybackEnd:)
         name:@"REMOTE_PLAYBACK_ENDED"
         object:[ACManager sharedInstance]];
        
        self.remotePlayerVC = vc;  //[[[RemotePlayerViewController alloc] init] autorelease];
        [self addChildViewController:self.remotePlayerVC];
        
        [self hideRemotePlayer];
        
        [vc performSelector:@selector(playbackStatusUpdated:)
                 withObject:[ACManager sharedInstance].statusRecorder.lastPlaybackStatus];
        //self.remotePlayerVC.shouldShowCloseButton = NO;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"AccedoConnect" ofType:@"bundle"];
        NSBundle *bundle = [NSBundle bundleWithPath:filePath];
        self.pairingViewController = [[[ACPairingViewController alloc] initWithNibName:@"FullscreenACPairingView" bundle:bundle] autorelease];
        [self addChildViewController:self.pairingViewController];
    }
    return self;
}

- (void)dealloc {
    [self.remotePlayerVC removeFromParentViewController];
    self.remotePlayerVC = nil;
    [self.pairingViewController removeFromParentViewController];
    self.pairingViewController = nil;
    
    [super dealloc];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.pairingViewController.view];
    self.pairingViewController.view.frame = self.view.frame;
    
    [self.view addSubview: self.remotePlayerVC.view];
    self.remotePlayerVC.view.frame = self.view.frame;
   // self.navigationItem.rightBarButtonItems = [NSArray arrayWithObject:[self.navigationItem.rightBarButtonItems objectAtIndex:0]];
}

- (void) showRemotePlayer {
    self.remotePlayerVC.view.hidden = NO;
    self.remotePlayerVC.view.alpha = 1.0;
}

- (void) hideRemotePlayer {
    self.remotePlayerVC.view.hidden = YES;
    self.remotePlayerVC.view.alpha = 0.0;
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                          duration:(NSTimeInterval)duration {

    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    //[self rearrangeSubviewsForOrientation:toInterfaceOrientation];
}

#pragma mark - RemoteOverlayViewDelegate

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self rearrangeSubviewsForOrientation:self.interfaceOrientation];
}

-(void) rearrangeSubviewsForOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    self.remotePlayerVC.view.frame = self.view.frame;
    self.pairingViewController.view.frame = self.view.frame;
}

-(void) closeRemoteOverlayView: (id) sender {
    if (!animating) {
        animating = YES;
        self.remotePlayerVC.view.hidden = YES;
        [UIView animateWithDuration:1 animations:^{
            self.remotePlayerVC.view.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.remotePlayerVC.view.hidden = YES;
            animating = NO;
        }];
    }
}

-(void) switchToLocalPlaybackWithPosUpdate:(BOOL)posUpdate {
    //[self playAsset:self.remotePlayerVC.statusRecorder.asset isStream:self.remotePlayerVC.statusRecorder.isStream isAC:NO initialPosition:self.remotePlayerVC.statusRecorder.currentPlaybackTime fullScreen:YES containerView:nil];
}

-(void) addPopupView:(NSString *)popupString {
    
}

- (void) didRemotePlaybackStart:(NSNotification*)n {
    
    //NSLog(@"**************************************PLAYBACK START");
    if (!animating) {
        animating = YES;
        self.remotePlayerVC.view.hidden = NO;
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        [UIView animateWithDuration:1 animations:^{
            self.remotePlayerVC.view.alpha = 1.0;
        } completion:^(BOOL finished) {
            animating = NO;
        }];
    } else {
        self.remotePlayerVC.view.hidden = NO;
        self.remotePlayerVC.view.alpha = 1.0;
        self.remotePlayerVC.navigationController.navigationBarHidden = YES;
    }
}

- (void) didRemotePlaybackEnd:(NSNotification*)n {
    
    //NSLog(@"**************************************PLAYBACK END");
    if (!animating) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        animating = YES;
        [UIView animateWithDuration:1 animations:^{
            self.remotePlayerVC.view.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.remotePlayerVC.view.hidden = YES;
            animating = NO;
        }];
    }
}

@end
