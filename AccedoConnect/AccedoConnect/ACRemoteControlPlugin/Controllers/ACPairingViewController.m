//
//  ACPairingPopupViewController.m
//  CMore
//
//  Created by Csaba Tűz on 2012.10.19..
//
//

#import "ACPairingViewController.h"
#import "ACStatusRecorder.h"

//#import "AppDelegate.h"

@interface ACPairingViewController () {
    NSTimer* failTimer;
    //ZFont * howToFont;
    BOOL fullscreen;
    
}

//@property (nonatomic, retain) ZFont* howToFont;

@end


@implementation ACPairingViewController

@synthesize backgroundImage;

@synthesize txtInputCode;
@dynamic connectedImageHidden;

//@synthesize howToFont;
#define NSLocalized(key, bund) [bund localizedStringForKey:(key) value:@"" table:nil]
#define AC_CONNECT_TIMEOUT 10
#define INFO_TOP_SHADOW_TAG 200

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([@"FullscreenACPairingView" isEqual:nibNameOrNil]) {
            fullscreen = YES;
            //self.howToFont = [[FontManager sharedManager] zFontWithName:@"ProximaNova-Reg" pointSize:16.5];
        } else if ([@"ACPairingViewController" isEqual:nibNameOrNil]) {
            fullscreen = NO;
            //self.howToFont = [[FontManager sharedManager] zFontWithName:@"ProximaNova-Reg" pointSize:11.5];
        }
        
        ACManager* acManager = [ACManager sharedInstance];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didACPaired) name:@"AC_PAIRED" object:acManager];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didACHostUnpaired) name:@"AC_HOST_UNPAIRED" object:acManager];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didACConnectionFailed) name:@"AC_CONNECTION_FAILED" object:acManager];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didACDisconnected) name:@"AC_UNPAIRED" object:acManager];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didACPairingFailed) name:@"AC_PAIRING_FAILED" object:acManager];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didACUnPairingFailed) name:@"AC_UNPAIRING_FAILED" object:acManager];
    }
    return self;
}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                          duration:(NSTimeInterval)duration {
     
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self rearrangeSubviewsForOrientation:toInterfaceOrientation];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"AccedoConnect" ofType:@"bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:filePath];

    lblHowToStep1.text = NSLocalized(@"lblHowToStep1", bundle);
    lblHowToStep2.text = NSLocalized(@"lblHowToStep2", bundle);
    lblHowToStep3.text = NSLocalized(@"lblHowToStep3", bundle);
    lblHowToStep4.text = NSLocalized(@"lblHowToStep4", bundle);
    lblHowToStep5.text = NSLocalized(@"lblHowToStep5", bundle);

    lblHowToStep1.hidden = YES;
    lblHowToStep2.hidden = YES;
    lblHowToStep3.hidden = YES;
    lblHowToStep4.hidden = YES;
    lblHowToStep5.hidden = YES;

    lblInputKode.text  = NSLocalized(@"lblInputCode", bundle);
    lblOrLabel.text    = NSLocalized(@"lblHowToOr", bundle);
    [self initPairingUI];
    
    
     NSString * closeString = @"Close";
     NSString * loginString = NSLocalized(@"Pair your device", bundle);
     
     lblInputHeader.text = @"Pair device using code";
     lblInputKode.text = @"Code";
     txtInputCode.placeholder = @"Enter code here";
     
     self.navigationItem.title = loginString;
     self.navigationItem.rightBarButtonItem  = [[[UIBarButtonItem alloc] initWithTitle:closeString style:UIBarButtonItemStylePlain
     target:self
     action:@selector(close:)] autorelease];
    // Do any additional setup after loading the view from its nib.
    
}

- (BOOL)connectedImageHidden {
    return connectedImageOffline.hidden && connectedImageOnline.hidden;
}

- (void)setConnectedImageHidden:(BOOL)hidden {
    BOOL isOnline = [ACManager sharedInstance].statusRecorder.playerOnline;
    if (hidden) {
        if (!connectedImageOffline.hidden) {
            [UIView animateWithDuration:0.5 animations:^{
                connectedImageOffline.alpha = 0;
            } completion:^(BOOL finished) {
                connectedImageOffline.hidden = YES;
            }];
        }
        if (!connectedImageOnline.hidden) {
            [UIView animateWithDuration:0.5 animations:^{
                connectedImageOnline.alpha = 0;
            } completion:^(BOOL finished) {
                connectedImageOnline.hidden = YES;
            }];
        }
    } else {
        if (isOnline) {
            if (!connectedImageOffline.hidden) {
                [UIView animateWithDuration:0.5 animations:^{
                    connectedImageOffline.alpha = 0.0;
                } completion:^(BOOL finished) {
                    connectedImageOffline.hidden = YES;
                }];
            }
            if (connectedImageOnline.hidden) {
                connectedImageOnline.hidden = NO;
                [UIView animateWithDuration:0.5 animations:^{
                    connectedImageOnline.alpha = 1.0;
                } completion:^(BOOL finished) {
                }];
            }
        } else {
            if (!connectedImageOnline.hidden) {
                [UIView animateWithDuration:0.5 animations:^{
                    connectedImageOnline.alpha = 0.0;
                } completion:^(BOOL finished) {
                    connectedImageOnline.hidden = YES;
                }];
            }
            if (connectedImageOffline.hidden) {
                connectedImageOffline.hidden = NO;
                [UIView animateWithDuration:0.5 animations:^{
                    connectedImageOffline.alpha = 1.0;
                } completion:^(BOOL finished) {
                }];
            }
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear: animated];

    
    [self rearrangeSubviewsForOrientation:self.interfaceOrientation];
    
    if ([[ACManager sharedInstance] isPaired]) {
        self.connectedImageHidden = NO;
        connectIndicator.hidden = YES;
        [connectIndicator stopAnimating];
        disconnectButton.hidden = NO;
        readQRButton.hidden = YES;
        
        connectImage.center = connectedImageOnline.center;
        connectButton.hidden = YES;
        inputCodeView.hidden = YES;
        readQRButton.hidden = YES;\
    }
        
    NSString *acCode = [[UIApplication sharedApplication].delegate acCode];
    if (acCode) {
        txtInputCode.text = acCode;
        [self didConnectButtonSelected:acCode];
        [[UIApplication sharedApplication].delegate setAcCode:nil];
    }
}

- (void) rearrangeSubviewsForOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return;

    if (fullscreen) {
        UIView* shadow = [self.view viewWithTag:INFO_TOP_SHADOW_TAG];
        shadow.frame = CGRectMake(0, infoView.frame.origin.y - 14, infoView.frame.size.width, 14);
        CGPoint center = connectImage.center;
        center.x = connectImage.superview.frame.size.width / 2;
        connectImage.center = center;
        
        if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
            viewNotPaired.hidden = NO;
            viewNotPaired_Portrait.hidden = YES;
            
            lblHowToStep1.frame = CGRectMake(33, 146, 154, 134);
            lblHowToStep2.frame = CGRectMake(233, 146, 154, 134);
            lblHowToStep3.frame = CGRectMake(433, 146, 154, 134);
            lblHowToStep4.frame = CGRectMake(633, 146, 154, 134);
            lblHowToStep5.frame = CGRectMake(833, 146, 154, 134);
            
            lblOrLabel.frame = CGRectMake(378, 84, 64, 25);
            //        ZFont * font = [[FontManager sharedManager] zFontWithName:@"ProximaNova-Reg" pointSize:16.5];
            //        [lblOrLabel setZFont:font];
        } else {
            
            lblHowToStep1.frame = CGRectMake(21, 164, 115, 134);
            lblHowToStep2.frame = CGRectMake(174, 164, 115, 134);
            lblHowToStep3.frame = CGRectMake(327, 164, 115, 134);
            lblHowToStep4.frame = CGRectMake(480, 164, 115, 134);
            lblHowToStep5.frame = CGRectMake(633, 164, 115, 134);
            
            lblOrLabel.frame = CGRectMake(282, 90, 42, 18);
            //        ZFont * font = [[FontManager sharedManager] zFontWithName:@"ProximaNova-Reg" pointSize:12.375];
            //        [lblOrLabel setZFont:font];
            
            viewNotPaired.hidden = YES;
            viewNotPaired_Portrait.hidden = NO;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) didReadQRButtonClicked {
    if (!fullscreen) {
    [self dismissViewControllerAnimated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"QRBUTTONCLICKED" object:self userInfo:@{@"fullscreen": @(fullscreen)}];
        });
    }];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"QRBUTTONCLICKED" object:self userInfo:@{@"fullscreen": @(fullscreen)}];
    }
}

#pragma mark - UITextFieldDelegate

- (IBAction) didConnectButtonSelected:(id)sender {
    [txtInputCode resignFirstResponder];
    
    if (txtInputCode.text.length ==  0) {
        return;
    }
    
    failTimer = [NSTimer scheduledTimerWithTimeInterval:AC_CONNECT_TIMEOUT target:self selector:@selector(didACConnectionFailed) userInfo:nil repeats:NO];
    [UIView animateWithDuration:0.5
                     animations:^{
                         connectImage.center = CGPointMake(connectImage.center.x, connectedImageOnline.center.y);
                         connectButton.hidden = YES;
                         inputCodeView.hidden = YES;
                         readQRButton.hidden = YES;
                     }
                     completion:^(BOOL finished) {
                         connectIndicator.hidden = NO;
                         [connectIndicator startAnimating];
                         [[ACManager sharedInstance] pairDevice:txtInputCode.text];
                     }];
}

- (IBAction) didDisconnectButtonSelected:(id)sender {
    [[ACManager sharedInstance] unpair];
}


- (void) initPairingUI {
    connectIndicator.hidden = YES;
    disconnectButton.hidden = YES;
    readQRButton.hidden = NO;
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"AccedoConnect" ofType:@"bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:filePath];
    
    lblHowToStep1.text = NSLocalized(@"lblHowToStep1", bundle);
    
    [connectButton setTitle:NSLocalized(@"lblConnect", bundle) forState:UIControlStateNormal];
    [disconnectButton setTitle:NSLocalized(@"lblDisconnect", bundle) forState:UIControlStateNormal];
}

- (void)didACPairingFailed {
    [self didACConnectionFailed];
}

- (void)didACUnPairingFailed {
    NSLog(@"didACUnPairingFailed called... Very very strange.");
}

- (void) didACPaired {
    if (failTimer) {
        [failTimer invalidate];
        failTimer = nil;
    }
    
    connectButton.hidden = YES;
    inputCodeView.hidden = YES;
    readQRButton.hidden = YES;
    connectIndicator.hidden = NO;
    self.connectedImageHidden = NO;
    [UIView animateWithDuration:0.5
                     animations:^{
                         connectIndicator.hidden = YES;
                         [connectIndicator stopAnimating];
                         disconnectButton.hidden = NO;
                         readQRButton.hidden = YES;
                     }
                     completion:^(BOOL finished) {
                         [self close:self.navigationItem.rightBarButtonItem];
                     }];
}

- (void)didACHostUnpaired {
    if (failTimer) {
        [failTimer invalidate];
        failTimer = nil;
    }
    [connectIndicator stopAnimating];
    
    self.connectedImageHidden = YES;
    [UIView animateWithDuration:0.5
                     animations:^{
                         connectImage.center = CGPointMake(connectImage.center.x, connectedImageOnline.center.y - 65);
                     }
                     completion:^(BOOL finished) {
                         connectButton.hidden = NO;
                         connectIndicator.hidden = YES;
                         disconnectButton.hidden = YES;
                         inputCodeView.hidden = NO;
                         readQRButton.hidden = NO;
                     }];
}

- (void) didACConnectionFailed {
    if (failTimer) {
        [failTimer invalidate];
        failTimer = nil;
    }
    [connectIndicator stopAnimating];
    self.connectedImageHidden = YES;
    [UIView animateWithDuration:0.5
                     animations:^{
                         connectImage.center = CGPointMake(connectImage.center.x, connectedImageOnline.center.y - 65);
                     }
                     completion:^(BOOL finished) {
                         connectButton.hidden = NO;
                         connectIndicator.hidden = YES;
                         inputCodeView.hidden = NO;
                         readQRButton.hidden = NO;
                     }];
    //  [CMoreViewController error:[[LocalizationSystem sharedLocalSystem] locStringForKey:@"msgACFailed"]];
}

- (void) didACDisconnected {
    txtInputCode.text = @"";
    if (failTimer) {
        [failTimer invalidate];
        failTimer = nil;
    }
    self.connectedImageHidden = YES;
    [UIView animateWithDuration:0.5
                     animations:^{
                         connectImage.center = CGPointMake(connectImage.center.x, connectedImageOnline.center.y - 65);
                     }
                     completion:^(BOOL finished) {
                         inputCodeView.hidden = NO;
                         connectButton.hidden = NO;
                         connectIndicator.hidden = YES;
                         disconnectButton.hidden = YES;
                         readQRButton.hidden = NO;
                     }];
}

-(void) close:(id) sender {
    
    
    if (sender) {
        if ([ACManager sharedInstance].isPaired) {
            if (self.successBlock) {
                self.successBlock();
                self.successBlock = nil;
            }
        } else {
            if (self.failureBlock) {
                self.failureBlock();
                self.failureBlock = nil;
            }
        }
    }
    
    [self.parentViewController dismissModalViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    //return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    return YES;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField.text.length > 0) {
        [self didConnectButtonSelected:textField];
        [textField resignFirstResponder];
        return YES;
    }
    return YES;
}

@end
