//
//  SmartTVViewController.h
//  CMore
//
//  Created by Csaba Tűz on 2012.10.04..
//
//

#import <UIKit/UIKit.h>
//#import "CMoreViewController.h"

//#import "FilmService.h"
//#import "VSRemoteImageView.h"
//#import "FontLabel.h"
//#import "FontLabelStringDrawing.h"
//#import "FontManager.h"
//#import "ZAttributedString.h"
#import "ACManager.h"
#import "ACPairingViewController.h"

@protocol RemoteOverlayViewDelegate <NSObject, UIPopoverControllerDelegate>
-(void) closeRemoteOverlayView: (id) sender;
-(void) switchToLocalPlaybackWithPosUpdate:(BOOL)posUpdate;
-(void) addPopupView:(NSString *)popupString;
@end

@interface SmartTVViewController :  UIViewController < RemoteOverlayViewDelegate>

@property (nonatomic, retain) ACPairingViewController* pairingViewController;

@property (nonatomic, retain) UIViewController* remotePlayerVC;

- (void) showRemotePlayer;
- (void) hideRemotePlayer;
- (id)initWithPlayerController: (UIViewController * ) vc;

@end
