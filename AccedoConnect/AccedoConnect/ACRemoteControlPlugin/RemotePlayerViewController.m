//
//  RemotePlayerViewController.m
//  CMore
//
//  Created by Csaba Tűz on 2012.10.19..
//
//

#import "RemotePlayerViewController.h"

//#import "AppDelegate.h"


static inline BOOL IsEmpty(id object) {
    return object == nil || (object == [NSNull null]);
}

@interface RemotePlayerViewController () {
    BOOL playbackStarted;
    BOOL playbackRunning;
}

@end




@implementation RemotePlayerViewController

@synthesize overlayView;
@synthesize statusRecorder;

@synthesize shouldShowCloseButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self customInit];
    }
    return self;
}

- (id) init {
    self = [super init];
    if (self) {
        [self customInit];
    }
    return self;
}

- (void) customInit {
    self.statusRecorder = [[ACManager sharedInstance] statusRecorder];
    self.shouldShowCloseButton = YES;
    playbackRunning = NO;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPlaybackStatusUpdate:) name:@"AC_PLAYBACK_STATUS" object:[UIApplication sharedApplication].delegate];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didClientDisconnected:) name:@"AC_CLIENT_DISCONNECTED" object:[UIApplication sharedApplication].delegate];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stateUpdate:) name:@"AC_UPDATE_OVERLAY" object:self.statusRecorder];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(assetUpdated:) name:@"AC_ASSET_UPDATED" object:self.statusRecorder];
}

- (void)dealloc {
    self.statusRecorder = nil;
      [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.overlayView = [[[RemoteOverlayView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)] autorelease];
    
    self.overlayView.bgImageDelegate = self;
    [self.view addSubview:self.overlayView];
    [self.overlayView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    
    self.overlayView.moviePlayer = self.statusRecorder;
   // self.overlayView.backgroundImage.delegate = self;
    
#ifdef BUILD_CMORE
    NSString* fontName = @"nettoot.ttf";
    NSString* titleFont = @"nettootbold.ttf";
#endif
#ifdef BUILD_FILMNET
    NSString* fontName = @"ProximaNova-Reg.ttf";
    NSString* titleFont = @"ProximaNova-Bold.ttf";
#endif
    
   //ZFont * fontTitleYear = [[FontManager sharedManager] zFontWithName:titleFont pointSize:22];
    //ZFont * fontDescription = [[FontManager sharedManager] zFontWithName:fontName pointSize:16.5];
    
    //[self.overlayView.titleLabel setZFont:fontTitleYear];
    //[self.overlayView.descriptionLabel setZFont:fontDescription];
    
    self.view.alpha = 0.0;
    self.view.hidden = YES;
    
    if (!self.shouldShowCloseButton) {
        [self.overlayView.navbar topItem].leftBarButtonItem = nil;
        [self.overlayView.navbar topItem].hidesBackButton = YES;
    }
    
	// Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.overlayView layoutSubviews];
}

- (void)viewWillUnload {
  //self.overlayView.backgroundImage.delegate = nil;
 // [self.overlayView.backgroundImage setImageUrl:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didClientDisconnected:(NSNotification*)notification {
    NSString* uuid = [notification.userInfo valueForKey:@"uuid"];
    if ([uuid isEqualToString:self.statusRecorder.player]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"REMOTE_PLAYBACK_ENDED" object:[ACManager sharedInstance]];
        playbackRunning = NO;
}
}
    
    
- (void)didPlaybackStatusUpdate:(NSNotification*)notification {
    NSDictionary* value = [[notification userInfo] valueForKey:@"payload"];
    NSString* deviceType = [[notification userInfo] valueForKey:@"device_type"];
    if (![deviceType hasPrefix:@"TV"]) return;
    
    self.statusRecorder.player = [[notification userInfo] valueForKey:@"sender"];
    if (!playbackRunning && [[value valueForKey:@"playback_state"] intValue] != ACPlaybackStateTerminated) {
    //    AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
     //   [[appDelegate currentCMoreViewController] hideHUD];
       // [[appDelegate currentCMoreViewController].remoteTimeoutTimer invalidate];
      //  [appDelegate currentCMoreViewController].remoteTimeoutTimer = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"REMOTE_PLAYBACK_STARTED" object:[ACManager sharedInstance]];
        playbackRunning = YES;
    }
    if (self.view.hidden) return;
    NSString* assetId = [value valueForKey:@"asset_id"];

    // else {
    //     if (![self.statusRecorder.player isEqual:[[notification userInfo] valueForKey:@"sender"]]) return;
    // }
    
    self.statusRecorder.playbackState = [[value valueForKey:@"playback_state"] intValue];
    self.statusRecorder.isStream = [[value valueForKey:@"stream"] boolValue];
    [self.overlayView updateLive:self.statusRecorder.isStream];
    
   // AppDelegate* appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    switch (self.statusRecorder.playbackState) {
        case ACPlaybackStateTerminated:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"REMOTE_PLAYBACK_ENDED" object:[ACManager sharedInstance]];
            playbackRunning = NO;
            break;
        default:
            if (!IsEmpty([value valueForKey:@"current_playback_time"])) {
               self.statusRecorder.currentPlaybackTime = [[value valueForKey:@"current_playback_time"] doubleValue];
            }
            if (!IsEmpty([value valueForKey:@"duration"])) {
              self.statusRecorder.duration = [[value valueForKey:@"duration"] doubleValue];
            }
            if (!IsEmpty([value valueForKey:@"selected_subtitle"])) {
                [self.overlayView subtitleChangedOnPlayer:[value valueForKey:@"selected_subtitle"]];
            }
            [self.statusRecorder updateAsset:assetId];
            break;
    }
    [self.overlayView update:nil];
}

- (void) updateTitleYearLabel:(NSDictionary*) mdl {
    //NSLog(@"MODEL: %@", mdl);
   // NSString * title = [mdl getSafeStringValueForKeyPath:@"metadata" andField:@"title"];
   // NSString * year = [mdl getSafeStringValueForKeyPath:@"metadata" andField:@"productionYear"];
   // NSString * text = [NSString stringWithFormat:@"%@ (%@)", title, year];
    
    //[self.overlayView.titleLabel setText:text];
}

//- (CGSize) getProperSizeForLabel:(FontLabel *)label {
//  //  CGSize size = [label.text sizeWithZFont:label.zFont
//   //                       constrainedToSize:CGSizeMake(label.frame.size.width, 100000)
//     //                         lineBreakMode:UILineBreakModeWordWrap];
//    
//    //size.width = label.frame.size.width;
//    //return size;
//}

- (void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self rearrangeSubviewsForOrientation:toInterfaceOrientation];
}

- (void) rearrangeSubviewsForOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (!self.isViewLoaded) return;
    if (interfaceOrientation == UIInterfaceOrientationPortrait ||
        interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
      //  self.overlayView.backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
    } else {
     //   self.overlayView.backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
    }
    
 //   if (self.overlayView.backgroundImage) {
    //    [self imageDidSet: self.overlayView.backgroundImage.image];
   // }
}

- (void)imageDidSet:(UIImage*)image {
    //if (IS_DEBUG_MODE)
      //  NSLog(@"RemotePlayerViewController: imageDidSet called...");
    
    double w = image.size.width;
    double h = image.size.height;
    
    double ratio = 0;
    
    if (h != 0) {
        ratio = w / h;
    }
    
   // CGRect frame = self.overlayView.backgroundImage.frame;
    UIInterfaceOrientation interfaceOrientation = self.interfaceOrientation;
    int height = 0;
    int width = 0;
    
    int screenH;
    int screenW;
    if (interfaceOrientation == UIInterfaceOrientationPortrait ||
        interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown) {
        screenH = 1024;
        screenW = 768;
        if (self.statusRecorder.isStream) {
            width = 315;
            height = 152;
        } else {
            height = 1024;
            width = 768;
        }
    } else {
        screenH = 768;
        screenW = 1024;
        if (self.statusRecorder.isStream) {
            width = 315;
            height = 152;
        } else {
            height = 768;
            width = 1024;
        }
    }
    int top_pad = 42;
    int bottom_pad = 0;
    if (!shouldShowCloseButton) {
        bottom_pad = 93;
    }
    
//    if (self.statusRecorder.isStream) {
//        frame.size.width = width;
//        frame.size.height = height;
//        self.overlayView.backgroundImage.frame = frame;
//        self.overlayView.backgroundImage.center = self.overlayView.center;
//    } else {
//        frame.size.height = height - top_pad - bottom_pad;
//        frame.origin.y    = top_pad;
//        frame.size.width = frame.size.height * ratio;
//        frame.origin.x = (width - frame.size.width) / 2;
//        self.overlayView.backgroundImage.frame = frame;
//    }
}

- (void) assetUpdated:(NSNotification*)n {
    if (self.statusRecorder.asset == nil) {
//        self.overlayView.backgroundImageUrl = nil;
//        [self.overlayView.backgroundImage setImageUrl: self.overlayView.backgroundImageUrl];
//        [self.overlayView.titleLabel setText:@""];
//        [self.overlayView.descriptionLabel setText:@""];
//        self.overlayView.subtitles = nil;
//        [self.overlayView resetSubtitleModel];
        //[self.overlayView hideHud];
        return;
    } else {
   //     NSString* imageUrl = [SimpleMediaItem getImageUrlForModel:self.statusRecorder.asset andImageKey:@"home_bg_landscape"];
        //if (self.statusRecorder.isStream) {
        //    imageUrl = [self.statusRecorder.asset valueForKeyPath:@"imageUrl"];
        //}
       // if (!self.overlayView.backgroundImageUrl || (self.overlayView.backgroundImageUrl && ![self.overlayView.backgroundImageUrl isEqual:imageUrl])) {
        //    self.overlayView.backgroundImageUrl = imageUrl;
         //   [self.overlayView.backgroundImage setImageUrl:imageUrl];
        //}
//        [self updateTitleYearLabel:self.statusRecorder.asset];
//       // CGSize size = [self getProperSizeForLabel:self.overlayView.titleLabel];
//        CGRect frame = self.overlayView.titleLabel.frame;
//        frame.size = size;
//        self.overlayView.titleLabel.frame = frame;
//        [self.overlayView.descriptionLabel setText:[self.statusRecorder.asset getSafeStringValueForKeyPath:@"metadata" andField:@"synopsis"]];
//        size = [self getProperSizeForLabel:self.overlayView.descriptionLabel];
//        frame = self.overlayView.descriptionLabel.frame;
//        frame.origin.y = self.overlayView.titleLabel.frame.origin.y + self.overlayView.titleLabel.frame.size.height + 20;
//        frame.size = size;
//        self.overlayView.descriptionLabel.frame = frame;
//        CGRect dFrame = self.overlayView.detailPanel.frame;
//        [self.overlayView.detailPanel setContentSize:CGSizeMake(dFrame.size.width, frame.origin.y + frame.size.height + 20)];
//        self.overlayView.subtitles = self.statusRecorder.subtitles;
//        [self.overlayView resetSubtitleModel];
    }
}

- (void) stateUpdate:(NSNotification*)n {
    [self.overlayView update:nil];
}

@end
