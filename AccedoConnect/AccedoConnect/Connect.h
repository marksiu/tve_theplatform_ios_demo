//
//  Connect.h
//  Connect
//
//  Created by Alexej Kubarev on 9/17/12.
//  Copyright (c) 2012 Accedo Broadband. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Channel.h"
#import "ChannelDelegate.h"

typedef enum {
    ResponseCodeOk,
    ResponseCodeGenericError,
    ResponseCodeAuthorizationError
} ResponseCode;

@protocol ConnectDelegate;

@protocol SessionRequestDelegate

-(void) sessionRequestSuccess:(NSDictionary*) response;

-(void) sessionRequestFail:(NSString*) message;

@end

@protocol ReconnectRequestDelegate

-(void) reconnectRequestSuccess:(NSString*) message isWarmReconnect:(BOOL)warm;

-(void) reconnectRequestFail:(NSString*) message isWarmReconnect:(BOOL)warm;

@end

@protocol PairRequestDelegate

-(void) pairRequestSuccess:(NSDictionary*) message;

-(void) pairRequestFail:(NSString*) message;

@end

@protocol UnpairRequestDelegate

-(void) unpairRequestSuccess:(NSString*) message;

-(void) unpairRequestFail:(NSString*) message;

@end

/**
 Connect SDK.
 */
@interface Connect : NSObject<SessionRequestDelegate, ReconnectRequestDelegate, PairRequestDelegate, UnpairRequestDelegate>

//------------------------------------------------------------------

/**
 Returns an UUID unique to the application.
 @return the UUID as NSString
 */
+ (NSString *) getUUID;

/**
 Get current SDK version
 @return SDK Version string
 */
+ (NSString*) getVersion;

//------------------------------------------------------------------

- (id) initWithDelegate: (id<ConnectDelegate>) delegate andChannelDelegate: (id<ChannelDelegate>) channelDelegate;

- (void) connect:(NSString*) apiKey server:(NSString*) server;

- (void) pair;

- (void) pair: (NSString*) code;

- (void) checkPairing;

- (void) unpair;


@end

@protocol ConnectDelegate

-(void) didConnect:(Channel*) channel code:(ResponseCode) code;

-(void) didPair:(Channel*) channel code:(ResponseCode) code;

-(void) didUnPair:(Channel*) channel code:(ResponseCode) code;

-(void) didCheckPairing:(Channel *)channel code:(ResponseCode) code;

@end
